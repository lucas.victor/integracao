#INCLUDE "TOTVS.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "ISAMQry.ch"
#INCLUDE "Fileio.ch"

/*/{Protheus.doc} SmIntegraFin
Classe com regras de integra��o para o App Financeiro do Fly Gest�o

@author Rafael Machado
@since 03/09/2018
@version 1.0
/*/

CLASS SmIntegraFin

	METHOD New() CONSTRUCTOR
	METHOD SyncPessoa()
	METHOD SyncFormPag()
	METHOD SyncCondParc()
	METHOD SyncCategoria()
	METHOD SyncConta()
	METHOD SyncReceber()
	METHOD SyncPagar()
	METHOD SyncBaixas()
	METHOD SyncMovBanc()

ENDCLASS

/*/{Protheus.doc} New
Construtor

@author Rafael Machado
@since 03/09/2018
@version 1.0
/*/

METHOD New() CLASS SmIntegraFin

Return


/*/{Protheus.doc} SyncPessoa
M�todo para cria��o do JSON da entidade Pessoa e envio � MainSync

@author Rafael Machado
@since 03/09/2018
@version 1.0
/*/
METHOD SyncPessoa(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFin
	Local cJson      := ""
	Local aEnd       := {}
	Local aMail      := StrTokArr(SA1->A1_EMAIL, ",")
	Default cEntidade := "Financeiro/Pessoa"
	
	//Tratamento para o endere�o
	If ("," $ SA1->A1_END)
		aEnd := StrTokArr(SA1->A1_END, ",")
	Else
		aEnd := StrTokArr(SA1->A1_END, ";")
	EndIf
	
	//For�a que o aEnd tenha pelo menos 3 posi��es.
	aaDD(aEnd, "")
	aaDD(aEnd, "")
	
	//Tratamento para zerar campo de e-mails sem @
	If !Empty(aMail[1]) .And. At("@", aMail[1]) == 0
		aMail[1] := ''
	EndIf	
	
	cJson := '{' + CRLF
	cJson += '"id":'+ SmTrimJson(cGuid) +',' + CRLF
	cJson += '"nome":'+ SmTrimJson(LimpaTx(SA1->A1_NOME)) +',' + CRLF
	cJson += '"tipoDocumento":'+ SmTrimJson(SA1->A1_TPCGC) +',' + CRLF
	cJson += '"cpfcnpj":'+ SmTrimJson(SmJustDig(SA1->A1_CGC)) +',' + CRLF
	cJson += '"cep":' + SmTrimJson(SA1->A1_CEP) +',' + CRLF
	cJson += '"endereco":' + SmTrimJson(LimpaTx(aEnd[1])) +',' + CRLF
	cJson += '"numero":' + SmTrimJson(SmJustDig(aEnd[2])) +',' + CRLF
	cJson += '"complemento":' + SmTrimJson(LimpaTx(aEnd[3])) +',' + CRLF
	cJson += '"bairro":' + SmTrimJson(LimpaTx(SA1->A1_BAIRRO)) +',' + CRLF
	If !Empty(SA1->A1_COD_MUN) .And. !Empty(SA1->A1_EST)
		cJson += '"cidadeCodigoIbge":' + SmTrimJson(cCodEst(SA1->A1_EST) + (SA1->A1_COD_MUN)) +',' + CRLF
		cJson += '"estadoCodigoIbge":' + SmTrimJson(cCodEst(SA1->A1_EST)) +',' + CRLF
	EndIf
	cJson += '"telefone":' + SmTrimJson(SmJustDig(SA1->A1_FONE)) +',' + CRLF
	cJson += '"celular":' + SmTrimJson(SmJustDig(SA1->A1_CELULAR)) +',' + CRLF
	cJson += '"contato":' + SmTrimJson(LimpaTx(SA1->A1_CONTATO)) +',' + CRLF
	cJson += '"observacao":' + SmTrimJson(LimpaTx(SA1->A1_OBSERV)) +',' + CRLF
	cJson += '"email":' + SmTrimJson(aMail[1]) +',' + CRLF
	cJson += '"nomeComercial":' + SmTrimJson(LimpaTx(SA1->A1_NREDUZ)) +',' + CRLF
	cJson += '"transportadora":'+ IIF(SA1->A1_ETRANSP == 'S', 'true', 'false') + ',' + CRLF
	cJson += '"cliente":'+ IIF(SA1->A1_CLIENTE == 'S', 'true', 'false') +',' + CRLF
	cJson += '"fornecedor":'+ IIF(SA1->A1_FORNEC == 'S', 'true', 'false') + ',' + CRLF
	cJson += '"vendedor":'+ IIF(SA1->A1_VEND == 'S', 'true', 'false') +',' + CRLF
	cJson += '"plataformaId":'+'"",' + CRLF
	If IsNumeric(SA1->A1_INSCR)
		cJson += '"inscricaoEstadual":'+ SmTrimJson(SA1->A1_INSCR) +',' + CRLF
	EndIf
	If IsNumeric(SA1->A1_INSCRM)
		cJson += '"inscricaoMunicipal":' + SmTrimJson(SA1->A1_INSCRM) +',' + CRLF
	EndIf
	cJson += '"consumidorFinal":'+ IIF(SA1->A1_TIPO == '1', 'true', 'false') +',' + CRLF
	cJson += '"tipoIndicacaoInscricaoEstadual":'+ SmTrimJson(IIF(SA1->A1_CONTRIB == '1', '1', '9')) +',' + CRLF //1 - Contribuinte ICMS ; 9 - N�o Contribuinte
	cJson += '"registroFixo":false' + CRLF
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)
Return


/*/{Protheus.doc} SyncFormPag
M�todo para cria��o do JSON da entidade FormaPagamento e envio � MainSync

@author Rafael Machado
@since 03/09/2018
@version 1.0
/*/
METHOD SyncFormPag(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFin
	Local cJson      := ""
	Local cTipo      := ""
	Default cEntidade := "Financeiro/FormaPagamento"
		
	If Z14->Z14_TIPO $ "1"
		cTipo := '01' // Dinheiro
	ElseIf Z14->Z14_TIPO == "2"
		cTipo := '02' // Cheque
	ElseIf Z14->Z14_TIPO == "3"
		cTipo := '03' // Cartao Cr�dito
	ElseIf Z14->Z14_TIPO == "4"
		cTipo := '04' // Cart�o D�bito
	ElseIf Z14->Z14_TIPO == "5"
		cTipo := '15' // Boleto Banc�rio
	ElseIf Z14->Z14_TIPO == "6"
		cTipo := '90' // Sem Pagamento
	EndIf

	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) + ',' + CRLF
	cJson += '"descricao":' + SmTrimJson(Z14->Z14_DESCRI) + ',' + CRLF
	cJson += '"tipoFormaPagamento":'+ SmTrimJson(cTipo) + ',' + CRLF
	cJson += '"registroFixo":false' + CRLF 
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)	
Return


/*/{Protheus.doc} SyncCondParc
M�todo para cria��o do JSON da entidade CondicaoParcelamento e envio � MainSync

@author Rafael Machado
@since 03/09/2018
@version 1.0
/*/
METHOD SyncCondParc(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFin
	Local cJson      := ""
	Default cEntidade := "Financeiro/CondicaoParcelamento"
	
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"descricao":' + SmTrimJson(SE6->E6_DESCRI) +',' + CRLF
	cJson += '"condicoesParcelamento":' +  SmTrimJson(SE6->E6_COND) +',' + CRLF
	cJson += '"registroFixo":false'  + CRLF
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)
Return


/*/{Protheus.doc} SyncCategoria
M�todo para cria��o do JSON da entidade Categoria e envio � MainSync

@author Rafael Machado
@since 04/09/2018
@version 1.0
/*/
METHOD SyncCategoria(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFin
	Local cJson      := ""
	Local cCatPai    := ""
	Default cEntidade := "Financeiro/Categoria"
	
	cCatPai := GuidCatSup(LEFT(SED->ED_CATSUP, 2))
	
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"descricao":' + SmTrimJson(AllTrim(SED->ED_CODCAT) + " " +  AllTrim(LimpaTx(SED->ED_DESCRI))) +',' + CRLF
	If !Empty(cCatPai)
		cJson += '"categoriaPaiId":'+  SmTrimJson(cCatPai) +',' + CRLF
	EndIf
	cJson += '"tipoCarteira":'+ SmTrimJson(IIF(SED->ED_TPCART $ '1|3|4|5|6', '1', '2')) +',' + CRLF //1 - Receita ; 2 - Despesa
	cJson += '"registroFixo":false'
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)
Return


/*/{Protheus.doc} SyncConta
M�todo para cria��o do JSON da entidade ContaBancaria e envio � MainSync

@author Rafael Machado
@since 04/09/2018
@version 1.0
/*/
METHOD SyncConta(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFin
	Local cJson      := ""
	Default cEntidade := "Financeiro/ContaBancaria"
	
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"nomeConta":' + SmTrimJson(LimpaTx(SA6->A6_NOME)) +',' + CRLF
	cJson += '"agencia":' +  SmTrimJson(SA6->A6_AGENCIA) +',' + CRLF
	cJson += '"digitoAgencia":' +  SmTrimJson(SA6->A6_DVAGE) +',' + CRLF
	cJson += '"conta":' +  SmTrimJson(SA6->A6_CONTA) +',' + CRLF
	cJson += '"digitoConta":' + SmTrimJson(IIF(Empty(SA6->A6_DVCTA), "X", AllTrim(SA6->A6_DVCTA))) +',' + CRLF
	cJson += '"codigoBanco":' + SmTrimJson(IIF('X' $ SA6->A6_BANCO, '999', AllTrim(SA6->A6_BANCO))) +',' + CRLF
	cJson += '"registroFixo":false'
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)
Return


/*/{Protheus.doc} SyncReceber
M�todo para cria��o do JSON da entidade ContaReceber e envio � MainSync

@author Rafael Machado
@since 14/09/2018
@version 1.0
/*/
METHOD SyncReceber(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFin
	Local cJson      := ""
	Local cEmissao   := DTOS(SE1->E1_EMISSAO)
	Local cVenc      := DTOS(SE1->E1_VENCREA)
	Local cParcela   := ""
	Local cTotParc   := ""
	Local cCond      := ""
	Default cEntidade  := "Financeiro/ContaReceber"
	
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	
	If SE1->E1_JUROS > 0 .Or. SE1->E1_DESC > 0
		cJson += '"valorPrevisto":' + SmTrimJson(SE1->E1_VALOR + SE1->E1_JUROS - SE1->E1_DESC) +',' + CRLF
		cJson += '"observacao":' + SmTrimJson(LimpaTx(SE1->E1_OBSERV) + CRLF +;
					"Valor original do t�tulo: R$" + SmValToChar(SE1->E1_VALOR) + CRLF + "Desconto: R$" + SmValToChar(SE1->E1_DESC) + CRLF + "Juros: R$" + SmValToChar(SE1->E1_JUROS) ) + ',' + CRLF
	Else
		cJson += '"valorPrevisto":' + SmTrimJson(SE1->E1_VALOR) +',' + CRLF
		cJson += '"observacao":' + SmTrimJson(LimpaTx(SE1->E1_OBSERV)) +',' + CRLF	
	EndIf
	
	cJson += '"valorPago":'+ '"0",' + CRLF //Os t�tulos s�o enviados em aberto e em seguida s�o enviadas as baixas que atualizar�o os valores pagos
	cJson += '"categoriaId":' + SmTrimJson(oObjFly:GetGuid(1, xFilial("SED") + SE1->E1_CODCAT, "SED", "ED_FLYID")) + ',' + CRLF
	cJson += '"condicaoParcelamentoId":' + SmTrimJson(oObjFly:GetGuid(1, xFilial("SE6") + IIF(!Empty(SE1->E1_COND), SE1->E1_COND, "001"), "SE6", "E6_FLYID")) +',' + CRLF
	If !Empty(SE1->E1_PARCELA)
		cParcela := AllTrim(SE1->E1_PARCELA)
		If !Empty(SE1->E1_COND)
			cCond := SE1->E1_COND
		Else
			cCond := POSICIONE("SF2", 1, SE1->E1_NUM + SE1->E1_SERIE + SE1->E1_PESSOA + SE1->E1_TIPO, "F2_COND")
		EndIf
		If Len(cParcela) < 2
			cParcela := "0"+cParcela
		EndIf
		
		//Caso n�o haja condi��o de parcelamento informada, utiliza o valor da parcela posicionada. Ex: Parcela 01/01
		IF Empty(cCond)
			cJson += '"descricaoParcela":' + SmTrimJson(cParcela + "/" + cParcela) + ',' + CRLF
		Else
			//Caso a condi��o tenha sido encontrada, busca o total de parcelas no seu cadastro. Ex: Parcela 01/05, onde o 05 est� no E6_QTDPARC
			cTotParc := cValtoChar(POSICIONE("SE6", 1, cCond, "E6_QTDPARC"))
			IIF(Len(cTotParc) < 2, cTotParc := "0"+cTotParc, )
			cJson += '"descricaoParcela":' + SmTrimJson(cParcela + "/" + cTotParc) + ',' + CRLF
		EndIf
	EndIf
	cJson += '"pessoaId":' + SmTrimJson(oObjFly:GetGuid(1, xFilial("SA1") + SE1->E1_PESSOA, "SA1", "A1_FLYID")) +',' + CRLF
	cJson += '"dataEmissao":' + SmTrimJson(LEFT(cEmissao, 4) + "-" + SUBSTR(cEmissao, 5, 2) + "-" +  RIGHT(cEmissao, 2)) +',' + CRLF
	cJson += '"dataVencimento":'+ SmTrimJson(LEFT(cVenc, 4) + "-" + SUBSTR(cVenc, 5, 2) + "-" +  RIGHT(cVenc, 2)) +',' + CRLF
	If !Empty(SE1->E1_DESCRI)
			cJson += '"descricao":' + SmTrimJson(LimpaTx(SE1->E1_DESCRI)) + ',' + CRLF
	Else
			cJson += '"descricao":' + SmTrimJson(AllTrim(SE1->E1_SERIE) + " " + AllTrim(SE1->E1_NUM)) +',' + CRLF
	EndIf
	cJson += '"observacao":' + SmTrimJson(LimpaTx(SE1->E1_OBSERV)) +',' + CRLF
	cJson += '"formaPagamentoId":' + SmTrimJson(oObjFly:GetGuid(1, xFilial("Z14") + IIF(!Empty(SE1->E1_FORMAPG), SE1->E1_FORMAPG, "001"), "Z14", "Z14_FLYID")) +',' + CRLF
	cJson += '"tipoContaFinanceira":"2",' + CRLF // A Receber
	cJson += '"statusContaBancaria":"1",' + CRLF // Os t�tulos s�o enviados em aberto e em seguida s�o enviadas as baixas que atualizar�o os valores pagos
	If SE1->E1_SERIE $ "REN"
		cJson += '"ativo": false,' + CRLF
	Endif
	cJson += '"registroFixo":false'
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)
	
Return


/*/{Protheus.doc} SyncPagar
M�todo para cria��o do JSON da entidade ContaPagar e envio � MainSync

@author Rafael Machado
@since 14/09/2018
@version 1.0
/*/
METHOD SyncPagar(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFin
	Local cJson      := ""
	Local cEmissao   := DTOS(SE2->E2_EMISSAO)
	Local cVenc      := DTOS(SE2->E2_VENCREA)
	Local cParcela   := ""
	Local cTotParc   := ""
	Local cCond      := ""
	Default cEntidade  := "Financeiro/ContaPagar" 
	
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	If SE2->E2_JUROS > 0 .Or. SE2->E2_DESC > 0
		cJson += '"valorPrevisto":' + SmTrimJson(SE2->E2_VALOR + SE2->E2_JUROS - SE2->E2_DESC) +',' + CRLF
		cJson += '"observacao":' + SmTrimJson(LimpaTx(SE2->E2_OBSERV) + CRLF +;
					"Valor original do t�tulo: R$" + SmValToChar(SE2->E2_VALOR) + CRLF + "Desconto: R$" + SmValToChar(SE2->E2_DESC) + CRLF + "Juros: R$" + SmValToChar(SE2->E2_JUROS) ) + ',' + CRLF
	Else
		cJson += '"valorPrevisto":' + SmTrimJson(SE1->E1_VALOR) +',' + CRLF
		cJson += '"observacao":' + SmTrimJson(LimpaTx(SE1->E1_OBSERV)) +',' + CRLF	
	EndIf
	cJson += '"valorPago":'+ '"0",' + CRLF //Os t�tulos s�o enviados em aberto e em seguida s�o enviadas as baixas que atualizar�o os valores pagos
	cJson += '"categoriaId":' + SmTrimJson(oObjFly:GetGuid(1, xFilial("SED") + SE2->E2_CODCAT, "SED", "ED_FLYID")) + ',' + CRLF
	cJson += '"condicaoParcelamentoId":' + SmTrimJson(oObjFly:GetGuid(1, xFilial("SE6") + IIF(!Empty(SE2->E2_COND), SE2->E2_COND, "001"), "SE6", "E6_FLYID")) +',' + CRLF
	If !Empty(SE2->E2_PARCELA)
		cParcela := AllTrim(SE2->E2_PARCELA)
		If !Empty(SE2->E2_COND)
			cCond := SE2->E2_COND
		Else
			cCond := POSICIONE("SF1", 1, SE2->E2_NUM + SE2->E2_SERIE + SE2->E2_PESSOA + SE2->E2_TIPO, "F1_COND")
		EndIf
		If Len(cParcela) < 2
			cParcela := "0"+cParcela
		EndIf
		
		//Caso n�o haja condi��o de parcelamento informada, utiliza o valor da parcela posicionada. Ex: Parcela 01/01
		IF Empty(cCond)
			cJson += '"descricaoParcela":' + SmTrimJson(cParcela + "/" + cParcela) + ',' + CRLF
		Else
			//Caso a condi��o tenha sido encontrada, busca o total de parcelas no seu cadastro. Ex: Parcela 01/05, onde o 05 est� no E6_QTDPARC
			cTotParc := cValtoChar(POSICIONE("SE6", 1, cCond, "E6_QTDPARC"))
			IIF(Len(cTotParc) < 2, cTotParc := "0"+cTotParc, )
			cJson += '"descricaoParcela":' + SmTrimJson(cParcela + "/" + cTotParc) + ',' + CRLF
		EndIf
	EndIf
	cJson += '"pessoaId":' + SmTrimJson(oObjFly:GetGuid(1, xFilial("SA1") + SE2->E2_PESSOA, "SA1", "A1_FLYID")) +',' + CRLF
	cJson += '"dataEmissao":' + SmTrimJson(LEFT(cEmissao, 4) + "-" + SUBSTR(cEmissao, 5, 2) + "-" +  RIGHT(cEmissao, 2)) +',' + CRLF
	cJson += '"dataVencimento":'+ SmTrimJson(LEFT(cVenc, 4) + "-" + SUBSTR(cVenc, 5, 2) + "-" +  RIGHT(cVenc, 2)) +',' + CRLF
	If !Empty(SE1->E1_DESCRI)
			cJson += '"descricao":' + SmTrimJson(LimpaTx(SE2->E2_DESCRI)) + ',' + CRLF
	Else
			cJson += '"descricao":' + SmTrimJson(AllTrim(SE2->E2_SERIE) + " " + AllTrim(SE2->E2_NUM)) +',' + CRLF
	EndIf
	cJson += '"observacao":' + SmTrimJson(LimpaTx(SE2->E2_OBSERV)) +',' + CRLF
	cJson += '"formaPagamentoId":' + SmTrimJson(oObjFly:GetGuid(1, xFilial("Z14") + IIF(!Empty(SE2->E2_FORMAPG), SE2->E2_FORMAPG, "001"), "Z14", "Z14_FLYID")) +',' + CRLF
	cJson += '"tipoContaFinanceira":"1",' + CRLF // A pagar
	cJson += '"statusContaBancaria":"1",' + CRLF // Os t�tulos s�o enviados em aberto e em seguida s�o enviadas as baixas que atualizar�o os valores pagos
	If SE2->E2_SERIE $ "REN"
		cJson += '"ativo": false,' + CRLF
	Endif
	cJson += '"registroFixo":false'
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)
Return

/*/{Protheus.doc} SyncBaixas
M�todo para cria��o do JSON da entidade ContaFinanceiraBaixa e envio � MainSync 

@author Rafael Machado
@since 03/10/2018
@version 1.0
/*/
METHOD SyncBaixas(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFin
	Local cJson      := ""
	Local cGuidCF    := ""
	Local cData      := DTOS(SE5->E5_DATA)
	Default cEntidade  := "Financeiro/ContaFinanceiraBaixa"
	
	If SE5->E5_RECPAG == "R"
		cGuidCF := oObjFly:GetGuid(1, xFilial("SE5") + SE5->E5_NUM + SE5->E5_PARCELA + SE5->E5_TIPO, "SE1", "E1_FLYID")
	Else
		cGuidCF := oObjFly:GetGuid(1, xFilial("SE5") + SE5->E5_NUM + SE5->E5_PARCELA + SE5->E5_TIPO, "SE2", "E2_FLYID")	
	EndIf  

	If Len(Alltrim(cGuidCF)) == 36 //N�o faz o envio da baixa caso n�o tenha encontrado uma conta financeira relacionada.
		cJson := '{' + CRLF
		cJson += '"id":' + SmTrimJson(cGuid) + ',' + CRLF
		cJson += '"data":'+ SmTrimJson(LEFT(cData, 4) + "-" + SUBSTR(cData, 5, 2) + "-" +  RIGHT(cData, 2)) +',' + CRLF
		cJson += '"contaFinanceiraId":' + SmTrimJson(cGuidCF) +',' + CRLF
		cJson += '"contaBancariaId":' + SmTrimJson(oObjFly:GetGuid(1, xFilial("SA6") + SE5->E5_BANCO + SE5->E5_AGENCIA + SE5->E5_CONTA , "SA6", "A6_FLYID")) +',' + CRLF
		cJson += '"valor":' + SmTrimJson(SE5->E5_VALPAG) +',' + CRLF
		cJson += '"observacao":' + SmTrimJson(LimpaTx(SE5->E5_HISTORI)) + CRLF
		cJson += '}'
		oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)
	Else
		cError := "N�o foi encontrada baixa relacionada para o t�tulo"
	EndIf
Return


/*/{Protheus.doc} SyncMovBanc
M�todo para cria��o do JSON da entidade Movimentacao e envio � MainSync 

@author Rafael Machado
@since 19/10/2018
@version 1.0
/*/
METHOD SyncMovBanc(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFin
	Local cJson      := ""
	Local cData      := DTOS(SE5->E5_DATA)
	Default cEntidade  := "Financeiro/Movimentacao"
	
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"data":'+ SmTrimJson(LEFT(cData, 4) + "-" + SUBSTR(cData, 5, 2) + "-" +  RIGHT(cData, 2)) + ',' + CRLF
	cJson += '"valor":'+ SmTrimJson(SE5->E5_VALPAG) +',' + CRLF
	If SE5->E5_RECPAG == "P"
		cJson += '"contaBancariaOrigemId":' + SmTrimJson(oObjFly:GetGuid(1, xFilial("SA6") + SE5->E5_BANCO + SE5->E5_AGENCIA + SE5->E5_CONTA , "SA6", "A6_FLYID")) +',' + CRLF
	Else
		cJson += '"contaBancariaDestinoId":' + SmTrimJson(oObjFly:GetGuid(1, xFilial("SA6") + SE5->E5_BANCO + SE5->E5_AGENCIA + SE5->E5_CONTA , "SA6", "A6_FLYID")) +',' + CRLF
	EndIf
	cJson += '"categoriaId":' + SmTrimJson(oObjFly:GetGuid(1, xFilial("SED") + SE5->E5_CODCAT, "SED", "ED_FLYID")) +',' + CRLF
	cJson += '"descricao":' + SmTrimJson(LimpaTx(SE5->E5_HISTORI)) + CRLF
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)
Return


/*/{Protheus.doc} SyncFin
Fun��o que cria a central de sincronismo para envio do hist�rico de registros no banco

@author Rafael Machado
@since 12/09/2018
@version 1.0
/*/
Function SyncFin(nTipo)
	CreateScreen(nTipo, 1)
Return

/*/{Protheus.doc} SyncAuxFin
Fun��o que faz as chamadas dos m�todos de sincronismo de dados para o Fly Gestao

@author Rafael Machado
@since 12/09/2018
@version 1.0
/*/
Function SyncAuxFin(oDlg, oObjFly)
Local cError     := ""  

oObjFly:self:cToken := oObjFly:GetToken(@cError, "Financeiro")
	
If Empty(cError)

	oObjFly:SyncFly("SA1", oObjFly, oDlg)
	oObjFly:SyncFly("Z14", oObjFly, oDlg)
	oObjFly:SyncFly("SE6", oObjFly, oDlg)
	oObjFly:SyncFly("SED", oObjFly, oDlg)
	oObjFly:SyncFly("SA6", oObjFly, oDlg)
	oObjFly:SyncFly("SE1", oObjFly, oDlg)
	oObjFly:SyncFly("SE2", oObjFly, oDlg)
	oObjFly:SyncFly("SE5", oObjFly, oDlg, .T.) //Baixas de t�tulos
	oObjFly:SyncFly("SE5", oObjFly, oDlg) //Outras Movimenta��es Banc�rias
	
EndIf

MsgInfo("Integra��o Finalizada.")

oDlg:End()

Return


/*/{Protheus.doc} GuidCatSup
Para o envio dos dados e fecha a tela de sincronismo. 

@author Rafael Machado
@since 12/09/2018
@version 1.0
/*/

Function GuidCatSup(cCodigo)
	Local aArea      := SED->(GetArea())
	Local cCod
		
	cCodigo := Alltrim(cCodigo)
	
	SED->(dbClearFilter())
	SED->(dbSetOrder(1))
	SED->(dbGoTop())
	
	cCodigo += PadR(" ",TamSX3("ED_CODCAT")[1] - Len(cCodigo))
	
	If MsSeek(xFilial("SED") + cCodigo, .F.)
		cCod := SED->ED_FLYID
	EndIf
	
	RestArea(aArea)

Return cCod