#INCLUDE "TOTVS.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "ISAMQry.ch"
#INCLUDE "Fileio.ch"

/*/{Protheus.doc} SmIntegraCom
Classe gen�rica com regras de integra��o para o Fly Gest�o

@author Rafael Machado
@since 18/12/2018
@version 1.0
/*/

CLASS SmIntegraCom

	METHOD New() CONSTRUCTOR
	METHOD SyncOrdCom()
	METHOD SyncOrdProd()
	METHOD SyncStatus()
	METHOD SyncNfEntrada()
	METHOD SyncItensEntrada()
	METHOD SyncNfeEntStatus()

ENDCLASS

/*/{Protheus.doc} New
Construtor

@author Rafael Machado
@since 18/12/2018
@version 1.0
/*/

METHOD New() CLASS SmIntegraCom

Return

/*/{Protheus.doc} SyncOrdCom
M�todo para cria��o do JSON da entidade de OrdemCompra e envio � MainSync

@author Rafael Machado
@since 18/12/2018
@version 1.0
/*/
METHOD SyncOrdCom(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraCom
	Local cJson      := ""
	Local cEmissao   := DTOS(SC7->C7_EMISSAO)		
	Local cNatOper   := ""
	Local cTransp    := oObjFly:GetGuid(1, xFilial("SA1") + SC7->C7_TRANSP, "SA1", "A1_FLYID")		
	Local nTotImp    := 0				 			
	Local nOutrosImp := 0	
	Default cEntidade  := "Compras/Pedido"

	cNatOper := POSICIONE("SC8", 1, SC7->C7_NUMPED, "C8_CFOP")

	//total impostos que agregam 
	nTotImp := IIF(!EMPTY(SC7->C7_VALIPI),SC7->C7_VALIPI,0)+IIF(!EMPTY(SC7->C7_ICMSRET),SC7->C7_ICMSRET,0)
	//imposto que nao agregam  ICMs, COFINS, FCP, PIS
	nOutrosImp := IIF(!EMPTY(SC7->C7_VALICM),SC7->C7_VALICM,0) 
	
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"numero":' + SmTrimJson(SC7->C7_NUMPED) +',' + CRLF
	cJson += '"tipoOrdemCompra":"2",'+ CRLF //Apenas pedidos
	cJson += '"status":"1",' + CRLF
	cJson += '"data":'+ SmTrimJson(LEFT(cEmissao, 4) + "-" + SUBSTR(cEmissao, 5, 2) + "-" +  RIGHT(cEmissao, 2)) +',' + CRLF
	cJson += '"fornecedorId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SA1") + SC7->C7_PESSOA, "SA1", "A1_FLYID")) +',' + CRLF
	If cTransp != "0"
		cJson += '"transportadoraId":'+ SmTrimJson(cTransp) +',' + CRLF
	EndIf
	cJson += '"tipoFrete":"9",' + CRLF //Sem Frete
	cJson += '"valorFrete":'+ SmTrimJson(CalcFrete(SC7->C7_NUMPED)) +',' + CRLF
	cJson += '"total":' + SmTrimJson(SC7->C7_VALBRUT) +',' + CRLF
	cJson += '"formaPagamentoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("Z14") + IIF(!Empty(SC7->C7_FORMAPG), SC7->C7_FORMAPG, "001"), "Z14", "Z14_FLYID")) +',' + CRLF
	cJson += '"condicaoParcelamentoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SE6") + IIF(!Empty(SC7->C7_COND), SC7->C7_COND, "001"), "SE6", "E6_FLYID")) +',' + CRLF
	cJson += '"categoriaId":'+ SmTrimJson(GetIdCatDescri("COMPRA PADRAO BEMACASH")) +',' + CRLF
	cJson += '"movimentaEstoque":false,' + CRLF	
	cJson += '"geraFinanceiro":false,' + CRLF	
	cJson += '"geraNotaFiscal":false,' + CRLF
	cJson += '"ajusteEstoqueAutomatico":false,' + CRLF
	cJson += '"totalImpostosProdutos":'+ SmTrimJson(nTotImp) +',' + CRLF
	cJson += '"totalImpostosProdutosNaoAgrega":' + SmTrimJson(nOutrosImp) +',' + CRLF	
	cJson += '"naturezaOperacao":'+ SmTrimJson(POSICIONE("SFF", 1, cNatOper, "FF_DESCRI")) +',' + CRLF
	cJson += '"mensagemPadraoNota":'+ SmTrimJson(LimpaTx(SC7->C7_MENNOTA)) +',' + CRLF
	cJson += '"registroFixo":false'
	cJson += '}'
	
	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

Return

/*/{Protheus.doc} OrdemProduto
M�todo para cria��o do JSON da entidade OrdemCompraProduto � MainSync

@author Rafael Machado
@since 18/12/2018
@version 1.0
/*/
METHOD SyncOrdProd(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraCom
	Local cJson := ""
	Default cEntidade := "Compras/PedidoItem"
	
	cJson := '{' + CRLF
	cJson += '"id":'+ SmTrimJson(cGuid) +',' + CRLF 
	cJson += '"pedidoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SC7") + SC8->C8_NUMPED, "SC7", "C7_FLYID")) +',' + CRLF
	cJson += '"grupoTributarioId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SF4") + SC8->C8_TES, "SF4", "F4_FLYID")) +',' + CRLF 
	If SC8->C8_CSOSN $ "500|900"
		cJson += '"valorCreditoICMS":'+ SmTrimJson(SC8->C8_VALICM) +',' + CRLF 
		cJson += '"valorICMSSTRetido":'+ SmTrimJson(SC8->C8_ICMSRET) +',' + CRLF 
		cJson += '"valorBCSTRetido":'+ SmTrimJson(SC8->C8_BRICMS) +',' + CRLF 
		cJson += '"valorFCPSTRetidoAnterior":0.0,' + CRLF 
		cJson += '"valorBCFCPSTRetidoAnterior":0.0,' + CRLF 
	Else
		cJson += '"valorCreditoICMS":0.0,' + CRLF 
		cJson += '"valorICMSSTRetido":0.0,' + CRLF 
		cJson += '"valorBCSTRetido":0.0,' + CRLF 
		cJson += '"valorFCPSTRetidoAnterior":0.0,' + CRLF 
		cJson += '"valorBCFCPSTRetidoAnterior":0.0,' + CRLF 	
	EndIf
	cJson += '"produtoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SB1") + SC8->C8_CODPROD, "SB1", "B1_FLYID")) +',' + CRLF 
	cJson += '"quantidade":'+ SmTrimJson(SC8->C8_QUANT) +',' + CRLF 
	cJson += '"valor":'+ SmTrimJson(SC8->C8_PRCUNI) +',' + CRLF 
	cJson += '"desconto":'+ SmTrimJson(SC8->C8_VALDESC) +',' + CRLF 
	cJson += '"total":'+ SmTrimJson(SC8->C8_TOTAL) +',' + CRLF 
	cJson += '"observacao":'+ SmTrimJson(LimpaTx(Alltrim(SC8->C8_OBS))) +',' + CRLF 
	cJson += '"registroFixo":false'+ CRLF 
	cJson += '}'
	
	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

Return


/*/{Protheus.doc} SyncStatus 
M�todo para cria��o do JSON que atualiza os status da entidade OrdemCompra

@author Rafael Machado
@since 18/12/2018
@version 1.0
/*/
METHOD SyncStatus(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraCom
	Local cJson      := ""
	Local cStatus    := "1"
	Default cEntidade := "Compras/OrdemCompra/" + Alltrim(SC7->C7_FLYID) 
	
	If SC7->C7_STATUS $ '1|2|4'
		cStatus := "1"
	Else
		cStatus := "2"
	EndIf
	
	cJson := '{' + CRLF
	cJson += '"status":'+ SmTrimJson(cStatus) + CRLF 
	cJson += '}'
	
	oObjFly:MainSync("put", cEntidade, cJson, @cError, @cGuid)

Return


/*/{Protheus.doc} SyncNfEntrada
Mtodo para criao do JSON da entidade de produto  e envio  MainSync

@author Rafael Machado
@since 18/12/2018
@version 1.0
/*/
METHOD SyncNfEntrada(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraCom
	Local cJson      := ""
	Local cStatus    := " "
	Local cTpCompra  := "1"
	Local cEmissao   := DTOS(SF1->F1_EMISSAO)
	Local cNatOper   := POSICIONE("SD1", 1, SF1->(F1_DOC+F1_SERIE+F1_PESSOA+F1_TIPO), "D1_CFOP")
	Local cNumOrdCom := SD1->D1_NUMPED
	Local cCodCat    := oObjFly:GetGuid(1, xFilial("SED") + SD1->D1_CODCAT, "SED", "ED_FLYID")
	Local cTransp    := oObjFly:GetGuid(1, xFilial("SA1") + SF1->F1_TRANSP, "SA1", "A1_FLYID")
	Local cNfOri     := POSICIONE("SD1",1,xFilial("SF1")+ SF1->(F1_DOC + F1_SERIE + F1_PESSOA + F1_TIPO) + "01", "D1_NFORI" ) 
	Local cSerieOri  := POSICIONE("SD1",1,xFilial("SF1")+ SF1->(F1_DOC + F1_SERIE + F1_PESSOA + F1_TIPO) + "01", "D1_SERIORI" )
	LOcal cChaveref  := ""
	Local nTotImp    := 0				 			
	Local nOutrosImp := 0	
	Local nTotImpSrv := 0
	Local nSerNAgreg := 0
	Local nTpNfCompl := 0	
	Default cEntidade  := "Compras/NFeEntrada"
	

	If (SF1->F1_TIPO) $ '3'
		nTpNfCompl := 1
	ElseIf (SF1->F1_TIPO) $ '6'
		nTpNfCompl := 2
	ElseIf (SF1->F1_TIPO) $ '7'
		nTpNfCompl := 4	
	EndIf

	If SF1->F1_TIPO == "2" //Devolu��o
		cTpCompra :="4"
	ElseIf SF1->F1_TIPO $ "3|6|7"	 // Notas de complemento 
		cTpCompra := "2"
	EndIf
	
	If (SF1->F1_TIPO $ '2') .AND. !EMPTY(cNfOri) .AND. !EMPTY(cSerieOri)
		cChaveref := POSICIONE("SF2", 1, xFilial("SF1")+ cNfOri + cSerieOri + SF1->F1_PESSOA + "1", "F2_CHVNFE")	
	EndIf
		
	// STATUS POSSIVEIS NO GESTAO Transmitida = 1,Autorizada = 2,UsoDenegado = 3,NaoAutorizada = 4,Cancelada = 5,NaoTransmitida = 6,EmCancelamento = 7,
	// FalhaNoCancelamento = 8,FalhaTransmissao = 9,CanceladaForaPrazo = 10,InutilizacaoSolicitada = 11,Inutilizada = 12
	// Novas notas s� podem ser enviadas com status 1 ou 6
	If SF1->F1_FIMP $ 'T'
		cStatus := "1"
	Else
		cStatus := "6"
	EndIf	
	
	//Total impostos que agregam 
	nTotImp := IIF(!EMPTY(SF1->F1_VALIPI), SF1->F1_VALIPI, 0) + IIF(!EMPTY(SF1->F1_ICMSRET), SF1->F1_ICMSRET, 0)
	//Imposto que nao agregam  ICMs, COFINS, FCP, PIS
	nOutrosImp := IIF(!EMPTY(SF1->F1_VALICM), SF1->F1_VALICM, 0) + IIF(!EMPTY(SF1->F1_VALPIS), SF1->F1_VALPIS, 0) + IIF(!EMPTY(SF1->F1_VALCOF), SF1->F1_VALCOF, 0)
	
	nTotImpSrv := IIF(!EMPTY(SF1->F1_ISS),SF1->F1_ISS,0)
	
	nSerNAgreg := IIF(!EMPTY(SF1->F1_VALCSLL),SF1->F1_VALCSLL,0) + IIF(!EMPTY(SF1->F1_VALPIS),SF1->F1_VALPIS,0) + ;
		IIF(!EMPTY(SF1->F1_VALCOF), SF1->F1_VALCOF, 0) + IIF(!EMPTY(SF1->F1_INSS),SF1->F1_INSS,0)
	
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"numNotaFiscal":' + SmTrimJson(SF1->F1_DOC) +',' + CRLF
	cJson += '"numeracaoVolumesTrans":'+ SmTrimJson(SF1->F1_NUMVOL) +',' + CRLF
	cJson += '"marca":'+ SmTrimJson(LimpaTx(SF1->F1_MARCA)) +',' + CRLF
	If !Empty(cNumOrdCom) .And. cNumOrdCom != "0"
		cJson += '"ordemCompraOrigemId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SC7") + cNumOrdCom, "SC7", "C7_FLYID")) +',' + CRLF
	EndIf
	cJson += '"tipoNotaFiscal":"1",' + CRLF
	cJson += '"tipoCompra":'+ SmTrimJson(cTpCompra) +',' + CRLF
	cJson += '"status":'+ SmTrimJson(cStatus) +',' + CRLF
	cJson += '"data":'+ SmTrimJson(LEFT(cEmissao, 4) + "-" + SUBSTR(cEmissao, 5, 2) + "-" +  RIGHT(cEmissao, 2)) +',' + CRLF
	cJson += '"fornecedorId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SA1") + SF1->F1_PESSOA, "SA1", "A1_FLYID")) +',' + CRLF
	If cTransp != "0"
		cJson += '"transportadoraId":'+ SmTrimJson(cTransp) +',' + CRLF
	EndIf
	cJson += '"tipoFrete":"9",' + CRLF
	cJson += '"tipoEspecie":"0",' + CRLF
	cJson += '"valorFrete":'+ SmTrimJson(SF1->F1_FRETE) +',' + CRLF	
	cJson += '"pesoBruto":'+ SmTrimJson(SF1->F1_PESBRUT) +',' + CRLF
	cJson += '"pesoLiquido":'+ SmTrimJson(SF1->F1_PESLIQ) +',' + CRLF
	cJson += '"quantidadeVolumes":'+ SmTrimJson(SF1->F1_QTDVOL) +',' + CRLF
	cJson += '"formaPagamentoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("Z14") + IIF(!Empty(SF1->F1_FORMAPG), SF1->F1_FORMAPG, "001"), "Z14", "Z14_FLYID")) +',' + CRLF
	cJson += '"condicaoParcelamentoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SE6") + IIF(!Empty(SF1->F1_COND), SF1->F1_COND, "001"), "SE6", "E6_FLYID")) +',' + CRLF
	cJson += '"categoriaId":'+ SmTrimJson(IIF(cCodCat == "0",GetIdCatDescri("COMPRA PADRAO BEMACASH"),cCodCat)) +',' + CRLF
	cJson += '"serieNotaFiscalId":'+ SmTrimJson(POSICIONE("SX5", 1, "SR" + Alltrim(SF1->F1_SERIE) , "X5_DESCRI" )) +',' + CRLF
	cJson += '"mensagem":' + SmTrimJson(LimpaTx(SF1->F1_MENNOTA)) +',' + CRLF
	cJson += '"naturezaOperacao":'+ SmTrimJson(POSICIONE("SFF", 1, cNatOper, "FF_DESCRI")) +',' + CRLF
	If !Empty(cChaveref)
		cJson += '"chaveNFeReferenciada":'+'"'+ Alltrim(cChaveref) +'",' + CRLF
	EndIf
	If !Empty(SF1->F1_CHVNFE)
		cJson += '"sefazId":' + SmTrimJson(SF1->F1_CHVNFE) +',' + CRLF	
	EndIf
	cJson += '"geraFinanceiro":false,' + CRLF
	cJson += '"totalImpostosProdutos":'+ SmTrimJson(nTotImp) +',' + CRLF
	cJson += '"totalImpostosProdutosNaoAgrega":' + SmTrimJson(nOutrosImp) +',' + CRLF		
	cJson += '"registroFixo":false'
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

Return


/*/{Protheus.doc} SyncItensEntrada
M�todo para cria��o do JSON das entidades NFeProduto � MainSync

@author Rafael Machado
@since 18/12/2018
@version 1.0
/*/
METHOD SyncItensEntrada(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraCom
	Local cJson      := ""
	Local cTipoProd  := POSICIONE("SB1", 1, SD1->D1_CODPROD, "B1_TIPO")
	Default cEntidade := "Compras/NFeProdutoEntrada"

	cJson := '{' + CRLF
	cJson += '"id":'+ SmTrimJson(cGuid) +',' + CRLF  
	cJson += '"notaFiscalEntradaId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SF1") + SD1->D1_DOC + SD1->D1_SERIE + SD1->D1_PESSOA + SD1->D1_TIPO, "SF1", "F1_FLYID")) +',' + CRLF 
	cJson += '"grupoTributarioId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SF4") + SD1->D1_TES, "SF4", "F4_FLYID")) +',' + CRLF 
	cJson += '"quantidade":'+ SmTrimJson(SD1->D1_QUANT) +',' + CRLF 
	cJson += '"valor":'+ SmTrimJson(SD1->D1_PRCUNI) +',' + CRLF 
	cJson += '"desconto":'+ SmTrimJson(SD1->D1_VALDESC) +',' + CRLF 
	cJson += '"total":'+ SmTrimJson(SD1->D1_TOTAL) +',' + CRLF 	
	cJson += '"produtoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SB1") + SD1->D1_CODPROD, "SB1", "B1_FLYID")) +',' + CRLF 
	If SD1->D1_CSOSN $ "500|900"
		cJson += '"valorCreditoICMS":'+ SmTrimJson(SD1->D1_VALICM) +',' + CRLF 
		cJson += '"valorICMSSTRetido":'+ SmTrimJson(SD1->D1_ICMSRET) +',' + CRLF 
		cJson += '"valorBCSTRetido":'+ SmTrimJson(SD1->D1_BRICMS) +',' + CRLF 
		cJson += '"valorFCPSTRetidoAnterior":0.0,' + CRLF 
		cJson += '"valorBCFCPSTRetidoAnterior":0.0,' + CRLF 
	Else
		cJson += '"valorCreditoICMS":0.0,' + CRLF 
		cJson += '"valorICMSSTRetido":0.0,' + CRLF 
		cJson += '"valorBCSTRetido":0.0,' + CRLF 
		cJson += '"valorFCPSTRetidoAnterior":0.0,' + CRLF 
		cJson += '"valorBCFCPSTRetidoAnterior":0.0,' + CRLF 	
	EndIf
	cJson += '"registroFixo":false'+ CRLF 
	cJson += '}'
	
	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

Return


/*/{Protheus.doc} SyncCom
Fun��o que cria a central de sincronismo para envio do hist�rico de registros no banco

@author Rafael Machado
@since 18/12/2018
@version 1.0
/*/
Function SyncCom(nTipo)
	CreateScreen(nTipo, 3)
Return


/*/{Protheus.doc} SyncAuxCom
Fun��o que faz as chamadas dos m�todos de sincronismo de dados para o Fly Gestao

@author Rafael Machado
@since 18/12/2018
@version 1.0
/*/
Function SyncAuxCom(oDlg, oObjFly)
Local cError     := ""

oObjFly:self:cToken := oObjFly:GetToken(@cError, "Compras")
	
If Empty(cError)
					
	SyncSerieNfe(oObjFly, "Compras/SerieNotaFiscal")
	
	SyncParTrib(oObjFly, "Compras/ParametroTributario")
	
	oObjFly:SyncFly("SA1", oObjFly, oDlg, ,"Compras/Pessoa")
	oObjFly:SyncFly("Z14", oObjFly, oDlg, ,"Compras/FormaPagamento")
	oObjFly:SyncFly("SE6", oObjFly, oDlg, ,"Compras/CondicaoParcelamento")
	GeraCatCom()
	oObjFly:SyncFly("SED", oObjFly, oDlg, ,"Compras/Categoria")	
	oObjFly:SyncFly("SBM", oObjFly, oDlg, ,"Compras/GrupoProduto")
	oObjFly:SyncFly("SB1", oObjFly, oDlg, ,"Compras/Produto")	
	oObjFly:SyncFly("SF4", oObjFly, oDlg, ,"Compras/GrupoTributario")
	oObjFly:SyncFly("SFZ", oObjFly, oDlg, ,"Compras/SubstituicaoTributaria")
	oObjFly:SyncFly("SC7", oObjFly, oDlg)
	oObjFly:SyncFly("SC8", oObjFly, oDlg)
	oObjFly:SyncFly("SF1", oObjFly, oDlg)
	oObjFly:SyncFly("SD1", oObjFly, oDlg)
	oObjFly:SyncFly("SC7", oObjFly, oDlg, .T., ,.F.)
	oObjFly:SyncFly("SB1", oObjFly, oDlg, .T., "Compras/Produto/",.F.)
	oObjFly:SyncFly("SA1", oObjFly, oDlg, .T., "Compras/Pessoa/", .F.)
		
EndIf

MsgInfo("Integra��o Finalizada.")

oDlg:End()

Return



Static Function CalcFrete(cNumPed)
Local nValFrete := 0

dbSelectArea("SC8")
SC8->(dbSetOrder(1))

While !SC8->(Eof()) .And. SC8->C8_NUMPED == cNumPed

	nValFrete += SC8->C8_FRETE
	
	SC8->(dbSkip())

End

Return nValFrete


/*/{Protheus.doc} GeraCatCom
Fun��o que insere uma categoria padr�o, exigida na ordem de compra

@author Lucas C. Victor
@since 19/12/2018
@version 1.0
/*/
Static Function GeraCatCom()
Local lPesq
Local nCodCat
Local nCatSup
Local aArea      := GetArea()
Local cFiltro    := ""

dbSelectArea("SED")
SED->(dbSetOrder(2)) //FILIAL + DESCRI

lPesq := dbSeek(xFilial("SED")+"COMPRA PADRAO BEMACASH")

If(!lPesq)
	SED->(dbSetOrder(1)) //FILIAL + CODCAT
	cFiltro := "SED->ED_TPCART == '2'" 
	SED->(dbSetFilter({|| &cFiltro },cFiltro))
	SED->(dbGoTop())

	If(Empty(SED->ED_CODCAT))
		//PAI
		SED->(dbAppend())
		SED->ED_FILIAL := "01"
		SED->ED_CODCAT := "99999" 
		SED->ED_DESCRI := "OUTROS"
		SED->ED_TPCART := "2"
		SED->ED_CLASSE := "2"
		SED->(dbCommit())

		//FILHO
		SED->(dbAppend())
		SED->ED_FILIAL := "01"
		SED->ED_CODCAT := "999999"
		SED->ED_DESCRI := "COMPRA PADRAO BEMACASH"
		SED->ED_TPCART := "2"
		SED->ED_CLASSE := "2"
		SED->ED_CATSUP := "99999"
		SED->(dbCommit())
	Else
		SED->(DBGoBottom())
		nCodCat := Val(SED->ED_CODCAT) + 1
		nCatSup := SED->ED_CATSUP
		//FILHO
		SED->(dbAppend())
		SED->ED_FILIAL := "01"
		SED->ED_CODCAT := cValTochar(nCodCat) 
		SED->ED_DESCRI := "COMPRA PADRAO BEMACASH"
		SED->ED_TPCART := "2"
		SED->ED_CLASSE := "2"
		SED->ED_CATSUP := nCatSup
		SED->(dbCommit())
	EndIf
	RestArea()
	DBCloseArea()
EndIf

Return

Static Function GetIdCatDescri(cDescri)
Local nCodCat

DbSelectArea("SED")
SED->(dbSetOrder(2)) //FILIAL + DESCRI
dbSeek(xFilial("SED")+cDescri)
nCodCat := SED->ED_FLYID
DBCloseArea()
Return nCodCat

/*/{Protheus.doc} SyncNfeEntStatus
M�todo para cria��o do JSON que atualiza os status ativo inativo de Pessoa  

@author Jefferson silva de sousa 
@since 14/01/2019
@version 1.0
/*/
METHOD SyncNfeEntStatus(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFat
	Local cJson      := ""
	Default cAtivo    := "True"
	Default cEntidade := "Compras/NFeEntrada" + Alltrim(SF1->F1_FLYID) 
	
	If SF1->F1_STATUS $ "2"

		cJson := '{' + CRLF
		cJson += '"status": 5' + CRLF 
		cJson += '}'
		
		oObjFly:MainSync("put", cEntidade, cJson, @cError, @cGuid)
	EndIf
Return