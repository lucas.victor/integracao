#INCLUDE "TOTVS.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "ISAMQry.ch"
#INCLUDE "Fileio.ch"

/*/{Protheus.doc} SmIntegraEst
Classe com regras de integra��o para o App Estoque do Fly Gest�o

@author Lucas C. Victor
@since 10/12/2018
@version 1.0
/*/

CLASS SmIntegraEst

	METHOD New() CONSTRUCTOR
	METHOD SyncTipMov()
	METHOD SyncInvCab()
	METHOD SyncAjuste()
	METHOD SyncInvIte()
	METHOD InvCabAtu()
	METHOD InvItemAtu()

ENDCLASS

/*/{Protheus.doc} New
Construtor

@author Lucas C. Victor
@since 10/12/2018
@version 1.0
/*/

METHOD New() CLASS SmIntegraEst

Return

/*/{Protheus.doc} SyncTipMov
M�todo para cria��o do JSON da entidade TipoMovimento e envio � MainSync

@author Lucas C. Victor
@since 10/12/2018
@version 1.0
/*/

METHOD SyncTipMov(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraEst	
	Local cJson      := ""
	Default cEntidade := "Estoque/TipoMovimento"
	
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"descricao":' + SmTrimJson(LimpaTx(SF5->F5_DESCRI)) +',' + CRLF
	cJson += '"tipoEntradaSaida":' +  SmTrimJson(IIF(SF5->F5_TIPO == '3', '1', '2')) +',' + CRLF
	cJson += '"registroFixo":false'  + CRLF
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)
Return


/*/{Protheus.doc} SyncInvCab
M�todo para cria��o do JSON da entidade Inventario e envio � MainSync

@author Lucas C. Victor
@since 11/12/2018
@version 1.0
/*/
METHOD SyncInvCab(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraEst	
	Local cJson      := ""
	Local cDataProc  := DTOS(SB7->B7_DTPROC)
	Local cData      := DTOS(SB7->B7_DATA)
	Local nStatus    := IIF(Empty(cDataProc), '1', '2')
	Default cEntidade := "Estoque/Inventario"
	
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"dataUltimaInteracao":' + SmTrimJson(LEFT(cDataProc, 4) + "-" + SUBSTR(cDataProc, 5, 2) + "-" +  RIGHT(cDataProc, 2)) +',' + CRLF
	cJson += '"descricao":' +  SmTrimJson(LimpaTx(SB7->B7_DESCINV)) +',' + CRLF
	cJson += '"inventarioStatus":"1"' + CRLF //Envia sempre em aberto e posteriormente outro m�todo ir� encerr�-lo se necess�rio.
	cJson += '}'
    oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

	If Empty(cError)
		Reclock("SX5", .T.)
			SX5->X5_FILIAL := "01  "
			SX5->X5_TABELA := "IN"
			SX5->X5_CHAVE  := cData
			SX5->X5_ITEM   := nStatus
			SX5->X5_DESCRI := cGuid
		MsUnlock()
	EndIf
Return

/*/{Protheus.doc} SyncInvIte
M�todo para cria��o do JSON da entidade InventarioItem e envio � MainSync

@author Lucas C. Victor
@since 11/12/2018
@version 1.0
/*/
METHOD SyncInvIte(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraEst
	Local cJson      := ""
	Local cData      := DTOS(SB7->B7_DATA)
	Default cEntidade := "Estoque/InventarioItem"
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"saldoInventariado":' + SmTrimJson(SB7->B7_QUANT) +',' + CRLF
	cJson += '"produtoId":' +  SmTrimJson(oObjFly:GetGuid(1, xFilial("SB1") + SB7->B7_COD, "SB1", "B1_FLYID")) +',' + CRLF
	cJson += '"inventarioId":' + SmtrimJson(POSICIONE("SX5",1,"IN"+cData, "X5_DESCRI")) + CRLF
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

Return 


/*/{Protheus.doc} InvCabAtu
M�todo para cria��o do JSON da entidade Inventario (saldo atual) e envio � MainSync

@author Rafael Machado
@since 13/12/2018
@version 1.0
/*/
METHOD InvCabAtu(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraEst	
	Local cJson      := ""
	Local cData      := DTOS(Date())
	Default cEntidade := "Estoque/Inventario"
	
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"dataUltimaInteracao":' + SmTrimJson(LEFT(cData, 4) + "-" + SUBSTR(cData, 5, 2) + "-" +  RIGHT(cData, 2)) +',' + CRLF
	cJson += '"descricao":' +  SmTrimJson("Saldo Atual (Gest�o) " + LEFT(cData, 4) + "-" + SUBSTR(cData, 5, 2) + "-" +  RIGHT(cData, 2)) +',' + CRLF
	cJson += '"inventarioStatus":"1"' + CRLF //Envia sempre em aberto e posteriormente outro m�todo ir� encerr�-lo se necess�rio.
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

	If Empty(cError)
		Reclock("SX5", .T.)
			SX5->X5_FILIAL := "01  "
			SX5->X5_TABELA := "IN"
			SX5->X5_CHAVE  := "SB2"
			SX5->X5_ITEM   := "2"
			SX5->X5_DESCRI := cGuid
		MsUnlock()
	EndIf
Return

/*/{Protheus.doc} InvItemAtu
M�todo para cria��o do JSON da entidade InventarioItem (Saldo Atual) e envio � MainSync

@author Rafael Machado
@since 13/12/2018
@version 1.0
/*/
METHOD InvItemAtu(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraEst
	Local cJson      := ""
	Default cEntidade := "Estoque/InventarioItem"
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"saldoInventariado":' + SmTrimJson(SB2->B2_QATU) +',' + CRLF
	cJson += '"produtoId":' +  SmTrimJson(oObjFly:GetGuid(1, xFilial("SB1") + SB2->B2_CODPROD, "SB1", "B1_FLYID")) +',' + CRLF
	cJson += '"inventarioId":' + SmtrimJson(POSICIONE("SX5",1,"INSB2", "X5_DESCRI")) + CRLF
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

Return 


/*/{Protheus.doc} SyncAjuste 
M�todo para cria��o do JSON da entidade AjusteManual e envio � MainSync

@author Rafael Machado
@since 13/12/2018
@version 1.0
/*/
METHOD SyncAjuste(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraEst
	Local cJson      := ""
	Local cTipoES    := POSICIONE("SF5", 1, SD3->D3_CODTM, "F5_TIPO")
	Local cTpMovId   := SF5->F5_FLYID
	Default cEntidade := "Estoque/AjusteManual"
	
	If cTipoES == "3"
		cTipoES := "1" //Entrada
	Else
		cTipoES := "2" //Sa�da
	EndIf

	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"tipoEntradaSaida":' + SmTrimJson(cTipoES) +',' + CRLF
	cJson += '"tipoMovimentoId":' +  SmTrimJson(cTpMovId) +',' + CRLF
	cJson += '"produtoId":' + SmTrimJson(oObjFly:GetGuid(1, xFilial("SB1") + SD3->D3_CODPROD, "SB1", "B1_FLYID")) +',' + CRLF
	cJson += '"quantidade":' +  SmTrimJson(SD3->D3_QUANT) +',' + CRLF
	cJson += '"observacao":' + SmTrimJson(LimpaTx(Alltrim(SD3->D3_OBSERV))) + CRLF
	cJson += '}'
	
Return


/*/{Protheus.doc} SyncEst
Fun��o que cria a central de sincronismo para envio do hist�rico de registros no banco

@author Lucas C. Victor
@since 10/12/2018
@version 1.0
/*/
Function SyncEst(nTipo)
	CreateScreen(nTipo, 4)
Return

/*/{Protheus.doc} SyncAuxEst
Fun��o que faz as chamadas dos m�todos de sincronismo de dados para o Fly Gestao

@author Lucas C. Victor
@since 10/12/2018
@version 1.0
/*/
Function SyncAuxEst(oDlg, oObjFly)
Local cError := ""
oObjFly:self:cToken := oObjFly:GetToken(@cError, "Estoque")
	
IF Empty(cError)

	oObjFly:SyncFly("SBM", oObjFly, oDlg, , "Estoque/GrupoProduto")
	oObjFly:SyncFly("SB1", oObjFly, oDlg, , "Estoque/Produto")
	oObjFly:SyncFly("SF5", oObjFly, oDlg)
	oObjFly:SyncFly("SD3", oObjFly, oDlg)
	oObjFly:SyncFly("SB7", oObjFly, oDlg)
	SyncStatus(SX5->X5_DESCRI, oObjFly)
	oObjFly:SyncFly("SB2", oObjFly, oDlg)
	SyncStatus(SX5->X5_DESCRI, oObjFly)
	oObjFly:SyncFly("SB1", oObjFly, oDlg, .T., "Estoque/Produto", .F.)
	
EndIf

MsgInfo("Integra��o Finalizada.")

oDlg:End()
Return



/*/{Protheus.doc} SyncStatus 
Fun��o para cria��o do JSON que atualiza os status da entidade Inventario

@author Rafael Machado
@since 13/12/2018
@version 1.0
/*/
Function SyncStatus(cGuid, oObj)
Local cJson      := ""
Local cError     := ""
Local cEntidade := "Estoque/Inventario/" + Alltrim(cGuid)

cJson := '{' + CRLF
cJson += '"inventarioStatus":"2"' + CRLF 
cJson += '}'

oObj:MainSync("put", cEntidade, cJson, @cError, @cGuid)

Return