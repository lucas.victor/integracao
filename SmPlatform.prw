#INCLUDE 'PROTHEUS.CH'
#INCLUDE "Fileio.ch"

/*/{Protheus.doc} FormPlatform
Cria��o da tela/formul�rio
@author Lucas Victor
@since 20/09/2018
@version 1.0
/*/
Main Function FormPlatform()
Local oFont     := TFont():New("Arial",,-15,.T.)
Local aDocs     := {}
Local cCombo    := NIL
Local nRow      := 40
Local nCol      := 85
Local aForm     := Array(14)
Local aForm[1]  := CriaVar("XX2","XX2_FANTAS")
Local aForm[2]  := CriaVar("XX2","XX2_INSCR")
Local aForm[3]  := CriaVar("XX2","XX2_CNAE")
Local aForm[4]  := CriaVar("XX2","XX2_MAIL")
Local aForm[5]  := CriaVar("XX2","XX2_TEL")
Local aForm[6]  := CriaVar("XX2","XX2_CEP")
Local aForm[7]  := CriaVar("XX2","XX2_ENDERE")
Local aForm[8]  := CriaVar("XX2","XX2_BAIRRO")
Local aForm[9]  := CriaVar("XX2","XX2_CIDADE")
Local aForm[10] := CriaVar("XX2","XX2_EST")
Local aForm[11] := CriaVar("XX2","XX2_CODMUN")

aDocs := LoadDocs()
cComboDoc := aDocs[1]
LoadForm(cComboDoc, @aForm)

DEFINE DIALOG oDlg TITLE "Cria��o da Plataforma First x FLY01 - Gest�o" FROM 180,180 TO 800,800 PIXEL
	 
    cTGetCNPJ := "CNPJ"
    oComboDoc := TComboBox():New(nRow,nCol,{|a|if(PCount()>0,cComboDoc:=a,cComboDoc)},aDocs,140,20,oDlg,,{||LoadForm(cComboDoc, @aForm)},,,,.T.,,,,,,,,,'cCombo1',cTGetCNPJ,1,oFont,)
    oComboDoc:bHelp := {||    ShowHelpCpo(    cTGetCNPJ, {""},2, {""},2)}
    cTGetRaz   := "Raz�o Social"
    oTGetRaz   := TGet():Create( oDlg,{|b|if(PCount()>0,aForm[1]:=b,aForm[1])},nRow+=20,nCol,140,009,"@!",,0,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F.,,cTGetRaz,,,,,,,cTGetRaz,1,oFont)
    oTGetRaz:bHelp := {||    ShowHelpCpo(    cTGetRaz,;
                            {"Nome dado � pessoa jur�dica que consta em documentos oficiais da empresa e notas fiscais "},2,;
                            {""},2)}
    cTGetIE    := "Inscri��o Estadual"
    oTGetIE    := TGet():Create( oDlg,{|c|if(PCount()>0,aForm[2]:=c,aForm[2])},nRow+=20,nCol,140,009,"@!",,0,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F.,,cTGetIE,,,,,,,cTGetIE,1,oFont)
    oTGetIE:bHelp := {||    ShowHelpCpo(    cTGetIE ,;
                            {"N�mero liberado pela Secretaria de Fazenda de cada estado no cadastro do ICMS"},2,;
                            {""},2)}
    cTGetRamo  := "Ramo de Atividade"
    oTGetRamo  := TGet():Create( oDlg,{|d|if(PCount()>0,aForm[3]:=d,aForm[3])},nRow+=20,nCol,140,009,"@!",,0,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F.,,cTGetRamo,,,,,,,cTGetRamo,1,oFont)
    oTGetRamo:bHelp := {||    ShowHelpCpo(    cTGetRamo,;
                            {"Classifica��o Nacional de Atividades Econ�micas � CNAE"},2,;
                            {""},2)}
    cTGetEmail := "E-mail"
    oTGetEmail := TGet():Create( oDlg,{|e|if(PCount()>0,aForm[4]:=e,aForm[4])},nRow+=20,nCol,140,009,"@!",,0,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F.,,cTGetEmail,,,,,,,cTGetEmail,1,oFont )
    oTGetEmail:bHelp := {||    ShowHelpCpo(    cTGetEmail, {""},2, {""},2)}
    cTGetTel   := "Telefone"
    oTGetTel   := TGet():Create( oDlg,{|f|if(PCount()>0,aForm[5]:=f,aForm[5])},nRow+=20,nCol,140,009,"@!",,0,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F.,,cTGetTel,,,,,,,cTGetTel,1,oFont )
    oTGetTel:bHelp := {||    ShowHelpCpo(    cTGetRamo,;
                            {"Classifica��o Nacional de Atividades Econ�micas � CNAE"},2,;
                            {""},2)}
    cTGetCep   := "CEP"
    oTGetCep   := TGet():Create( oDlg,{|g|if(PCount()>0,aForm[6]:=g,aForm[6])},nRow+=20,nCol,140,009,"@!",,0,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F.,,cTGetCep,,,,,,,cTGetCep,1,oFont )
    oTGetCep:bHelp := {||    ShowHelpCpo(    cTGetRamo,;
                            {"C�digo de endere�amento postal"},2,;
                            {""},2)}
    cTGetEnd   := "Endere�o"
    oTGetEnd   := TGet():Create( oDlg,{|h|if(PCount()>0,aForm[7]:=h,aForm[7])},nRow+=20,nCol,140,009,"@!",,0,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F.,,cTGetEnd,,,,,,,cTGetEnd,1,oFont )
    oTGetEnd:bHelp := {||    ShowHelpCpo(    cTGetEnd, {""},2, {""},2)}
    //As informa��es de endere�o ainda est�o fixas no Json, ser� necess�rio criar funcionalidades para altera��o de Cidade/Estado e seus respectivos IDs
    cTGetBair  := "Bairro"
    oTGetBair  := TGet():Create( oDlg,{|i|if(PCount()>0,aForm[10]:=i,aForm[10])},nRow+=20,nCol,140,009,"@!",,0,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F.,,cTGetBair,,,,,,,cTGetBair,1,oFont )
    oTGetBair:bHelp := {||    ShowHelpCpo(    cTGetBair, {""},2, {""},2)}
    cTGetCid  := "Cidade"
    oTGetCid  := TGet():Create( oDlg,{|j|if(PCount()>0,aForm[11]:=j,aForm[11])},nRow+=20,nCol,140,009,"@!",,0,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F.,,cTGetCid,,,,,,,cTGetCid,1,oFont )
    oTGetCid:bHelp := {||    ShowHelpCpo(    cTGetCid, {""},2, {""},2)}
    oTGetCid:cF3
    cTGetEst  := "Estado"
    oTGetEst  := TGet():Create( oDlg,{|k|if(PCount()>0,aForm[12]:=k,aForm[12])},nRow+=20,nCol,140,009,"@!",,0,,,.F.,,.T.,,.F.,,.F.,.F.,,.F.,.F.,,cTGetEst,,,,,,,cTGetEst,1,oFont )
    oTGetEst:bHelp := {||    ShowHelpCpo(    cTGetEst, {""},2, {""},2)}
	TButton():New( nRow+30, 80, "CRIAR", oDlg,{|| JsonPlatform(cComboDoc, aForm,cTGetEmail,oDlg) },50,015,,,.F.,.T.,.F.,,.F.,,,.F. )
	
	TButton():New( nRow+30, 180, "CANCELAR", oDlg,{||oDlg:End()},50,015,,,.F.,.T.,.F.,,.F.,,,.F. )
    
ACTIVATE DIALOG oDlg CENTERED

Return

/*/{Protheus.doc} JsonPlatform
Montagem do JSON de cria��o da plataforma.
@author Lucas Victor
@since 20/09/2018
@version 1.0
/*/
Function JsonPlatform(cCnpj, aForm, cTGetEmail, oDlg)
Local cJson   := ""
Local lValid  := .T. 
Local cUF     := ""
Local cEndBkp := "" 
Local lRet 	:= .F.

RemoveMask(@cCnpj, @aForm)

//preenche o c�digo IBGE do estado e, se necess�rio, do munic�pio
IntCIbge(@aForm,@cUF)

//Verifica se algum campo est� vazio ou inv�lido
lValid := chkFrmIt(cCNPJ, aForm)

If lValid

    //preenche as informa��es de endere�o
    intEnd(@aForm, @cEndBkp)

    cJson := "{" + CRLF
    cJson += '"Documento":'+ SmTrimJson(cCnpj)+ ',' + CRLF
    cJson += '"RazaoSocial":'+ SmTrimJson(SmnoAcento(aForm[1])) + ',' + CRLF 
    cJson += '"Contato":' + SmTrimJson(SmnoAcento(aForm[1])) + ',' + CRLF
    cJson += '"InscricaoEstadual":' + SmTrimJson(aForm[2]) + ',' + CRLF
    cJson += '"RamoAtividade":' + SmTrimJson(SmnoAcento(aForm[3])) + ',' + CRLF
    cJson += '"Email":' + SmTrimJson(SmnoAcento(aForm[4])) + ',' + CRLF
    cJson += '"Telefone":'+ SmTrimJson(SmnoAcento(aForm[5])) + ',' + CRLF
    cJson += '"CEP":' + SmTrimJson(aForm[6]) + ',' + CRLF
    cJson += '"Endereco":' + SmTrimJson(SmnoAcento(cEndBkp)) + ',' + CRLF
    cJson += '"Numero":' + SmTrimJson(aForm[8]) + ',' + CRLF
    If !Empty(AllTrim(aForm[9]))

        cJson += '"Complemento":' + SmTrimJson(SmnoAcento(aForm[9])) + ',' + CRLF 
    EndIf
    cJson += '"Bairro":' + SmTrimJson(SmnoAcento(aForm[10])) + ',' + CRLF
    cJson   += '"CidadeIbge":' + SmTrimJson(aForm[13]) + ',' + CRLF
    cJson   += '"UFIbge":' + SmTrimJson(cUF) + ',' + CRLF
    cJson += '"'+"CodigoIntegracaoProduto"+'"'+":"+'"#18"'+","+ CRLF //Informa��o fixa
    cJson += '"'+"FimVigencia"+'"'+":"+'"'+"2034-12-12"+'"'+CRLF
    cJson += "}"
    

    FWMsgRun(, {|| CreatePlatform(cJson, cCnpj,oDlg)}, "Criando Plataforma...") 
    
EndIf  

Return 

/*/{Protheus.doc} RemoveMask
Tratativas para remo��o das mascaras dos campos Cnpj, IE, Telefone e CEP
@author Lucas Victor
@since 20/09/2018
@version 1.0
/*/
Function RemoveMask(cCnpj, aForm)

cCnpj := AllTrim(StrTran(cCnpj,".",""))
cCnpj := AllTrim(StrTran(cCnpj,"-",""))
cCnpj := AllTrim(StrTran(cCnpj,"/",""))
aForm[2] := AllTrim(StrTRan(aForm[2],".","")) //IE
aForm[5] := AllTrim(StrTRan(aForm[5],"-","")) //Tel
aForm[5] := AllTrim(StrTRan(aForm[5],"(","")) //Tel
aForm[5] := AllTrim(StrTRan(aForm[5],")","")) //Tel
aForm[6] := AllTrim(StrTRan(aForm[6],"-","")) //CEP

Return

/*/{Protheus.doc} LoadDocs
Carregamento dos CNPJs/CPFs cadastrados na XX2
@author Lucas Victor
@since 20/09/2018
@version 1.0
@return		aDocs			Array com todos os CNPJS/CPF
/*/
Function LoadDocs()
    Local aArea := GetArea()
    Local aDocs := {}

    dbSelectArea("XX2")
    dbSetOrder(1)
    XX2->(dbGotop())

    While XX2->(!Eof())
            AADD(aDocs, XX2->XX2_CNPJ)												
            XX2->(dbSkip())
    EndDo

    RestArea(aArea)
Return aDocs

/*/{Protheus.doc} LoadForm
Carregamento dos campos do formul�rio
@author Lucas Victor
@since 20/09/2018
@version 1.0
@param		cCNPJ			CNPJ/CPF Selecionado para retorno
@param		aForm			Array que armazena o resultado 
/*/
Function LoadForm(cCNPJ, aForm)
Local aArea  := GetArea()
 // Local aSplit := Array(3)

dbSelectArea("XX2")
dbSetOrder(1)
XX2->(dbGotop())

While XX2->(!Eof())
    If Alltrim(XX2->XX2_CNPJ) == AllTrim(cCNPJ)

        aForm[1]  := AllTrim(SmnoAcento(XX2->XX2_FANTAS))          	//Raz�o Social
        aForm[2]  := AllTrim(SmnoAcento(XX2->XX2_INSCR))           	//Inscri��o Estadual
        aForm[3]  := AllTrim(SmnoAcento(XX2->XX2_CNAE))            	//Ramo de atividade
        aForm[4]  := SmnoAcento(XX2->XX2_MAIL)							//E-mail
        aForm[5]  := AllTrim(XX2->XX2_TEL)                         	//Telefone
        aForm[6]  := AllTrim(XX2->XX2_CEP)                         	//CEP
        aForm[7]  := AllTrim(SmnoAcento(XX2->XX2_ENDERE))          	//Endere�o
        aForm[8]  := ""                                            	//Numero
        aForm[9]  := ""                                            	//Complemento
        aForm[10] := AllTrim(SmnoAcento(XX2->XX2_BAIRRO))          	//Bairro
        aForm[11] := AllTrim(SmnoAcento(XX2->XX2_CIDADE))          	//Cidade	
        aForm[12] := AllTrim(XX2->XX2_EST)                         	//Estado	
        aForm[13] := AllTrim(XX2->XX2_CODMUN)                      	//C�digo do Municipio
        
    EndIf											
    XX2->(dbSkip())
EndDo

    RestArea(aArea)
Return


/*/{Protheus.doc} CreatePlatform
Fun��o de envio do Json montado anteriormente
@author Lucas Victor
@since 20/09/2018
@version 1.0
@param		cJson			Dados para envio
@param		cCnPJ			Refer�ncia ao CNPJ da empresa
@param		oDlg			Janela atual
/*/
Function CreatePlatform(cJson, cCnpj, oDlg)
Local cLog
Local aHeader    := {}
Local cHeader
Local cRet := ""
Local nTimeOut := 120
Local aError
Local cErro
Local oRet
local lRet := .F.

aadd(aHeader,'Content-Type: '+'application/json')

cRet := HttpPost('http://licenciamentohomolog.serie1.com.br/api/store',, cJson, nTimeOut, aHeader, @cHeader)

If !Empty(cRet)
	If SmJSON2Obj(cRet, @oRet, @cErro,, , .F.)
		aError := { cErro }
	EndIf
	
EndIf

If !Empty(aError[1])
	cLog := oRet:avalues[2]:avalues[1][1]
Else
	cLog := "Plataforma criada com sucesso!" + CRLF + "Verifique o seu e-mail."
	PutMV("MV_CGCPLAT", cCnpj)
	lRet := .T.
EndIf

MsgInfo(cLog)

If lRet
    oDlg:End()
End

Return

/*/{Protheus.doc} chkFrmIt
Verifica os campos do formul�rio, analizando quais campos est�o vazios e inv�lidos no caso do e-mail, informando-os no fim do processo
@author Eduardo Grigoletto
@since 04/10/2018
@version 1.0
@param		cCNPJ			CNPJ/CPF Selecionado para retorno
@param		aForm			Array que armazena os campos do form 
/*/
Function chkFrmIt(cCNPJ, aForm)
Local lValid := .T.
Local aErros:= {}
Local nI     := 0
Local nSep   := 0
Local oLogger:= Logger():New("Erros Pr�-Integra��o")

If Empty(Alltrim(cCnpj))
    aadd(aErros, "CNPJ ou CPF n�o preenchido")
EndIf    
If Empty(AllTrim(aForm[1]))
    aadd(aErros, "Raz�o Social n�o preenchida")
ElseIf !Empty(AllTrim(aForm[1])) .And. !vlText(cValToChar(aForm[1]),"nome")
    aadd(aErros, "Raz�o Social inv�lida, a raz�o social n�o deve possuir um nome composto")
EndIf  
If Empty(AllTrim(aForm[2]))
    aadd(aErros, "Inscri��o estadual n�o preenchida")
EndIf  
If Empty(Alltrim(aForm[3]))
    aadd(aErros, "C�digo de atividade(CNAE) da empresa n�o preenchido")
EndIf  
If Empty(AllTrim(aForm[4]))
    aadd(aErros, "E-mail n�o preenchido")
ElseIf !IsEmail(AllTrim(aForm[4]))
    aadd(aErros, "E-mail informado est� inv�lido, informe um e-mail no formato email@dominio.com")

EndIf  
If Empty(AllTrim(aForm[5]))
    aadd(aErros, "Telefone para contato n�o preenchido")
EndIf
If Empty(Alltrim(aForm[6]))
    aadd(aErros, "CEP n�o preenchido")
EndIf  
If Empty(Alltrim(aForm[7]))
    aadd(aErros, "Endere�o n�o preenchido")
Else 
    nSep    := At(",",AllTrim(aForm[7]))
    If nSep  == 0 
        aadd(aErros, "Endere�o inv�lido, numera��o n�o prenchida ou preenchida incorretamente")
    EndIf     
EndIf  
If Empty(AllTrim(aForm[10]))
    aadd(aErros, "Bairro n�o preenchido")
EndIf   
If Empty(AllTrim(aForm[11]))
    aadd(aErros, "Cidade n�o preenchida")
EndIf  
If Empty(AllTrim(aForm[12]))
    aadd(aErros, "Estado n�o preenchido")
EndIf  
If Empty(AllTrim(aForm[13]))
    aadd(aErros, "Cidade ou estado invalidos")
EndIf 

If Len(aErros) > 0
    lValid := .F.
EndIf 

If !lValid
    oLogger:Put("Cria��o n�o conclu�da","Foram detectados um ou mais erros que impedem a cria��o da plataforma.")

    For nI:= 1 To Len(aErros)
        oLogger:Put(aErros[nI],"")
    Next nI

    oLogger:Show()
EndIf

oLogger:Finalize()

Return lValid

/*/{Protheus.doc} IntCIbge
Preenche os dados referentes ao c�digo IBGE do estado e do munic�pio caso n�o esteja preenchido
@author Eduardo Grigoletto
@since 04/10/2018
@version 1.0
@param		aForm			Array que armazena os campos do form 
@param      cUF         Passagem por refer�ncia do valor do c�digo IBGE do estado
/*/
Function IntCIbge(aForm,cUF)
Local cMun      := ""
Local cCodMun   := ""
Local cMunIbge  := ""
Local cNewCod   := ""
Default cUF := ""

cUF  := cCodEst(aForm[12])

dbSelectArea("CC2")
dbSetOrder(1)
If CC2->(DbSeek(Alltrim(aForm[12])))
    While !Eof()
        If Alltrim(CC2->CC2_MUN) == AllTrim(aForm[11])
            cMun := Alltrim(CC2->CC2_MUN)
            cMunIbge := Alltrim(CC2->CC2_CODMUN)
        EndIf 
        CC2->(DBSkip())
    EndDo
EndIf


If !Empty(cMunIbge) .And. !Empty(cUF)
    cNewCod  := AllTrim(cUF)+AllTrim(cMunIbge)
EndIf   

aForm[13] := AllTrim(cNewCod)

Return 

/*/{Protheus.doc} IntEnd

Preenche os dados referentes ao endere�o(endere�o, numero e complemento) 
@author Eduardo Grigoletto
@since 04/10/2018
@version 1.0
@param		aForm			Array que armazena os campos do form 
@param      cEndBkp         Endere�o backup que ser� usado no json ao inv�s do endere�o presente no form deixando a apresenta��o mais coesa
/*/
Function IntEnd(aForm,cEndBkp)

Default cEndBkp := ""

cEndBkp     := FisGetEnd(aForm[7])[1]//Endereco
aForm[8]    := FisGetEnd(aForm[7])[3]//Numero 
aForm[9]    := FisGetEnd(aForm[7])[4]//Complemento

Return 

/*/{Protheus.doc} IntEnd

Valida textos nos campos de texto do form de integra��o
@author Eduardo Grigoletto
@since 15/01/2019
@version 1.0
@param	   cTexto		  Texto a ser validado 
@param     cTipo          Tipo de valida��o a ser feita
@Return    lRet           Indica se o texto � v�lido
/*/
Function vlText(cTexto,cTipo)
Local aMail := {}
Local lRet := .F.

If cTipo == "nome"
    aNome := Strtokarr( Alltrim(cTexto), " ")
    If Len(aNome) >= 2
	    lRet := .T.	
    EndIf
EndIf

Return lRet

/*/{Protheus.doc} MostraJs

Mostra o arquivo de sa�da Json, criada para fim de debug 
@author Eduardo Grigoletto
@since 04/10/2018
@version 1.0
@param		cJson			Dados para envio
/*/
Function MostraJs(cJson)
Local oLogger := Logger():New("Json da Integra��o")
Local lRet := .F.

oLogger:Put(cJson,"")
oLogger:Show()

oLogger:Finalize()

Return lRet