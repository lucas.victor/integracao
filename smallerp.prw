#INCLUDE "smallerp.ch" 
#include "protheus.ch"
#include "sdmenu.ch"
#include "sdic.ch"
#include "shell.ch"
#include "ISAMQry.ch"

Static __oMainApp
Static __aMainMenu := {}
Static aAcessos
Static cAcesso := ""
Static lhasDomain := !Empty(GetPvProfString("DOMAINDB", "Server", "", &("GetSrvIniName()")))

Main Function FirstMulti()
Local uRet

#IFDEF TOP
	Local aCombo
	Local cEnv   := Space(100)
	Local oDlg
	Local nOk    := 0
	Local oBtn1

	If lhasDomain
		Define MSDialog oDlg Title "Selecione o ambiente a ser usado" From 0,0 To 100,220 Pixel
		
		@10,10 Get cEnv Size 100,10 Picture "@!" Pixel Of oDlg
	
		@ 030,010 BUTTON oBtn1 PROMPT "FIRST" SIZE 030,011 PIXEL ; //'Add >'
				ACTION (nOk := 1 ,oDlg:End())
		@ 030,040 BUTTON oBtn1 PROMPT "SDU" SIZE 030,011 PIXEL ; //'Add >'
				ACTION (nOk := 2 ,oDlg:End()) OF oDlg
		@ 030,070 BUTTON oBtn1 PROMPT "CANCELAR" SIZE 040,011 PIXEL ; //'Add >'
				ACTION (aRegs := {},oDlg:End()) OF oDlg

		ACTIVATE MSDIALOG oDlg CENTERED
		
		If nOk == 1 .Or. nOk == 2
			If Empty(cEnv)
				Alert("Ambiente n�o informado!")
			ElseIf !FWExistEnv(cEnv := Upper(Alltrim(cEnv)))
				Alert("Ambiente informado n�o existe ou n�o � SQL")
			Else
				FWSetRootPath(cEnv)
				If nOk == 1
					uRet := SmallErp()
				Else
					SDU()
				EndIf
			EndIf
		EndIf
	Else
		Alert("Op��o apenas dispon�vel quando banco de dom�nio est� configurado")
	EndIf
#ELSE
	Alert("Fun��o n�o suportada para ambientes CTREE")
#ENDIF

Return uRet

//----------------------------------------

Main Function First(cUser,cPassword)

Return SmallErp(cUser,cPassword)

//----------------------------------------

Function SmallERP(cUser, cPassword)
//Se est� com novo patch de lib
If FindFunction("FWPoliceLoad")
	//Inicializa as vari�veis principais
	CriaPublica()
Else
Public dDataBase := DATE()
Public cPaisLoc  := GetSmlString("PAISLOC","BRA")
Public cAliasEsp := GetAliasEsp()   //Vari�vel P�blica que conter� todas as tabelas espec�ficas do First
									//Usado na fun��o xFilial - por Daniel Matos em 06/06/08
									
//If FindFunction("FWGetProduct") .And. FWGetProduct() $ "RM|UO" - comentado (Daniel M.) - Unificacao 
	__xchghcons({{"Protheus 8","First"},{"Protheus","First"},{"Microsiga","First"}})
//EndIf

SetSmallEnv()
EndIf
LoadMenu(.T.)

__oMainApp := SApplication():New()
__oMainApp:bValid := {|| Final(,,.T.)}
__oMainApp:bKillApp := {|| Final()}
__oMainApp:bInit  := {|| Processa({|| MainInit()},STR0167) }
If !FindFunction("FWPoliceLoad")
__oMainApp:SetMenu(__aMainMenu)
EndIf
__oMainApp:Activate(cUser,cPassword)

Return

//----------------------------------------

Function MainInit()
Local lIni400
Local dData //projeto BSB - descomentado (Daniel M.) - Unificacao 
Local cDirBSB, cDirA, cDirB, cDirC //projeto BSB - descomentado (Daniel M.) - Unificacao
Local cGrupos	:=	""
Local cTmp		:=	""
Local aInfo	:=	{}
Local nX
Local oUser := SmUserUtil():New()
cAliasEsp := GetAliasEsp()

If cPaisLoc <> "BRA"
	ProcRegua(19)
Else
	ProcRegua(16)
EndIf

//Se est� com novo patch de lib
If !FindFunction("FWPoliceLoad")
IncProc(STR0154)  //"Efetuando conexao com o servidor"
Connect() 									   
EndIf
IncProc()
DelArqTmp()
IncProc(STR0155)  //"Verificando banco de dados"
OpenData()
IncProc()
SMChkVer()
IncProc()
SMUpdate()
IncProc()
ChkCFO()
ChkGRPCST()
IncProc()
ChkSAC()
IncProc()
ChkSAH()
IncProc()
ChkSA8()

IncProc()
SMCNAB()

DbSelectArea("DA0")
DA0->(dbSetOrder(1))
If DA0->(!dbSeek(xFilial("DA0")+"001")) .And. DA0->(!dbSeek(xFilial("DA0")+"002"));
	.And. DA0->(!dbSeek(xFilial("DA0")+"003")) 
		FAtuTabPrc()
EndIf

IncProc()
M1A090Pop_Exerc()
IncProc()
ChkSF5() 
IncProc()
ChkRMO()
If cPaisLoc <> "BRA"
	IncProc() 
	CriaSFB()
	IncProc()
	CriaSF4()
	IncProc()
	CriaSFC()
	IncProc()
	CriaSFG()
EndIf

IncProc(STR0156)  //"Verificando licen�a da empresa"
If FindFunction("FWGetProduct") .And. FWGetProduct() $ "RM|UO" //inserido (Daniel M.) - Unificacao 
	ChkSmallLic()     // Apenas para testar o arquivo de licen�as do cliente
Else
	If !ChkSmallLic(.T.)
		ChkSmallLic()
	EndIf 
EndIf

//IncProc(STR0157)  //"Inicializando configura��es do sistema"
//SMIniCFG(.F.)

dbSelecTArea("SX6")
lIni400 := GetMv("MV_INIINDC", .F., "2") == "1"


If FindFunction("FWGetProduct") .And. !(FWGetProduct() $ "RM|UO") //inserido (Daniel M.) - Unificacao
	// projeto BSB
	dData := CtoD(GetMv("MV_DATARQ"))
	If Date() != dData
		GetMv( "MV_DATARQ" )
		RecLock( "SX6",.F. )
		SX6->X6_CONTEUD := DtoC(Date())
		msUnlock()
	
		GetMv( "MV_NROARQ" )
		RecLock( "SX6",.F. )
		SX6->X6_CONTEUD := "000000"
		msUnlock()
	EndIf
	
	cDirBSB := GetMv( "MV_DRCNAB" )
	If Right( Alltrim( cDirBSB ),1 ) != "\"
		cDirBSB += "\"
	EndIF
	
	cDirA := cDirBSB + GetEnvServer() + "\" + "REMESSA\"
	cDirB := cDirBSB + GetEnvServer() + "\" + "RETORNO\"
	cDirC := cDirBSB + GetEnvServer() + "\" + "EXTRATO\"
	MakeDir( cDirBSB )
	MakeDir( cDirA   )
	MakeDir( cDirB   )
	MakeDir( cDirC   )
EndIf
//����������������������������������������������������Ŀ
//� Posiciona Usuario                                  �
//������������������������������������������������������
PswOrder(1)
PswSeek(__oMainApp:cUserID)
AAdd(aInfo,PswRet())
cGrupos		:=	RETPROFDEF(cUsuario(),"GRUPOS","ACCESS",cUsuario())        
While !Empty(cGrupos)
	cTmp	:=	Substr(cGrupos,1,6)
	cGrupos	:=	Substr(cGrupos,8)     
	If PswSeek(cTmp)
		AAdd(aInfo,PswRet())
	Endif
Enddo
//����������������������������������������������������Ŀ
//� Posiciona Usuario                                  �
//������������������������������������������������������
PswOrder(1)
PswSeek(__oMainApp:cUserID)

For nX:= 1 To Len(aInfo)
	//Se est� com novo patch de lib
	If FindFunction("FWPoliceLoad")
		If isAdmin(cUsuario()) .Or. Substr(aInfo[nX, 2, 5],IIF(FindFunction("FWGetProduct") .And. FWGetProduct() $ "RM|UO",4,5),1) == "S" 
			cAliasEsp := GetAliasEsp()
			M040CHECK() // Transacoes programadas
			Exit
		EndIf
	Else
		If Substr(aInfo[nX,5],IIF(FindFunction("FWGetProduct") .And. FWGetProduct() $ "RM|UO",4,5),1) == "S" 
			cAliasEsp := GetAliasEsp()
			M040CHECK() // Transacoes programadas
			Exit
		EndIf
	EndIf
Next

//Verifica se deve exibir a tela com as atualizacoes
If FindFunction("M4A070Str")
	M4A070Str(.T.)
EndIf

//Roda todos os aceleradores do sistema
//Tabelas atualizadas:
//SF4, SE6, CTT, SED, SA7, CC2, CT1, CT5, CVA, SEI, SEJ, SEO, ST3, SXV, SZ6, SZ7

Acelerador()
If FindFunction("FWdbCloseAll")
	FWdbCloseAll()
Else
	dbCloseAll()
	Disconnect()
EndIf
// Colocada verificacao temporaria "hard code" para liberar acesso ao Administrador 
If lIni400 
	For nX:= 1 To Len(aInfo)
		//Se est� com novo patch de lib
		If FindFunction("FWPoliceLoad")
			//If LEN(aInfo[nX]) > 2 .And. ( !Empty( Ascan( aInfo[nX,4], "FIN" ) ) .Or. aInfo[nX,1] == "000000" )  
				If Substr(aInfo[nX, 2,5],IIF(FindFunction("FWGetProduct") .And. FWGetProduct() $ "RM|UO",3,4),1) == "S"
					__oMainApp:Execute("Indicadores de Gest�o","SM1M400") 
					Exit	
				EndIf
			//EndIf
		Else
		If LEN(aInfo[nX]) > 2 .And. ( !Empty( Ascan( aInfo[nX,4], "FIN" ) ) .Or. aInfo[nX,1] == "000000" )  
			If Substr(aInfo[nX,5],IIF(FindFunction("FWGetProduct") .And. FWGetProduct() $ "RM|UO",3,4),1) == "S"
				__oMainApp:Execute("Indicadores de Gest�o","SM1M400") 
				Exit	
			EndIf
		EndIf
		EndIf
	Next
Endif

Return

//----------------------------------------

Function SetMainApp(oApp)
__oMainApp := oApp
Return

//----------------------------------------

Function GetMainWnd()
Return __oMainApp:oWnd

//----------------------------------------

Function dDataBase()
Local dRet

//Quando for job, sempre ir� assumir a data atual
If GetRemotetype() == -1
	dRet := Date()
Else
	dRet := __oMainApp:dDataBase
EndIf

Return  dRet

//----------------------------------------

Function cUsuario()
Local cUser := "admin"

If ValType(__oMainApp) == "O"
	cUser := __oMainApp:cUser
	If Empty(cUser)
		cUser := "admin"
	EndIf
EndIf


Return cUser

//----------------------------------------

Function GetMainApp()
Return __oMainApp

//----------------------------------------

Function FunName()
Local cFunName  := ""

If ValType(__oMainApp) == "O"
	cFunName := __oMainApp:cFunName
EndIf

Return cFunName

//----------------------------------------

Function VersaoRPO()
Return "12.04"

//----------------------------------------

Function SetFunName(cFunName)
Local cFunOld := __oMainApp:cFunName

__oMainApp:cFunName := cFunName
Return cFunOld 

//----------------------------------------

Function Abertura(cCredential)
//Se est� com novo patch de lib
If FindFunction("FWPoliceLoad")
	__oMainApp:cCredential := cCredential
EndIf
ValPsw(.F.)
LoadMenu() 
Connect()
OpenData()

SET KEY VK_F1 TO HelProg()
Return

//----------------------------------------

Function ValPsw(lMainThread)
Local cMsg
Local lValPsw := .T.
//Se est� com novo patch de lib
If FindFunction("FWPoliceLoad")
	If !__oMainApp:VldCredential(@cMsg)
		Final(cMsg) //"Usu�rio ou Senha inv�lido"
	EndIf
Else
	lValPsw := ValPsw_(lMainThread)
EndIf
Return lValPsw

Static Function ValPsw_(lMainThread)
Local lValPsw := .F.
Local cExec
Local lMsg

DEFAULT lMainThread := .T.

PswOrder(2)
If PswSeek(__oMainApp:cUser) .and. PswName(__oMainApp:cPassword)
	__oMainApp:cUserID := PswRetAll()[1]  		
    
	If(!__oMainApp:cUserID=="000000")
		If (InvalidUser(__oMainApp:cUser))			
			MsgAlert(STR0322) //"Usu�rio ou Senha inv�lido"
			return .F.
		EndIf
	EndIf      
		

	If lMainThread .and. ChkSmallLic(.T.)
		cExec := "U_SMLICMSG"
		If FindFunction(cExec)
			lMsg := &cExec.(CompanyInfo("M0_CNPJ"),,)
			If ValType(lMsg) <> "L"
				lMsg := NIL
			EndIf
		EndIf

		If CheckStation(CompanyName(.T.),GetLicense(),lMsg)
			cExec := "U_SMLOGIN"
			If FindFunction(cExec)
				lValPsw := &cExec.(CompanyInfo("M0_CNPJ"),,)
				If ValType(lValPsw) <> "L"
					lValPsw := .F.
				EndIf
			Else
				lValPsw := .T.
			EndIf
		Else
			If lMainThread
				MsgStop(STR0086)	//"O n�mero de esta��es simultaneas permitidas foi excedido"
			Else
				Final(STR0086)	//"O n�mero de esta��es simultaneas permitidas foi excedido"
			EndIf
		EndIf
	Else
		lValPsw := .T.
	EndIf
Else
	If !PswName(__oMainApp:cPassword)
		MsgAlert(STR0323)
	EndIf
EndIf
Return lValPsw


//-------------------------------------------------------------------
/*/{Protheus.doc} InvalidUser
Verifica se o usu�rio tem acesso ao sistema, se existir o usu�rio/senha
mas n�o existir profile, � criado um registro novo na tabela de profile

@param cUser Tipo: CHARACTER nome do usu�rio
@author Felipe Leno
@since 10/09/2012

/*/
//-------------------------------------------------------------------
Static Function InvalidUser(cUser)
Local lInvalid:= .F.

//Procura usu�rio no profile
If(!FindProfDef(AllTrim(__oMainApp:cUser),"MENU","ACCESS",AllTrim(__oMainApp:cUser)) .AND.;
			!FindProfDef(AllTrim(__oMainApp:cUser),"GRUPOS","ACCESS",AllTrim(__oMainApp:cUser)))
	lInvalid:= .T.
EndIf	

Return lInvalid

//----------------------------------------
Function LoadMenu(lMain, lNoLoad)
Local ni,nj
Local aInfo
Local aNewMnu := {}
Local aDocs
Local nRemoteType := GetRemoteType()	//-1 = sem remote/ 0 = delphi/ 1 = QT windows/ 2 = QT Linux
Local aUsrAcs
Local lIsRm := (FindFunction("FWGetProduct") .And. FWGetProduct() $ "RM|UO")
Local oLicence := SmGetLicense()
Local cHomolog := " (HOMOLOGA��O)"
DEFAULT lMain := .F.
Default lNoLoad := .F.

If lMain .Or. Len(__aMainMenu) == 0

	aDocs := APDirDocs()
			
		SDMENU __aMainMenu
		
			SDMENUITEM STR0001 ID "010" //"Cadastros"
			SDMENU
				SDMENUITEM STR0211 ID "010010"	LOCKED //"B�sico"
				SDMENU
					SDMENUITEM STR0002 ID "010010010" LOCKED ACTION "SM1A010" //"Pessoa"
					If FindFunction("SM2A230")
						SDMENUITEM STR0269 ID "010010230" LOCKED ACTION "SM2A230" //"Cliente"
					EndIf
					If FindFunction("SM2A240")
						SDMENUITEM STR0270 ID "010010240" LOCKED ACTION "SM2A240" //"Fornecedor"
					EndIf
					If FindFunction("SM2A250")
						SDMENUITEM STR0271 ID "010010250" LOCKED ACTION "SM2A250" //"Vendedor"
					EndIf
					If FindFunction("SM2A260")
						SDMENUITEM STR0272 ID "010010260" LOCKED ACTION "SM2A260" //"Transportadora"
					EndIf
					SDMENUITEM STR0010 ID "010010020" LOCKED ACTION "SM2A010" //"Produto"
					If oLicence:Homolog()
						SDMENUITEM STR0010 + cHomolog ID "010010025" LOCKED ACTION "SM2A290MVC" //"Produto (Homologa��o)"
					EndIf
					SDMENUITEM STR0009 ID "010010030" LOCKED ACTION "SM2A020" //"Grupo de Produto"
					SDMENUITEM STR0257 ID "010010210" LOCKED ACTION "SM2A210" //"Produto X Fornecedor"
					SDMENUITEM STR0317 ID "010010330" LOCKED ACTION "SM2A320" //"Produto X Cliente"
					SDMENUITEM STR0012 ID "010010040" LOCKED ACTION "SM2A050MVC" PAISLOC "BRA"  //"Tipos de Entrada Saida"
					SDMENUITEM STR0281 ID "010010270" LOCKED ACTION "SM2A280" //"TES Inteligente"
					SDMENUITEM STR0012 ID "010010041" LOCKED ACTION "SM2A051" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Tipos de Entrada Saida"
					SDMENUITEM STR0013 ID "010010050" LOCKED ACTION "SM2A060" //"CFOP"
					SDMENUITEM STR0011 ID "010010060" LOCKED ACTION "SM2A040" //"Condi��o de Pagamento"
					SDMENUITEM STR0174 ID "010010070" LOCKED //"Centro de Custo"	  
					SDMENU                          
						SDMENUITEM STR0174 ID "010010070010" LOCKED ACTION "SM1A200" //"Centro de Custo"	  
						SDMENUITEM STR0175 ID "010010070020" LOCKED ACTION "SM1A210" //"Criterios de Rateio	  
					SDENDMENU
					SDMENUITEM STR0005 ID "010010080" LOCKED ACTION "SM1A030" //"Categoria Financeira"
					SDMENUITEM STR0212 ID "010010090" LOCKED ACTION "SM4M120" //"Tabela de Pre�o"
					SDMENUITEM STR0004 ID "010010100" LOCKED ACTION "SM1A020" //"Banco"
					SDMENUITEM STR0003 ID "010010110" LOCKED ACTION "SM1A040" //"Endere�o Alternativo"
					SDMENUITEM STR0120 ID "010010120" LOCKED ACTION "SM1A090" //"Exerc�cio"
					SDMENUITEM STR0006 ID "010010130" LOCKED ACTION "SM1A050" //"Moeda"
					SDMENUITEM STR0007 ID "010010140" LOCKED ACTION "SM1A060" //"Feriado"
					SDMENUITEM STR0163 ID "010010150" LOCKED ACTION "SM1A170" //"Regi�o"
					SDMENUITEM STR0166 ID "010010160" LOCKED ACTION "SM1A180" //"Ramo de Atividade"
					SDMENUITEM STR0008 ID "010010170" LOCKED ACTION "SM2A030" //"Unidade de Medida"
					SDMENUITEM STR0169 ID "010010180" LOCKED ACTION "SM2A110" //"Armaz�m"
					SDMENUITEM STR0247 ID "010010190" LOCKED ACTION "SM2A160" //"Regra de Comiss�o"
					SDMENUITEM STR0254 ID "010010200" LOCKED ACTION "SM2A200" //"Tecnico"
					SDMENUITEM STR0260 ID "010010220" LOCKED ACTION "SM2A220MVC" //"Munic�pio"
					SDMENUITEM STR0279 ID "010010270" LOCKED ACTION "SM2A270" //"NCM e NBS"
					SDMENUITEM STR0307 ID "010010280" LOCKED ACTION "SM2A310" //"Departamento de produtos"
					If oLicence:nProdID == 18 //Apenas se for Fly01
						SDMENUITEM STR0310 ID "010010290" LOCKED ACTION "SMBANDCARD" //"Bandeiras de Cart�o"
						SDMENUITEM STR0311 ID "010010300" LOCKED ACTION "SMOPERACARD" //"Operadoras de pagamento"
						SDMENUITEM STR0312 ID "010010310" LOCKED ACTION "SMFORMAPAG" //"Formas de pagamento"
						SDMENUITEM STR0314 ID "010010320" LOCKED ACTION "SMSERIES" //"S�ries de nota"
					EndIf
				SDENDMENU
				SDMENUITEM STR0213 ID "010020" LOCKED //"Avan�ado"
				SDMENU
					SDMENUITEM STR0214 ID "010020010"	LOCKED 
					SDMENU
						SDMENUITEM STR0103 ID "010020010010" LOCKED ACTION "SM2A100" PAISLOC "BRA" //"Grupo de Tributacao"    		
						SDMENUITEM STR0077 ID "010020010020" LOCKED ACTION "SM2A090" PAISLOC "BRA" //"Excecao Fiscal"  
						SDMENUITEM STR0171 ID "010020010030" LOCKED ACTION "SM2A170" PAISLOC "BRA" //"Substituicao Tributaria"
						SDMENUITEM STR0068 ID "010020010040" LOCKED ACTION "SM2A070" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Impostos"
						SDMENUITEM STR0076 ID "010020010050" LOCKED ACTION "SM2A080" PAISLOC "ARG" //"Ret. Percep. Imposto"
						SDMENUITEM STR0077 ID "010020010060" LOCKED ACTION "SM2A081" PAISLOC "MEX/URU" //"Excecao Fiscal"
						SDMENUITEM STR0098 ID "010020010070" LOCKED ACTION "SM2A084" PAISLOC "PAR" //"Retencao de Impostos"
						SDMENUITEM STR0078 ID "010020010080" LOCKED ACTION "SM2A082" PAISLOC "COL" //"ImpostoxZ.Fiscal"
						SDMENUITEM STR0078 ID "010020010090" LOCKED ACTION "SM2A083" PAISLOC "EUA" //"ImpostoxZ.Fiscal"
					SDENDMENU
	 				SDMENUITEM STR0184 ID "010020020" LOCKED //"Grade de Produto"
					SDMENU
						SDMENUITEM STR0185 ID "010020020010" LOCKED ACTION "SM2A121" //"Tabelas de Grade"
						SDMENUITEM STR0184 ID "010020020020" LOCKED ACTION "SM2A120" //"Grade de Produto"
					SDENDMENU
		   			SDMENUITEM STR0215 ID "010020030" LOCKED //"Comunicacao Bancaria - CNAB"
					SDMENU
						SDMENUITEM STR0200 ID "010020030010" LOCKED ACTION "SM1A190" //"Conv�nio Banco Empresa"
						SDMENUITEM STR0132 ID "010020030020" LOCKED ACTION "SM1A100" //"Parametros Bancos"
						SDMENUITEM STR0216 ID "010020030030" LOCKED ACTION "SM1A120" //"Rejeicoes CNAB"
						SDMENUITEM STR0135 ID "010020030040" LOCKED ACTION "SM1A130" //"Tipos de T�tulos"
						SDMENUITEM STR0137 ID "010020030050" LOCKED ACTION "SM1A150"  //"Situacao de Cobranca"						
						SDMENUITEM STR0217 ID "010020030060" LOCKED ACTION "SM1A140MVC" //"Ocorr.CNAB Bancos x Sistema"
						//SDMENUITEM "Arquivo Retorno Cobranca"  ID "05001013010" LOCKED ACTION "SM1M200" 
						//"Trata Arq.Retorno Cobranca"
						//SDMENUITEM "Arquivo Retorno Pagamentos" ID "05001013010" LOCKED ACTION "SM1M210" //"Trata Arq.Retorno Pagamentos"
						//SDMENUITEM STR0133 ID "05001013020" LOCKED ACTION "SM1A110" //"Ocorrencias CNAB"
					SDENDMENU
					SDMENUITEM STR0199 ID "010020040" LOCKED ACTION "SM4A040" //"F�rmula"
					SDMENUITEM STR0308 ID "010020090" LOCKED ACTION "SM1A270"//"Enquadramento Legal IPI"
					SDMENUITEM STR0219 ID "010020050" LOCKED ACTION "SM1A260"//"Tipo de Servi�o"
					SDMENUITEM STR0198 ID "010020060" LOCKED
					SDMENU
						SDMENUITEM STR0193 ID "010020060010" LOCKED ACTION "SM1A220" //"Plano de Contas"
						SDMENUITEM STR0194 ID "010020060020" LOCKED ACTION "SM1A240" //"Tipos de Lan�amento Padr�o"
						SDMENUITEM STR0195 ID "010020060030" LOCKED ACTION "SM1A230" //"Lan�amento Padr�o"
					SDENDMENU
					SDMENUITEM STR0179 ID "010020070" LOCKED ACTION "SM4A020" PAISLOC "BRA" //"Numera��o Documento"
					If CompanyInfo("OCCURS") == "S"
						SDMENUITEM STR0115 ID "010020080" LOCKED//"Ocorr�ncias"
						SDMENU
							SDMENUITEM  STR0116 ID "010020080010" LOCKED ACTION "SM1A070" // "Tipos de Ocorr�ncias"
							SDMENUITEM  STR0117 ID "010020080020" LOCKED ACTION "SM1A080" // "Registro de Ocorr�ncias"
						SDENDMENU
					EndIf
				SDENDMENU
			SDENDMENU                                                                            
			
		
			SDMENUITEM STR0014 ID "020" LOCKED MODNAME "COM" //"Compras"
			SDMENU
			
				SDMENUITEM STR0015 ID "020010" LOCKED //"Atualiza��es"
				SDMENU
					SDMENUITEM STR0016 ID "020010010" LOCKED ACTION "SM2M010" //"Solicit. de Compras"
					SDMENUITEM STR0017 ID "020010020" LOCKED ACTION "SM2M020" //"Solic.p/Ponto Pedido"
					SDMENUITEM STR0189 ID "020010025" LOCKED //"Cota��o de Compras"
					SDMENU
						SDMENUITEM STR0190 ID "02001002510" LOCKED ACTION "SM2A130" //"Gera Cota��o"
						SDMENUITEM STR0191 ID "02001002520" LOCKED ACTION "SM2A140" //"Atualiza Cota��o"
						SDMENUITEM STR0192 ID "02001002530" LOCKED ACTION "SM2A150" //"Analisa Cota��o"
					SDENDMENU	   								
					SDMENUITEM STR0018 ID "020010030" LOCKED ACTION "SM2M030" //"Pedido de Compra"
					SDMENUITEM  {|| ChkRem(1)} ID "020010040" LOCKED ACTION "SM2M070" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Remito"									
					SDMENUITEM  {|| ChkRem(2)} ID "020010050" LOCKED ACTION "SM2M080" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Devolucao Remito"      			
					SDMENUITEM STR0019 ID "020010060" LOCKED ACTION "SM2M040" //"Documento de Entrada"
					If oLicence:Homolog()
						SDMENUITEM STR0019 + cHomolog ID "020010061" LOCKED ACTION "SM2M040MVC" //"Documento de Entrada"
					EndIf
					SDMENUITEM STR0070 ID "020010070" LOCKED ACTION "SM2M050" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Nota Debito Fornec."			   
					SDMENUITEM STR0071 ID "020010080" LOCKED ACTION "SM2M060" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Nota Credito Fornec."						
					SDMENUITEM STR0237 ID "020010090" LOCKED ACTION "SM2M116" //"Conhecimento de Frete"
				SDENDMENU
		
				SDMENUITEM STR0020 ID "020020" LOCKED //"Consultas"
				SDMENU
					SDMENUITEM STR0022 ID "020020010" LOCKED ACTION "SM2C010" //"Acomp.Solic.de Compras"
					SDMENUITEM STR0023 ID "020020020" LOCKED ACTION "SM2C020" //"Acomp.Ped. de Compras"
					SDMENUITEM STR0024 ID "020020030" LOCKED ACTION "SM2C040" //"Boletim de Entrada"
					SDMENUITEM STR0018 ID "020020040" LOCKED ACTION "SM2C050" //"Pedido de Compra"
					SDMENUITEM {|| ChkRem(3)} ID "020020050" LOCKED ACTION "SM2C070" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Rel. Remito de Entrada"															
					SDMENUITEM STR0025 ID "020020060"	LOCKED ACTION "SM2C060" //"Rel. Notas Fiscais"			
					SDMENUITEM {|| ChkRem(5)} ID "020020070" LOCKED ACTION "U_IMPRREM" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Impressao de Remito"                        			
					SDMENUITEM STR0089 ID "020020080" LOCKED ACTION "U_IMPRNF" //"Impressao da NF"			
					SDMENUITEM STR0104 ID "020020090" LOCKED ACTION "SM2C080" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Listagem Nota Cred./Deb."        
					SDMENUITEM STR0114 ID "020020100" LOCKED ACTION "U_IMPRCOMP" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Impr. Nota Cred/Deb." 	    	
					SDMENUITEM STR0182 ID "020020110" LOCKED ACTION "SM2C090" //"Pedido de Compra (Layout de Impress�o)"
					SDMENUITEM STR0251 ID "020020120" LOCKED ACTION "SM2C100"//"Cota��o de Compras"
					SDMENUITEM STR0252 ID "020010121" LOCKED ACTION "sm2c110" //"Ponto de Pedido"
					SDMENUITEM STR0235 ID "020020130" LOCKED ACTION "SM4C140" //"Curva ABC por fornecedor" 
					SDMENUITEM STR0238 ID "020020140" LOCKED ACTION "SM2C116" //"NFs Originais X NFs Frete"					
				SDENDMENU
			SDENDMENU
		
			SDMENUITEM STR0026 ID "030" LOCKED MODNAME "EST" //"Estoque"
			SDMENU
		
				SDMENUITEM STR0015 ID "030010" LOCKED //"Atualiza��es"
				SDMENU
					SDMENUITEM STR0062 ID "030010010" LOCKED ACTION "SM3M010" //"Tipo de Movimenta��o"
					SDMENUITEM STR0027 ID "030010020" LOCKED ACTION "SM3M020" //"Saldos Iniciais"
					SDMENUITEM STR0028 ID "030010030" LOCKED ACTION "SM3M030" //"Saldos em Estoque"
					SDMENUITEM STR0030 ID "030010040" LOCKED ACTION "SM3M040" //"Internos"
					SDMENUITEM STR0029 ID "030010050" LOCKED ACTION "SM3M050" //"Estrutura"
					If oLicence:HasProduction()
						SDMENUITEM STR0031 ID "030010060" LOCKED ACTION "SM3M060" //"Ordem de Produ��o"
						SDMENUITEM STR0240 ID "030010130" LOCKED ACTION "SM3M140" //"Gera��o de Ordem de Produ��o"
						SDMENUITEM STR0032 ID "030010070" LOCKED ACTION "SM3M070" //"Ajuste de Empenhos"
					Endif
					SDMENUITEM STR0033 ID "030010080" LOCKED ACTION "SM3M080" //"Transfer�ncia"
					SDMENUITEM STR0176 ID "030010090" LOCKED ACTION "SM3M120" //"Invent�rio"
					If oLicence:Homolog()
						SDMENUITEM STR0176 + cHomolog ID "030010090" LOCKED ACTION "SM3M120MVC" //"Invent�rio"
					EndIf
					SDMENUITEM STR0108 ID "030010100" LOCKED ACTION "SM3M100" //"Rec�lculo Sdo Estoque" 
					SDMENUITEM STR0127 ID "030010110" LOCKED ACTION "SM3M320" //"Custo de Reposi��o"
					SDMENUITEM STR0088 ID "030010120" LOCKED ACTION "SM3M090" //"Fechamento"
					If oLicence:HasProduction()
						SDMENUITEM STR0265 ID "030010140" LOCKED ACTION "SM3M350" //"Explos�o"
					EndIf
				SDENDMENU
		
				SDMENUITEM STR0020 ID "030020" LOCKED //"Consultas"
				SDMENU
					SDMENUITEM STR0021 ID "030020010" LOCKED ACTION "SM3C010" //"Consulta Produto"
					SDMENUITEM STR0029 ID "030020020" LOCKED ACTION "SM3C020" //"Estrutura"
					SDMENUITEM STR0028 ID "030020030" LOCKED ACTION "SM3C050" //"Saldos em Estoque"
					SDMENUITEM STR0037 ID "030020040" LOCKED ACTION "SM3C900"  //"Kardex"
					SDMENUITEM STR0165 ID "030020050" LOCKED ACTION "SM3C910"  //"Kardex por Dia"
					If oLicence:HasProduction()
	            		SDMENUITEM STR0031 ID "030020060" LOCKED ACTION "SM3C820"  //"Ordem de Produ��o"
	            	EndIf          
	            	SDMENUITEM STR0188 ID "030020070" LOCKED ACTION "SM3C340"  //"Poder de/Em Terc."  	            	
	            	SDMENUITEM STR0196 ID "030020080" LOCKED ACTION "SM3C350"  //"Reg. Kardex Mod 3"
	            	SDMENUITEM STR0197 ID "030020090" LOCKED ACTION "SM3C360"  //"Reg. Invent. Mod 7"
					SDMENUITEM STR0228 ID "030020100" LOCKED ACTION "SM3C930"  //"Rastro"
					SDMENUITEM STR0232 ID "030020110" LOCKED ACTION "SM1C210" //"Consumo Mensal"
					SDMENUITEM STR0236 ID "030020120" LOCKED ACTION "SM3C400" //"Fechamento"
					SDMENUITEM STR0241 ID "030020130" LOCKED ACTION "SM3C285" //"Conf. Invent�rio"
					If oLicence:HasProduction()
						SDMENUITEM STR0266 ID "030020140" LOCKED ACTION "SM3C940" //"Explos�o por Item"
						SDMENUITEM STR0267 ID "030020150" LOCKED ACTION "SM3C950" //"Explos�es"
						SDMENUITEM STR0268 ID "030020160" LOCKED ACTION "SM3C960" //"Apontamentos de Produ��o"
					EndIf
					SDMENUITEM STR0306 ID "030020170" LOCKED ACTION "SmPergSelo" //"Relat�rio de selos"
				SDENDMENU
			SDENDMENU
		
			SDMENUITEM STR0038 ID "040" LOCKED MODNAME "FAT" //"Faturamento"
			SDMENU
		
				SDMENUITEM STR0015 ID "040010" LOCKED //"Atualiza��es"
				SDMENU
					If oLicence:HasServiceOrder()
						SDMENUITEM "Ordem de Servi�o" ID "040010010"	LOCKED ACTION "SMOS"
					EndIf 
					SDMENUITEM STR0065 ID "040010020" LOCKED ACTION "SM4M010"  //"Pedido de Venda"
					SDMENUITEM {|| ChkRem(1)} ID "040010030" LOCKED ACTION "SM4M060" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Remito"									
					SDMENUITEM {|| ChkRem(2)} ID "040010040" LOCKED ACTION "SM4M070" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Devolucao Remito"      						
					SDMENUITEM STR0067 ID "040010050" LOCKED ACTION "SM4M030" //"Documento de Saida"
					SDMENUITEM STR0128 ID "040010060" LOCKED ACTION "SM3M110" //"Forma��o Pre�o de Venda"
					SDMENUITEM STR0087 ID "040010070" LOCKED ACTION "SM4M080" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Fat. Automatica"			
					SDMENUITEM STR0072 ID "040010080" LOCKED ACTION "SM4M040" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Nota Credito Cliente"						
					SDMENUITEM STR0073 ID "040010090" LOCKED ACTION "SM4M050" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Nota Debito Cliente"			      			
					SDMENUITEM STR0181 ID "040010100" LOCKED ACTION "SM4A030" PAISLOC "BRA" //"NFe."   
					SDMENUITEM "NFSe"  ID "040010110" LOCKED ACTION "FISA022" PAISLOC "BRA"
					SDMENUITEM STR0249 ID "040010120" LOCKED ACTION "SM4M130" PAISLOC "BRA"   
					SDMENUITEM STR0282 ID "040010130" LOCKED ACTION "SPEDMANIFE" PAISLOC "BRA" //"Manifesta��o do Destinat�rio."
					SDMENUITEM STR0285 ID "040010140" LOCKED ACTION "SM4M400" PAISLOC "BRA"  // "Gest�o de Venda"
				SDENDMENU
		
				SDMENUITEM STR0020 ID "040020" LOCKED //"Consultas"
		                           
				SDMENU
					SDMENUITEM STR0084 ID "040020010" LOCKED ACTION "SM4C030" //"Consulta NF Sa�da"
					SDMENUITEM STR0085 ID "040020020" LOCKED ACTION "SM4C040" //"Relatorio Comiss�es" 
					SDMENUITEM STR0162 ID "040020030" LOCKED ACTION "SM4C070" //"Relatorio Vendas"								
					SDMENUITEM STR0125 ID "040020040" LOCKED ACTION "SM4C150" //"Impressao do Pedido Venda"
					SDMENUITEM STR0089 ID "040020050" LOCKED ACTION "SMIMPNFFAT" //"Impressao da NF"
					SDMENUITEM STR0104 ID "040020060" LOCKED ACTION "SM4C060"   PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Listagem Nota Cred./Deb."        
					SDMENUITEM STR0114 ID "040020070" LOCKED ACTION "U_IMPRFAT" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Impr. Nota Cred/Deb." 	    				
					SDMENUITEM {|| ChkRem(4)} ID "040020080" LOCKED ACTION "SM4C050" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Rel. Remito de Saida"																		
					
					//Daniel Matos				
					SDMENUITEM STR0180 ID "040020090" LOCKED ACTION "SM4C100" PAISLOC "BRA" //"Correla��o NF NSU"
					SDMENUITEM STR0183 ID "040020100" LOCKED ACTION "SM4C110"					
					SDMENUITEM STR0227 ID "040020110" LOCKED ACTION "SM4C120" //"Relat�rio de Pedido de Venda"
					SDMENUITEM STR0231 ID "040020120" LOCKED ACTION "SM1C200" //"Curva ABC"
					
					SDMENUITEM "Curva ABC por Produto Mensal" ID "040020260" LOCKED ACTION "SM4C270" //"Curva ABC por Produto Mensal"
					
					SDMENUITEM STR0234 ID "040020130" LOCKED ACTION "SM4C130" //"Curva ABC por cliente"
					SDMENUITEM STR0295 ID "040020250" LOCKED ACTION "SM4C260" //"Curva ABC por Vendedor"
					SDMENUITEM STR0233 ID "040020140" LOCKED ACTION "SM1C220" //"N.F. emitidas por cliente"
					SDMENUITEM STR0239 ID "040020150" LOCKED ACTION "SM1C230" //"Notas Fiscais emitidas"
					SDMENUITEM STR0243 ID "040020160" LOCKED ACTION "SM1C240" //"Etiquetas de Expedi��o(Layout de Impress�o)"
					If oLicence:HasServiceorder()
						SDMENUITEM STR0246 ID "040020170" LOCKED ACTION "SM1C250" //"Relat�rio de Ordens de Servi�o"
					Endif
					SDMENUITEM STR0248 ID "040020180" LOCKED ACTION "SM1C270" //"Minuta de Despacho"
					SDMENUITEM STR0250 ID "040020190" LOCKED ACTION "SM1C280" //"Impress�o da Rotade Entrega"
					SDMENUITEM STR0277 ID "040020270" LOCKED ACTION "SM4C200" //"Faturamento/Venda Di�ria"
					SDMENUITEM STR0255 ID "040020200" LOCKED ACTION "SM4C160" //"Faturamento/Vendas Mensal"
					SDMENUITEM STR0256 ID "040020210" LOCKED ACTION "SM4C170" //"Tabela de Pre�o"
					If FindFunction("SM4C180")
						SDMENUITEM STR0261 ID "040020220" LOCKED ACTION "SM4C180" //"Resumo Fiscal da Faturamento"
					EndIf
					SDMENUITEM STR0264 ID "040020230" LOCKED ACTION "SM4C190" //"Laudo de an�lise"
					SDMENUITEM STR0284 ID "040020240" LOCKED ACTION "SM4C240" //"Comparativo tabela de pre�os"
					SDMENUITEM STR0286 ID "040020241" LOCKED ACTION "SM4C250" //"Relat�rio de Separa��o de Pedido de Venda"
				SDENDMENU
		
			SDENDMENU
		
			SDMENUITEM STR0039 ID "050" LOCKED MODNAME "FIN" //"Financeiro"
			SDMENU
		
				SDMENUITEM STR0015 ID "050010" LOCKED //"Atualiza��es"
				SDMENU
		
					SDMENUITEM STR0040 ID "050010010" LOCKED ACTION "SM1M010" //"Ctas a Receber"
					If oLicence:Homolog()
						SDMENUITEM STR0040 + cHomolog ID "050010011" LOCKED ACTION "SM1M010MVC" //"Ctas a Receber MVC"
					EndIf
					SDMENUITEM STR0041 ID "050010020" LOCKED ACTION "SM1M020" //"Ctas a Pagar"
					If oLicence:Homolog() 
						SDMENUITEM STR0041 + cHomolog ID "050010021" LOCKED ACTION "SM1M020MVC" //"Ctas a Pagar MVC"
					EndIf
					SDMENUITEM STR0242 ID "050010140" LOCKED ACTION "SM1M140" //"Manut. CNAB" 
					SDMENUITEM STR0170 ID "050010030" LOCKED ACTION "SM1M130" //"Manut. Comiss�es"
					SDMENUITEM STR0042 ID "050010040" LOCKED ACTION "SM1M060" //"Saldos Banc�rios"
					SDMENUITEM STR0043 ID "050010050" LOCKED ACTION "SM1M030" //"Mov. Banc�rio"
					SDMENUITEM STR0044 ID "050010060" LOCKED ACTION "SM1M040" //"Transa��es Prog."
					If oLicence:Homolog()
						SDMENUITEM STR0044 + cHomolog ID "050010190" LOCKED ACTION "SM1M040MVC" //"Transa��es Prog."
						SDMENUITEM "Hist�rico de transa��es programadas" + cHomolog ID "050010200" LOCKED ACTION "SMLANCAGEN" //"Transa��es Prog."
					EndIf
//					SDMENUITEM STR0045 ACTION "SM1M050" //"Or�amentos"
					SDMENUITEM STR0121 ID "050010070" LOCKED ACTION "SM1M110" //"OrctoXExercicio"
					SDMENUITEM STR0106 ID "050010080" LOCKED ACTION "SM1M070" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Ordem de Pago"			
					SDMENUITEM STR0092 ID "050010090" LOCKED ACTION "SM1M080" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Recibo"						
//					SDMENUITEM STR0097 ID "050010100" LOCKED ACTION "SM1M090" PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU"  //"Compensacao de Titulos" 			
					SDMENUITEM STR0109 ID "050010110" LOCKED ACTION "SM1M100" //"Rec�lculo Sdo Banc�rio"
					SDMENUITEM STR0153 ID "050010120" LOCKED ACTION "SM1M300" //"Importa��o de Arquivos QIF"   
					If oLicence:Homolog()
						SDMENUITEM STR0153 + cHomolog ID "050010170" LOCKED ACTION "SMIMPEXTR" //"Importa��o de Arquivos OFC/OFX"
						SDMENUITEM STR0315 + cHomolog ID "050010180" LOCKED ACTION "SMDRE" //"DRE"
					EndIf   
					SDMENUITEM STR0173 ID "050010130" LOCKED ACTION "SM1M021" //"Cheque Avulso"
					If FindFunction("SM1M310")
						SDMENUITEM STR0262 ID "050010150" LOCKED ACTION "SM1M310" //"Lan�amentos Cont�beis"
					EndIf
					SDMENUITEM STR0303 ID "050010160" LOCKED //"Rede"
					SDMENU
						SDMENUITEM STR0304 ID "050010160010" LOCKED ACTION "SmExEl" //"Extrato Eletronico"
					SDENDMENU 
					SDMENUITEM STR0316 ID "050010170" LOCKED ACTION "SmCheques" //"Importa��o de Arquivos QIF"
					If oLicence:Homolog() 
						SDMENUITEM STR0320 ID "050010180" LOCKED ACTION "SmCNABAPI" //"API CNAB"
					EndIf 
					If oLicence:Homolog() 
						SDMENUITEM STR0321 ID "050010190" LOCKED ACTION "SMCNABRETURN" //"RETORNO API CNAB"
					EndIf 
					/*Else
						SDMENUITEM STR0143 	LOCKED 							  //"Comunicacao Bancaria - CNAB"
						SDMENU
							SDMENUITEM STR0201  LOCKED  						  //"Pagamentos"
							SDMENU
								SDMENUITEM STR0202  LOCKED ACTION "SM1M240" //"Bordero de Pagamentos"
								SDMENUITEM STR0203  LOCKED ACTION "M240SIS" //"Gera Arq.Remessa Pagamentos"
								SDMENUITEM STR0204  LOCKED ACTION "SM1M250" //"Trata Arq.Retorno Pagamentos"
								SDMENUITEM STR0205  LOCKED ACTION "SM1M251" //"Historico Remessa Pagamentos"
							SDENDMENU
							SDMENUITEM STR0206  LOCKED 						  //"Cobranca"
							SDMENU
								SDMENUITEM STR0207  LOCKED ACTION "SM1M260" //"Bordero de Cobranca"
								SDMENUITEM STR0208  LOCKED ACTION "M260SIS" //"Gera Arq.Remessa Cobranca"
								SDMENUITEM STR0209  LOCKED ACTION "SM1M280" //"Trata Arq.Retorno Cobranca" 
								SDMENUITEM STR0210  LOCKED ACTION "SM1M281" //"Historico titulos tratados"
							SDENDMENU
						SDENDMENU
					EndIf*/

				SDENDMENU
	
				SDMENUITEM STR0020 ID "050020" LOCKED //"Consultas"
				SDMENU
					SDMENUITEM STR0253 ID "050020001" LOCKED ACTION "SM1C010" //"Fluxo de Caixa Realizado"
					SDMENUITEM STR0047 ID "050020010" LOCKED ACTION "SM1C020" //"Fluxo de Caixa"
					SDMENUITEM STR0258 ID "050020220" LOCKED ACTION "SM1C290"//"Fluxo de Caixa por Cat.Fin."
					SDMENUITEM STR0048 ID "050020020" LOCKED ACTION "SM1C030" //"Extrato Banc�rio"
					SDMENUITEM STR0049 ID "050020030" LOCKED ACTION "SM1C040" //"T�tulos a Receber"
					SDMENUITEM STR0050 ID "050020040" LOCKED ACTION "SM1C050" //"T�tulos a Pagar"
					SDMENUITEM STR0051 ID "050020050" LOCKED ACTION "SM1C060" //"T�tulos Baixados"
					//SDMENUITEM STR0091	ACTION "SM1C070" //"Orcado x Real"
					SDMENUITEM STR0121 ID "050020060" LOCKED ACTION "SM1C100" //"Relatorio OrctoXExercicio"
		           	SDMENUITEM STR0160 ID "050020070" LOCKED ACTION "SM1C110" //"Rastreamento de recebimentos"
		            SDMENUITEM STR0161 ID "050020080" LOCKED ACTION "SM1C120" //"Rastreamento por baixas" 
					SDMENUITEM STR0159 ID "050020090" LOCKED ACTION "u_IMPRBOL" //"Boleto Bancario"
					SDMENUITEM STR0090 ID "050020100" LOCKED ACTION "SM1C080"      PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU" //Listagem Ordem Pago
					SDMENUITEM STR0111 ID "050020110" LOCKED ACTION "SM1C090"      PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU" //Listagem Recibo
					SDMENUITEM STR0105 ID "050020120" LOCKED ACTION "U_IMPROP"     PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU" //Impr. Ordem Pago
					SDMENUITEM STR0107 ID "050020130" LOCKED ACTION "U_IMPRREC"    PAISLOC "ARG/CHI/PAR/MEX/URU/POR/DOM/EUA/COL/VEN/PER/COS/BOL/PAN/SAL/EQU" //Impr. Recibo
					SDMENUITEM STR0110 ID "050020140" LOCKED ACTION "U_IMPRRETIB"  PAISLOC "ARG"  //"Certif. Retenc. IB"
					SDMENUITEM STR0112 ID "050020150" LOCKED ACTION "U_IMPRRETGAN" PAISLOC "ARG"  //"Certif. Retenc. Gananc."
					SDMENUITEM STR0172 ID "050020160" LOCKED ACTION "SM1C130"  //"Tributos"   				
					SDMENUITEM STR0177 ID "050020170" LOCKED ACTION "SM1C140" //"Cheques"
					SDMENUITEM STR0178 ID "050020180" LOCKED ACTION "SM1C170" //"Lanc. C.Custo"  
					SDMENUITEM STR0229 ID "050020190" LOCKED ACTION "SM1C180" //"T�tulos a receber por Venc/Pessoa"
					SDMENUITEM STR0230 ID "050020200" LOCKED ACTION "SM1C190" //"T�tulos a Pagar por Venc/Pessoa"
					SDMENUITEM STR0215 ID "050020210" LOCKED //"Comunicacao Bancaria - CNAB"
					SDMENU
						SDMENUITEM "T�tulos - Bordero" ID "050020210010" LOCKED ACTION "SM1C150" //"T�tulos - Bordero"
						SDMENUITEM "T�tulos - CNAB"	ID "050020210020"	LOCKED ACTION "SM1C160" //"T�tulos - CNAB"	
						SDMENUITEM "Hist�rico CNAB Cobran�a"  ID "050020210030" LOCKED ACTION "SM1M281" //"Historico titulos tratados"
						SDMENUITEM "Hist�rico CNAB Pagamento" ID "050020210040" LOCKED ACTION "SM1M251" //"Historico Remessa Pagamentos"
						If FindFunction("SM1C300")
							SDMENUITEM STR0275 ID "050020210050" LOCKED ACTION "SM1C300" //"Impress�o de Retorno CNAB"
						EndIf
					SDENDMENU
					SDMENUITEM STR0316 ID "050020230" LOCKED ACTION "SmChequeR" //"Controle de cheques"
				SDENDMENU
		
			SDENDMENU
		
			SDMENUITEM STR0146 ID "060" LOCKED MODNAME "TOOLS" //"Ferramentas"
			SDMENU
	
	
			If nRemoteType == 1 .Or. nRemoteType == -1 //Se obter via job deve carregar tudo tb
		   		SDMENUITEM STR0122 ID "060010" LOCKED ACTION "Sm_Word"          //"Integracao Microsoft Word"
				SDMENUITEM STR0124 ID "060020" LOCKED ACTION "M1A010Imp"      //"Integracao Microsoft OutLook"
				SDMENUITEM STR0123 ID "060030" LOCKED //"Integracao Microsoft Excel"
				SDMENU
	
					SDMENUITEM STR0150 ID "060030010" LOCKED ACTION "APEXCEL" //"Abrir planilha pelo MS-Excel"
					SDMENUITEM STR0151 ID "060030020" LOCKED // "Planilhas padr�es" 
						
						If !FindFunction("FWPoliceLoad")
							if Len(aDocs) <> 0
								SDMENU
									For ni := 1 To Len(aDocs)
										SDMENUITEM Capital(APDocName(aDocs[ni])) ID "060030020" + STRZERO(ni,2) + "0" ACTION "APDOCEXEC('" + aDocs[ni] + "')"
									Next
								SDENDMENU
							EndIf
						EndIf
			
				SDENDMENU
			EndIf
				// Caso haja alteracao na ordem do menu Financeira(0004) analisar
				// este fonte e o SM1M400.PRW
				SDMENUITEM STR0158 ID "060030030" LOCKED ACTION "SM1M400" //"Indicadores de Gest�o"
				SDMENUITEM STR0220 ID "060030070" LOCKED ACTION "SMView" //"Consulta relacional"
				SDMENUITEM STR0221 ID "060030040" LOCKED ACTION "SMExpTXT" //"Exportar dados"
				SDMENUITEM STR0222 ID "060030045" LOCKED ACTION "SMExpLay" //"Exportar com layout"
				SDMENUITEM STR0223 ID "060030050" LOCKED ACTION "SMExpNF" //"Exp. Nota Fiscal"
				If SmIsSaaS() .Or. !Empty(GetPvProfString("General","MASTERADDRESS","",GetADV97()))
					SDMENUITEM STR0300 ID "060030055" LOCKED ACTION "SmMonitor" //Monitor de Conex�es (apenas saas)
				EndIf
				
				If lIsRm
					SDMENUITEM STR0224 ID "060030060" LOCKED ACTION "SMBKREST" //"Backup / Restore"
				EndIf

				SDMENUITEM "Exporta arquivo texto etiquetas" ID "060030080" LOCKED ACTION "SMExpetq" //"Exp. Arquivo texto Etiquetas"
				SDMENUITEM STR0244 ID "060030071" LOCKED ACTION "SM4A060" //"Exporta��o de arquivo texto para S�rie 3"
				If FindFunction("SMGERENC")
					SDMENUITEM STR0276 ID "060030085" LOCKED ACTION "SMGERENC" //"Gerenciador de arquivos"
				EndIf
				If File("/system/GenExport.xml")
					SDMENUITEM STR0280 ID "060030090" LOCKED ACTION "SM4A080" //"Exporta��o gen�rica"
				EndIf
				SDMENUITEM STR0313 ID "060030095" LOCKED ACTION "SMDOMINIO" //"Exporta��o software DOMINIO"
			SDENDMENU

			SDMENUITEM STR0287 ID "065" LOCKED //"Meu Fly01"
			SDMENU
				If !Empty(Val(oLicence:GetPRDSerie()))
					SDMENUITEM STR0288 ID "065010" LOCKED ACTION "SMS1CONTRT" //"Meus contratos"
				EndIf
				If GetRemoteType()!=2 // Esta op��o n�o � suportada em MacOs e Linux
					SDMENUITEM STR0283 ID "065200" LOCKED ACTION 'SmHoraChat' //"Atendimento via Chat"
				EndIf
				SDMENUITEM STR0259 ID "065300" LOCKED ACTION "SM4A070" //"Atualiza��es do sistema"
				SDMENUITEM STR0296 ID "065400" LOCKED ACTION "SmRmtAsist" //"Atualiza��es do sistema"
				SDMENUITEM STR0319 ID "065600" LOCKED ACTION "SMFISTRIB" //"Par�metros tribut�rios"
				
				SDMENUITEM STR0305 ID "065500" LOCKED //Integra��o PDV
				SDMENU
					SDMENUITEM STR0298 ID "065510" LOCKED ACTION "SmIPDV510" //"Gera Token para Integra��o com o PDV"										
					SDMENUITEM STR0292 + " - " + STR0305 ID "065520" LOCKED ACTION "SmIPDVLogE" //"Log de Exporta��o"
					SDMENUITEM STR0291 + " - " + STR0305 ID "065530" LOCKED ACTION "SmIPDVLogI" //"Log de Importa��o"
					SDMENUITEM STR0299 ID "065540" LOCKED ACTION "Smtstpdv" //"Atualiza estoque para o PDV"
					SDMENUITEM STR0309 ID "065550" LOCKED ACTION 'SmMsgRun(, {|| SmIPDV530() }, "Verificando vendas pendentes enviadas pelo PDV. Por favor aguarde...")' //"Baixar Vendas Pendentes PDV"
					SDMENUITEM STR0318 ID "065560" LOCKED ACTION "SmAutoExec" //"Importa��o Autom�tica"
				SDENDMENU
				
				SDMENUITEM STR0289 ID "065410" LOCKED //"UmovMe"
				SDMENU
					SDMENUITEM STR0290 + " - " + STR0289 ID "06541010" LOCKED ACTION "SmUMov030" //"Configurador"
					SDMENUITEM STR0292 + " - " + STR0289 ID "06541020" LOCKED ACTION "SmUMov060E" //"Log de Importa��o"
					SDMENUITEM STR0291 + " - " + STR0289 ID "06541030" LOCKED ACTION "SmUMov060I" //"Log de Exporta��o"
				SDENDMENU

				SDMENUITEM STR0301 ID "065700" LOCKED //"Loja virtual"
				SDMENU
					SDMENUITEM STR0302 + " - " + STR0301 ID "065710" LOCKED ACTION "SmCShopCnf" //"Configurador"
					SDMENUITEM STR0292 + " - " + STR0301 ID "065720" LOCKED ACTION "SmCshopExp" //"Log de Exporta��o"
					SDMENUITEM STR0291 + " - " + STR0301 ID "065730" LOCKED ACTION "SmCshopImp" //"Log de Importa��o"						
				SDENDMENU
				
			SDENDMENU

			SDMENUITEM STR0129 ID "070" LOCKED //"Miscel�nea"
			SDMENU
				/*If !lIsRm
					SDMENUITEM STR0130 ID "070010" LOCKED ACTION "SMEXPORT"  //"Exp.Dados p/ Master/PyME" 
				Endif*/
				//SDMENUITEM STR0164 ACTION "SMGERINFO"  //"Bonagura"								
				SDMENUITEM STR0147 ID "070020" LOCKED ACTION "SM4A010" //"Configurador Layout Impressao"
				SDMENUITEM STR0148 ID "070030" LOCKED ACTION "SM1A160"  //"Campos para Layout de Impressao"
				//SDMENUITEM STR0152 ID "070040" LOCKED ACTION "SMIniCFG" //"Assistente de Configuracoes"  
				SDMENUITEM STR0226 ID "070040" LOCKED ACTION "ImprDic"  //"Campos para Layout de Impressao"	
				If FindFunction("SM4A180")
					SDMENUITEM STR0273 ID "0700110" LOCKED ACTION "SM4A180" //"Campos para Configura��o CNAB"
				EndIf
				If FindFunction("SM4A190")
					SDMENUITEM STR0274 ID "0700120" LOCKED ACTION "SM4A190" //"Campos para Configura��o de Boleto"
				EndIf			
				SDMENUITEM "Arquivos Magn�ticos" ID "070050" LOCKED
					SDMENU
						SDMENUITEM STR0186 ID "070050010" LOCKED ACTION "SM4M100" //"Instru��es Normativas"
						SDMENUITEM STR0187 ID "070050020" LOCKED ACTION "SM4M110" //"Reprocessamento"       
						SDMENUITEM "Sintegra" ID "070050030" LOCKED ACTION "SMSintegra" 
						SDMENUITEM "Importa��o NFS-e" ID "070050040" LOCKED ACTION "SMIMPNFE" //"Importa��o NFS-e"
						//------------------------------------------------------------
						//Apenas adiciona essa rotina caso seja ambiente windows
						//(integra��o ms-office nao homologada para mac)
						//------------------------------------------------------------
						If(isWindows() .Or. nRemoteType == -1)
							SDMENUITEM "Impress�o RPS" ID "070050050" LOCKED ACTION "SMIMPRPS" //"Impress�o RPS"
						EndIf
						If FindFunction("SMPROSOFT")
							SDMENUITEM STR0263 ID "070050060" LOCKED ACTION "SMPROSOFT" //"Exp Contabil Prosoft"
						EndIf
						SDMENUITEM STR0278 ID "070050070" LOCKED ACTION "FISA061" //"FCI"
					SDENDMENU				
				SDMENUITEM STR0225 ID "070070" LOCKED ACTION "SMBOLETO" //"Configurador de Boleto Bancario"
				SDMENUITEM STR0218 ID "070090" LOCKED	ACTION "SM1G049"  //"Configura��o CNAB Modelo 2"
				SDMENUITEM STR0245 ID "070080" LOCKED ACTION "SM4A050" //"Configura��o de exporta��o para S�rie 3"												
			SDENDMENU
		SDENDMENU
ElseIf !FindFunction("FWPoliceLoad") .Or. (!lNoLoad .And. Type("cFilAnt") == "C" .And. !Empty(cFilAnt))
	//Se est� com novo patch de lib
	If !FindFunction("FWPoliceLoad")
	aInfo := PswRetAll()
                                           	
	If Len(aInfo) > 4
		cAcesso := aInfo[5]
		aUsrAcs := GetAccessList()
		If Len(cAcesso) < Len(aUsrAcs)
			For ni := Len(cAcesso)+1 To Len(aUsrAcs)
				cAcesso += If(aUsrAcs[ni][1],"S","N")
			Next
		EndIf
	Else
		cAcesso := ""
	EndIf
	cAcesso := Padr(cAcesso,512,"N") 

    //If FindFunction("FWGetProduct") .And. FWGetProduct() $ "RM|UO"
    	__oMainApp:SetMenu(__aMainMenu) 
    /*	
    Else  		
		   	
		For ni := 1 To Len(__aMainMenu)
			If __aMainMenu[ni][4] .or. IsAdmin(cUsuario())		
					Aadd(aNewMnu,__aMainMenu[ni])
			ElseIf ni == 1
				If VerSenha(2)
					Aadd(aNewMnu,__aMainMenu[ni])
				EndIf
			Else
				If Ascan(aInfo[4],__aMainMenu[ni][6]) > 0
					Aadd(aNewMnu,__aMainMenu[ni])
				EndIf
			EndIf
		Next
		
		__oMainApp:SetMenu(aNewMnu) 
	EndIf
	*/
	Else
		If __oMainApp != nil
			cAcesso := __oMainApp:cAccess
			If !__oMainApp:lBPM
	    	__oMainApp:SetMenu(__aMainMenu) 
					EndIf
		EndIf
	EndIf
EndIf

Return __aMainMenu

//----------------------------------------

Function OpenData()
Local ni
Local aDic := SmallDic(, 5)

CursorWait()
For ni := 1 To Len(aDic)
	EvalRule(aDic[ni][SD_TALIAS],-512)
Next

If FindFunction("SmDicCache") .And. !SmDicCache()
	FWFreeObj(aDic)
Endif

CursorArrow()
Return

//----------------------------------------

Function CreateSX6()
Local oLic     := SmGetlicense()
Local cMoeda1  := ""
Local cMoeda2  := ""
Local cMoeda3  := ""
Local cMoeda4  := ""
Local cMoeda5  := ""
Local nDecimal := 2
Local cSimb1   := ""
Local cSimb2   := ""
Local cSimb3   := ""
Local cSimb4   := ""
Local cSimb5   := ""
Local cMoedaPl1:= ""
Local cMoedaPl2:= ""
Local cMoedaPl3:= ""
Local cMoedaPl4:= ""
Local cMoedaPl5:= ""
Local nCalcImpV:= 0
Local cCustoRem:= "12"  
Local cEstado  := ""

dbSelectArea("SX6")

If cPaisLoc != "BRA"
   nCalcImpV  := 1    //Por default, habilitar calculo de impostos variaveis (Localizacoes)   
EndIf
//EXEMPLO PARA CRIAR PARAMETROS
//NewMv(<parametro>,<conteudo>,<tipo>,<descricao>,<descricao espanhol>,<descricao ingles>)
NewMv("MV_DOCSEQ","000001","C","Ultimo numero sequencial utilizado para movimentos do First.",;	// PORTUGUES
										"Ultimo numero secuencial utilizado para movimientos del First.",; // ESPANHOL
										"Last sequence number used in First." )	// INGLES

NewMv("MV_GERIMPV",nCalcImpV,"N","Determina se a Empresa vai utilizar roteiro para calculo de Impostos Variaveis (Internacionaliza��o)",;	// PORTUGUES
										"Determina si sera utilizada la funcion para Tratamiento y Calculo de los Impuestos Variables.",; // ESPANHOL
										"Define if will be used route for calculation of  variable taxes (internacionalization)." )	// INGLES

NewMv("MV_LISTDEV","F","L","Indica se os movimentos de devolucao devem ser listados na mesma coluna que os movimentos originais",;	// PORTUGUES
										"Indica si los movimientos de devolucion deben ser listados en la misma columna que los movimientos",; // ESPANHOL
										"Indicate if the return transactions should be listed in the same column as the original" )	// INGLES


NewMv("MV_ICMVENC","03,1","C","Informar a quantidade de dias uteis para pgto.ICMS e periodo de apuracao, a ser utilizado no calculo do custo de entrada nas moedas 2, 3, 4 e 5.",;	// PORTUGUES
										"Informar el numero de dias utiles para el pago del IVA y para el periodo de computo usado en el calculo de entrada en las monedas 2, 3, 4, 5.",; // ESPANHOL
										"Inform the number of working days for payment of value added tax and the period of calculation used in calculation of entry cost in curr.2,3,4,5." )	// INGLES

NewMv("MV_IPIVENC","01,2","C","Informar a quantidade de dias a ser considerada para recolhimento do IPI e periodo de apuracao para calculo do custo de entrada.",;	// PORTUGUES
										"Informar la  cantidad de dias a considerar en la percepcion del IPI y el periodo de computo del calculo del costo de entrada.",; // ESPANHOL
										"Inform the number of days to be considered for the payment of the excise tax and the period of apuration for the calculation of the entry cost." )	// INGLES

NewMv("MV_PISVENC","01,1","C","Informar a quantidade de dias a ser considerada para recolhimento do PIS e periodo de apuracao para calculo do custo de entrada.",;	// PORTUGUES
										"Informar la  cantidad de dias a considerar en la percepcion del PIS y el periodo de computo del calculo del costo de entrada.",; // ESPANHOL
										"Inform the number of days to be considered for the payment of the excise tax and the period of apuration for the calculation of the entry cost." )	// INGLES

NewMv("MV_CUSENT","1","C",	"Define o calculo do custo de entrada sera calculado por Vencimento (2) ou Diario (1) (Moedas 2345). ",;	// PORTUGUES
									"Define si el calculo del costo de entrada sera por vencimiento (2) o sera diario (1).",; // ESPANHOL
									"Define the calculation of the inflow cost. will be calculated based on (maturity date - 2) or (dayly - 1).(currencies 2345)." )	// INGLES

NewMv("MV_NUMNF","1=000000001;PDV=000000001;=000000001","C",	"Este parametro contem as series de Notas Fiscais e suas respectivas sequencias. Ex: UNI=210340;A=203910.",;	// PORTUGUES
																"Este parametro contiene las series de las Facturas y sus respectivas secuencias. Ej.: UNI=210340;A=203910.",; // ESPANHOL
																"This parameter contains the Invoices series and their respective sequences. e.g.: UNI=210340;A=203910." )	// INGLES
NewMv("MV_RELT","\SPOOL\","C",	"Diretorio especificado para impressao de relatorio",;	// PORTUGUES
									"Directorio para grabacion de informes.",;				// ESPANHOL
									"Directory specific of report printing.")				// INGLES
									
NewMv("MV_GERAOPI","T","L",	"Gera ordens de producao para os produtos intermediarios na inclus�o da ordem de produ��o ao produto principal.",;	//PORTUGUES
										"Genera ordenes de produccion para los productos intermedios al incluir la orden de produccion del producto principal.",;			//ESPANHOL
										"It generates production orders for intermediary products when inserting a production order in the main product.")		//INGLES

NewMv("MV_GERASC","T","L",	"Gera automaticamente as solicitacoes de compras a partir da inclusao da ordem de producao.",;				//PORTUGUES
							"Emite solicitud de compras a partir de la inclusion de una orden de produccion.",;		//ESPANHOL
							"Generate purchase requis. from the inclusion of the production order.")					//INGLES

NewMv("MV_QUEBROP","N","C",	"Indica se deve quebrar as ordens de producao em lotes economicos, ou gerar uma unica ordem de producao.  S = quebra, N = nao quebra.",;				//PORTUGUES
								"Indica si las ordenes de produccion se dividen en lotes economicos, o si se genera una orden de produccion unica. S = Dividir; N = No Dividir.",;			//ESPANHOL
								"Indicate whether the orders of production must be splited or not in economic lots, or generate on sole order of production. y = break, n = not break.")		//INGLES

NewMv("MV_QUEBRSC","N","C",	"Indica se deve quebrar as solicitacoes de compra em lotes economicos, ou gerar uma unica solicitacao. S = quebra, N = nao quebra.",;	//PORTUGUES
								"Indica si debe dividir en lotes economicos las Solicitudes de Compra, o si genera una solicitud unica. S=Dividir; N=No Dividir.",;			//ESPANHOL
								"Indicate whether the purchase requests must be broken in economic lots, or generate one sole request. s = break, n = not break.")			//INGLES

NewMv("MV_MARK","0000","N",	"Flag de marcacao dos registros para MarkBrowse.",;
								"Flag de Anotacion de registros para MarkBrowse.",;
								"Marking of registers in the function markbrowse.")     

NewMv("MV_MARK4","0000","N",	"Flag de .arcacao dos registros para Markbrowse com 4 bytes.",;
								"Flag de Anotacion de registros para MarkBrowse com 4 bytes.",;
								"Marking of registers in the function markbrowse with 4 bytes.")


NewMv("MV_ALIQISS","5","N","Aliquota de ISS interna de acordo com o municipio da empresa.",;
                            "Alicuota de ISS interna de acuerdo con el municipio de la empresa.",;
							"ISS internal tax rate according to the company�s city.")

NewMv("MV_ALIQIRR","5","N","Aliquota de IRRF.",;
                            "Aliquota de IRRF.",;
							"Aliquota de IRRF.")

NewMv("MV_ALIQINS","5","N","Aliquota de INSS.",;
                            "Aliquota de INSS.",;
							"Aliquota de INSS.")

NewMv("MV_ALIQCSL","5","N","Aliquota de CSLL.",;
                            "Aliquota de CSLL",;
							"Aliquota de CSLL")  
							

NewMv("MV_CLIINAD","0","N","Quantidade de dias para considerar um cliente inadimplente com base nos t�tutlos vencidos.",;
                            "Quantidade de dias para considerar um cliente inadimplente com base nos t�tutlos vencidos.",;
							"Quantidade de dias para considerar um cliente inadimplente com base nos t�tutlos vencidos.")							
																					
NewMv("MV_ICMPAD","18","N","Aliquota de ICMS interna de acordo com o Estado.",;
                            "Alicuota de ICMS interna de acuerdo con la Provincia.",;
							"ICMS internal tax rate according to the State.") 

NewMv("MV_ESTNEG","2","C",	"Identifica se o sistema permitira que os saldos em estoque dos produtos fique negativo atraves de movimentacao. Conteudo deve ser (1)Sim ou (2)Nao.",;	  //PORTUGUES
							"Identifica si el sistema permitira que los saldos en stock de los productos quede negativo durante las transacciones. Contenido debe ser (1)Si o (2)No.",; //ESPANHOL
							"It identifies if the system will allow that balan ces in stock of products remains negative through movement.Content must be (1)Yes or (2)No.")		  //INGLES

NewMv("MV_CUSTENT","1","C",	"Define se calculo do custo de entrada sera calculado Off-Line(1) ou On-Line (2). ",;	// PORTUGUES
							"Define si el calculo del costo de entrada sera Off-Line(1) ou On-Line (2).",; // ESPANHOL
							"Define if calculation of the inflow cost. will be calculated Off-Line(1) ou On-Line (2)." )	// INGLES

NewMv("MV_LOGPDV","","C",	"Caminho onde se encontra o Logotipo para a impress�o gr�fica do Pedido de Venda ",;	// PORTUGUES
							"Caminho onde se encontra o Logotipo para a impress�o gr�fica do Pedido de Venda ",;	// ESPANHOL
							"Caminho onde se encontra o Logotipo para a impress�o gr�fica do Pedido de Venda " )	// INGLES

NewMv("MV_AUTVOL","2","C",	"Define se o campo do volume no pedido de venda e NF de sa�da ser� autom�tico de acordo com a quantidade de cada item.(1=Sim,2=N�o) ",;	// PORTUGUES
							"Define se o campo do volume no pedido de venda e NF de sa�da ser� autom�tico de acordo com a quantidade de cada item.(1=Sim,2=N�o) ",;	// ESPANHOL
							"Define se o campo do volume no pedido de venda e NF de sa�da ser� autom�tico de acordo com a quantidade de cada item.(1=Sim,2=N�o) " )	// INGLES

NewMv("MV_VLMTED","3000","N",	"Define o valor minimo para transferencias bancarias via CNAB do tipo TED.",;	// PORTUGUES
							"Define o valor minimo para transferencias bancarias via CNAB do tipo TED.",;	// ESPANHOL
							"Define o valor minimo para transferencias bancarias via CNAB do tipo TED." )	// INGLES

NewMv("MV_ALICMSN","0","N",	"Define a aliquota de icms para empresas optantes pelo simples nacional",;	// PORTUGUES
							"Define a aliquota de icms para empresas optantes pelo simples nacional",;	// ESPANHOL
							"Define a aliquota de icms para empresas optantes pelo simples nacional" )	// INGLES							
If cPaisLoc $ "ARG|URU|CHI|COL|MEX|DOM"
	cMoeda1  := "PESO"
	cMoeda2  := "DOLAR"
ElseIf cPaisLoc == "PAR"
	cMoeda1  := "GUARANI"
	cMoeda2  := "DOLAR"
ElseIf cPaisLoc $ "EUA|POR|EQU"
	cMoeda1  := "DOLAR"
ElseIf cPaisLoc == "PER"
	cMoeda1  := "SOL"
	cMoeda2  := "DOLAR"
ElseIf cPaisLoc == "VEN"
	cMoeda1  := "BOLIVAR"
	cMoeda2  := "DOLAR"
ElseIf cPaisLoc == "BOL"
	cMoeda1  := "BOLIVIANO"
	cMoeda2  := "DOLAR"
ElseIf cPaisLoc == "PAN"
	cMoeda1  := "BALBOA"
	cMoeda2  := "DOLAR"
ElseIf cPaisLoc == "SAL"
	cMoeda1  := "SALVADORAN COLON"
	cMoeda2  := "DOLAR"
ElseIf cPaisLoc == "COS"
	cMoeda1  := "COLON"
	cMoeda2  := "DOLAR"
ElseIf cPaisLoc == "BRA"
	cMoeda1  := "REAL"
	cMoeda2  := "DOLAR"
	cMoeda3	:= "PESO"
	cMoeda4  := "EURO"
	cMoeda5	:= "IENE"
EndIf

NewMv("MV_MOEDA1",cMoeda1,"C",	"Titulo da moeda 1.",;	// PORTUGUES
								"Titulo de la moneda 1.",; // ESPANHOL
								"Title of Currency 1.")	// INGLES

NewMv("MV_MOEDA2",cMoeda2,"C",	"Titulo da moeda 2.",;	// PORTUGUES
								"Titulo de la moneda 2.",; // ESPANHOL
								"Title of Currency 2.")	// INGLES

NewMv("MV_MOEDA3",cMoeda3,"C",	"Titulo da moeda 3.",;	// PORTUGUES
								"Titulo de la moneda 3.",; // ESPANHOL
								"Title of Currency 3.")	// INGLES

NewMv("MV_MOEDA4",cMoeda4,"C",	"Titulo da moeda 4.",;	// PORTUGUES
								"Titulo de la moneda 4.",; // ESPANHOL
								"Title of Currency 4.")	// INGLES

NewMv("MV_MOEDA5",cMoeda5,"C",	"Titulo da moeda 5.",;	// PORTUGUES
								"Titulo de la moneda 5.",; // ESPANHOL
								"Title of Currency 5.")	// INGLES

If cPaisLoc $ "CHI|PAR"
	nDecimal  := 0
EndIf

NewMv("MV_CENT1",nDecimal,"N",	"Quantidade de casas decimais na moeda 1.",;	// PORTUGUES
								"Cantidad de casas decimales en la moneda 1.",; // ESPANHOL
								"Number of decimal places in currency 1.")	// INGLES

NewMv("MV_CENT2",2,"N","Quantidade de casas decimais na moeda 2.",;	// PORTUGUES
						"Cantidad de casas decimales en la moneda 2.",; // ESPANHOL
						"Number of decimal places in currency 2.")	// INGLES

NewMv("MV_CENT3",2,"N","Quantidade de casas decimais na moeda 3.",;	// PORTUGUES
						"Cantidad de casas decimales en la moneda 3.",; // ESPANHOL
						"Number of decimal places in currency 3.")	// INGLES

NewMv("MV_CENT4",2,"N","Quantidade de casas decimais na moeda 4.",;	// PORTUGUES
						"Cantidad de casas decimales en la moneda 4.",; // ESPANHOL
						"Number of decimal places in currency 4.")	// INGLES

NewMv("MV_CENT5",2,"N","Quantidade de casas decimais na moeda 5.",;	// PORTUGUES
						"Cantidad de casas decimales en la moneda 5.",; // ESPANHOL
						"Number of decimal places in currency 5.")	// INGLES

If cPaisLoc <> "BRA"
	NewMv("MV_NUMREM","000001","C",	"Este parametro contem a sequencia de numeracao do Remito.",;	// PORTUGUES
										"Este parametro contiene la secuencia de numeracion del Remito.",; // ESPANHOL
										"This parameter contains the Remito's sequential number." )	// INGLES
EndIF

NewMv("MV_ULMES","20020101","D",	"Data ultimo fechamento do estoque.",;			//PORTUGUES
									"Fecha del ultimo cierre del stock.",;			//ESPANHOL
									"Date of last closing of inventory.")			//INGLES

NewMv("MV_DTRETRO","","D",	"Data de refer�ncia para fechamento de estoque retroativo. Mantenha sempre em branco.",;			//PORTUGUES
									"fecha de referencia para el cierre de inventario retroactiva. Siempre mantenga en blanco.",;			//ESPANHOL
									"Reference date for closing retroactive stock. Always keep blank.")			//INGLES									

If cPaisLoc == "POR"
	NewMv("MV_SRVPOR","T0P5N7","C","Aliquotas da retencao do Imposto sobre Servicos de acordo com os tipos de isencao: Total, Parcial ou Nenhuma.",; // PORTUGUES
									"Alicuotas de la retencions de Impuesto sobre Servicios segun el tipo exencion: Total, Parcial o Ninguna.",;      // ESPANHOL
									"Withholding for Services Rendered according with the types exemptions: Full, Parcial or None." )	             // INGLES
	
	
	NewMv("MV_ACESAI","55#1.00#0.60","C",	"Parametros para calculo do imposto sobre venda de oleo em Porto Rico - Lei 172",; // PORTUGUES
											"Parametros para el cargo del aceite en Puerto Rico - Ley 172",;                   // ESPANHOL
											"Parameters for calculation of oil outflow tax in Porto Rico - Law # 172" )	    // INGLES
EndIf

NewMv("MV_RELACNT","","C",	"Conta a ser utilizada no envio de relatorios por e-mail.",;		//PORTUGUES
							"Cuenta a ser utilizada en el envio de informes por e-mail.",;	//ESPANHOL
							"Account to be used to send reports by e-mail.")					//INGLES

NewMv("MV_RELPSW","","C",	"Senha da conta de e-mail para envio de relatorios.",;	//PORTUGUES
							"Clave de cuenta de e-mail para envio de informes.",;	//ESPANHOL
							"Password of email's account to send reports.")			//INGLES

NewMv("MV_RELSERV","","C",	"Nome do servidor de envio de e-mail utilizado nos relatorios.",;	//PORTUGUES
							"Nombre de servidor de envio de e-mail utilizado en los informes.",;	//ESPANHOL
							"Reports e-mails server name.")										//INGLES

NewMv("MV_RELFROM","","C",	'Endere�o usado no "From" do relatorio enviado por e-mail.',;	//PORTUGUES
							'Direcci�n usada en el "From" del informe enviado por e-mail.',;	//ESPANHOL
							'Addressee "From" of the report sent via e-mail.')				//INGLES

NewMv("MV_RELAUTH","","L",	'Servidor de e-mail necessita de Autenticac�o?',;	//PORTUGUES
							'�El servidor de e-mail requiere Autenticacion?',;	//ESPANHOL
							'Does the e-mail Server need Authentication?')				//INGLES

If cPaisLoc == "ARG"
	NewMv("MV_RETOP","GAN=000001;IBP=000001","C",	"Este parametro contem a sequencia de numeracao da retencao na Ordem de Pago",;	// PORTUGUES
													"Este parametro contiene la secuencia de numeracion de la retencion de la Orden de Pago",; // ESPANHOL
													"Este parametro contem a sequencia de numeracao da retencao na Ordem de Pago" )	// INGLES

	NewMv("MV_AGENTE","11111","C",	"Determina se a empresa usuaria � Agente de Retencao de Impostos",;	// PORTUGUES
									"Determina si la empresa usuaria es Agente de Retencion de Impuestos",; // ESPANHOL
									"Determine if the company has a tax agent")	// INGLES
ElseIf cPaisLoc == "MEX"									
	NewMv("MV_IMPSDEP","IVC;SUC","C", "Este parametro determina quais impostos sao dependentes entre si",;	// PORTUGUES
									  "Este parametro determina que impuestos son dependientes entre si",; // ESPANHOL
									  "This pa	rameter determines which taxes are linked to each other.")	// INGLES   
EndIf

If cPaisLoc $ "ARG|URU|CHI|COL|MEX"
	cSimb1  := "$"
	cSimb2  := "US$"
ElseIf cPaisLoc == "PAR"
	cSimb1  := "Gs."
	cSimb2  := "US$"
ElseIf cPaisLoc == "DOM"
	cSimb1  := "RD$"
	cSimb2  := "US$"
ElseIf cPaisLoc $ "EUA|POR|EQU"
	cSimb1  := "US$"
ElseIf cPaisLoc == "BRA"
	cSimb1  := "R$"
	cSimb2  := "US$"
	cSimb3  := "$"                     
	cSimb4  := "EUR"
	cSimb5  := "�" 
EndIf                            

NewMv("MV_SIMB1",cSimb1,"C",	"Simbolo utilizado pela moeda 1 do sistema.",;	// PORTUGUES
								"Simbolo usado por la moneda 1 del sistema.",; // ESPANHOL
								"Simbol used by currency 1 of system.")	// INGLES

NewMv("MV_SIMB2",cSimb2,"C",	"Simbolo utilizado pela moeda 2 do sistema.",;	// PORTUGUES
								"Simbolo usado por la moneda 2 del sistema.",; // ESPANHOL
								"Simbol used by currency 2 of system.")	// INGLES
								
NewMv("MV_SIMB3",cSimb3,"C",	"Simbolo utilizado pela moeda 3 do sistema.",;	// PORTUGUES
								"Simbolo usado por la moneda 3 del sistema.",; // ESPANHOL
								"Simbol used by currency 3 of system.")	// INGLES
								
NewMv("MV_SIMB4",cSimb4,"C",	"Simbolo utilizado pela moeda 4 do sistema.",;	// PORTUGUES
								"Simbolo usado por la moneda 4 del sistema.",; // ESPANHOL
								"Simbol used by currency 4 of system.")	// INGLES
								
NewMv("MV_SIMB5",cSimb5,"C",	"Simbolo utilizado pela moeda 5 do sistema.",;	// PORTUGUES
								"Simbolo usado por la moneda 5 del sistema.",; // ESPANHOL
								"Simbol used by currency 5 of system.")	// INGLES
										
NewMv("MV_MASCNAT","0","C",	"Mascara do codigo da natureza do titulo financeiro. Devera ser informado o numero de digitos para cada n�vel. Ex.: '242' = '99.9999.99'.",;			//PORTUGUES
								"Mascara del  codigo de la modalidad del titulo financiero, debe ser informado el numero de d�gito para cada nivel. Ex.: '242' = '99.9999.99'.",;	//ESPANHOL
								"Mask of the class code of the financial bill, the number of digits for each level must be informed. Ex.: '242' = '99.9999.99'.")					//INGLES
								

NewMv("MV_PRCVEN1","Padrao","C","Descricao da tabela de precos de venda 1.",;	//PORTUGUES
								"Descripcion de la tabla de precios de venta 1.",;	//ESPANHOL
								"Sale price table description 1.")		//INGLES

NewMv("MV_PRCVEN2","Promocional","C","Descricao da tabela de precos de venda 2. ",;	//PORTUGUES
								"Descripcion de la tabla de precios de venta 2.",;	//ESPANHOL
								"Sale price table description 2.")		//INGLES

NewMv("MV_PRCVEN3","Preferencial","C","Descricao da tabela de precos de venda 3. ",;	//PORTUGUES
								"Descripcion de la tabla de precios de venta 3.",;	//ESPANHOL
								"Sale price table description 3.")		//INGLES

If cPaisLoc != "BRA"
	If cPaisLoc $ "ARG|URU|CHI|COL|MEX|DOM"
		cMoedaPl1  := "PESOS"
		cMoedaPl2  := "DOLARES"
	ElseIf cPaisLoc == "PAR"
		cMoedaPl1  := "GUARANIES"
		cMoedaPl2  := "DOLARES"
	ElseIf cPaisLoc $ "EUA|POR"
		cMoedaPl1  := "DOLLARS"
	EndIf
	
	NewMv("MV_MOEDAP1",cMoedaPl1,"C",	"Titulo da moeda 1 no plural.",;	// PORTUGUES
										"Titulo de la moneda 1 en el plural.",; // ESPANHOL
										"Title of Currency 1 in plural.")	// INGLES
										
	NewMv("MV_MOEDAP2",cMoedaPl2,"C",	"Titulo da moeda 2 no plural.",;	// PORTUGUES
										"Titulo de la moneda 2 en el plural.",; // ESPANHOL
										"Title of Currency 2 in plural.")	// INGLES
										
	NewMv("MV_MOEDAP3",cMoedaPl3,"C",	"Titulo da moeda 3 no plural.",;	// PORTUGUES
										"Titulo de la moneda 3 en el plural.",; // ESPANHOL
										"Title of Currency 3 in plural.")	// INGLES
										
	NewMv("MV_MOEDAP4",cMoedaPl4,"C",	"Titulo da moeda 4 no plural.",;	// PORTUGUES
										"Titulo de la moneda 4 en el plural.",; // ESPANHOL
										"Title of Currency 4 in plural.")	// INGLES
										
	NewMv("MV_MOEDAP5",cMoedaPl5,"C",	"Titulo da moeda 5 no plural.",;	// PORTUGUES
										"Titulo de la moneda 5 en el plural.",; // ESPANHOL
										"Title of Currency 5 in plural.")	// INGLES
										
	NewMv("MV_CUSTREM",cCustoRem,"C",	"Modo de calculo do custo de entrada para o Remito",;	// PORTUGUES
										"Modo de calculo del costo de entrada para el Remito",; // ESPANHOL
    									"Calculation mode of inflow cost to Remito")	        // INGLES										
    //"1"-custo digitado    									
    //"2"-custo medio    
    //"3"-ultimo preco de compra
    //"4"-ultimo custo de entrada
    //"5"-preco do pedido de compra
    If cPaisLoc == "DOM"
       cEstado   := "05"
    EndIf   
	NewMv("MV_ESTADO",cEstado,"C",	"Sigla do estado da empresa usuaria do Sistema, para efeito de calculo de Imposto.",;                   // PORTUGUES
								"Abreviatura de la provincia de la empresa usuaria del Sistema, para efecto de calculo del Impuesto.",; // ESPANHOL
								"Abbreviation of state of the company user of the System, for the purpose of calculation of the tax." ) // INGLES
    
Else
   NewMv("MV_ESTADO","SP","C",	"Sigla do estado da empresa usuaria do Sistema, para efeito de calculo de ICMS (7, 12 ou 18%).",;	// PORTUGUES
      							"Sigla o abreviatura de la provincia de la empresa usuaria del Sistema, para calculo de ICMS (7, 12 u 18%).",; // ESPANHOL
	     						"State abbreviation related to the system user company used in order to calculated ICMS (7, 12 or 18%)." )	// INGLES
EndIf

NewMv("MV_BASERET","N","C",	"Define se as Reducoes de Base de Calculo do ICMS normal aplicam-se tambem na Base de Calculo do ICMS Solidario (retido)",;	// PORTUGUES
							"Define si las Reducciones de Base de Calculo del ICMS normal se aplican tambien a la Base de Calculo del ICMS Solidario (retenido)",; // ESPANHOL
							"Determine whether the standard ICMS Calculation Basis Reduction is also applied in the mutual ICMS Calculation Basis  (withheld)." )	// INGLES

NewMv("MV_ATIVIDA","","C",	"Ramo(s) de atividade(s) da empresa usuaria do Sistema sendo I=Industria, S=Servi�os e C=Comercio.",;
							"Ramo(s) de actividad(es) de la empresa usuaria del Sistema son: I=Industria, S=Servicios y C=Comercio.",;
							"Area(s) of activity(ies) related to the sytem user company: I=Industry, S=Services and C=Trading." )

NewMv("MV_ICMSIND","0","N",	"Se deve calcular ICMS nas operac�es de industria.",;
							"Se debe calcular ICMS en las operaciones de industria.",;
							"If ICMS must be calculated for industrial operations." )

NewMv("MV_ESPPADS","               ","C",	"Especie de volumes padrao utilizada no Pedido de Venda/Nota Fiscal de Saida.",;	// PORTUGUES
											"Especie de volumenes estandar utilizada en el Pedido de Venta/Factura de salida.",; // ESPANHOL
											"Type of standard volumes used in Sales Order/Outflow Invoice." )	// INGLES

NewMv("MV_MARPADS","               ","C",	"Marca padrao utilizada no Pedido de Venda/Nota Fiscal de Saida.",;	// PORTUGUES
											"Marca estandar utilizada en el Pedido de Venta/Factura de salida.",; // ESPANHOL
											"Standard brand used in Sales Order/Outflow Invoice." )	// INGLES
	
NewMv("MV_ESPPADE","               ","C",	"Especie de volumes padrao utilizada no Documento de Entrada.",;	// PORTUGUES
											"Especie de volumenes estandar utilizada en el Documento de entrada.",; // ESPANHOL
											"Type of standard volumes used in the Inflow Document." )	// INGLES
	                        		
NewMv("MV_MARPADE","               ","C",	"Marca padrao utilizada no Documento de Entrada.",;	// PORTUGUES
											"Marca estandar utilizada en el Documento de entrada.",; // ESPANHOL
											"Standard brand used in the Inflow Document." )	// INGLES

NewMv("MV_TXPIS",0.65,"N",	"Taxa para Calculo do PIS",;	// PORTUGUES
							"Tasa para calculo del PIS.",; // ESPANHOL
							"Rate for PIS calculation." )	// INGLES

NewMv("MV_TXCOFIN",2,"N",	"Taxa para Calculo do COFINS",;	// PORTUGUES
							"Tasa para calculo del COFINS.",; // ESPANHOL
							"Rate for COFINS calculation." )	// INGLES

NewMv("MV_DEDBPIS","N","C",	"Indica se retira o valor do ICMS/IPI creditavel da base de calculo do PIS. Conteudos : I/P/S/N",;	// PORTUGUES
							"Indica si retira el valor del ICMS de la base de calculo del PIS.Contenidos: I/P/S/N",; // ESPANHOL
							"Ind. if deducts ICMS/IPI value to be credited from the PIS tax basis. Contents : I/P/S/N" )	// INGLES

NewMv("MV_DEDBCOF","N","C",	"Indica se retira o valor do ICMS/IPI creditavel da base de calculo do COFINS. Conteudos : I/P/S/N",;	// PORTUGUES
							"Indica si retira el valor del ICMS de la base de calculo del COFINS.Contenidos: I/P/S/N",; // ESPANHOL
							"Ind. if deducts ICMS/IPI value to be credited from the COFINS tax basis. Contents : I/P/S/N" )	// INGLES

NewMv("MV_INIINDC","1","C",	"Este parametro informa se os indicadores de gest�o ser�o visualizados automaticamente ap�s a inicializa��o do sistema. Configura : 1 = Sim ou 2= Nao.",;			//PORTUGUES
								"Este parametro informa se os indicadores de gest�o ser�o visualizados automaticamente ap�s a inicializa��o do sistema. Configura : 1 = Sim ou 2= Nao.",;	//ESPANHOL
								"Este parametro informa se os indicadores de gest�o ser�o visualizados automaticamente ap�s a inicializa��o do sistema. Configura : 1 = Sim ou 2= Nao.")					//INGLES
								

NewMv("MV_CIDCHK","","C",	"Nome da Cidade a ser impressa no cheque.",; // PORTUGUES
							"Nome da Cidade a ser impressa no cheque.",; // ESPANHOL
							"Nome da Cidade a ser impressa no cheque." ) // INGLES
							
NewMv("MV_MODECHK","","C",	"Modelo da Impressoa de Cheques.",; // PORTUGUES
							"Modelo da Impressoa de Cheques.",; // ESPANHOL
							"Modelo da Impressoa de Cheques.")  // INGLES
							
NewMv("MV_PORTCHK","","C",	"Porta serial da impressora de Cheques.",; // PORTUGUES
							"Porta serial da impressora de Cheques.",; // ESPANHOL
							"Porta serial da impressora de Cheques.")  // INGLES		

// Parametro utilizado para configurar o centro de custo
NewMv("MV_MASCCUS","1123","C","Definicao da mascara do Centro de Custo. Informar o n�mero de digitos para cada nivel",; // PORTUGUES
							"Devera ser	Definicion de la mascara del centro de costo. Informarse el numero de digitos para cada nivel.",; // ESPANHOL
							"Define the Cost Center mask. The number of digits for each level must be informed.")  // INGLES		


NewMv("MV_PEDCRE","N","C","Considera pedidos de venda colocados e em aberto para compor o limite de credito. Conteudo deve ser (S) Sim ou (N) Nao.",;	 // PORTUGUES
								   "Considera pedidos de venda colocados e em aberto para compor o limite de credito. Conteudo deve ser (S) Sim ou (N) Nao.",; // ESPANHOL
								   "Considera pedidos de venda colocados e em aberto para compor o limite de credito. Conteudo deve ser (S) Sim ou (N) Nao." ) // INGLES

NewMv("MV_DIRBMP","\system\imagens\","C",	"Nome do diretorio de armazenamento das imagens .BMP e .JPG",;	// PORTUGUES
														"Nome do diretorio de armazenamento das imagens .BMP e .JPG",; // ESPANHOL
														"Nome do diretorio de armazenamento das imagens .BMP e .JPG" )	// INGLES

NewMv("MV_VALNFE","2","C",	"Indica se deseja validar o total da NF de Entrada (1-Sim | 2-N�o).",;	// PORTUGUES
									"�Desea convalidar el total de entrada NF (1-Si | 2-No).",; // ESPANHOL
									"Want to validate the total NF of Entry (1-Yes | 2-No)." )	// INGLES

NewMv("MV_PRXRER","000000000","C",	"Numero Sequencial do Lan�amento Renegociado (A Receber)",;	// PORTUGUES
											"Numero Sequencial do Lan�amento Renegociado (A Receber)",; // ESPANHOL
											"Numero Sequencial do Lan�amento Renegociado (A Receber)" )	// INGLES

NewMv("MV_PRXPGR","000000000","C",	"Numero Sequencial do Lan�amento Renegociado (A Pagar)",;	// PORTUGUES
											"Numero Sequencial do Lan�amento Renegociado (A Pagar)",; // ESPANHOL
											"Numero Sequencial do Lan�amento Renegociado (A Pagar)" )	// INGLES
NewMv("MV_SPEDURL","http://tss5-cloud.totvs.com.br/nfe/","C","URL SPED NFe",;	 // PORTUGUES
								  "URL SPED NFe",; // ESPANHOL
								  "URL SPED NFe" ) // INGLES											
NewMv("MV_MASCGRD","11,02,02","C","Indica a mascara da grade. 1o Parametro - Tamanho do Codigo Fixo. 2o e 3o Parametros - Tamanho dos Codigos Variaveis.",;	 // PORTUGUES
								 "Indica la mascara de la cuadricula. 1er parametro - Tamano codigo fijo. 2do y 3er Parametros-Tamano de los codigos variabl",; // ESPANHOL
								 "Indicate the grid mask. 1st parameter - Size of Fixed Code; 2nd and 3rd parameters - Size of Variable Codes" ) // INGLES								  

											
NewMv("MV_SUBTRIB","","C",	"Inscri��es Estaduais para opera��es de Substitui��o Tribut�ria com outros Estados.",;	// PORTUGUES
									"Inscri��es Estaduais para opera��es de Substitui��o Tribut�ria com outros Estados.",; // ESPANHOL
									"Inscri��es Estaduais para opera��es de Substitui��o Tribut�ria com outros Estados." )	// INGLES
									
NewMv("MV_SERIE","UNI/000","C",	"Configura��o da s�rie a ser apresentada pelas notas fiscais emitidas.",;	// PORTUGUES
									"Configura��o da s�rie a ser apresentada pelas notas fiscais emitidas.",; // ESPANHOL
									"Configura��o da s�rie a ser apresentada pelas notas fiscais emitidas." )	// INGLES

NewMv("MV_DATAFIS","","D","Ultima data de encerramento de operacoes fiscais.",;	// PORTUGUES
									"Ultima Fecha de Cierre de las Operaciones Fiscales.",; // ESPANHOL
									"Last fiscal operation closing date." )	// INGLES
																																			
NewMv("MV_NUMBORP","000000","C","Numero Sequencia de Borderos de Pagamentos",;	// PORTUGUES
								"Numero Sequencia de Borderos de Pagamentos",; // ESPANHOL
								"Numero Sequencia de Borderos de Pagamentos" )	// INGLES				 
NewMv("MV_NUMLOTS","0000","C",		"Numero Sequencial de Identificacao de um Lote de Servico" ,;	// PORTUGUES
												"Numero Sequencial de Identificacao de um Lote de Servico" ,; // ESPANHOL
												"Numero Sequencial de Identificacao de um Lote de Servico" )	// INGLES

NewMv("MV_NUMREGL","00000","C",		"Numero Sequencial do Registro no Lote" ,;	// PORTUGUES
												"Numero Sequencial do Registro no Lote" ,; // ESPANHOL
												"Numero Sequencial do Registro no Lote" )	// INGLES

NewMv("MV_NROARQ","000000","C",		"Numero Sequencial adotado na nomenclatura do arquivo CNAB" ,;	// PORTUGUES
												"Numero Sequencial adotado na nomenclatura do arquivo CNAB" ,; // ESPANHOL
												"Numero Sequencial adotado na nomenclatura do arquivo CNAB" )	// INGLES

NewMv("MV_DATARQ","00/00/00","C",	"Data para controlar nome adotado no arquivo CNAB" ,;	// PORTUGUES
												"Data para controlar nome adotado no arquivo CNAB" ,; // ESPANHOL
												"Data para controlar nome adotado no arquivo CNAB" )	// INGLES				 
												
If FindFunction("FWGetProduct") .And. FWGetProduct() $ "RM|UO"
	NewMv("MV_DRCNAB","C:\CNAB_TOTVS\"	,"C",	"Nome do diretorio de armazenamento dos arquivos .CNAB",;	// PORTUGUES
																	"Nome do diretorio de armazenamento dos arquivos .CNAB",; // ESPANHOL
																	"Nome do diretorio de armazenamento dos arquivos .CNAB" )	// INGLES
Else
	NewMv("MV_DRCNAB","C:\SANTANDER_TOTVS\"	,"C",	"Nome do diretorio de armazenamento dos arquivos .CNAB",;	// PORTUGUES
																	"Nome do diretorio de armazenamento dos arquivos .CNAB",; // ESPANHOL
																	"Nome do diretorio de armazenamento dos arquivos .CNAB" )	// INGLES
EndIf

NewMv("MV_LIMCOM","N","C",	"Controla Limite de Compras na inclus�o de Pedido de Compras",;	// PORTUGUES
							"Controla Limite de Compras na inclus�o de Pedido de Compras",;	// ESPANHOL
							"Controla Limite de Compras na inclus�o de Pedido de Compras" )	// INGLES

NewMv("MV_LIMPAG","N","C",	"Controla Limite de Pagamento na inclus�o de T�tulo a Pagar",;	// PORTUGUES
							"Controla Limite de Pagamento na inclus�o de T�tulo a Pagar",; 	// ESPANHOL
							"Controla Limite de Pagamento na inclus�o de T�tulo a Pagar" )	// INGLES

// Parametro utilizado para configurar o centro de custo
NewMv("MV_MASCCTA","1112","C","Definicao da mascara da Conta Cont�bil. Informar o n�mero de digitos para cada nivel",; // PORTUGUES
							"Devera ser	Definicion de la mascara del Conta Cont�bil. Informarse el numero de digitos para cada nivel.",; // ESPANHOL
							"Define the Conta Cont�bil mask. The number of digits for each level must be informed.")  // INGLES																																																								
																										
NewMv("MV_TAMGETDB",99999 ,"N","Tamanho m�ximo de linhas da MsGetDados",;              // PORTUGUES
							"Tamanho m�ximo de linhas da MsGetDados",; 				// ESPANHOL
							"Tamanho m�ximo de linhas da MsGetDados" )              // INGLES

NewMv("MV_ENDUPD","webshare.rm.com.br","C","Endere�o da UPDATE Autom�tico",;              // PORTUGUES
						  				   "Endere�o da UPDATE Autom�tico",; 			   // ESPANHOL
						  				   "Endere�o da UPDATE Autom�tico" )              // INGLES

NewMv("MV_USUUPD","anonymous","C","Login para o endere�o da UPDATE Autom�tico",;              // PORTUGUES
								  "Login para o endere�o da UPDATE Autom�tico",; 			  // ESPANHOL
								  "Login para o endere�o da UPDATE Autom�tico" )              // INGLES
						  								
NewMv("MV_PSWUPD","","C","Senha para o endere�o da UPDATE Autom�tico",;            // PORTUGUES
						 "Senha para o endere�o da UPDATE Autom�tico",; 		   // ESPANHOL
						 "Senha para o endere�o da UPDATE Autom�tico" )            // INGLES
						 						 
//Projeto BSB  
NewMv("MV_BANCO", "","C","Codigo do Banco",;	// PORTUGUES
							"Codigo do Banco",;  // ESPANHOL
							"Codigo do Banco" )	// INGLES

NewMv("MV_IMPNFC","2","C","Indica se deseja emitir as NFs Canceladas nos relat�rios de Nota Fiscal de Entrada. (1-Sim | 2-N�o).",;	// PORTUGUES
						  "Indica se deseja emitir as NFs Canceladas nos relat�rios de Nota Fiscal de Entrada. (1-Si | 2-No).",;	// ESPANHOL
						  "Indica se deseja emitir as NFs Canceladas nos relat�rios de Nota Fiscal de Entrada. (1-Yes | 2-No)." )	// INGLES

NewMv("MV_GERCOM","1","C","Indica se deseja gerar a comiss�o dos Vendedores levando em considera��o os Impostos da Movimenta��o ou n�o. (1-Sim | 2-N�o).",;	// PORTUGUES
						  "Indica se deseja gerar a comiss�o dos Vendedores levando em considera��o os Impostos da Movimenta��o ou n�o. (1-Si | 2-No).",;	// ESPANHOL
						  "Indica se deseja gerar a comiss�o dos Vendedores levando em considera��o os Impostos da Movimenta��o ou n�o. (1-Yes | 2-No)." )	// INGLES						  

NewMv("MV_CONTSOC","2.2/2.3/2.7","C","Percentual referente Contr.Seguridade Social Utilizado para calculo do FUNRURAL : Pessoa Fisica:=2.2,Seg.Especial 2.3,Juridica:= 2.7",;	// PORTUGUES
						  			 "Porcentajes referentes a Contribucion Seguridad Social: Persona fisica=2.2, Seg. Especial =2.7; Juridica =2.7",;	// ESPANHOL
						  			 "Percentages refering to Social Security Contrib: Natural Person:=2.2, Special Security 2.3, Legal Entity:=2.7", .T. )	// INGLES						  
				  
NewMv("MV_BXTITBO","2","C","Indica se sera permitida a baixa do titulo manual que esta em bordero(1-Sim | 2-N�o).",;	// PORTUGUES
						  "Indica se sera permitida a baixa do titulo manual que esta em bordero(1-Sim | 2-N�o).",;	// ESPANHOL
						  "Indica se sera permitida a baixa do titulo manual que esta em bordero(1-Sim | 2-N�o)." )	// INGLES						  

NewMv("MV_VL10925",0,"N","Valor maximo de pagamentos no periodo para dispensa da retencao de PIS/COFINS/CSLL",;	// PORTUGUES
						  "Valor maximo de pagamentos no periodo para dispensa da retencao de PIS/COFINS/CSLL",;	// ESPANHOL
						  "Valor maximo de pagamentos no periodo para dispensa da retencao de PIS/COFINS/CSLL" )	// INGLES						  
						  
NewMv("MV_VLRETIR",0,"N",	"Valor minimo para dispensa de retencao de IRRF.",;	// PORTUGUES
							"Valor minimo para dispensa de retencao de IRRF.",;	// ESPANHOL
							"Valor minimo para dispensa de retencao de IRRF." )	// INGLES						  

NewMv("MV_NFESERV","1","C",	"Tipo de mensagem a ser exibida na nfse.1=Desc. Serv;2=Desc.Serv+Msg NF;3=Desc.Serv+Imp+Msg NF;4=Desc.Prod;5=Desc.Prod+Imp+Msg NF;6=Msg NF+Imp;7=Desc.Prod+Desc.Aux+Msg NF",;	// PORTUGUES
							"Tipo de mensagem a ser exibida na nfse.1=Desc. Serv;2=Desc.Serv+Msg NF;3=Desc.Serv+Imp+Msg NF;4=Desc.Prod;5=Desc.Prod+Imp+Msg NF;6=Msg NF+Imp;7=Desc.Prod+Desc.Aux+Msg NF",;	// ESPANHOL
							"Tipo de mensagem a ser exibida na nfse.1=Desc. Serv;2=Desc.Serv+Msg NF;3=Desc.Serv+Imp+Msg NF;4=Desc.Prod;5=Desc.Prod+Imp+Msg NF;6=Msg NF+Imp;7=Desc.Prod+Desc.Aux+Msg NF")	// INGLES							  

NewMv("MV_TIPORET","1","C",	"1=Reten��o de impostos no valor total da Nota Fiscal. 2=Reten��o de impostos somente no financeiro.",;	// PORTUGUES
							"1=Reten��o de impostos no valor total da Nota Fiscal. 2=Reten��o de impostos somente no financeiro.",;	// ESPANHOL
							"1=Reten��o de impostos no valor total da Nota Fiscal. 2=Reten��o de impostos somente no financeiro." )	// INGLES						  


NewMv("MV_OSHRFAT","1","C",	"Indica o tipo de horas usadas para c�lcular o valor do servi�o: 1=Livre, 2=Sugere Previsto, 3=Sugere Realizado,4=Usa Previsto,5=Usa Realizado",;	// PORTUGUES
							"Indica o tipo de horas usadas para c�lcular o valor do servi�o: 1=Livre, 2=Sugere Previsto, 3=Sugere Realizado,4=Usa Previsto,5=Usa Realizado",;	// ESPANHOL
							"Indica o tipo de horas usadas para c�lcular o valor do servi�o: 1=Livre, 2=Sugere Previsto, 3=Sugere Realizado,4=Usa Previsto,5=Usa Realizado" )	// INGLES						  
						   
NewMv("MV_NCOLNCM",2,"N",	"1=Imprime 1 coluna na legenda do NCM, 2=Imprime 2 colunas na legenda do NCM.",;	// PORTUGUES
							"1=Imprime 1 coluna na legenda do NCM, 2=Imprime 2 colunas na legenda do NCM.",;	// ESPANHOL
							"1=Imprime 1 coluna na legenda do NCM, 2=Imprime 2 colunas na legenda do NCM." )	// INGLES						  

NewMv("MV_NDIASMB",3,"N",	"Quantidade de dias para pesquisar titulos no movimento bancario",;	// PORTUGUES
							"Quantidade de dias para pesquisar titulos no movimento bancario",;	// ESPANHOL
							"Quantidade de dias para pesquisar titulos no movimento bancario" )	// INGLES						  
								 
NewMv("MV_SIMPLES","N","C",	"Empresa optante pelo SIMPLES (S-SIM/N-NAO/E=Excedeu o limite do simples )",;
							"Empresa optante pelo SIMPLES (S-SIM/N-NAO/E=Excedeu o limite do simples )",;	// ESPANHOL
							"Empresa optante pelo SIMPLES (S-SIM/N-NAO/E=Excedeu o limite do simples )", .T.)	// INGLES						  

NewMv("MV_DECVUNI",5,"N",	"Decimais para precis�o do campo Valor unit�rio da Danfe",;	// PORTUGUES
							"Decimais para precis�o do campo Valor unit�rio da Danfe",;	// ESPANHOL
							"Decimais para precis�o do campo Valor unit�rio da Danfe" )	// INGLES						  

// Permitir a entrada de CPF/CNPJ duplicados no cadastro mediante paramatros abaixo:
// Frasson - 23/06/2010

NewMv("MV_VALCNPJ","2","C",	"Parametro que informa se permite CNPJ duplicado no cadastro de pessoas. 1=Permite, 2=N�o Permite (Padr�o = 2)",;	// PORTUGUES
							"Parametro que informa se permite CNPJ duplicado no cadastro de pessoas. 1=Permite, 2=N�o Permite (Padr�o = 2)",;	// ESPANHOL
							"Parametro que informa se permite CNPJ duplicado no cadastro de pessoas. 1=Permite, 2=N�o Permite (Padr�o = 2)" )	// INGLES						  

NewMv("MV_VALCPF","2","C",	"Parametro que informa se permite CPF duplicado no cadastro de pessoas. 1=Permite, 2=N�o Permite (Padr�o = 2)",;	// PORTUGUES
							"Parametro que informa se permite CPF duplicado no cadastro de pessoas. 1=Permite, 2=N�o Permite (Padr�o = 2)",;	// ESPANHOL
							"Parametro que informa se permite CPF duplicado no cadastro de pessoas. 1=Permite, 2=N�o Permite (Padr�o = 2)" )	// INGLES						  

NewMv("MV_VLOTPED","N","C",	"Valida lote no pedido de venda / documento de sa�da (S-SIM/N-NAO)",;	// PORTUGUES
							"Valida lote no pedido de venda / documento de sa�da (S-SIM/N-NAO)",;	// ESPANHOL
							"Valida lote no pedido de venda / documento de sa�da (S-SIM/N-NAO)" )	// INGLES						  

NewMv("MV_DANTADE",45,"N",	"Determina o tamanho da descri��o do produto do item da DANFE para quebra de linha da descri��o",;	// PORTUGUES
							"Determina o tamanho da descri��o do produto do item da DANFE para quebra de linha da descri��o",;	// ESPANHOL
							"Determina o tamanho da descri��o do produto do item da DANFE para quebra de linha da descri��o" )	// INGLES

NewMv("MV_DANTIDE","1","C",	"Determina a descri��o do produto do item da DANFE a ser impressa (1-Item NF, 2-Item NF+Desc. Auxiliar, 3-Item+Desc. Aux.+NF Origem)",;	// PORTUGUES
							"Determina a descri��o do produto do item da DANFE a ser impressa (1-Item NF, 2-Item NF+Desc. Auxiliar, 3-Item+Desc. Aux.+NF Origem)",;	// ESPANHOL
							"Determina a descri��o do produto do item da DANFE a ser impressa (1-Item NF, 2-Item NF+Desc. Auxiliar, 3-Item+Desc. Aux.+NF Origem)" )	// INGLES

NewMv("MV_CNPJAUT","","C","Cnpj da matriz para validacao do certificado digital",;	// PORTUGUES
							"Cnpj da matriz para validacao do certificado digital",;	// ESPANHOL
							"Cnpj da matriz para validacao do certificado digital" )	// INGLES

// Criar par�metro para determinar o tipo de nomenclatura do arquivo de remessa de cnab:
// Frasson - 08/07/2010

NewMv("MV_TIPCNAB","1","C",	"Tp Arq.CNAB Rec/Pag. (1=Tipo+Data+Seq�enc., 2=Tipo+Seq�enc., 3=Tipo+Banco+Seq�enc., 4=Banco+Seq�enc., 5=Seq�enc)",;	// PORTUGUES
							"Tp Arq.CNAB Rec/Pag. (1=Tipo+Data+Seq�enc., 2=Tipo+Seq�enc., 3=Tipo+Banco+Seq�enc., 4=Banco+Seq�enc., 5=Seq�enc)",;	// ESPANHOL
							"Tp Arq.CNAB Rec/Pag. (1=Tipo+Data+Seq�enc., 2=Tipo+Seq�enc., 3=Tipo+Banco+Seq�enc., 4=Banco+Seq�enc., 5=Seq�enc)") 	// INGLES


NewMv("MV_PVALTPR","S","C",	"Permite alterar o pre�o unit�rio do item do pedido de venda (S-SIM/N-NAO)",;	// PORTUGUES
							"Permite alterar o pre�o unit�rio do item do pedido de venda (S-SIM/N-NAO)",;	// ESPANHOL
							"Permite alterar o pre�o unit�rio do item do pedido de venda (S-SIM/N-NAO)") 	// INGLES

NewMv("MV_PVALTCO","N","C",	"Permite alterar a comiss�o do produto no pedido de venda (S-SIM/N-NAO)",;	// PORTUGUES
							"Permite alterar a comiss�o do produto no pedido de venda (S-SIM/N-NAO)",;	// ESPANHOL
							"Permite alterar a comiss�o do produto no pedido de venda (S-SIM/N-NAO)") 	// INGLES

NewMv("MV_PVSALDO","1","C",	"Utiliza saldo em estoque no pedido de venda (1=Saldo Atual, 2=Saldo Atual - Empenho, 3= Valida Saldo Atual - Empenho)",;	// PORTUGUES
				  			"Utiliza saldo em estoque no pedido de venda (1=Saldo Atual, 2=Saldo Atual - Empenho, 3= valida Saldo Atual - Empenho)",;	// ESPANHOL
							"Utiliza saldo em estoque no pedido de venda (1=Saldo Atual, 2=Saldo Atual - Empenho, 3= valida Saldo Atual - Empenho)") 	// INGLES

NewMv("MV_CODSERV","","C",	"Informar o c�digo de servi�o utilizado para gerar ordem de servi�o pelo pedido",;	// PORTUGUES
				  			"Informar o c�digo de servi�o utilizado para gerar ordem de servi�o pelo pedido",;	// ESPANHOL
							"Informar o c�digo de servi�o utilizado para gerar ordem de servi�o pelo pedido") 	// INGLES

NewMv("MV_SINCODP","1","C",	"Parametro para tratamento do cadigo do produto para o arquivo sintegra. (1=Para retirar primeiro caracter / 2=Para retirar ultimo caracter.)",;	// PORTUGUES
							"Parametro para tratamento do cadigo do produto para o arquivo sintegra. (1=Para retirar primeiro caracter / 2=Para retirar ultimo caracter.)",;	// ESPANHOL
							"Parametro para tratamento do cadigo do produto para o arquivo sintegra. (1=Para retirar primeiro caracter / 2=Para retirar ultimo caracter.)") 	// INGLES

NewMv("MV_INCREST","1","C",	"Parametro para tratamento da inscri��o estadual para o cadastro de pessoas. (1=N�o permite repetir IE previamente cadastrada / 2=Permite repetir IE previamente cadastrada.)",;	// PORTUGUES
							"Parametro para tratamento da inscri��o estadual para o cadastro de pessoas. (1=N�o permite repetir IE previamente cadastrada / 2=Permite repetir IE previamente cadastrada.)",;	// ESPANHOL
							"Parametro para tratamento da inscri��o estadual para o cadastro de pessoas. (1=N�o permite repetir IE previamente cadastrada / 2=Permite repetir IE previamente cadastrada.)") 	// INGLES
														
NewMv("MV_REGESIM",".F.","L",	"Habilita o Registro Simplificado MT(.T.=Habilita / .F.=N�o habilita).",;	// PORTUGUES
								"Habilita o Registro Simplificado MT(.T.=Habilita / .F.=N�o habilita).",;	// ESPANHOL
								"Habilita o Registro Simplificado MT(.T.=Habilita / .F.=N�o habilita).") 	// INGLES
								
NewMv("MV_STBONIF",".T.","L",	"Define se ST de produto com bonifica��o � calculada no financeiro(.T.=SIM / .F.=N�O).",;	// PORTUGUES
									"Define se ST de produto com bonifica��o � calculada no financeiro(.T.=SIM / .F.=N�O).",;	// ESPANHOL
									"Define se ST de produto com bonifica��o � calculada no financeiro(.T.=SIM / .F.=N�O).") 	// INGLES
									
NewMv("MV_DTCOMIS","1","N",	"Define se a data da comiss�o ser� igual a do extrato ou a data base (1=EXTRATO / 2=DATA BASE).",;	// PORTUGUES
									"Define se a data da comiss�o ser� igual a do extrato ou a data base (1=EXTRATO / 2=DATA BASE).",;	// ESPANHOL
									"Define se a data da comiss�o ser� igual a do extrato ou a data base (1=EXTRATO / 2=DATA BASE).") 	// INGLES									

NewMv("MV_PERCATM","0","N","Percentual da Carga Tributaria Media, do destinatario MT.",;	// PORTUGUES
							"Percentual da Carga Tributaria Media, do destinatario MT.",;	// ESPANHOL
							"Percentual da Carga Tributaria Media, do destinatario MT.") 	// INGLES
							
NewMv("MV_PERCOMS","1","C","Considera Percentual de Comiss�o? (1= Documento / 2= Cadastro de Pessoa.) ",;   // PORTUGUES
						   "Considera Percentual de Comiss�o? (1= Documento / 2= Cadastro de Pessoa.)",;    // ESPANHOL
						   "Considera Percentual de Comiss�o? (1= Documento / 2= Cadastro de Pessoa.)")	    // INGLES 
						   
NewMv("MV_NOMENFE","1","C","Define Nomenclatura do XML? (1= Chave de Acesso da NF-e, 2= Protocolo de uso)",;   // PORTUGUES
					 	   "Define Nomenclatura do XML? (1= Chave de Acesso da NF-e, 2= Protocolo de uso)",;   // ESPANHOL
					   	   "Define Nomenclatura do XML? (1= Chave de Acesso da NF-e, 2= Protocolo de uso)")	// INGLES

NewMv("MV_LOTEVEN","0","N",	"Sequencia numerica do lote",;	// PORTUGUES
							"Sequencia numerica do lote",;
							"Sequencia numerica do lote")
							
NewMv("MV_TVAROS","","C",	"Tabela Vari�vel da Ordem de Servi�o (Chaves: 01, 02, 03 e 04)",;	// PORTUGUES
							"Tabela Vari�vel da Ordem de Servi�o (Chaves: 01, 02, 03 e 04)",;
							"Tabela Vari�vel da Ordem de Servi�o (Chaves: 01, 02, 03 e 04)")

NewMv("MV_TVARPES","","C",	"Tabelas Vari�veis do Cadastro de Pessoa (Chaves: 05, 06, 07 e 08)",;	// PORTUGUES
							"Tabelas Vari�veis do Cadastro de Pessoa (Chaves: 05, 06, 07 e 08)",;
							"Tabelas Vari�veis do Cadastro de Pessoa (Chaves: 05, 06, 07 e 08)")

NewMv("MV_TVARPRO","","C",	"Tabelas Vari�veis do Cadastro de Produto (Chaves: 09, 10, 11 e 12)",;	// PORTUGUES
							"Tabelas Vari�veis do Cadastro de Produto (Chaves: 09, 10, 11 e 12)",;
							"Tabelas Vari�veis do Cadastro de Produto (Chaves: 09, 10, 11 e 12)")

NewMv("MV_TREPORT","2","N","Habilita impress�o dos relat�rios utilizando componente gr�fico (TReport). Op��es: 1 - N�o Utiliza | 2 � Utiliza | 3 � Pergunta se utiliza",;   // PORTUGUES
					 	   "Habilita impress�o dos relat�rios utilizando componente gr�fico (TReport). Op��es: 1 - N�o Utiliza | 2 � Utiliza | 3 � Pergunta se utiliza",;      // ESPANHOL
					   	   "Habilita impress�o dos relat�rios utilizando componente gr�fico (TReport). Op��es: 1 - N�o Utiliza | 2 � Utiliza | 3 � Pergunta se utiliza")	    // INGLES
													
NewMv("MV_ESTICM","AC17AL17AM17AP17BA17CE17DF17ES17GO17MA17MG18MS17MT17PA17PB17PE17PI17PR18RJ18RN17RO17RR17RS18SC17SE17SP18TO17","C","Aliquota de ICMS de cada estado. Informar a sigla e em seguida a aliquota, para calculo de ICMS complementar.",;	// PORTUGUES
							"Aliquota de ICMS de cada estado. Informar a sigla e em seguida a aliquota, para calculo de ICMS complementar.",;
							"Aliquota de ICMS de cada estado. Informar a sigla e em seguida a aliquota, para calculo de ICMS complementar.")

NewMv("MV_RELTLS",.F.,"L","Informe se o servidor de SMTP possui conex�o do tipo segura(TLS)",;	//PORTUGUES
							'Informi se el servidor de SMTP tiene conexion del tipo segura(TLS)',;	//ESPANHOL
							'Enter if the SMTP server has a safe-type connection (TLS)')				//INGLES
					
NewMv("MV_RELSSL",.F.,"L","Define se o envio e recebimento de e-mails utilizar� conex�o segura (SSL)",;	//PORTUGUES
							'Define si debe habilitarse el SSL en el envio y recepcion de e-mails',;	//ESPANHOL
							'Difene whether SSL is enabled when receiving e-mails.')				//INGLES

NewMv("MV_DESCZF",.T.,"L","Indica se calcula o desconto Suframa. (.T.-Calcula,.F.-N�o Calcula)",;	// PORTUGUES
							"Indica si calcula descuento Suframa. (.T.-Calcula,.F.-No Calcula)",;
							"Indicates if calculates SUFRAMA discount. (T.-Calculate,.F.-Do not Calculate)")
							
NewMv("MV_DESCALC","N","C","Considera Desconto Calculado",;   // PORTUGUES
 					 	       "Considera Desconto Calculado",;  // ESPANHOL
 					   	       "Considera Desconto Calculado")	 // INGLES

NewMv("MV_ULTPRST",.F.,"L","Indica se considera a ST para o �ltimo pre�o de compra. (.T.-Considera,.F.-N�o Considera)",;	// PORTUGUES
							"Indica se considera a ST para o �ltimo pre�o de compra. (.T.-Considera,.F.-N�o Considera)",;
							"Indica se considera a ST para o �ltimo pre�o de compra. (.T.-Considera,.F.-N�o Considera)")
							
NewMv("MV_IMPNFE","C:\FIRST_IMP_NFE\","C","Diret�rio de armazenamento dos arquivos XML e TXT de importa��o",;   // PORTUGUES
												"Diret�rio de armazenamento dos arquivos XML e TXT de importa��o",;  // ESPANHOL
												"Diret�rio de armazenamento dos arquivos XML e TXT de importa��o")	 // INGLES

NewMv("MV_ALIQR50",.T.,"L","Indica se o Sistema ir� gerar a al�quota de ICMS no registro 54 da mesma maneira que � gerado no registro 50, para opera��es isentas ou outras.",;   // PORTUGUES
												"Indica se o Sistema ir� gerar a al�quota de ICMS no registro 54 da mesma maneira que � gerado no registro 50, para opera��es isentas ou outras.",;  // ESPANHOL
												"Indica se o Sistema ir� gerar a al�quota de ICMS no registro 54 da mesma maneira que � gerado no registro 50, para opera��es isentas ou outras. ")	 // INGLES
						 					   	      
NewMv("MV_REGIESP","","C","Informar o Regime especial de tributa��o :  1 � ME Municipal; 2 � Estimativa; 3 � Sociedade de Profissionais; 4 � Cooperativa; 5 � MEI; 6 � ME ou EPP",;   // PORTUGUES
 					 	       "Informar o Regime especial de tributa��o :  1 � ME Municipal; 2 � Estimativa; 3 � Sociedade de Profissionais; 4 � Cooperativa; 5 � MEI; 6 � ME ou EPP",;  // ESPANHOL
 					   	       "Informar o Regime especial de tributa��o :  1 � ME Municipal; 2 � Estimativa; 3 � Sociedade de Profissionais; 4 � Cooperativa; 5 � MEI; 6 � ME ou EPP")	 // INGLES						   
							   
NewMv("MV_INCECUL","2","C","Informar se o contribuinte � optante do incentivo a cultura para que seja gerada a TAG <IncentivadorCultural>. 1 � Sim; 2 � N�o",;   // PORTUGUES
 					 	    "Informar se o contribuinte � optante do incentivo a cultura para que seja gerada a TAG <IncentivadorCultural>. 1 � Sim; 2 � N�o",;  // ESPANHOL
 					   	    "Informar se o contribuinte � optante do incentivo a cultura para que seja gerada a TAG <IncentivadorCultural>. 1 � Sim; 2 � N�o")	 // INGLES							   

NewMv("MV_IMPPED","SM4C090","C","Informe o fonte de impress�o Gr�fica do Pedido de Venda.",;   // PORTUGUES
								"Informe o fonte de impress�o Gr�fica do Pedido de Venda.",;  // ESPANHOL
								"Informe o fonte de impress�o Gr�fica do Pedido de Venda.")	// INGLES
								
NewMv("MV_SPEDEXC","24","N","Prazo em horas para cancelamento da NFe conforme determina��o da Sefaz de cada estado.",;	// PORTUGUES
										"Prazo em horas para cancelamento da NFe conforme determina��o da Sefaz de cada estado.",; // ESPANHOL
										"Prazo em horas para cancelamento da NFe conforme determina��o da Sefaz de cada estado." )	// INGLES

NewMv("MV_NFECAEV","T","L","Ativar o Cancelamento da NF-e como Evento",;	// PORTUGUES
										"Ativar o Cancelamento da NF-e como Evento",; // ESPANHOL
										"Ativar o Cancelamento da NF-e como Evento" )	// INGLES
								
NewMv("MV_CODPROS","","C","Informe seu c�digo da Prosoft para compor o nome do arquivo de Exporta��o",;   // PORTUGUES
 					 	    "Informe seu c�digo da Prosoft para compor o nome do arquivo de Exporta��o",;  // ESPANHOL
 					   	    "Informe seu c�digo da Prosoft para compor o nome do arquivo de Exporta��o")	 // INGLES	
 					   	    
NewMv("MV_SEQPROS","","C","�ltima Sequ�ncia da Exporta��o Prosoft",;   // PORTUGUES
 					 	    "�ltima Sequ�ncia da Exporta��o Prosoft",;  // ESPANHOL
 					   	    "�ltima Sequ�ncia da Exporta��o Prosoft")	 // INGLES
							
NewMv("MV_SR54IPI","F","L",	"Habilita/Desabilita a soma do valor do IPI no Registro 54, quando tiver calculo do IPI na Nota Fiscal, mas este n�o gera credito no livro.",;	//PORTUGUES
								"Habilita/Desabilita a soma do valor do IPI no Registro 54, quando tiver calculo do IPI na Nota Fiscal, mas este n�o gera credito no livro.",;			//ESPANHOL
								"Habilita/Desabilita a soma do valor do IPI no Registro 54, quando tiver calculo do IPI na Nota Fiscal, mas este n�o gera credito no livro.")		//INGLES

// Par�metros para o requisito 001417 do projeto F11.5.4_FI
// Criar uma fun��o para definir se a SA1 estiver vazio o par�metro � igual a S,  se estiver preenchido deve ser igual a N.
IF !GetMv("MV_NUMAUTP",.T.)
	NewMv("MV_NUMAUTP",Numautp(),"C",	"Utiliza numera��o autom�tica na inclus�o de Cliente, Fornecedor, Vendedor e Transportadora(S/N).",;	//PORTUGUES
											"Utiliza numera��o autom�tica na inclus�o de Cliente, Fornecedor, Vendedor e Transportadora(S/N).",;			//ESPANHOL
											"Utiliza numera��o autom�tica na inclus�o de Cliente, Fornecedor, Vendedor e Transportadora(S/N).")		//INGLES
EndIf
NewMv("MV_NUMAUTC","C00000","C",	"C�digo do �ltimo cliente inclu�do por numera��o autom�tica.",;	//PORTUGUES
										"C�digo do �ltimo cliente inclu�do por numera��o autom�tica.",;	//ESPANHOL
										"C�digo do �ltimo cliente inclu�do por numera��o autom�tica.")		//INGLES

NewMv("MV_NUMAUTF","F00000","C",	"C�digo do �ltimo fornecedor inclu�do por numera��o autom�tica.",;	//PORTUGUES
										"C�digo do �ltimo fornecedor inclu�do por numera��o autom�tica.",;	//ESPANHOL
										"C�digo do �ltimo fornecedor inclu�do por numera��o autom�tica.")	//INGLES

NewMv("MV_NUMAUTV","V00000","C",	"C�digo do �ltimo vendedor inclu�do por numera��o autom�tica.",;	//PORTUGUES
										"C�digo do �ltimo vendedor inclu�do por numera��o autom�tica.",;	//ESPANHOL
										"C�digo do �ltimo vendedor inclu�do por numera��o autom�tica.")	//INGLES

NewMv("MV_NUMAUTT","T00000","C",	"C�digo da �ltima transportadora inclu�da por numera��o autom�tica.",;	//PORTUGUES
										"C�digo da �ltima transportadora inclu�da por numera��o autom�tica.",;	//ESPANHOL
										"C�digo da �ltima transportadora inclu�da por numera��o autom�tica.")		//INGLES
										
NewMv("MV_TPRETPC","1","N",	"Define se o acumulador da reten��o do PCC ser� baseada no(s): 1 - Documentos Fiscais; 2 - T�tulos Financeiros",;	//PORTUGUES
										"Define se o acumulador da reten��o do PCC ser� baseada no(s): 1 - Documentos Fiscais; 2 - T�tulos Financeiros",;	//ESPANHOL
										"Define se o acumulador da reten��o do PCC ser� baseada no(s): 1 - Documentos Fiscais; 2 - T�tulos Financeiros")	//INGLES										

NewMv("MV_VALTPRO","S","C",	"Permite alterar o pre�o unit�rio do subitem na ordem de servi�o (S-SIM/N-NAO)",;	// PORTUGUES
							"Permite alterar o pre�o unit�rio do subitem na ordem de servi�o (S-SIM/N-NAO)",;	// ESPANHOL
							"Permite alterar o pre�o unit�rio do subitem na ordem de servi�o (S-SIM/N-NAO)") 	// INGLES
							
NewMv("MV_QUADCUB","N","C","Permite informar a quantidade do produto ou servi�o no pedido de venda atrav�s da grandeza f�sica do m� ou m�: M2 = Metro quadrado � M3 = metro c�bico � N = Normal.",;	// PORTUGUES
							"Permite informar a quantidade do produto ou servi�o no pedido de venda atrav�s da grandeza f�sica do m� ou m�: M2 = Metro quadrado � M3 = metro c�bico � N = Normal.",;	// ESPANHOL
							"Permite informar a quantidade do produto ou servi�o no pedido de venda atrav�s da grandeza f�sica do m� ou m�: M2 = Metro quadrado � M3 = metro c�bico � N = Normal.") 	// INGLES

NewMv("MV_LTDANFE","S","C","Informa se deve incluir o Lote no XML da DANFE. S=SIM e N=NAO",;	// PORTUGUES
							"Informa se deve incluir o Lote no XML da DANFE. S=SIM e N=NAO",;	// ESPANHOL
							"Informa se deve incluir o Lote no XML da DANFE. S=SIM e N=NAO") 	// INGLES

NewMv("MV_FCICFOP","101|105|107|109|111|113|116|118|122|124|125",;
		"C","Informa os CFOPs que implicar�o em FCI (apenas os �ltimos tr�s d�gitos de cada)",;	// PORTUGUES
							"Informa os CFOPs que implicar�o em FCI (apenas os �ltimos tr�s d�gitos de cada)",;	// ESPANHOL
							"Informa os CFOPs que implicar�o em FCI (apenas os �ltimos tr�s d�gitos de cada)") 	// INGLES

NewMv("MV_FCIDANF",".F.","L",".T. Se deve imprimir FCI na DANFE, .F. caso contr�rio",;	// PORTUGUES
							".T. Se deve imprimir FCI na DANFE, .F. caso contr�rio",;	// ESPANHOL
							".T. Se deve imprimir FCI na DANFE, .F. caso contr�rio") 	// INGLES

NewMv("MV_PRCBRUT","N","C","Determina se utiliza pre�o bruto no pedido de venda",;	// PORTUGUES
							"Determina se utiliza pre�o bruto no pedido de venda",;	// ESPANHOL
							"Determina se utiliza pre�o bruto no pedido de venda") 	// INGLES

NewMv("MV_IPIBRUT","","C","Informar o tipo da base de c�lculo para o Vlr. de IPI, sendo (S) para considerar sobre o valor bruto ou (N) para considerar sobre o valor l�quido do Pedido de Venda.",;	// PORTUGUES
						  "Informar o tipo da base de c�lculo para o Vlr. de IPI, sendo (S) para considerar sobre o valor bruto ou (N) para considerar sobre o valor l�quido do Pedido de Venda.",;	// ESPANHOL
						  "Informar o tipo da base de c�lculo para o Vlr. de IPI, sendo (S) para considerar sobre o valor bruto ou (N) para considerar sobre o valor l�quido do Pedido de Venda.") 	// INGLES							

NewMv("MV_QUBRAOS",".F.","L","Determina se a partir do Ordem de Servi�o cria o or�amento separado para as mercadorias e outro para servi�os",;	// PORTUGUES
								 "Determina se a partir do Ordem de Servi�o cria o or�amento separado para as mercadorias e outro para servi�os",;	// ESPANHOL
								 "Determina se a partir do Ordem de Servi�o cria o or�amento separado para as mercadorias e outro para servi�os") 	// INGLES
NewMv("MV_FRMPRC","1","C",	"Determina o tipo de forma��o de pre�o utilizada: 1 - Antiga, 2 - Nova",;	// PORTUGUES
							"Determina o tipo de forma��o de pre�o utilizada: 1 - Antiga, 2 - Nova",;	// ESPANHOL
							"Determina o tipo de forma��o de pre�o utilizada: 1 - Antiga, 2 - Nova") 	// INGLES

IF !GetMv("MV_DEFSPEC",.T.)
	NewMv("MV_DEFSPEC",SmDefSpec(),"C",	"Determina o valor default para o campo Esp�cie do cabe�alho do Pedido de Venda, Documento de Sa�da e Documento de Entrada",;	// PORTUGUES
								"Determina o valor default para o campo Esp�cie do cabe�alho do Pedido de Venda, Documento de Sa�da e Documento de Entrada",;	// ESPANHOL
								"Determina o valor default para o campo Esp�cie do cabe�alho do Pedido de Venda, Documento de Sa�da e Documento de Entrada") 	// INGLES
EndIf

NewMv("MV_CODPROD","000000000000001","C","N�mera��o autom�tica do C�digo do Produto (tamb�m usado na importa��o do arquivo xml)",;	// PORTUGUES
							"N�mera��o autom�tica do C�digo do Produto (tamb�m usado na importa��o do arquivo xml)",;	// ESPANHOL
							"Determina o valor para c�digo do produto na importa��o do arquivo xml") 	// INGLES
NewMv("MV_LOCALXML","","C","Determina o valor para armazem na importa��o do arquivo xml",;	// PORTUGUES
							"Determina o valor para armazem na importa��o do arquivo xml",;	// ESPANHOL
							"Determina o valor para armazem na importa��o do arquivo xml") 	// INGLES
NewMv("MV_TESXML","","C","Determina o valor para TES de Entrada na importa��o do arquivo xml",;	// PORTUGUES
							"Determina o valor para TES de Entrada na importa��o do arquivo xml",;	// ESPANHOL
							"Determina o valor para TES de Entrada na importa��o do arquivo xml") 	// INGLES

NewMv("MV_ALTCOMV","S","C",	"Permite alterar a comiss�o do vendedor no pedido de venda/documento de sa�da (S-SIM/N-NAO)",;	// PORTUGUES
							"Permite alterar a comiss�o do vendedor no pedido de venda/documento de sa�da (S-SIM/N-NAO)",;	// ESPANHOL
							"Permite alterar a comiss�o do vendedor no pedido de venda/documento de sa�da (S-SIM/N-NAO)") 	// INGLES
									
NewMv("MV_QATUPV","N","C",	"Determina se o Saldo em Estoque estar� atualizado ao alterar/vizualizar o pedido de venda: (S-SIM/N-NAO)",;	// PORTUGUES
							"Determina se o Saldo em Estoque estar� atualizado ao alterar/vizualizar o pedido de venda: (S-SIM/N-NAO)",;	// ESPANHOL
							"Determina se o Saldo em Estoque estar� atualizado ao alterar/vizualizar o pedido de venda: (S-SIM/N-NAO)") 	// INGLES

NewMv("MV_NFELIMI","0","N",	"Limite mensal de envio de nfe",;	// PORTUGUES
							"Limite mensal de envio de nfe",;	// ESPANHOL
							"Limite mensal de envio de nfe") 	// INGLES
													
NewMv("MV_RELPEDC","1","N",	"Tipo do relat�rio do pedido de compra",;	// PORTUGUES
							"Tipo do relat�rio do pedido de compra",;	// ESPANHOL
							"Tipo do relat�rio do pedido de compra") 	// INGLES

NewMv("MV_INFOLOT","1","N",	"Determina no PV se produto com lote vir� preenchido automaticamente. Informe 1 para Sim e 2 para N�o",; //PORTUGUES
							"Determina no PV se produto com lote vir� preenchido automaticamente. Informe 1 para Sim e 2 para N�o",; //ESPANHOL
							"Determina no PV se produto com lote vir� preenchido automaticamente. Informe 1 para Sim e 2 para N�o")  //INGLES
							
NewMv("MV_TVARPV","","C",	"Tabela Vari�vel do Pedido de Venda (Chaves: 13, 14, 15 e 16)",;	// PORTUGUES
							"Tabela Vari�vel do Pedido de Venda (Chaves: 13, 14, 15 e 16)",;		//ESPANHOL
							"Tabela Vari�vel do Pedido de Venda (Chaves: 13, 14, 15 e 16)")		//INGLES
							
NewMv("MV_ALTPVPA","1","N",	"Define se permite alterar PV faturados parcialmente. Informe 1 para N�o ou 2 para Sim",;	// PORTUGUES
							"Define se permite alterar PV faturados parcialmente. Informe 1 para N�o ou 2 para Sim",;		//ESPANHOL
							"Define se permite alterar PV faturados parcialmente. Informe 1 para N�o ou 2 para Sim")		//INGLES

NewMv("MV_AGRSERV","1","C",	"Define se os servi�os ser�o agrupados na Nfs-e e RPS. Informe 1 para N�o ou 2 para Sim",;	// PORTUGUES
							"Define se os servi�os ser�o agrupados na Nfs-e e RPS. Informe 1 para N�o ou 2 para Sim",;		//ESPANHOL
							"DDefine se os servi�os ser�o agrupados na Nfs-e e RPS. Informe 1 para N�o ou 2 para Sim")		//INGLES

NewMv("MV_WFSRVDV"," ","C",	"URL do Servidor WF",;	// PORTUGUES
							"URL do Servidor WF",;		//ESPANHOL
							"URL do Servidor WF")		//INGLES

NewMv("MV_MULTRAT"," ","C",	"Permite a manipula��o m�ltipla de rateios de t�tulos",;	// PORTUGUES
							"Permite la manipulaci�n m�ltipla de rateos de facturas",;		//ESPANHOL
							"Allows the manipulation of multiples apportionment of fares")		//INGLES

NewMv("MV_ESTCOMI","0","N",	"N�mero de dias aceitos para a devolu��o para que o desconto de comiss�o seja lan�ado automaticamente (-1 para sem limite, 0 para nunca lan�ar)",;	// PORTUGUES
							"N�mero de dias aceitos para a devolu��o para que o desconto de comiss�o seja lan�ado automaticamente (-1 para sem limite, 0 para nunca lan�ar)",;		//ESPANHOL
							"N�mero de dias aceitos para a devolu��o para que o desconto de comiss�o seja lan�ado automaticamente (-1 para sem limite, 0 para nunca lan�ar)")		//INGLES

NewMv("MV_DOCCOMI","C00000000","C",	"Numera��o de comiss�o manual",;	// PORTUGUES
									"Numera��o de comiss�o manual",;	//ESPANHOL
									"Numera��o de comiss�o manual")		//INGLES

NewMv("MV_DANFTSS","","C",	"Determina a pasta onde a DANFE do TSS ser� salva",;	// PORTUGUES
							"Determina a pasta onde a DANFE do TSS ser� salva",;	//ESPANHOL
							"Determina a pasta onde a DANFE do TSS ser� salva")		//INGLES

NewMv("MV_PRCPAD","   ","C",	"Define Tabela de Pre�o padr�o para importa��o do arquivo .txt no Pedido de Venda",;	// PORTUGUES
								"Define Tabela de Pre�o padr�o para importa��o do arquivo .txt no Pedido de Venda",;	//ESPANHOL
								"Define Tabela de Pre�o padr�o para importa��o do arquivo .txt no Pedido de Venda")		//INGLES

If !GetMv("MV_OBRICGC",.T.)
	NewMv("MV_OBRICGC",SmObgCgc(),"C",	"Determina se o CNPJ/CPF ser� obrigat�rio no cadastro de Pessoa/Cli/Forn/Vend e Transp. Informe 1 para Sim e 2 para N�o",;	// PORTUGUES
								"Determina se o CNPJ/CPF ser� obrigat�rio no cadastro de Pessoa/Cli/Forn/Vend e Transp. Informe 1 para Sim e 2 para N�o",;	//ESPANHOL
								"Determina se o CNPJ/CPF ser� obrigat�rio no cadastro de Pessoa/Cli/Forn/Vend e Transp. Informe 1 para Sim e 2 para N�o")		//INGLES
EndIf

NewMv("MV_FATUNI","4","C",	"Determina o n�vel de unifica��o do faturamento. 0 � Nenhum, 1 � At� Envio de NFe, 2 � Valida��o de Inconsist�ncias, 3 � Impress�o de DANFe, 4 � Impress�o de Boleto",;	// PORTUGUES
							"Determina o n�vel de unifica��o do faturamento. 0 � Nenhum, 1 � At� Envio de NFe, 2 � Valida��o de Inconsist�ncias, 3 � Impress�o de DANFe, 4 � Impress�o de Boleto",;	//ESPANHOL
							"Determina o n�vel de unifica��o do faturamento. 0 � Nenhum, 1 � At� Envio de NFe, 2 � Valida��o de Inconsist�ncias, 3 � Impress�o de DANFe, 4 � Impress�o de Boleto")		//INGLES
							
NewMv("MV_MOEDAPV","1","N", "Determina qual a moeda do Pedido de Venda. 1=Real 2=Dolar 3=Peso 4=Euro 5=Iene",; //PORTUGUES
							"Determina qual a moeda do Pedido de Venda. 1=Real 2=Dolar 3=Peso 4=Euro 5=Iene",; //ESPANHOL
							"Determina qual a moeda do Pedido de Venda. 1=Real 2=Dolar 3=Peso 4=Euro 5=Iene") //INGLES
							
NewMv("MV_LOGPDC","","C",	"Caminho onde se encontra o Logotipo para a impress�o do Pedido de Compra ",;	// PORTUGUES
							"Caminho onde se encontra o Logotipo para a impress�o do Pedido de Compra ",;	// ESPANHOL
							"Caminho onde se encontra o Logotipo para a impress�o do Pedido de Compra " )	// INGLES

NewMv("MV_TABLOG",".F.","L",	"Determina se deve ou n�o gravar log de tabelas.",;				//PORTUGUES
							"Determina se deve ou n�o gravar log de tabelas.",;		//ESPANHOL
							"Determina se deve ou n�o gravar log de tabelas.")		//INGLES
							
NewMv("MV_TPDANFE","1","N",   "Tipo DANFE 1=Padr�o 2=Novo Retrato 3=Novo Paisagem",; //PORTUGUES
							  "Tipo DANFE 1=Padr�o 2=Novo Retrato 3=Novo Paisagem",; //ESPANHOL
							  "Tipo DANFE 1=Padr�o 2=Novo Retrato 3=Novo Paisagem") //INGLES


NewMv("MV_GETUPRC","1","C",	"Atualiza os campos de Pre�o Unit�rio e TES com base na �ltima compra com o mesmo fornecedor. Informe 1 para Sim e 2 para N�o",;	// PORTUGUES
							"Atualiza os campos de Pre�o Unit�rio e TES com base na �ltima compra com o mesmo fornecedor. Informe 1 para Sim e 2 para N�o",;	//ESPANHOL
							"Atualiza os campos de Pre�o Unit�rio e TES com base na �ltima compra com o mesmo fornecedor. Informe 1 para Sim e 2 para N�o")		//INGLES

NewMv("MV_DIMLBL","","C",	"Define o nome das dimens�es. Ex: COMPRIMENTO=Comprimento;ALTURA=Altura;PROFUNDIDADE=Profundidade",;	// PORTUGUES
							"Define o nome das dimens�es. Ex: COMPRIMENTO=Comprimento;ALTURA=Altura;PROFUNDIDADE=Profundidade",;	//ESPANHOL
							"Define o nome das dimens�es. Ex: COMPRIMENTO=Comprimento;ALTURA=Altura;PROFUNDIDADE=Profundidade")	//INGLES
							
NewMv("MV_DIMMULT","","C",	"Define o m�ltiplo m�nimo de cada medida (0 para sem tratamento). Ex: COMPRIMENTO=1;ALTURA=1;PROFUNDIDADE=0",;	// PORTUGUES
							"Define o m�ltiplo m�nimo de cada medida (0 para sem tratamento). Ex: COMPRIMENTO=1;ALTURA=1;PROFUNDIDADE=0",;	//ESPANHOL
							"Define o m�ltiplo m�nimo de cada medida (0 para sem tratamento). Ex: COMPRIMENTO=1;ALTURA=1;PROFUNDIDADE=0")	//INGLES
							
NewMv("MV_OSORC","1","C",	"Define se OS sempre exigem sub-itens para gera��o do or�amento 1-Sim, 2-N�o",;	// PORTUGUES
							"Define se OS sempre exigem sub-itens para gera��o do or�amento 1-Sim, 2-N�o",;	//ESPANHOL
							"Define se OS sempre exigem sub-itens para gera��o do or�amento 1-Sim, 2-N�o")	//INGLES
							
NewMv("MV_FRETDEV","2","C",	"Define se o frete ser� condiderado na devolu��o de nota de sa�da 1-N�o, 2-Sim",;	// PORTUGUES
							"Define se o frete ser� condiderado na devolu��o de nota de sa�da 1-N�o, 2-Sim",;	//ESPANHOL
							"Define se o frete ser� condiderado na devolu��o de nota de sa�da 1-N�o, 2-Sim")	//INGLES
							
NewMv("MV_DTCOM","20301231","D", "Define a data para mudan�a do calculo de comiss�o sem imposto.",;	// PORTUGUES
								 "Define a data para mudan�a do calculo de comiss�o sem imposto.",;	//ESPANHOL
								 "Define a data para mudan�a do calculo de comiss�o sem imposto.")	//INGLES
NewMv("MV_UMOVMAN",".F.","L",	"Define se a exporta��o para mobilidade � s�ncrona (.t.) ou ass�ncrona (.f.)",;	// PORTUGUES
								"Define se a exporta��o para mobilidade � s�ncrona (.t.) ou ass�ncrona (.f.)",;	//ESPANHOL
								"Define se a exporta��o para mobilidade � s�ncrona (.t.) ou ass�ncrona (.f.)")	//INGLES

NewMv("MV_UMOVMAI",".F.","L",	"Define se a importa��o para mobilidade � s�ncrona (.t.) ou ass�ncrona (.f.)",;	// PORTUGUES
								"Define se a importa��o para mobilidade � s�ncrona (.t.) ou ass�ncrona (.f.)",;	//ESPANHOL
								"Define se a importa��o para mobilidade � s�ncrona (.t.) ou ass�ncrona (.f.)")	//INGLES

NewMv("MV_URLSAAS","http://serie1.totvs.com.br:88","C",	"URL dos servi�os de SaaS",;	// PORTUGUES
								"URL dos servi�os de SaaS",;	//ESPANHOL
								"URL dos servi�os de SaaS")	//INGLES

NewMv("MV_LOCETQ","","C",	"Define local onde as etiquetas impressas ser�o salvas.",;	// PORTUGUES
							"Define local onde as etiquetas impressas ser�o salvas.",;	//ESPANHOL
							"Define local onde as etiquetas impressas ser�o salvas.")	//INGLES

NewMv("MV_PRCENT", "","C",	"Define tabela de pre�o padr�o para entrada.",;	// PORTUGUES
							"Define tabela de pre�o padr�o para entrada.",;	//ESPANHOL
							"Define tabela de pre�o padr�o para entrada.")	//INGLES

NewMv("MV_ARQBALA","","C",	"Define local e arquivo para integra��o da balan�a.",;	// PORTUGUES
							"Define local e arquivo para integra��o da balan�a.",;	//ESPANHOL
							"Define local e arquivo para integra��o da balan�a.")	//INGLES
							
NewMv("MV_ALTTOT","2","C",	"Indica se � permitida a altera��o do valor total no Pedido de Compra e Documento de Entrada. 1-Sim, 2-N�o",;	// PORTUGUES
							"Indica se � permitida a altera��o do valor total no Pedido de Compra e Documento de Entrada. 1-Sim, 2-N�o",;	//ESPANHOL
							"Indica se � permitida a altera��o do valor total no Pedido de Compra e Documento de Entrada. 1-Sim, 2-N�o")	//INGLES						

NewMv("MV_ALTVENC","1","C",	"Indica se � permitida a altera��o do vencimento no contas a receber. 1-Sim, 2-N�o",;	// PORTUGUES
							"Indica se � permitida a altera��o do vencimento no contas a receber. 1-Sim, 2-N�o",;	//ESPANHOL
							"Indica se � permitida a altera��o do vencimento no contas a receber. 1-Sim, 2-N�o")	//INGLES						

NewMv("MV_CFOPBEF","901","C",	"Indica quais CFOP s�o de beneficiamento. Informar apenas os 3 �ltimos d�gitos de cada uma",;	// PORTUGUES
							"Indica quais CFOP s�o de beneficiamento. Informar apenas os 3 �ltimos d�gitos de cada uma",;	//ESPANHOL
							"Indica quais CFOP s�o de beneficiamento. Informar apenas os 3 �ltimos d�gitos de cada uma")	//INGLES
							
NewMv("MV_VENDCOM","1","C",	"Indica se o vendedor sempre ir� ter comiss�o  1-Sim, 2-N�o",;	// PORTUGUES
							"Indica se o vendedor sempre ir� ter comiss�o  1-Sim, 2-N�o",;	//ESPANHOL
							"Indica se o vendedor sempre ir� ter comiss�o  1-Sim, 2-N�o")	//INGLES

NewMv("MV_MENPADE","SO ACEITAREMOS A MERCADORIA SE NA SUA NOTA FISCAL CONSTAR O NUMERO DO NOSSO PEDIDO DE COMPRAS.", "C",	"Mensagem padr�o ao incluir pedido de compra",;	// PORTUGUES
							"Mensagem padr�o ao incluir pedido de compra",;	//ESPANHOL
							"Mensagem padr�o ao incluir pedido de compra")	//INGLES

NewMv("MV_UKCBAR","S", "C", "Define se o c�digo de barras dever� ser �nico no produto",;	// PORTUGUES
							"Define se o c�digo de barras dever� ser �nico no produto",;	//ESPANHOL
							"Define se o c�digo de barras dever� ser �nico no produto")	//INGLES

NewMv("MV_ALTDATA","0", "C", "N�mero de dias anteriores a data base permitidos para altera��o da data de emiss�o de pedidos. (N�mero negativo para deixar ilimitado).",;	// PORTUGUES
							"N�mero de dias anteriores a data base permitidos para altera��o da data de emiss�o de pedidos. (N�mero negativo para deixar ilimitado).",;	//ESPANHOL
							"N�mero de dias anteriores a data base permitidos para altera��o da data de emiss�o de pedidos. (N�mero negativo para deixar ilimitado).")	//INGLES

NewMv("MV_IPDVSER","PDV", "C", "Serie para gera��o de Notas Fiscais na integra��o do PDV",;	// PORTUGUES
							"N�mero de dias anteriores a data base permitidos para altera��o da data de emiss�o de pedidos. (N�mero negativo para deixar ilimitado).",;	//ESPANHOL
							"N�mero de dias anteriores a data base permitidos para altera��o da data de emiss�o de pedidos. (N�mero negativo para deixar ilimitado).")	//INGLES
NewMv("MV_HRVERAO","1", "C", "Indica se haver� ajuste autom�tico de hor�rio de ver�o. 1-Sim, 2-N�o",;	// PORTUGUES
							"Indica se haver� ajuste autom�tico de hor�rio de ver�o. 1-Sim, 2-N�o",;	//ESPANHOL
							"Indica se haver� ajuste autom�tico de hor�rio de ver�o. 1-Sim, 2-N�o")	//INGLES

NewMv("MV_JSONPED","", "C", "Define layout para impress�o de pedido em HTML",;	// PORTUGUES
							"Define layout para impress�o de pedido em HTML",;	//ESPANHOL
							"Define layout para impress�o de pedido em HTML")	//INGLES

NewMv("MV_QRNFSE","", "C",  "Define quebra para descri��o do servi�o.",;	// PORTUGUES
							    "Define quebra para descri��o do servi�o.",;	//ESPANHOL
							    "Define quebra para descri��o do servi�o.")	//INGLES

NewMv("MV_IMPCI",".F.", "L",  "Imprimir conteudo de importa��o no DANFE junto ao FCI.",;	// PORTUGUES
							    "Imprimir conteudo de importa��o no DANFE junto ao FCI.",;	//ESPANHOL
							    "Imprimir conteudo de importa��o no DANFE junto ao FCI.")	//INGLES
							    
NewMv("MV_PRUNTNF","S", "C",	"Realiza c�lculo reverso do pr. unit. NFE",;	// PORTUGUES
							    	"Realiza c�lculo reverso do pr. unit. NFE",;	//ESPANHOL
							    	"Realiza c�lculo reverso do pr. unit. NFE")	//INGLES							    

NewMv("MV_ORDEMFT","", "C",		"Ordena produtos conforme PV no Faturamento",;	// PORTUGUES
							    	"Ordena produtos conforme PV no Faturamento",;	//ESPANHOL
							    	"Ordena produtos conforme PV no Faturamento")		//INGLES

IF !GetMv("MV_LOCAL",.T.)
	NewMv("MV_LOCAL  ", mvlocal(),"C",	"Determina armaz�m padr�o.",;	// PORTUGUES
										"Determina armaz�m padr�o.",; // ESPANHOL
										"Determina armaz�m padr�o." )	// INGLES
EndIf
							    					
NewMv("MV_CSVENC","30;10", "C",  "Dias de fechamento e pagamento de t�tulos do CIASHOP respectivamente",;	// PORTUGUES
							    "Dias de fechamento e pagamento de t�tulos do CIASHOP respectivamente",;	//ESPANHOL
							    "Dias de fechamento e pagamento de t�tulos do CIASHOP respectivamente")	//INGLES							

NewMv("MV_INCEFIS","2","C",	"Indicador de incentivo Fiscal. 1= Sim; 2=N�o",;	// PORTUGUES
								"Indicador de incentivo Fiscal. 1= Sim; 2=N�o",;	// ESPANHOL
								"Indicador de incentivo Fiscal. 1= Sim; 2=N�o" )	// INGLES

NewMv("MV_UTC","","C",     "Indicador de Fuso Horario. 2=UTC2 3=UTC3 4=UTC4 5=UTC5 ",;	// PORTUGUES
								"Indicador de Fuso Horario. 2=UTC2 3=UTC3 4=UTC4 5=UTC5 ",;	// PORTUGUES
								"Indicador de Fuso Horario. 2=UTC2 3=UTC3 4=UTC4 5=UTC5 " )	// PORTUGUES

NewMv("MV_REDEDIR","", "C",	"Diret�rio dos arquivos de extrato eletr�nico Rede",;	// PORTUGUES
							    	"Diret�rio dos arquivos de extrato eletr�nico Rede",;	//ESPANHOL
							    	"Diret�rio dos arquivos de extrato eletr�nico Rede")	//INGLES

NewMv("MV_REDEAI",".T.", "L",	"Identifica se a importa��o ser� autom�tica",;	// PORTUGUES
							    	"Identifica se a importa��o ser� autom�tica",;	//ESPANHOL
							    	"Identifica se a importa��o ser� autom�tica")	//INGLES

NewMv("MV_REDECAT","", "C",	"Categoria financeira para Extrato Eletronico Rede",;	// PORTUGUES
							    "Categoria financeira para Extrato Eletronico Rede",;	//ESPANHOL
							    "Categoria financeira para Extrato Eletronico Rede")	//INGLES

NewMv("MV_PDVCATO","","C","Categoria Origem da Transfer�ncia Banc�ria da Integra��o PDV.",;	// PORTUGUES
										"Categor�a Origen de la transferencia banc�ria de la integraci�n POS",; // ESPANHOL
										"Category Source from POS Integration Transfer Bank" )	// INGLES

NewMv("MV_PDVCATD","","C","Categoria Destino da Transfer�ncia Banc�ria da Integra��o PDV.",;	// PORTUGUES
										"Categor�a Origen de la transferencia banc�ria de la integraci�n POS",; // ESPANHOL
										"Category Destination from POS Integration Transfer Bank" )	// INGLES
										
NewMv("MV_JSONRPS","", "C", "Define layout para impress�o de RPS em HTML",;	// PORTUGUES
								"Define layout para impress�o de RPS em HTML",;	//ESPANHOL
								"Define layout para impress�o de RPS em HTML")	//INGLES										

NewMv("MV_CONSUMO", "2", "C", "Regra para definir item de consumo: 1 - campo Mat. Consumo, 2 - Pelo CFOP (6107, 6108 ou 6929)",;	// PORTUGUES
								"Regra para definir item de consumo: 1 - campo Mat. Consumo, 2 - Pelo CFOP (6107, 6108 ou 6929)",;	//ESPANHOL
								"Regra para definir item de consumo: 1 - campo Mat. Consumo, 2 - Pelo CFOP (6107, 6108 ou 6929)")	//INGLES

NewMv("MV_DATALOT","2","C",	"Informe se lotes zerados devem ou n�o aparecer como op��o no PV. Apenas para TES que n�o calcula estoque. 1=SIM; 2=N�O",;	// PORTUGUES
							"Informe se lotes zerados devem ou n�o aparecer como op��o no PV. Apenas para TES que n�o calcula estoque. 1=SIM; 2=N�O",;		//ESPANHOL
							"Informe se lotes zerados devem ou n�o aparecer como op��o no PV. Apenas para TES que n�o calcula estoque. 1=SIM; 2=N�O")		//INGLES

NewMv("MV_DEVOLZF",.F.,"L","Indica se calcula o desconto Suframa em documentos de Devolu��o. (.T.-Calcula,.F.-N�o Calcula)",;	// PORTUGUES
							"Indica se calcula o desconto Suframa em documentos de Devolu��o. (.T.-Calcula,.F.-N�o Calcula)",;
							"Indica se calcula o desconto Suframa em documentos de Devolu��o. (.T.-Calcula,.F.-N�o Calcula)")	

NewMv("MV_TABITEM",.F.,"L","Indica se a op��o de Tabela de Pre�o por itens est� ativa. (.T.-Ativa,.F.-N�o ativa)",;	// PORTUGUES
							"Indica se a op��o de Tabela de Pre�o por itens est� ativa. (.T.-Ativa,.F.-N�o ativa)",;
							"Indica se a op��o de Tabela de Pre�o por itens est� ativa. (.T.-Ativa,.F.-N�o ativa)")	
							
NewMv("MV_LAYNFSE","","C","Informe o layout da NFSe. 001: DSFNET, 002: ABRASF - WebIss, 003: Egoverne, 004: ISSWEB, 101: ABRASF - Publica, 102: XML",;	// PORTUGUES
							"Informe o layout da NFSe. 001: DSFNET, 002: ABRASF - WebIss, 003: Egoverne, 004: ISSWEB, 101: ABRASF - Publica, 102: XML",; //ESPANHOL
							"Informe o layout da NFSe. 001: DSFNET, 002: ABRASF - WebIss, 003: Egoverne, 004: ISSWEB, 101: ABRASF - Publica, 102: XML")  //INGLES

NewMv("MV_VL13137",10,"N","Define o valor m�nimo para reten��o do Pis/Cofins/Csll de acordo com a Lei 13.137",;	// PORTUGUES
							"Define o valor m�nimo para reten��o do Pis/Cofins/Csll de acordo com a Lei 13.137",; //ESPANHOL
							"Define o valor m�nimo para reten��o do Pis/Cofins/Csll de acordo com a Lei 13.137")  //INGLES
							
NewMv("MV_ALIQRED",.F.,"L","Indica se os campos de al�quota de desconto CSLL e IR est�o ativos no cadastro de produto. (.T.-Ativo,.F.-N�o ativo)",;	// PORTUGUES
							"Indica se os campos de al�quota de desconto CSLL e IR est�o ativos no cadastro de produto. (.T.-Ativo,.F.-N�o ativo)",;
							"Indica se os campos de al�quota de desconto CSLL e IR est�o ativos no cadastro de produto. (.T.-Ativo,.F.-N�o ativo)")

NewMv("MV_RATDESP","FR=1;DESP=1","C","Indica se nas NFs de Sa�da o rateio do frete/despesas ser� efetuado por: 1 - Valor; 2 - Peso.",;	// PORTUGUES
							"Indica se nas NFs de Sa�da o rateio do frete/despesas ser� efetuado por: 1 - Valor; 2 - Peso.",;
							"Indica se nas NFs de Sa�da o rateio do frete/despesas ser� efetuado por: 1 - Valor; 2 - Peso.")

NewMv("MV_WFCSTM",.F.,"L","Define se WorkFlow de cota��o utiliza layout customizado",;	// PORTUGUES
							"Define se WorkFlow de cota��o utiliza layout customizado",; //ESPANHOL
							"Define se WorkFlow de cota��o utiliza layout customizado")  //INGLES

NewMv("MV_DANDESC",.F.,"L","Indica se os valores de desconto do produto devem ou n�o ser impressos no DANFE. (.T.-Ativo,.F.-N�o ativo)",;	// PORTUGUES
							"Indica se os valores de desconto do produto devem ou n�o ser impressos no DANFE. (.T.-Ativo,.F.-N�o ativo)",;
							"Indica se os valores de desconto do produto devem ou n�o ser impressos no DANFE. (.T.-Ativo,.F.-N�o ativo)")

NewMv("MV_TRNSMLT",.F.,"L","Utilizar transferencia m�ltipla de estoque",;	// PORTUGUES
							"Utilizar transferencia m�ltipla de estoque",; //ESPANHOL
							"Utilizar transferencia m�ltipla de estoque")  //INGLES

NewMv("MV_TRNDOC","000001","C","N�mero sequencial do documento para transfer�ncia m�ltipla de estoque",;	// PORTUGUES
							"N�mero sequencial do documento para transfer�ncia m�ltipla de estoque",; //ESPANHOL
							"N�mero sequencial do documento para transfer�ncia m�ltipla de estoque")  //INGLES
							
NewMv("MV_MENRET","Valor recebido s/ titulo","C","Mensagem padr�o que ser� gravada no campo Hist�rio, nas movimenta��es Banc�rias, ao realizar Retorno CNAB",;	// PORTUGUES
							"Mensagem padr�o que ser� gravada no campo Hist�rio, nas movimenta��es Banc�rias, ao realizar Retorno CNAB",; //ESPANHOL
							"Mensagem padr�o que ser� gravada no campo Hist�rio, nas movimenta��es Banc�rias, ao realizar Retorno CNAB")  //INGLES

NewMv("MV_CALCDOU","S","C","Define se o calculo do PIS/COFINS no custo do produto ser� de acordo com o DOU",;	// PORTUGUES
							"Define se o calculo do PIS/COFINS no custo do produto ser� de acordo com o DOU",; //ESPANHOL
							"Define se o calculo do PIS/COFINS no custo do produto ser� de acordo com o DOU")  //INGLES

NewMv("MV_ARRITNF",.F.,"L","Define a corre��o dos arredondamentos do item da NF",;	// PORTUGUES
							"Define a corre��o dos arredondamentos do item da NF",; //ESPANHOL
							"Define a corre��o dos arredondamentos do item da NF")  //INGLES

NewMv("MV_VIDEOS",.T.,"L","Define se os v�deos de treinamento devem ser ou n�o exibidos (.T. para SIM, .F. para N�O)",;	// PORTUGUES
							"Define se os v�deos de treinamento devem ser ou n�o exibidos (.T. para SIM, .F. para N�O)",; //ESPANHOL
							"Define se os v�deos de treinamento devem ser ou n�o exibidos (.T. para SIM, .F. para N�O)")  //INGLES

NewMv("MV_BOLPDF",.F.,"L","Define se os boletos ser�o gerados em PDF (.T. para SIM, .F. para N�O)",;	// PORTUGUES
							"Define se os boletos ser�o gerados em PDF  (.T. para SIM, .F. para N�O)",; //ESPANHOL
							"Define se os boletos ser�o gerados em PDF  (.T. para SIM, .F. para N�O)")  //INGLES
							
NewMv("MV_BOLLOC","","C","Define o local onde os boletos em PDF ser�o salvos.",;	// PORTUGUES
							"Define o local onde os boletos em PDF ser�o salvos.",; //ESPANHOL
							"Define o local onde os boletos em PDF ser�o salvos.")//INGLES

NewMv("MV_FTPNFSE",.F.,"L","Define se ir� utilizar o FTP para notas fiscais de servi�o no TSS.",;	// PORTUGUES
							"Define se ir� utilizar o FTP para notas fiscais de servi�o no TSS.",; //ESPANHOL
							"Define se ir� utilizar o FTP para notas fiscais de servi�o no TSS.")//INGLES
							
NewMv("MV_DESCSRV",.T.,"L","Define se, para NFSE, o desconto deve ser calculado em TAGs de Totais. (Base Calculo ISS, Valor total da nota, etc...)",;	// PORTUGUES
							"Define se, para NFSE, o desconto deve ser calculado em TAGs de Totais. (Base Calculo ISS, Valor total da nota, etc...)",; //ESPANHOL
							"Define se, para NFSE, o desconto deve ser calculado em TAGs de Totais. (Base Calculo ISS, Valor total da nota, etc...)")//INGLES

NewMv("MV_VALOP",.F.,"L","Define se o valor das movimenta��es geradas pela OP ser� o custo m�dio do produto.",;	// PORTUGUES
							"Define se o valor das movimenta��es geradas pela OP ser� o custo m�dio do produto.",; //ESPANHOL
							"Define se o valor das movimenta��es geradas pela OP ser� o custo m�dio do produto.")//INGLES

NewMv("MV_BXVEN",.T.,"L","Se .T. baixa as vendas envidas pelo PDV ao acessar as rotinas de PV e Documento de Sa�da. Se .F. baixa manual.",;	// PORTUGUES
							"Se .T. baixa as vendas envidas pelo PDV ao acessar as rotinas de PV e Documento de Sa�da. Se .F. baixa manual.",; //ESPANHOL
							"Se .T. baixa as vendas envidas pelo PDV ao acessar as rotinas de PV e Documento de Sa�da. Se .F. baixa manual.")//INGLES

NewMv("MV_LOCALTX","","C","Determina o valor para armazem na importa��o do arquivo txt",;	// PORTUGUES
							"Determina o valor para armazem na importa��o do arquivo txt",;	// ESPANHOL
							"Determina o valor para armazem na importa��o do arquivo txt") 	// INGLES

NewMv("MV_TESTXT","","C","Determina o valor para TES de Entrada na importa��o do arquivo txt",;	// PORTUGUES
							"Determina o valor para TES de Entrada na importa��o do arquivo txt",;	// ESPANHOL
							"Determina o valor para TES de Entrada na importa��o do arquivo txt") 	// INGLES
							  
NewMv("MV_CRGTRIB",.F.,"L","Define se dever� ser apresentada a mensagem da Carga Tribut�ria - Lei da Transpar�ncia para NFSe.",; // PORTUGUES
							   "Define se dever� ser apresentada a mensagem da Carga Tribut�ria - Lei da Transpar�cia para NFSe.",; // ESPANHOL
							   "Define se dever� ser apresentada a mensagem da Carga Tribut�ria - Lei da Transpar�cia para NFSe.") // INGLES
							   
NewMv("MV_NFSEMOD",.F.,"L","Informar se ser� utilizado o novo modelo �nico de XML a ser enviado ao TSS para NFS-e. .T. = Sim; .F. = N�o",; // PORTUGUES
							   "Informar se ser� utilizado o novo modelo �nico de XML a ser enviado ao TSS para NFS-e. .T. = Sim; .F. = N�o",; // ESPANHOL
							   "Informar se ser� utilizado o novo modelo �nico de XML a ser enviado ao TSS para NFS-e. .T. = Sim; .F. = N�o") // INGLES
							   
NewMv("MV_DESCIST",.F.,"L","Informar se o desconto SUFRAMA deve ser aplicado antes do MVA no c�lculo da base do ICMS-ST. .T. = Sim; .F. = N�o",; // PORTUGUES
							   "Informar se o desconto SUFRAMA deve ser aplicado antes do MVA no c�lculo da base do ICMS-ST. .T. = Sim; .F. = N�o",; // ESPANHOL
							   "Informar se o desconto SUFRAMA deve ser aplicado antes do MVA no c�lculo da base do ICMS-ST. .T. = Sim; .F. = N�o") // INGLES
							   
NewMv("MV_CONSORC",.F.,"L","Define se deve considerar o valor do or�amento no saldo antes de liber�-lo. .T. = Sim; .F. = N�o",; // PORTUGUES
							   "Define se deve considerar o valor do or�amento no saldo antes de liber�-lo. .T. = Sim; .F. = N�o",; // ESPANHOL
							   "Define se deve considerar o valor do or�amento no saldo antes de liber�-lo. .T. = Sim; .F. = N�o") // INGLES								   							   
							   
NewMv("MV_URLMSHP","http://mashups-proxy.cloudtotvs.com.br:8055/TOTVSSoa.Host/SOAManager.svc","C",	"URL dos servi�os de MASHUP",;	// PORTUGUES
								"URL dos servi�os de MASHUP",;	//ESPANHOL
								"URL dos servi�os de MASHUP")	//INGLES

NewMv("MV_RATOBR","PedCom=2;DocEnt=2;MovInt=2;PedVen=2;DocSai=2;ConRec=2;ConPag=2;MovBan=2;TrnPrg=2","C","Define em quais rotinas o rateio � obrigatorio. 1 = Sim, 2 = N�o",; // PORTUGUES
							   "Define em quais rotinas o rateio � obrigatorio. 1 = Sim, 2 = N�o",; // ESPANHOL
							   "Define em quais rotinas o rateio � obrigatorio. 1 = Sim, 2 = N�o") // INGLES								   							   

NewMv("MV_VENCREA",.F.,"L","Utilizar na transmiss�o o campo VENCIMENTO REAL ao invez do campo VENCIMENTO. .T. = Sim; .F. = N�o",; // PORTUGUES
							   "Utilizar na transmiss�o o campo VENCIMENTO REAL ao invez do campo VENCIMENTO. .T. = Sim; .F. = N�o",; // ESPANHOL
							   "Utilizar na transmiss�o o campo VENCIMENTO REAL ao invez do campo VENCIMENTO. .T. = Sim; .F. = N�o") // INGLES

IF !GetMv("MV_FORMAPG",.T.)
	NewMv("MV_FORMAPG", SmFormaPG(),"N", "Se forma de pagamento � obrigat�ria em documentos, pedidos e t�tulos (1 - Sim, 2 - N�o)",; // PORTUGUES
							"Se forma de pagamento � obrigat�ria em documentos, pedidos e t�tulos (1 - Sim, 2 - N�o)",; // ESPANHOL
							"Se forma de pagamento � obrigat�ria em documentos, pedidos e t�tulos (1 - Sim, 2 - N�o)") // INGLES
EndIf

NewMv("MV_QTDLOTE",.F.,"L","Indica se no Ajuste de Empenho o campo D4_LOTE deve ou n�o exibir lotes sem saldo. .T. = Sim; .F. = N�o",; // PORTUGUES
							   "Indica se no Ajuste de Empenho o campo D4_LOTE deve ou n�o exibir lotes sem saldo. .T. = Sim; .F. = N�o",; // ESPANHOL
							   "Indica se no Ajuste de Empenho o campo D4_LOTE deve ou n�o exibir lotes sem saldo. .T. = Sim; .F. = N�o") // INGLES

NewMv("MV_CUSTADD",.F.,"L","Indica se o campo de custo adicional na fabrica��o deve ser utilizado na OP. .T. = Sim; .F. = N�o",; // PORTUGUES
							   "Indica se o campo de custo adicional na fabrica��o deve ser utilizado na OP. .T. = Sim; .F. = N�o",; // ESPANHOL
							   "Indica se o campo de custo adicional na fabrica��o deve ser utilizado na OP. .T. = Sim; .F. = N�o") // INGLES

NewMv("MV_COMBAIX",.F.,"L","Indica se a comiss�o ser� calculada com base no valor da baixa do t�tulo. .T. = Sim; .F. = N�o",; // PORTUGUES
							   "Indica se a comiss�o ser� calculada com base no valor da baixa do t�tulo. .T. = Sim; .F. = N�o",; // ESPANHOL
							   "Indica se a comiss�o ser� calculada com base no valor da baixa do t�tulo. .T. = Sim; .F. = N�o") // INGLES

NewMv("MV_ALTCSPV",.F.,"L","Indica se � permitido editar os Pedidos de Venda importados do e-commerce. .T. = Sim; .F. = N�o",; // PORTUGUES
							   "Indica se � permitido editar os Pedidos de Venda importados do e-commerce. .T. = Sim; .F. = N�o",; // ESPANHOL
							   "Indica se � permitido editar os Pedidos de Venda importados do e-commerce. .T. = Sim; .F. = N�o") // INGLES

NewMv("MV_ISSMV","3543402-3518800-3106705-3548807-3549904","C",;
					"Define quais munic�pios devem utilizar o par�metro MV_ALIQISS na tag de Al�quota do ISS, para NFSE, quando a nota n�o possuir um valor de Base de ISS",; // PORTUGUES
					"Define quais munic�pios devem utilizar o par�metro MV_ALIQISS na tag de Al�quota do ISS, para NFSE, quando a nota n�o possuir um valor de Base de ISS",; // ESPANHOL
					"Define quais munic�pios devem utilizar o par�metro MV_ALIQISS na tag de Al�quota do ISS, para NFSE, quando a nota n�o possuir um valor de Base de ISS") // INGLES
					
NewMv("MV_DESCDAN",.F.,"L","Indica se as informa��es do LOTE do produto devem ser impressas na mesma linha que a descri��o do produto. .T. = Sim; .F. = N�o",; // PORTUGUES
							   "Indica se as informa��es do LOTE do produto devem ser impressas na mesma linha que a descri��o do produto. .T. = Sim; .F. = N�o",; // ESPANHOL
							   "Indica se as informa��es do LOTE do produto devem ser impressas na mesma linha que a descri��o do produto. .T. = Sim; .F. = N�o") // INGLES								   							   

NewMv("MV_1M040PR","AT","C",;
					"Define o Prefixo dos T�tulos gerados pela Rotina de Transa��es Programadas (Fixo em 2 caracteres)",; // PORTUGUES
					"Establece el prefijo de Duplicatas generadas por la rutina de Transacci�n Prevista (Fijado en 2 caracteres)",; // ESPANHOL
					"Sets the prefix for Financial Bonds generated by Scheduled Transactions routine (Set at 2 characters)") // INGLES

NewMv("MV_LOCCNAB","","C",	"Define o nome da subpasta do armazenamento dos arquivos .CNAB",;	// PORTUGUES
								"Define o nome da subpasta do armazenamento dos arquivos .CNAB",; // ESPANHOL
								"Define o nome da subpasta do armazenamento dos arquivos .CNAB" )	// INGLES
								
NewMv("MV_FORMXML",.F.,"L","Indica se, ao importar XML, a NFe ser� transmitida ou n�o. .T. = Sim; .F. = N�o",; // PORTUGUES
							   "Indica se, ao importar XML, a NFe ser� transmitida ou n�o. .T. = Sim; .F. = N�o",; // ESPANHOL
							   "Indica se, ao importar XML, a NFe ser� transmitida ou n�o. .T. = Sim; .F. = N�o") // INGLES

NewMv("MV_EXEAUT",5,"N","Define, em minutos, o tempo que a rotina de Integra��o Autom�tica deve aguardar para realizar uma nova integra��o com o PDV",;	// PORTUGUES
							"Define, em minutos, o tempo que a rotina de Integra��o Autom�tica deve aguardar para realizar uma nova integra��o com o PDV",; //ESPANHOL
							"Define, em minutos, o tempo que a rotina de Integra��o Autom�tica deve aguardar para realizar uma nova integra��o com o PDV")  //INGLES

NewMv("MV_IESTICM",.F.,"L","Indica se a TAG <IEST> deve ser apresentada no XML sem considerar se a NF c�lcula ICMS. .T. = Sim; .F. = N�o",; // PORTUGUES
							   "Indica se a TAG <IEST> deve ser apresentada no XML sem considerar se a NF c�lcula ICMS. .T. = Sim; .F. = N�o",; // ESPANHOL
							   "Indica se a TAG <IEST> deve ser apresentada no XML sem considerar se a NF c�lcula ICMS. .T. = Sim; .F. = N�o") // INGLES

NewMv("MV_CFOPREF","1117|","C",	"Define quais CFOP geram a tag <refNFe> na rotina de NFe. Exemplo: 1117|1118|1119",;	// PORTUGUES
								"Define quais CFOP geram a tag <refNFe> na rotina de NFe. Exemplo: 1117|1118|1119",; // ESPANHOL
								"Define quais CFOP geram a tag <refNFe> na rotina de NFe. Exemplo: 1117|1118|1119" )	// INGLES

NewMv("MV_SINTVIT","","C",	"Define o caminho onde est�o salvos os arquivos do Sintegra gerados no sistema Vitrine. Exemplo: C:\SINTEGRA",;	// PORTUGUES
								"Define o caminho onde est�o salvos os arquivos do Sintegra gerados no sistema Vitrine. Exemplo: C:\SINTEGRA",; // ESPANHOL
								"Define o caminho onde est�o salvos os arquivos do Sintegra gerados no sistema Vitrine. Exemplo: C:\SINTEGRA" )	// INGLES

NewMv("MV_PDVTENT",5,"N","Define o n�mero de tentativas de exporta��o de dados para o PDV",;	// PORTUGUES
							"Define o n�mero de tentativas de exporta��o de dados para o PDV",; //ESPANHOL
							"Define o n�mero de tentativas de exporta��o de dados para o PDV")  //INGLES

NewMv("MV_TRPROG", IIF(oLic:nProdId == 18, 549, 0), "N", "Anteced�ncia padr�o para lan�amento de transa��es programadas",;	// PORTUGUES
								"Anteced�ncia padr�o para lan�amento de transa��es programadas",; // ESPANHOL
								"Anteced�ncia padr�o para lan�amento de transa��es programadas" )	// INGLES
								
NewMv("MV_PCODISS", 0, "N", "Indica se o c�digo do ISS ser� potuado automaticamente ou n�o. (0 = Padr�o, 1 = Sempre pontuar, 2 = Nunca pontuar)",;	// PORTUGUES
								"Indica se o c�digo do ISS ser� potuado automaticamente ou n�o. (0 = Padr�o, 1 = Sempre pontuar, 2 = Nunca pontuar)",; // ESPANHOL
								"Indica se o c�digo do ISS ser� potuado automaticamente ou n�o. (0 = Padr�o, 1 = Sempre pontuar, 2 = Nunca pontuar)" )	// INGLES

NewMv("MV_DESCPRD", .F., "L", "Determina se utiliza limite de desconto pelo cadastro do produto. .T. = Sim; .F. = N�o ",;	// PORTUGUES
								"Determina se utiliza limite de desconto pelo cadastro do produto. .T. = Sim; .F. = N�o ",; // ESPANHOL
								"Determina se utiliza limite de desconto pelo cadastro do produto. .T. = Sim; .F. = N�o " )	// INGLES

NewMv("MV_SLDINVT", .F., "L", "Determina se, ao fazer o Fechamento, o campo 'Saldo em quantidade no fim do m�s' recebe o valor do �ltimo invent�rio processado. .T. = Sim; .F. = N�o ",;	// PORTUGUES
								"Determina se, ao fazer o Fechamento, o campo 'Saldo em quantidade no fim do m�s' recebe o valor do �ltimo invent�rio processado. .T. = Sim; .F. = N�o ",; // ESPANHOL
								"Determina se, ao fazer o Fechamento, o campo 'Saldo em quantidade no fim do m�s' recebe o valor do �ltimo invent�rio processado. .T. = Sim; .F. = N�o " )	// INGLES

NewMv("MV_NECESSI", .F., "L", "Determina se, ao gerar a OP, a quantidade a ser considerada ser� sempre a necessidade. .T. = Sim; .F. = N�o ",;	// PORTUGUES
								"Determina se, ao gerar a OP, a quantidade a ser considerada ser� sempre a necessidade. .T. = Sim; .F. = N�o ",; // ESPANHOL
								"Determina se, ao gerar a OP, a quantidade a ser considerada ser� sempre a necessidade. .T. = Sim; .F. = N�o " )	// INGLES

NewMv("MV_NIMP2UM", .F., "L", "Define se sera impresso a 2a. Unidade de Medida para operacoes dentro do Pa�s - Opcoes: [.T.]-N�o Imprime ou [.F.]-Imprime (Default)",;	// PORTUGUES
								"Define se sera impresso a 2a. Unidade de Medida para operacoes dentro do Pa�s - Opcoes: [.T.]-N�o Imprime ou [.F.]-Imprime (Default)",; // ESPANHOL
								"Define se sera impresso a 2a. Unidade de Medida para operacoes dentro do Pa�s - Opcoes: [.T.]-N�o Imprime ou [.F.]-Imprime (Default)" )	// INGLES

NewMv("MV_CFOPEXP", "1501-2501-5501-5502-5504-5505-6501-6502-6504-6505", "C",;
					"Rela��o de CFOP's vinculadas a exporta��o - NT 2016.001",;	// PORTUGUES
					"Rela��o de CFOP's vinculadas a exporta��o - NT 2016.001",; // ESPANHOL
					"Rela��o de CFOP's vinculadas a exporta��o - NT 2016.001" )	// INGLES
					
NewMv("MV_OBSNFSE", .F., "L", "Define se a tag <observacao> ser� preenchida com as mensagens da NFSe (v�lido apenas para Modelo �nico e MV_NFESERV = 5) .T. = SIM ; .F. = N�O",;	// PORTUGUES
								"Define se a tag <observacao> ser� preenchida com as mensagens da NFSe (v�lido apenas para Modelo �nico e MV_NFESERV = 5) .T. = SIM ; .F. = N�O",; // ESPANHOL
								"Define se a tag <observacao> ser� preenchida com as mensagens da NFSe (v�lido apenas para Modelo �nico e MV_NFESERV = 5) .T. = SIM ; .F. = N�O" )	// INGLES

NewMv("MV_TAMDESC", 120, "N", "Define o tamanho m�ximo do valor da tag <DiscriminacaoServico> em NF de Servi�o",;	// PORTUGUES
								"Define o tamanho m�ximo do valor da tag <DiscriminacaoServico> em NF de Servi�o",; // ESPANHOL
								"Define o tamanho m�ximo do valor da tag <DiscriminacaoServico> em NF de Servi�o" )	// INGLES

NewMv("MV_ISSNFE", .F., "L", "Define se a tag <ISSQN> ser� preenchida no XML da NFe, mesmo se a al�quota do ISS for zero. .T. = SIM ; .F. = N�O",;	// PORTUGUES
								"Define se a tag <ISSQN> ser� preenchida no XML da NFe, mesmo se a al�quota do ISS for zero. .T. = SIM ; .F. = N�O",; // ESPANHOL
								"Define se a tag <ISSQN> ser� preenchida no XML da NFe, mesmo se a al�quota do ISS for zero. .T. = SIM ; .F. = N�O" )	// INGLES

NewMv("MV_PISNFE", .F., "L", "Define se a tag <PIS> ser� preenchida no XML da NFe, mesmo se a al�quota do PIS for zero. .T. = SIM ; .F. = N�O",;	// PORTUGUES
								"Define se a tag <PIS> ser� preenchida no XML da NFe, mesmo se a al�quota do PIS for zero. .T. = SIM ; .F. = N�O",; // ESPANHOL
								"Define se a tag <PIS> ser� preenchida no XML da NFe, mesmo se a al�quota do PIS for zero. .T. = SIM ; .F. = N�O" )	// INGLES

NewMv("MV_COFNFE", .F., "L", "Define se a tag <COFINS> ser� preenchida no XML da NFe, mesmo se a al�quota do COFINS for zero. .T. = SIM ; .F. = N�O",;	// PORTUGUES
								"Define se a tag <COFINS> ser� preenchida no XML da NFe, mesmo se a al�quota do COFINS for zero. .T. = SIM ; .F. = N�O",; // ESPANHOL
								"Define se a tag <COFINS> ser� preenchida no XML da NFe, mesmo se a al�quota do COFINS for zero. .T. = SIM ; .F. = N�O" )	// INGLES

NewMv("MV_AUTXML", "", "C", "Define o cpf ou cnpj do autorizado a obter download do xml na receita separe por ponto e virgula se houver mais de um",;	// PORTUGUES
								"Define o cpf ou cnpj do autorizado a obter download do xml na receita separe por ponto e virgula se houver mais de um",; // ESPANHOL
								"Define o cpf ou cnpj do autorizado a obter download do xml na receita separe por ponto e virgula se houver mais de um" )	// INGLES

NewMv("MV_DECVXML", 8, "N", "Define a quantidade de casas decimais para as tags vUnCom e vUnTrib",;	// PORTUGUES
							"Define la cantidad de decimales para tags vUnCom e VUnTrib",; // ESPANHOL
							"Defines the number of decimal places for tags vUnCom and vUnTrib" ) // INGLES


NewMv("MV_CGCPLAT","","C","CNPJ utilizado para cria��o da plataforma do Bemacash Gest�o",;	// PORTUGUES
							"CNPJ utilizado para cria��o da plataforma do Bemacash Gest�o",;	// ESPANHOL
							"CNPJ utilizado para cria��o da plataforma do Bemacash Gest�o" )	// INGLES


//Final dos MVs

Return( .T. )

Static Function mvlocal()
Local cRet       := ""
Local aAreas
Local nCount

If !SmExistVer( "20150521" )
	Processa({|| SmAuxChkVer({"SB1","SAG"},"20150521")},"Processando atualiza��es")
EndIf

aAreas := GetAreas({"SB1"})

DbSelectArea("SB1")
dbSetOrder(1)
If !DbSeek(xFilial("SB1"))
	dbSelectArea("SAG")
	nCount := XSELECT #COUNT(.T.) FROM (TAB SAG WHERE AG_FILIAL == ?xFilial("SAG")?)
	If nCount <= 1 //S� tem armaz�m CQ ou nenhum
		RecLock("SAG", .T.)
		AG_FILIAL := xFilial("SAG")
		AG_LOCAL := cRet := "01"
		AG_DESCRI := "PADRAO"
		AG_ATIVO := "1"
		MSUnlock()
	EndIf
EndIf

If Empty(cRet)
	cRet := XSELECT #(AG_LOCAL) FROM (TAB SAG ;
					WHERE AG_FILIAL == ?xFilial("SAG")? ;
						.And. AG_LOCAL != "CQ" ;
						.And. AG_ATIVO == "1" ;
					ORDER 1) ;
				TOP 1
Endif
Default cRet := ""


RestAreas( aAreas )

Return cRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
��� Programa  � CreateSX1  � Autor � Wanderley       � Data �  07/03/02   ���
�������������������������������������������������������������������������͹��
��� Descricao � Verifica as perguntas, inclu�ndo-as caso n�o existam	  ���
�������������������������������������������������������������������������͹��
��� Sintaxe   � CreateSX1( ExpA1 )										  ���
�������������������������������������������������������������������������͹��
��� Parametros� ExpA1 - Array com os parametros abaixo : 				  ���
���        01 � 	Nome da Pergunta			// X1_GRUPO	  			  ���
���        02 � 	Sequencia da Pergunta  		// X1_ORDEM				  ���
���        03 � 	Titulo Perg. Padrao			// X1_PERGUNT			  ���
���        04 � 	Titulo Perg. Espanhol 	 	// X1_PERSPA			  ���
���        05 � 	Titulo Perg. Ingles		 	// X1_PERENG			  ���
���        06 � 	Variavel					// X1_VARIAVL			  ���
���        07 � 	Tipo						// X1_TIPO  	 		  ���
���        08 � 	Tamanho						// X1_TAMANHO			  ���
���        09 � 	Decimais					// X1_DECIMAL			  ���
���        10 � 	Opcao Pre Selecionada       // X1_PRESEL 			  ���
���        11 � 	Tipo da Pergunta			// X1_GSC    			  ���
���        12 � 	Validacao					// X1_VALID  			  ���
���        13 � 	Nome Var  1	      	  	 	// X1_VAR01 			  ���
���        14 � 	Definicao 1	Padrao		 	// X1_DEF01  			  ���
���        15 � 	Definicao 1 Espanhol		// X1_DEFSPA1			  ���
���        16 � 	Definicao 1 Ingles 		 	// X1_DEFENG1			  ���
���        17 � 	Conteudo  1					// X1_CNT01				  ���
���        18 � 	Nome Var  2       	  	 	// X1_VAR02 			  ���
���        19 � 	Definicao 2	Padrao		 	// X1_DEF02  			  ���
���        20 � 	Definicao 2 Espanhol		// X1_DEFSPA2			  ���
���        21 � 	Definicao 2 Ingles 	 		// X1_DEFENG2			  ���
���        22 � 	Conteudo  2					// X1_CNT02				  ���
���        23 � 	Nome Var  3       		 	// X1_VAR03  			  ���
���        24 � 	Definicao 3	Padrao		 	// X1_DEF03  			  ���
���        25 � 	Definicao 3 Espanhol		// X1_DEFSPA3			  ���
���        26 � 	Definicao 3 Ingles 		 	// X1_DEFENG3			  ���
���        27 � 	Conteudo  3					// X1_CNT03				  ���
���        28 � 	Nome Var  4       		 	// X1_VAR04 			  ���
���        29 � 	Definicao 4	Padrao		 	// X1_DEF04  			  ���
���        30 � 	Definicao 4 Espanhol		// X1_DEFSPA4			  ���
���        31 � 	Definicao 4 Ingles 		 	// X1_DEFENG4			  ���
���        32 � 	Conteudo  4					// X1_CNT05				  ���
���        33 � 	Nome Var  5       		 	// X1_VAR05				  ��� 
���        34 � 	Definicao 5	Padrao		 	// X1_DEF05  			  ���
���        35 � 	Definicao 5 Espanhol		// X1_DEFSPA5			  ���
���        36 � 	Definicao 5 Ingles 		 	// X1_DEFENG5			  ���
���        37 � 	Conteudo  5					// X1_CNT05				  ���
���        38 � 	Consulta F3					// X1_F3				  ��� 
�������������������������������������������������������������������������͹��
��� Retorno   � NIL                                                       ���
�������������������������������������������������������������������������͹��
��� Uso       � SMALLERP                         						  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Function CreateSX1(aRegs)
                         
LOCAL nSX1Order:= SX1->(IndexOrd())
LOCAL nFCount	:= SX1->( Fcount() )
LOCAL nARegs 
LOCAL nI

Default aRegs := {}
                
nARegs := Len( aRegs )

SX1->(dbSetOrder(1))

FOR nI := 1 TO nARegs
	IF !SX1->(dbSeek(aRegs[nI][1]+aRegs[nI][2]))
		RecLock('SX1',.T.)
		Aeval( aRegs[nI], {|e,n| SX1->( FieldPut( n, e ) ) },, nFCount )
		MsUnlock()
	ENDIF
NEXT nI		
                                        
SX1->(dbSetOrder(nSX1Order))

RETURN NIL

Return .T.

//--------------------------------------
Function CreateSX5()
                         
LOCAL nARegs 
LOCAL nI
LOCAL	aRegs	 	:=	{ ;
         	      				{ "00" ,"05       " 	, "   " , "TIPOS DE TITULOS BR"	,"S", "N" },;
              	      			{ "00" ,"06       " 	, "   " , "TIPOS DE TITULOS" ,"S", "N" },;
								{ "00" ,"09       " 	, "   " , "LOTES CONTABILIDADE" ,"S", "N" },;
								{ "00" ,"10       " 	, "   " , "TABELAS VARI�VEIS" ,"S", "S" },;
								{ "00" ,"42       " 	, "   " , "ESP�CIES NOTA FISCAL" ,"S", "N"},;								
 								{ "00" ,"51       " 	, "   " , "SITUACAO TRIBUTARA SIMPLES NACIONAL","S", "N" },;
 								{ "00" ,"89       " 	, "   " , "CAMPOS PARA RETORNO CNAB","S", "N" },;		
 								{ "00" ,"90       " 	, "   " , "TIPO DE INSTRUCAO" ,"S", "S"},;		
 								{ "00" ,"91       " 	, "   " , "TIPO DE BAIXA","S", "S" },;		
 								{ "00" ,"92       " 	, "   " , "TIPO DE JUROS","S", "S" },;		
 								{ "00" ,"93       " 	, "   " , "TIPO DE DESCONTO","S", "S" },;		 								
 								{ "00" ,"94       " 	, "   " , "CARTEIRA","S", "S" },;		
 								{ "00" ,"95       " 	, "   " , "LOCAL DE IMPRESS�O DO BOLETO","S", "S" },;									
								{ "00" ,"96       " 	, "   " , "CODIGOS DE PROTESTO POR BANCO" ,"S", "S"},;								  
								{ "00" ,"97       " 	, "   " , "CODIGO DE SITUACAO TRIBUTARIA IPI" ,"S", "N"},;								  
								{ "00" ,"98       " 	, "   " , "LISTA DE SERVICOS LEI COMPLEMENTAR 116- 03" ,"S", "S"},;
								{ "00" ,"99       " 	, "   " , "CODIGO DE SITUACAO TRIBUTARIA PIS COFINS" ,"S", "N"},;
								{ "00" ,"DJ       " 	, "   " , "TIPOS DE OPERACAO" ,"S", "S"},;
              	      			{ "05" ,"1        " 	, "   " , "NF ENTRADA" ,"S", "N"},;
              	      			{ "05" ,"2        " 	, "   " , "NF SAIDA" ,"S", "N"},;
              	      			{ "05" ,"3        " 	, "   " , "DUPLICATA" ,"S", "N"},;
              	      			{ "05" ,"4        " 	, "   " , "CARNET" ,"S", "N"},;
              	      			{ "05" ,"5        " 	, "   " , "FATURA" ,"S", "N"},;
              	      			{ "05" ,"6        " 	, "   " , "NOTA PROMISSORIA" ,"S", "N"},;
              	      			{ "05" ,"7        " 	, "   " , "CHEQUE" ,"S", "N"},;
              	      			{ "05" ,"8        " 	, "   " , "DOCUMENTO" ,"S", "N"},;
              	      			{ "05" ,"9        " 	, "   " , "COMISSAO" ,"S", "N"},;
              	      			{ "05" ,"0        " 	, "   " , "IMPOSTOS" ,"S", "N"},;
              	      			{ "05" ,"A        " 	, "   " , "RA - RECEBIMENTO ANTECIPADO" ,"S", "N"},;
              	      			{ "05" ,"B        " 	, "   " , "PA - PAGAMENTO ANTECIPADO" ,"S", "N"},;
              	      			{ "05" ,"C        " 	, "   " , "AB- ABATIMENTO" ,"S", "N"},;
              	      			{ "05" ,"M        " 	, "   " , "NCC - NOTA DE CR�DITO CLIENTE" ,"S", "N"},;
              	      			{ "05" ,"N        " 	, "   " , "NDC - NOTA DE D�BITO CLIENTE" ,"S", "N"},;
              	      			{ "05" ,"O        " 	, "   " , "NCF - NOTA DE CR�DITO FORNECEDOR" ,"S", "N"},;
              	      			{ "05" ,"P        " 	, "   " , "NDF - NOTA DE D�BITO FORNECEDOR" ,"S", "N"},;
              	      			{ "06" ,"1        " 	, "   " , "NF ENTRADA" ,"S", "N"},;
              	      			{ "06" ,"2        " 	, "   " , "NF SAIDA" ,"S", "N"},;
              	      			{ "06" ,"3        " 	, "   " , "CHEQUE" ,"S", "N"},;
              	      			{ "06" ,"4        " 	, "   " , "LETRA" ,"S", "N"},;
              	      			{ "06" ,"5        " 	, "   " , "NCC - NOTA CR�DITO CLIENTE" ,"S", "N"},;
              	      			{ "06" ,"6        " 	, "   " , "NDC - NOTA D�BITO CLIENTE" ,"S", "N"},;
              	      			{ "06" ,"7        " 	, "   " , "NCF - NOTA CR�DITO FORNECEDOR" ,"S", "N"},;
           	      				{ "06" ,"8        " 	, "   " , "NDF - NOTA D�BITO FORNECEDOR" ,"S", "N"},;
        	      				{ "06" ,"9        " 	, "   " , "DOCUMENTO" ,"S", "N"},; 
        	      				{ "06" ,"0        " 	, "   " , "IMPOSTOS" ,"S", "N"},;
        	      				{ "06" ,"A        " 	, "   " , "RA - RECEBIMENTO ANTECIPADO" ,"S", "N"},;
        	      				{ "06" ,"B        " 	, "   " , "PA - PAGAMENTO ANTECIPADO" ,"S", "N"},;
        	      				{ "06" ,"C        " 	, "   " , "AB- ABATIMENTO" ,"S", "N"},;
								{ "09" ,"COM      " 	, "001" , "8810" ,"S", "N"},;
								{ "09" ,"FAT      " 	, "002" , "8820" ,"S", "N"},;
								{ "09" ,"EST      " 	, "003" , "8840" ,"S", "N"},;		
								{ "09" ,"FIN      " 	, "004" , "8850" ,"S", "N"},;
								{ "42" ,"NF       " 	, "001" , "NOTA FISCAL (01)" ,"S", "N"},;
								{ "42" ,"NFCF     " 	, "002" , "NOTA FISCAL DE VENDA A CONSUMIDOR (02) " ,"S", "N"},;
								{ "42" ,"NFE      " 	, "003" , "NOTA FISCAL DE ENTRADA (01)" ,"S", "N"},;
								{ "42" ,"NFP      " 	, "004" , "NOTA FISCAL DE PRODUTOR (04)" ,"S", "N"},;
								{ "42" ,"NFPS     " 	, "005" , "NOTA FISCAL DE PRESTA��O DE SERVI�O (  )" ,"S", "N"},;
								{ "42" ,"NFS      " 	, "006" , "NOTA FISCAL DE DE SERVI�O (  )" ,"S", "N"},;
								{ "42" ,"SPED     " 	, "007" , "NOTA FISCAL ELETR�NICA DO SEFAZ (55)" ,"S", "N"},;	
								{ "42" ,"RPS      " 	, "008" , "RECIBO PROVIS�RIO DE SERVI�OS (  ) " ,"S", "N"},;									
								{ "42" ,"CTR      "		, "009" , "NOTA FISCAL CONHECIMENTO DE FRETE RODOVIARIO (11) ", "S","N"},;
								{ "42" ,"CTA      "		, "010" , "NOTA FISCAL CONHECIMENTO DE FRETE AQUAVIARIO (09) ", "S","N"},;
								{ "42" ,"CTF      "		, "011" , "NOTA FISCAL CONHECIMENTO DE FRETE FERROVIARIO (11)","S","N"},;
								{ "42" ,"CA       "		, "012" , "CONHECIMENTO AEREO (10) ","S","N"},;	
								{ "42" ,"NFCEE    "		, "014" , "CONTA DE ENERGIA ELETRICA (06) ","S","N"},;
								{ "42" ,"NFCFG    "		, "015" , "NOTA FISCAL/CONTA FORNECIMENTO DE G�S (01) ","S","N"},;
								{ "42" ,"NFFA     "		, "016" , "NOTA FISCAL DE FORNECIMENTO DE �GUA (01)","S","N"},;
								{ "42" ,"NFSC     "		, "016" , "NOTA FISCAL DE SERVICO DE COMUNICACAO (21)","S","N"},;
								{ "42" ,"NTST     "		, "018" , "NOTA FISCAL DE SERVICO DE TELECOMUNICACOES (22) ","S","N"},;
								{ "42" ,"NFST     "		, "017" , "NOTA FISCAL DE SERVICO DE TRANSPORTE (07) ","S","N"},;
								{ "42" ,"CTE      "		, "019" , "CONHECIMENTO DE TRANSPORTE ELETR�NICO (57) ","S","N"},;								
								{ "42" ,"PDV      "		, "020" , "INTEGRADO PDV ( ) ","S","N"},;								
								{ "51" ,"101      "     , "001" , "Tributada pelo Simples Nacional com permiss�o de cr�dito","S","N"},;
								{ "51" ,"102      "     , "002" , "Tributada pelo Simples Nacional sem permiss�o de cr�dito","S","N"},;
								{ "51" ,"103      "     , "003" , "Isen��o do ICMS no Simples Nacional para faixa de receita bruta","S","N"},;
								{ "51" ,"201      "     , "004" , "Tributada pelo Simples Nacional com permiss�o de cr�dito e com cobran�a do ICMS por substitui��o tribut�ria","S","N"},;
								{ "51" ,"202      "     , "005" , "Tributada pelo Simples Nacional sem permiss�o de cr�dito e com cobran�a do ICMS por substitui��o tribut�ria","S","N"},;
								{ "51" ,"203      "     , "006" , "Isen��o do ICMS no Simples Nacional para faixa de receita bruta e com cobran�a do ICMS por substitui��o tribut�ria","S","N"},;
								{ "51" ,"300      "     , "007" , "Imune","S","N"},;
								{ "51" ,"400      "     , "008" , "N�o tributada pelo Simples Nacional","S","N"},;
								{ "51" ,"500      "     , "009" , "ICMS cobrado anteriormente por substitui��o tribut�ria (substitu�do) ou por antecipa��o","S","N"},;
								{ "51" ,"900      "     , "010" , "Outros","S","N"},;
								{ "58" ,"01       " 	, "000" , "CREDITO EM CONTA CORRENTE" ,"S", "N"},;
								{ "58" ,"02       " 	, "018" , "TRASNFERENCIAS BANCARIAS - TED CIP" ,"S", "N"},;
								{ "58" ,"03       " 	, "700" , "TRASNFERENCIAS BANCARIAS - DOC" ,"S", "N"},;
								{ "58" ,"05       " 	, "   " , "CREDITO EM CONTA POUPACA" ,"S", "N"},;
								{ "58" ,"10       " 	, "   " , "ORDEM DE PAGAMENTO" ,"S", "N"},;
								{ "58" ,"11       " 	, "   " , "PAGAMENTO DE CONTAS E TRIBUTOS COM COD.BARRAS" ,"S", "N"},;
								{ "58" ,"30       " 	, "   " , "LIQUIDACAO DE TITULOS SANTANDER" ,"S", "N"},;
								{ "58" ,"31       " 	, "   " , "LIQUIDACAO DE TITULOS OUTRO BANCO" ,"S", "N"},;
								{ "59" ,"01       " 	, "   " , "CREDITO EM CONTA CORRENTE" ,"S", "N"},;
								{ "59" ,"02       " 	, "   " , "PAGAMENTO DE ALUGUEL/CONDOMINIO" ,"S", "N"},;
								{ "59" ,"03       " 	, "   " , "PAGAMENTO DE DUPLICATAS E TITULOS" ,"S", "N"},;
								{ "59" ,"04       " 	, "   " , "PAGAMENTO DE DIVIDENDOS" ,"S", "N"},;
								{ "59" ,"05       " 	, "   " , "PAGAMENTO DE MENSALIDADES ESCOLARES" ,"S", "N"},;
								{ "59" ,"06       " 	, "   " , "PAGAMENTO DE SALARIOS" ,"S", "N"},;
								{ "59" ,"07       " 	, "   " , "PAGAMENTO A FORNECEDORES / HONORARIOS" ,"S", "N"},;
								{ "59" ,"08       " 	, "   " , "PAGAMENTO DE CAMBIO / FUNDOS / BOLSAS" ,"S", "N"},;
								{ "59" ,"09       " 	, "   " , "REPASSE DE ARRECADACAO / PAGAMENTO DE TRIBUTOS" ,"S", "N"},;
								{ "59" ,"11       " 	, "   " , "DOC / TED PARA POUPANCA" ,"S", "N"},;
								{ "59" ,"12       " 	, "   " , "DOC / TED PARA DEPOSITO JUDICIAL" ,"S", "N"},;
								{ "59" ,"13       " 	, "   " , "PENSAO ALIMENTICIA" ,"S", "N"},;
								{ "59" ,"14       " 	, "   " , "RESTITUICAO DE IMPOSTO DE RENDA" ,"S", "N"},;
								{ "59" ,"99       " 	, "   " , "OUTROS" ,"S", "N"},;
								{ "60" ,"01       " 	, "   " , "ENTRADA DE TITULOS" ,"S", "N"},;
								{ "60" ,"02       " 	, "   " , "PEDIDO DE BAIXA" ,"S", "N"},;
								{ "60" ,"04       " 	, "   " , "CONCESSAO DE ABATIMENTO" ,"S", "N"},;
								{ "60" ,"05       " 	, "   " , "CANCELAMENTO DE ABATIMENTO" ,"S", "N"},;
								{ "60" ,"06       " 	, "   " , "ALTERACAO DE VENCIMENTO" ,"S", "N"},;
								{ "60" ,"07       " 	, "   " , "ALTERACAO SEU NUMERO" ,"S", "N"},;
								{ "60" ,"09       " 	, "   " , "PEDIDO DE PROTESTO" ,"S", "N"},;
								{ "60" ,"18       " 	, "   " , "PEDIDO DE SUSTACAI DE PROTESTO" ,"S", "N"},;
								{ "60" ,"10       " 	, "   " , "CONCESSAO DE DESCONTO" ,"S", "N"},;
								{ "60" ,"11       " 	, "   " , "CANCELAMENTO DE DESCONTO" ,"S", "N"},;
								{ "60" ,"02       " 	, "   " , "ALTERACAO DE OUTROS DADOS" ,"S", "N"},;
								{ "61" ,"02       " 	, "   " , "DM - DUPLICATA MERCANTIL" ,"S", "N"},;
								{ "61" ,"04       " 	, "   " , "DS - DUPLICATA DE SERVICO" ,"S", "N"},;
								{ "61" ,"12       " 	, "   " , "NP - NOTA PROMISSORIA" ,"S", "N"},;
								{ "61" ,"13       " 	, "   " , "NR - NOTA PROMISSORIA RURAL" ,"S", "N"},;
								{ "61" ,"17       " 	, "   " , "RC - RECIBO" ,"S", "N"},;
								{ "61" ,"20       " 	, "   " , "AP - APOLICE DE SEGURO" ,"S", "N"},;
								{ "61" ,"97       " 	, "   " , "CH - CHEQUE" ,"S", "N"},;
								{ "61" ,"98       " 	, "   " , "ND - NOTA PROMISSORIA DIRETA" ,"S", "N"},;
								{ "70" ,"0        " 	, "   " , "Tributada integralmente" ,"S", "N"},;
								{ "70" ,"01       " 	, "   " , "Tributada integralmente e sujeita ao regime do Simples Nacional" ,"S", "N"},;
								{ "70" ,"02       " 	, "   " , "Tributada integralmente e com ISQN retido na fonte" ,"S", "N"},;
								{ "70" ,"03       " 	, "   " , "Tributada integralmente, sujeita ao regime do Simples Nacional e com o ISQN retido na fonte" ,"S", "N"},;
								{ "70" ,"04       " 	, "   " , "Tributada integralmente e sujeita ao regime da substitui��o tribut�ria" ,"S", "N"},;
								{ "70" ,"05       " 	, "   " , "Tributada integralmente e sujeita ao regime da substitui��o tribut�ria pelo agenciador ou intermedi�rio da presta��o do servi�o" ,"S", "N"},;
								{ "70" ,"06       " 	, "   " , "Tributada integralmente, sujeita ao regime do Simples Nacional e da substitui��o tribut�ria" ,"S", "N"},;
								{ "70" ,"07       " 	, "   " , "Tributada integralmente e com o ISQN retido anteriormente pelo substituto tribut�rio" ,"S", "N"},;
								{ "70" ,"08       " 	, "   " , "Tributada com redu��o da base de c�lculo ou al�quota" ,"S", "N"},;
								{ "70" ,"09       " 	, "   " , "Tributada com redu��o da base de c�lculo ou al�quota e com ISQN retido na fonte" ,"S", "N"},;
								{ "70" ,"10       " 	, "   " , "Tributada com redu��o da base de c�lculo ou al�quota e sujeita ao regime da substitui��o tribut�ria" ,"S", "N"},;
								{ "70" ,"11       " 	, "   " , "Tributada com redu��o da base de c�lculo  ou al�quota e com o ISQN retido anteriormente pelo substituto tribut�rio" ,"S", "N"},;
								{ "70" ,"12       " 	, "   " , "Isenta ou Imune" ,"S", "N"},;																																																																								
								{ "70" ,"13       " 	, "   " , "N�o Tributada" ,"S", "N"},;	
								{ "70" ,"14       " 	, "   " , "Tributada por meio do imposto fixo" ,"S", "N"},;	                                                                                                                   	                                                                                                                       	
								{ "70" ,"15       " 	, "   " , "N�o Tributada em raz�o do destino dos bens ou objetos � Mercadorias para a industrializa��o ou comercializa��o" ,"S", "N"},;
								{ "70" ,"16       " 	, "   " , "N�o Tributada em raz�o do diferimento da presta��o do servi�o." ,"S", "N"},;
								{ "89" ,"         " 	, "01" , "TITULO" ,"S", "N"},;
								{ "89" ,"         " 	, "02" , "ESPECIE" ,"S", "N"},;
								{ "89" ,"         " 	, "03" , "OCORRENCIA" ,"S", "N"},;
								{ "89" ,"         " 	, "04" , "DATA" ,"S", "N"},;
								{ "89" ,"         " 	, "05" , "VALOR" ,"S", "N"},;
								{ "89" ,"         " 	, "06" , "DESPESA" ,"S", "N"},;
								{ "89" ,"         " 	, "07" , "DESCONTO" ,"S", "N"},;
								{ "89" ,"         " 	, "08" , "ABATIMENTO" ,"S", "N"},;
								{ "89" ,"         " 	, "09" , "JUROS" ,"S", "N"},;
								{ "89" ,"         " 	, "10" , "MULTA" ,"S", "N"},;
								{ "89" ,"         " 	, "11" , "IOF" ,"S", "N"},;
								{ "89" ,"         " 	, "12" , "OUTROSCREDITOS" ,"S", "N"},;
								{ "89" ,"         " 	, "13" , "DATACREDITO" ,"S", "N"},;
								{ "89" ,"         " 	, "14" , "MOTIVO" ,"S", "N"},;
								{ "89" ,"         " 	, "15" , "NOSSONUMERO" ,"S", "N"},;
								{ "89" ,"         " 	, "16" , "RESERVADO" ,"S", "N"},;
								{ "89" ,"         " 	, "17" , "SEGMENTO" ,"S", "N"},;
								{ "89" ,"         " 	, "18" , "SEUNUMERO" ,"S", "N"},;   
								{ "90" ,"341      " 	, "02" , "DEVOLVER AP�S 05 DIAS DO VENCIMENTO","S", "S" },;
								{ "90" ,"341      " 	, "03" , "DEVOLVER AP�S 30 DIAS DO VENCIMENTO","S", "S" },;
								{ "90" ,"341      " 	, "05" , "RECEBER CONFORME INSTRU��ES NO PR�PRIO T�TULO","S", "S" },;
								{ "90" ,"341      " 	, "06" , "DEVOLVER AP�S 10 DIAS DO VENCIMENTO","S", "S" },;
								{ "90" ,"341      " 	, "07" , "DEVOLVER AP�S 15 DIAS DO VENCIMENTO","S", "S" },;
								{ "90" ,"341      " 	, "08" , "DEVOLVER AP�S 20 DIAS DO VENCIMENTO","S", "S" },;
								{ "90" ,"341      " 	, "09" , "PROTESTAR (emite aviso ao sacado ap�s XX dias do vencimento, e envia ao cart�rio ap�s 5 dias �teis)","S", "S" },;
								{ "90" ,"341      " 	, "10" , "N�O PROTESTAR (inibe protesto, quando houver instru��o permanente na conta corrente) ","S", "S" },;
								{ "90" ,"341      " 	, "11" , "DEVOLVER AP�S 25 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "12" , "DEVOLVER AP�S 35 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "13" , "DEVOLVER AP�S 40 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "14" , "DEVOLVER AP�S 45 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "15" , "DEVOLVER AP�S 50 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "16" , "DEVOLVER AP�S 55 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "17" , "DEVOLVER AP�S 60 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "18" , "DEVOLVER  AP�S 90 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "19" , "N�O RECEBER AP�S 05 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "20" , "N�O RECEBER AP�S 10 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "21" , "N�O RECEBER AP�S 15 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "22" , "N�O RECEBER AP�S 20 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "23" , "N�O RECEBER AP�S 25 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "24" , "N�O RECEBER AP�S 30 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "25" , "N�O RECEBER AP�S 35 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "26" , "N�O RECEBER AP�S 40 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "27" , "N�O RECEBER AP�S 45 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "28" , "N�O RECEBER AP�S 50 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "29" , "N�O RECEBER AP�S 55 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "30" , "IMPORT�NCIA DE DESCONTO POR DIA" ,"S", "S"},;
								{ "90" ,"341      " 	, "31" , "N�O RECEBER AP�S 60 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "32" , "N�O RECEBER AP�S 90 DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "33" , "CONCEDER ABATIMENTO REF. � PIS-PASEP/COFIN/CSSL, MESMO AP�S VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "37" , "RECEBER AT� O �LTIMO DIA DO M�S DE VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "38" , "CONCEDER DESCONTO MESMO AP�S VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "39" , "N�O RECEBER AP�S O VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "40" , "CONCEDER DESCONTO CONFORME NOTA DE CR�DITO" ,"S", "S"},;
								{ "90" ,"341      " 	, "42" , "PROTESTO PARA FINS FALIMENTARES" ,"S", "S"},;
								{ "90" ,"341      " 	, "43" , "SUJEITO A PROTESTO SE N�O FOR PAGO NO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "44" , "IMPORT�NCIA POR DIA DE ATRASO A PARTIR DE DDMMAA" ,"S", "S"},;
								{ "90" ,"341      " 	, "45" , "TEM DIA DA GRA�A" ,"S", "S"},;
								{ "90" ,"341      " 	, "47" , "DISPENSAR JUROS/COMISS�O DE PERMAN�NCIA" ,"S", "S"},;
								{ "90" ,"341      " 	, "51" , "RECEBER SOMENTE COM A PARCELA ANTERIOR QUITADA" ,"S", "S"},;
								{ "90" ,"341      " 	, "52" , "FAVOR EFETUAR PGTO  SOMENTE ATRAV�S DESTA COBRAN�A BANC�RIA" ,"S", "S"},;
								{ "90" ,"341      " 	, "54" , "AP�S VENCIMENTO PAG�VEL SOMENTE NA EMPRESA" ,"S", "S"},;
								{ "90" ,"341      " 	, "57" , "SOMAR VALOR DO T�TULO AO VALOR DO CAMPO MORA/MULTA CASO EXISTA" ,"S", "S"},;
								{ "90" ,"341      " 	, "58" , "DEVOLVER AP�S 365 DIAS DE VENCIDO" ,"S", "S"},;
								{ "90" ,"341      " 	, "59" , "COBRAN�A NEGOCIADA. PAG�VEL SOMENTE POR ESTE BLOQUETO NA REDE BANC�RIA" ,"S", "S"},;
								{ "90" ,"341      " 	, "61" , "T�TULO ENTREGUE EM PENHOR EM FAVOR DO CEDENTE ACIMA" ,"S", "S"},;
								{ "90" ,"341      " 	, "62" , "T�TULO TRANSFERIDO A FAVOR DO CEDENTE" ,"S", "S"},;
								{ "90" ,"341      " 	, "78" , "VALOR DA IDA ENGLOBA MULTA DE 10% PRO RATA" ,"S", "S"},;
								{ "90" ,"341      " 	, "79" , "COBRAR JUROS AP�S 15 DIAS DA EMISS�O (para t�tulos com vencimento � vista)" ,"S", "S"},;
								{ "90" ,"341      " 	, "80" , "PAGAMENTO EM CHEQUE: SOMENTE RECEBER COM CHEQUE DE EMISS�O DO SACADO" ,"S", "S"},;
								{ "90" ,"341      " 	, "81" , "PROTESTAR AP�S XX DIAS CORRIDOS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "82" , "PROTESTAR AP�S XX DIAS �TEIS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "83" , "OPERA��O REF A VENDOR" ,"S", "S"},;
								{ "90" ,"341      " 	, "84" , "AP�S VENCIMENTO CONSULTAR A AG�NCIA CEDENTE" ,"S", "S"},;
								{ "90" ,"341      " 	, "86" , "ANTES DO VENCIMENTO OU AP�S 15 DIAS, PAG�VEL SOMENTE EM NOSSA SEDE" ,"S", "S"},;
								{ "90" ,"341      " 	, "88" , "N�O RECEBER ANTES DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "90" , "NO VENCIMENTO PAG�VEL EM QUALQUER AG�NCIA BANC�RIA" ,"S", "S"},;
								{ "90" ,"341      " 	, "91" , "N�O RECEBER AP�S XX DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "92" , "DEVOLVER AP�S XX DIAS DO VENCIMENTO" ,"S", "S"},;
								{ "90" ,"341      " 	, "93" , "MENSAGENS NOS BLOQUETOS COM 30 POSI��ES" ,"S", "S"},;
								{ "90" ,"341      " 	, "94" , "MENSAGENS NOS BLOQUETOS COM 40 POSI��ES" ,"S", "S"},;
								{ "90" ,"341      " 	, "98" , "DUPLICATA / FATURA N�" ,"S", "S"},;
								{ "90" ,"001      " 	, "00" , "O SISTEMA DE COBRAN�A DO BANCO ASSUMIR� O PRAZO DE PROTESTO DE 5 DIAS �TEIS" ,"S", "S"},;
								{ "90" ,"001      " 	, "01" , "COBRAR JUROS (DISPENS�VEL SE INFORMADO O VALOR A SER COBRADO POR DIA DE ATRASO)","S", "S" },;
								{ "90" ,"001      " 	, "03" , "PROTESTAR NO 3� DIA �TIL AP�S VENCIDO" ,"S", "S"},;
								{ "90" ,"001      " 	, "04" , "PROTESTAR NO 4� DIA �TIL AP�S VENCIDO" ,"S", "S"},;
								{ "90" ,"001      " 	, "05" , "PROTESTAR NO 5� DIA �TIL AP�S VENCIDO" ,"S", "S"},;
								{ "90" ,"001      " 	, "06" , "INDICA PROTESTO EM DIAS CORRIDOS, COM PRAZO DE 6 A 29, 35 OU 40 DIAS CORRIDOS" ,"S", "S"},;
								{ "90" ,"001      " 	, "10" , "PROTESTAR NO 10� DIA CORRIDO AP�S VENCIDO" ,"S", "S"},;
								{ "90" ,"001      " 	, "15" , "PROTESTAR NO 15� DIA CORRIDO AP�S VENCIDO" ,"S", "S"},;
								{ "90" ,"001      " 	, "20" , "PROTESTAR NO 20� DIA CORRIDO AP�S VENCIDO" ,"S", "S"},;
								{ "90" ,"001      " 	, "25" , "PROTESTAR NO 25� DIA CORRIDO AP�S VENCIDO" ,"S", "S"},;
								{ "90" ,"001      " 	, "30" , "PROTESTAR NO 30� DIA CORRIDO AP�S VENCIDO" ,"S", "S"},;
								{ "90" ,"001      " 	, "35" , "PROTESTAR NO 35� DIA CORRIDO AP�S VENCIDO" ,"S", "S"},;
								{ "90" ,"001      " 	, "40" , "PROTESTAR NO 40� DIA CORRIDO AP�S VENCIDO" ,"S", "S"},;
								{ "90" ,"001      " 	, "45" , "PROTESTAR NO 45� DIA CORRIDO AP�S VENCIDO" ,"S", "S"},;
								{ "90" ,"237      " 	, "00" , "SEM INSTRU��O" ,"S", "S"},;
								{ "90" ,"237      " 	, "01" , "REMESSA" ,"S", "S"},;
								{ "90" ,"237      " 	, "02" , "PEDIDO DE BAIXA" ,"S", "S"},;
								{ "90" ,"237      " 	, "03" , "PEDIDO DE PROTESTO FALIMENTAR" ,"S", "S"},;
								{ "90" ,"237      " 	, "04" , "CONCESS�O DE ABATIMENTO" ,"S", "S"},;
								{ "90" ,"237      " 	, "05" , "CANCELAMENTO DE ABATIMENTO CONCEDIDO" ,"S", "S"},;
								{ "90" ,"237      " 	, "06" , "ALTERA��O DE VENCIMENTO" ,"S", "S"},;
								{ "90" ,"237      " 	, "07" , "ALTERA��O DO CONTROLE DO PARTICIPANTE" ,"S", "S"},;
								{ "90" ,"237      " 	, "08" , "ALTERA��O DE SEU N�MERO" ,"S", "S"},;
								{ "90" ,"237      " 	, "09" , "PEDIDO DE PROTESTO" ,"S", "S"},;
								{ "90" ,"237      " 	, "18" , "SUSTAR PROTESTO E BAIXAR T�TULO" ,"S", "S"},;
								{ "90" ,"237      " 	, "19" , "SUSTAR PROTESTO E MANTER EM CARTEIRA" ,"S", "S"},;
								{ "90" ,"237      " 	, "22" , "TRANSFER�NCIA CESS�O CR�DITO ID. PROD. 10" ,"S", "S"},;
								{ "90" ,"237      " 	, "23" , "TRANSFER�NCIA ENTRE CARTEIRAS" ,"S", "S"},;
								{ "90" ,"237      " 	, "24" , "DEV. TRANSFER�NCIA ENTRE CARTEIRAS" ,"S", "S"},;
								{ "90" ,"237      " 	, "31" , "ALTERA��O DE OUTROS DADOS" ,"S", "S"},;
								{ "90" ,"237      " 	, "68" , "ACERTO NOS DADOS DO RATEIO DE CR�DITO" ,"S", "S"},;
								{ "90" ,"237      " 	, "69" , "CANCELAMENTO DO RATEIO DE CR�DITO" ,"S", "S"},;
								{ "91" ,"         " 	, "1  " , "BAIXAR" ,"S", "S"},;		
 								{ "91" ,"         " 	, "2  " , "NAO BAIXAR" ,"S", "S"},;		
  								{ "91" ,"         " 	, "3  " , "USAR PERFIL CEDENTE" ,"S", "S"},;		
								{ "92" ,"         " 	, "1  " , "DIA" ,"S", "S"},;		
								{ "92" ,"         " 	, "2  " , "MES" ,"S", "S"},;		
 								{ "93" ,"         " 	, "0  " , "NAO CONSTA DESCONTO" ,"S", "S"},;		 								
								{ "93" ,"         " 	, "1  " , "VALOR FIXO ATE A DATA INFORMADA" ,"S", "S"},;		 								
 								{ "93" ,"         " 	, "2  " , "PERCENTUAL ATE A DATA INFORMADA" ,"S", "S"},;		 								
 								{ "93" ,"         " 	, "3  " , "DESCONTO POR DIA CORRIDO" ,"S", "S"},;		 								 	
 								{ "93" ,"         "  	, "4  " , "DESCONTO POR DIA UTIL" ,"S", "S"},;	        								
 								{ "94" ,"         " 	, "1  " , "SIMPLES SEM REGISTRO" ,"S", "S"},;		
 								{ "94" ,"         " 	, "2  " , "SIMPLES COM REGISTRO" ,"S", "S"},;		
								{ "94" ,"         " 	, "3  " , "PENHOR COM REGISTRO" ,"S", "S"},;		
 								{ "94" ,"         " 	, "4  " , "DESCONTO" ,"S", "S"},;		
 								{ "94" ,"         " 	, "8  " , "CARTEIRA" ,"S", "S"},;		 								
								{ "95" ,"         " 	, "1  " , "CARTEIRA" ,"S", "S"},;		
 								{ "95" ,"         " 	, "2  " , "BANCO" ,"S", "S"},;		
								{ "96" ,"001      " 	, "1  " , "PROTESTAR DIAS CORRIDOS" ,"S", "S"},;   
								{ "96" ,"001      " 	, "2  " , "PROTESTAR DIAS UTEIS" ,"S", "S"},;   
								{ "96" ,"001      " 	, "3  " , "NAO PROTESTAR" ,"S", "S"},;
								{ "96" ,"033      " 	, "0  " , "NAO PROTESTAR" ,"S", "S"},;   								
								{ "96" ,"033      " 	, "1  " , "PROTESTAR DIAS CORRIDOS" ,"S", "S"},;   
								{ "96" ,"033      " 	, "2  " , "PROTESTAR DIAS UTEIS" ,"S", "S"},;   
								{ "96" ,"033      " 	, "3  " , "USAR PERFIL CEDENTE" ,"S", "S"},;							   								
								{ "96" ,"237      " 	, "1  " , "PROTESTAR DIAS CORRIDOS" ,"S", "S"},;   
								{ "96" ,"237      " 	, "2  " , "PROTESTAR DIAS UTEIS" ,"S", "S"},;   
								{ "96" ,"237      " 	, "3  " , "NAO PROTESTAR" ,"S", "S"},;	
								{ "96" ,"341      " 	, "1  " , "PROTESTAR DIAS CORRIDOS" ,"S", "S"},;   
								{ "96" ,"341      " 	, "2  " , "PROTESTAR DIAS UTEIS" ,"S", "S"},; 
								{ "96" ,"341      " 	, "3  " , "NAO PROTESTAR" ,"S", "S"},;  
								{ "96" ,"356      " 	, "1  " , "PROTESTAR DIAS CORRIDOS" ,"S", "S"},;   
								{ "96" ,"356      " 	, "2  " , "PROTESTAR DIAS UTEIS" ,"S", "S"},;   
								{ "96" ,"356      " 	, "3  " , "NAO PROTESTAR" ,"S", "S"},; 
								{ "97" ,"00       " 	, "   " , "ENTRADA COM RECUPERACAO DE CREDITO" ,"S", "N"},;
								{ "97" ,"01       " 	, "   " , "ENTRADA TRIBUTADA COM ALIQUOTA ZERO" ,"S", "N"},;
								{ "97" ,"02       " 	, "   " , "ENTRADA ISENTA" ,"S", "N"},;
								{ "97" ,"03       " 	, "   " , "ENTRADA NAO TRIBUTADA" ,"S", "N"},;
								{ "97" ,"04       " 	, "   " , "ENTRADA IMUNE" ,"S", "N"},;
								{ "97" ,"05       " 	, "   " , "ENTRADA COM SUSPENSAO" ,"S", "N"},;
								{ "97" ,"49       " 	, "   " , "OUTRAS ENTRADAS" ,"S", "N"},;
								{ "97" ,"50       " 	, "   " , "SAIDA TRIBUTADA" ,"S", "N"},;
								{ "97" ,"51       " 	, "   " , "SAIDA TRIBUTADA ALIQUOTA ZERO" ,"S", "N"},;
								{ "97" ,"52       " 	, "   " , "SAIDA ISENTA" ,"S", "N"},;
								{ "97" ,"53       " 	, "   " , "SAIDA NAO TRIBUTADA" ,"S", "N"},;
								{ "97" ,"54       " 	, "   " , "SAIDA IMUNE" ,"S", "N"},;
								{ "97" ,"55       " 	, "   " , "SAIDA COM SUSPENSAO" ,"S", "N"},;
								{ "97" ,"99       " 	, "   " , "OUTRAS SAIDAS" ,"S", "N"},;					  
								{ "98" ,"101      " 	, "   " , "AN�LISE E DESENVOLVIMENTO DE SISTEMAS." ,"S", "S"},;   
								{ "98" ,"102      " 	, "   " , "PROGRAMA��O." ,"S", "S"},;   
								{ "98" ,"103      " 	, "   " , "PROCESSAMENTO DE DADOS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"104      " 	, "   " , "ELABORA��O DE PROGRAMAS DE COMPUTADORES, INCLUSIVE DE JOGOS ELETR�NICOS." ,"S", "S"},;   
								{ "98" ,"105      " 	, "   " , "LICENCIAMENTO OU CESS�O DE DIREITO DE USO DE PROGRAMAS DE COMPUTA��O." ,"S", "S"},;   
								{ "98" ,"106      " 	, "   " , "ASSESSORIA E CONSULTORIA EM INFORM�TICA." ,"S", "S"},;   
								{ "98" ,"107      " 	, "   " , "SUPORTE T�CNICO EM INFORM�TICA, INCLUSIVE INSTALA��O, CONFIGURA��O E MANUTEN��O DE PROGRAMAS DE COMPUTA��O E BANCOS DE DADOS." ,"S", "S"},;   
								{ "98" ,"108      " 	, "   " , "PLANEJAMENTO, CONFEC��O, MANUTEN��O E ATUALIZA��O DE P�GINAS ELETR�NICAS." ,"S", "S"},;   
								{ "98" ,"201      " 	, "   " , "SERVI�OS DE PESQUISAS E DESENVOLVIMENTO DE QUALQUER NATUREZA." ,"S", "S"},;   
								{ "98" ,"302      " 	, "   " , "CESS�O DE DIREITO DE USO DE MARCAS E DE SINAIS DE PROPAGANDA." ,"S", "S"},;   
								{ "98" ,"303      " 	, "   " , "EXPLORA��O SAL�ES DE FESTAS, ESCRIT�RIOS VIRTUAIS, STANDS, EST�DIOS,  E CONG�NERES, PARA REALIZA��O DE EVENTOS OU NEG�CIOS DE QUALQUER NATUREZA" ,"S", "S"},;   
								{ "98" ,"304      " 	, "   " , "LOCA��O, ARRENDAMENTO, DIREITO DE PASSAGEM, COMPARTILHADO OU N�O, DE FERROVIA, RODOVIA, POSTES, CABOS, DUTOS DE QUALQUER NATUREZA." ,"S", "S"},;   
								{ "98" ,"305      " 	, "   " , "CESS�O DE ANDAIMES, PALCOS, COBERTURAS E OUTRAS ESTRUTURAS DE USO TEMPOR�RIO." ,"S", "S"},;   
								{ "98" ,"401      " 	, "   " , "MEDICINA E BIOMEDICINA." ,"S", "S"},;   
								{ "98" ,"402      " 	, "   " , "AN�LISES CL�NICAS, PATOLOGIA,  RADIOTERAPIA, QUIMIOTERAPIA, ULTRA-SONOGRAFIA, RESSON�NCIA MAGN�TICA, RADIOLOGIA, TOMOGRAFIA E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"403      " 	, "   " , "HOSPITAIS, CL�NICAS, LABORAT�RIOS, SANAT�RIOS, MANIC�MIOS, CASAS DE SA�DE, PRONTOS-SOCORROS, AMBULAT�RIOS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"404      " 	, "   " , "INSTRUMENTA��O CIR�RGICA." ,"S", "S"},;   
								{ "98" ,"405      " 	, "   " , "ACUPUNTURA." ,"S", "S"},;   
								{ "98" ,"406      " 	, "   " , "ENFERMAGEM, INCLUSIVE SERVI�OS AUXILIARES." ,"S", "S"},;   
								{ "98" ,"407      " 	, "   " , "SERVI�OS FARMAC�UTICOS." ,"S", "S"},;   
								{ "98" ,"408      " 	, "   " , "TERAPIA OCUPACIONAL, FISIOTERAPIA E FONOAUDIOLOGIA." ,"S", "S"},;   
								{ "98" ,"409      " 	, "   " , "TERAPIAS DE QUALQUER ESP�CIE DESTINADAS AO TRATAMENTO F�SICO, ORG�NICO E MENTAL." ,"S", "S"},;   
								{ "98" ,"410      " 	, "   " , "NUTRI��O." ,"S", "S"},;   
								{ "98" ,"411      " 	, "   " , "OBSTETR�CIA." ,"S", "S"},;   
								{ "98" ,"412      " 	, "   " , "ODONTOLOGIA." ,"S", "S"},;   
								{ "98" ,"413      " 	, "   " , "ORT�PTICA." ,"S", "S"},;   
								{ "98" ,"414      " 	, "   " , "PR�TESES SOB ENCOMENDA." ,"S", "S"},;   
								{ "98" ,"415      " 	, "   " , "PSICAN�LISE." ,"S", "S"},;   
								{ "98" ,"416      " 	, "   " , "PSICOLOGIA." ,"S", "S"},;   
								{ "98" ,"417      " 	, "   " , "CASAS DE REPOUSO E DE RECUPERA��O, CRECHES, ASILOS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"418      " 	, "   " , "INSEMINA��O ARTIFICIAL, FERTILIZA��O IN VITRO E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"419      " 	, "   " , "BANCOS DE SANGUE, LEITE, PELE, OLHOS, �VULOS, S�MEN E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"420      " 	, "   " , "COLETA DE SANGUE, LEITE, TECIDOS, S�MEN, �RG�OS E MATERIAIS BIOL�GICOS DE QUALQUER ESP�CIE." ,"S", "S"},;   
								{ "98" ,"421      " 	, "   " , "UNIDADE DE ATENDIMENTO, ASSIST�NCIA OU TRATAMENTO M�VEL E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"422      " 	, "   " , "PLANOS DE MEDICINA DE GRUPO OU INDIVIDUAL E CONV�NIOS PARA PRESTA��O DE ASSIST�NCIA M�DICA, HOSPITALAR, ODONTOL�GICA E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"423      " 	, "   " , "OUTROS PLANOS DE SA�DE QUE SE CUMPRAM ATRAV�S DE SERVI�OS TERCEIROS OU APENAS PAGOS PELO OPERADOR DO PLANO MEDIANTE INDICA��O DO BENEFICI�RIO." ,"S", "S"},;   
								{ "98" ,"501      " 	, "   " , "SERVI�OS DE MEDICINA E ASSIST�NCIA VETERIN�RIA E CONG�NERES E ZOOTECNIA." ,"S", "S"},;   
								{ "98" ,"502      " 	, "   " , "HOSPITAIS, CL�NICAS, AMBULAT�RIOS, PRONTOS-SOCORROS E CONG�NERES, NA �REA VETERIN�RIA." ,"S", "S"},;   
								{ "98" ,"503      " 	, "   " , "LABORAT�RIOS DE AN�LISE NA �REA VETERIN�RIA." ,"S", "S"},;   
								{ "98" ,"504      " 	, "   " , "INSEMINA��O ARTIFICIAL, FERTILIZA��O IN VITRO E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"505      " 	, "   " , "BANCOS DE SANGUE E DE �RG�OS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"506      " 	, "   " , "COLETA DE SANGUE, LEITE, TECIDOS, S�MEN, �RG�OS E MATERIAIS BIOL�GICOS DE QUALQUER ESP�CIE." ,"S", "S"},;   
								{ "98" ,"507      " 	, "   " , "UNIDADE DE ATENDIMENTO, ASSIST�NCIA OU TRATAMENTO M�VEL E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"508      " 	, "   " , "GUARDA, TRATAMENTO, AMESTRAMENTO, EMBELEZAMENTO, ALOJAMENTO E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"509      " 	, "   " , "PLANOS DE ATENDIMENTO E ASSIST�NCIA M�DICO-VETERIN�RIA." ,"S", "S"},;   
								{ "98" ,"601      " 	, "   " , "BARBEARIA, CABELEIREIROS, MANICUROS, PEDICUROS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"602      " 	, "   " , "ESTETICISTAS, TRATAMENTO DE PELE, DEPILA��O E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"603      " 	, "   " , "BANHOS, DUCHAS, SAUNA, MASSAGENS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"604      " 	, "   " , "GIN�STICA, DAN�A, ESPORTES, NATA��O, ARTES MARCIAIS E DEMAIS ATIVIDADES F�SICAS." ,"S", "S"},;   
								{ "98" ,"605      " 	, "   " , "CENTROS DE EMAGRECIMENTO, SPA E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"701      " 	, "   " , "ENGENHARIA, AGRONOMIA, AGRIMENSURA, ARQUITETURA, GEOLOGIA, URBANISMO, PAISAGISMO E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"702      " 	, "   " , "EXECU��O, POR ADMINISTRA��O, EMPREITADA DE OBRAS DE CONSTRU��O CIVIL." ,"S", "S"},;   
								{ "98" ,"703      " 	, "   " , "ELABORA��O DE PLANOS DIRETORES, ESTUDOS DE VIABILIDADE, ESTUDOS ORGANIZACIONAIS E OUTROS, RELACIONADOS COM OBRAS E SERVI�OS DE ENGENHARIA." ,"S", "S"},;   
								{ "98" ,"704      " 	, "   " , "DEMOLI��O." ,"S", "S"},;   
								{ "98" ,"705      " 	, "   " , "REPARA��O, CONSERVA��O E REFORMA DE EDIF�CIOS, ESTRADAS, PONTES, PORTOS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"706      " 	, "   " , "INSTALA��O DE TAPETES, CARPETES, ASSOALHOS, CORTINAS, REVESTIMENTOS DE PAREDE E CONG�NERES, COM MATERIAL FORNECIDO PELO TOMADOR DO SERVI�O." ,"S", "S"},;   
								{ "98" ,"707      " 	, "   " , "RECUPERA��O, RASPAGEM, POLIMENTO E LUSTRA��O DE PISOS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"708      " 	, "   " , "CALAFETA��O." ,"S", "S"},;   
								{ "98" ,"709      " 	, "   " , "VARRI��O, COLETA, REMO��O, INCINERA��O, TRATAMENTO, RECICLAGEM, SEPARA��O E DESTINA��O FINAL DE LIXO, REJEITOS E OUTROS RES�DUOS QUAISQUER." ,"S", "S"},;   
								{ "98" ,"710      " 	, "   " , "LIMPEZA, MANUTEN��O E CONSERVA��O DE VIAS E LOGRADOUROS P�BLICOS, IM�VEIS, CHAMIN�S, PISCINAS, PARQUES, JARDINS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"711      " 	, "   " , "DECORA��O E JARDINAGEM, INCLUSIVE CORTE E PODA DE �RVORES." ,"S", "S"},;   
								{ "98" ,"712      " 	, "   " , "CONTROLE E TRATAMENTO DE EFLUENTES DE QUALQUER NATUREZA E DE AGENTES F�SICOS, QU�MICOS E BIOL�GICOS." ,"S", "S"},;   
								{ "98" ,"713      " 	, "   " , "DEDETIZA��O, DESINFEC��O, DESINSETIZA��O, IMUNIZA��O, HIGIENIZA��O, DESRATIZA��O, PULVERIZA��O E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"716      " 	, "   " , "FLORESTAMENTO, REFLORESTAMENTO, SEMEADURA, ADUBA��O E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"717      " 	, "   " , "ESCORAMENTO, CONTEN��O DE ENCOSTAS E SERVI�OS CONG�NERES." ,"S", "S"},;
								{ "98" ,"718      " 	, "   " , "LIMPEZA E DRAGAGEM DE RIOS, PORTOS, CANAIS, BA�AS, LAGOS, LAGOAS, REPRESAS, A�UDES E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"719      " 	, "   " , "ACOMPANHAMENTO E FISCALIZA��O DA EXECU��O DE OBRAS DE ENGENHARIA, ARQUITETURA E URBANISMO." ,"S", "S"},;   
								{ "98" ,"720      " 	, "   " , "AEROFOTOGRAMETRIA (INCLUSIVE INTERPRETA��O), CARTOGRAFIA, LEVANTAMENTOS TOPOGR�FICOS, BATIM�TRICOS, GEOGR�FICOS, GEOL�GICOS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"721      " 	, "   " , "PESQUISA, TESTEMUNHAGEM, ESTIMULA��O E OUTROS SERVI�OS RELACIONADOS COM A EXPLORA��O DE PETR�LEO, G�S NATURAL E DE OUTROS RECURSOS MINERAIS." ,"S", "S"},;   
								{ "98" ,"722      " 	, "   " , "NUCLEA��O E BOMBARDEAMENTO DE NUVENS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"801      " 	, "   " , "ENSINO REGULAR PR�-ESCOLAR, FUNDAMENTAL, M�DIO E SUPERIOR." ,"S", "S"},;   
								{ "98" ,"802      " 	, "   " , "INSTRU��O, TREINAMENTO, ORIENTA��O PEDAG�GICA E EDUCACIONAL, AVALIA��O DE CONHECIMENTOS DE QUALQUER NATUREZA." ,"S", "S"},;   
								{ "98" ,"901      " 	, "   " , "HOSPEDAGEM DE QUALQUER NATUREZA EM HOT�IS E CONG�NERES; OCUPA��O POR TEMPORADA COM FORNECIMENTO DE SERVI�O." ,"S", "S"},;   
								{ "98" ,"902      " 	, "   " , "AGENCIAMENTO, ORGANIZA��O, PROMO��O, INTERMEDIA��O E EXECU��O DE PROGRAMAS DE TURISMO, PASSEIOS, VIAGENS, EXCURS�ES, HOSPEDAGENS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"903      " 	, "   " , "GUIAS DE TURISMO." ,"S", "S"},;   
								{ "98" ,"1001     " 	, "   " , "AGENCIAMENTO, CORRETAGEM OU INTERMEDIA��O DE C�MBIO, DE SEGUROS, DE CART�ES DE CR�DITO, DE PLANOS DE SA�DE E DE PLANOS DE PREVID�NCIA PRIVADA." ,"S", "S"},;   
								{ "98" ,"1002     " 	, "   " , "AGENCIAMENTO, CORRETAGEM OU INTERMEDIA��O DE T�TULOS EM GERAL, VALORES MOBILI�RIOS E CONTRATOS QUAISQUER." ,"S", "S"},;   
								{ "98" ,"1003     " 	, "   " , "AGENCIAMENTO, CORRETAGEM OU INTERMEDIA��O DE DIREITOS DE PROPRIEDADE INDUSTRIAL, ART�STICA OU LITER�RIA." ,"S", "S"},;   
								{ "98" ,"1004     " 	, "   " , "AGENCIAMENTO, CORRETAGEM OU INTERMEDIA��O DE CONTRATOS DE ARRENDAMENTO MERCANTIL (LEASING), DE FRANQUIA (FRANCHISING) E DE FACTORING." ,"S", "S"},;   
								{ "98" ,"1005     " 	, "   " , "AGENCIAMENTO, CORRETAGEM OU INTERMEDIA��O DE BENS M�VEIS OU IM�VEIS, N�O ABRANGIDOS EM OUTROS ITENS OU SUBITENS." ,"S", "S"},;   
								{ "98" ,"1006     " 	, "   " , "AGENCIAMENTO MAR�TIMO." ,"S", "S"},;   
								{ "98" ,"1007     " 	, "   " , "AGENCIAMENTO DE NOT�CIAS." ,"S", "S"},;   
								{ "98" ,"1008     " 	, "   " , "AGENCIAMENTO DE PUBLICIDADE E PROPAGANDA, INCLUSIVE O AGENCIAMENTO DE VEICULA��O POR QUAISQUER MEIOS." ,"S", "S"},;   
								{ "98" ,"1009     " 	, "   " , "REPRESENTA��O DE QUALQUER NATUREZA, INCLUSIVE COMERCIAL." ,"S", "S"},;   
								{ "98" ,"1010     " 	, "   " , "DISTRIBUI��O DE BENS DE TERCEIROS." ,"S", "S"},;   
								{ "98" ,"1101     " 	, "   " , "GUARDA E ESTACIONAMENTO DE VE�CULOS TERRESTRES AUTOMOTORES, DE AERONAVES E DE EMBARCA��ES." ,"S", "S"},;   
								{ "98" ,"1102     " 	, "   " , "VIGIL�NCIA, SEGURAN�A OU MONITORAMENTO DE BENS E PESSOAS." ,"S", "S"},;   
								{ "98" ,"1103     " 	, "   " , "ESCOLTA, INCLUSIVE DE VE�CULOS E CARGAS." ,"S", "S"},;   
								{ "98" ,"1104     " 	, "   " , "ARMAZENAMENTO, DEP�SITO, CARGA, DESCARGA, ARRUMA��O E GUARDA DE BENS DE QUALQUER ESP�CIE." ,"S", "S"},;   
								{ "98" ,"1201     " 	, "   " , "ESPET�CULOS TEATRAIS." ,"S", "S"},;   
								{ "98" ,"1202     " 	, "   " , "EXIBI��ES CINEMATOGR�FICAS." ,"S", "S"},;   
								{ "98" ,"1203     " 	, "   " , "ESPET�CULOS CIRCENSES." ,"S", "S"},;   
								{ "98" ,"1204     " 	, "   " , "PROGRAMAS DE AUDIT�RIO." ,"S", "S"},;   
								{ "98" ,"1205     " 	, "   " , "PARQUES DE DIVERS�ES, CENTROS DE LAZER E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1206     " 	, "   " , "BOATES, TAXI-DANCING E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1207     " 	, "   " , "SHOWS, BALLET, DAN�AS, DESFILES, BAILES, �PERAS, CONCERTOS, RECITAIS, FESTIVAIS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1208     " 	, "   " , "FEIRAS, EXPOSI��ES, CONGRESSOS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1209     " 	, "   " , "BILHARES, BOLICHES E DIVERS�ES ELETR�NICAS OU N�O." ,"S", "S"},;   
								{ "98" ,"1210     " 	, "   " , "CORRIDAS E COMPETI��ES DE ANIMAIS." ,"S", "S"},;   
								{ "98" ,"1211     "  	, "   " , "COMPETI��ES ESPORTIVAS OU DE DESTREZA F�SICA OU INTELECTUAL, COM OU SEM A PARTICIPA��O DO ESPECTADOR." ,"S", "S"},;   
								{ "98" ,"1212     " 	, "   " , "EXECU��O DE M�SICA." ,"S", "S"},;   
								{ "98" ,"1213     " 	, "   " , "PRODU��O, MEDIANTE OU SEM ENCOMENDA PR�VIA, DE EVENTOS, ESPET�CULOS, ENTREVISTAS, SHOWS, DAN�AS, DESFILES, TEATROS, CONCERTOS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1214     " 	, "   " , "FORNECIMENTO DE M�SICA PARA AMBIENTES FECHADOS OU N�O, MEDIANTE TRANSMISS�O POR QUALQUER PROCESSO." ,"S", "S"},;   
								{ "98" ,"1215     " 	, "   " , "DESFILES DE BLOCOS CARNAVALESCOS OU FOLCL�RICOS, TRIOS EL�TRICOS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1216     " 	, "   " , "EXIBI��O DE FILMES, ENTREVISTAS, MUSICAIS, ESPET�CULOS, CONCERTOS, DESFILES, COMPETI��ES ESPORTIVAS, DE DESTREZA INTELECTUAL OU CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1217     " 	, "   " , "RECREA��O E ANIMA��O, INCLUSIVE EM FESTAS E EVENTOS DE QUALQUER NATUREZA." ,"S", "S"},;   
								{ "98" ,"1302     " 	, "   " , "FONOGRAFIA OU GRAVA��O DE SONS, INCLUSIVE TRUCAGEM, DUBLAGEM, MIXAGEM E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1303     " 	, "   " , "FOTOGRAFIA E CINEMATOGRAFIA, INCLUSIVE REVELA��O, AMPLIA��O, C�PIA, REPRODU��O, TRUCAGEM E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1304     " 	, "   " , "REPROGRAFIA, MICROFILMAGEM E DIGITALIZA��O." ,"S", "S"},;   
								{ "98" ,"1305     " 	, "   " , "COMPOSI��O GR�FICA, FOTOCOMPOSI��O, CLICHERIA, ZINCOGRAFIA, LITOGRAFIA, FOTOLITOGRAFIA." ,"S", "S"},;   
								{ "98" ,"1401     " 	, "   " , "LUBRIFICA��O, LIMPEZA, LUSTRA��O, REVIS�O, RECARGA, CONSERTO, BLINDAGEM, MANUTEN��O DE M�QUINAS, VE�CULOS, ELEVADORES OU DE QUALQUER OBJETO." ,"S", "S"},;   
								{ "98" ,"1402     " 	, "   " , "ASSIST�NCIA T�CNICA." ,"S", "S"},;   
								{ "98" ,"1403     " 	, "   " , "RECONDICIONAMENTO DE MOTORES (EXCETO PE�AS E PARTES EMPREGADAS, QUE FICAM SUJEITAS AO ICMS)." ,"S", "S"},;   
								{ "98" ,"1404     " 	, "   " , "RECAUCHUTAGEM OU REGENERA��O DE PNEUS." ,"S", "S"},;   
								{ "98" ,"1405     " 	, "   " , "RESTAURA��O, RECONDICIONAMENTO, BENEFICIAMENTO, LAVAGEM, SECAGEM, TINGIMENTO, GALVANOPLASTIA, ANODIZA��O, CORTE, PLASTIFICA��O E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1406     " 	, "   " , "INSTALA��O E MONTAGEM DE APARELHOS, INCLUSIVE MONTAGEM INDUSTRIAL, PRESTADOS AO USU�RIO FINAL, EXCLUSIVAMENTE COM MATERIAL POR ELE FORNECIDO." ,"S", "S"},;   
								{ "98" ,"1407     " 	, "   " , "COLOCA��O DE MOLDURAS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1408     " 	, "   " , "ENCADERNA��O, GRAVA��O E DOURA��O DE LIVROS, REVISTAS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1409     " 	, "   " , "ALFAIATARIA E COSTURA, QUANDO O MATERIAL FOR FORNECIDO PELO USU�RIO FINAL, EXCETO AVIAMENTO." ,"S", "S"},;   
								{ "98" ,"1410     " 	, "   " , "TINTURARIA E LAVANDERIA." ,"S", "S"},;   
								{ "98" ,"1411     " 	, "   " , "TAPE�ARIA E REFORMA DE ESTOFAMENTOS EM GERAL." ,"S", "S"},;   
								{ "98" ,"1412     " 	, "   " , "FUNILARIA E LANTERNAGEM." ,"S", "S"},;   
								{ "98" ,"1413     " 	, "   " , "CARPINTARIA E SERRALHERIA." ,"S", "S"},;   
								{ "98" ,"1501     " 	, "   " , "ADMINISTRA��O DE FUNDOS QUAISQUER, DE CONS�RCIO, DE CART�O DE CR�DITO OU D�BITO E CONG�NERES, DE CARTEIRA DE CLIENTES E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1502     " 	, "   " , "ABERTURA DE CONTAS EM GERAL, INCLUSIVE CONTA-CORRENTE, CONTA DE INVESTIMENTOS E APLICA��O E CADERNETA DE POUPAN�A, NO PA�S E NO EXTERIOR." ,"S", "S"},;   
								{ "98" ,"1503     " 	, "   " , "LOCA��O E MANUTEN��O DE COFRES PARTICULARES, DE TERMINAIS ELETR�NICOS, DE TERMINAIS DE ATENDIMENTO E DE BENS E EQUIPAMENTOS EM GERAL." ,"S", "S"},;   
								{ "98" ,"1504     " 	, "   " , "FORNECIMENTO OU EMISS�O DE ATESTADOS EM GERAL, INCLUSIVE ATESTADO DE IDONEIDADE, ATESTADO DE CAPACIDADE FINANCEIRA E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1505     " 	, "   " , "CADASTRO, RENOVA��O CADASTRAL E CONG�NERES, INCLUS�O OU EXCLUS�O NO CADASTRO DE EMITENTES DE CHEQUES SEM FUNDOS CCF OU EM QUAISQUER OUTROS." ,"S", "S"},;   
								{ "98" ,"1506     " 	, "   " , "FORNECIMENTO DE DOCUMENTOS EM GERAL; ABONO DE FIRMAS; COLETA E ENTREGA DE VALORES; LICENCIAMENTO ELETR�NICO DE VE�CULOS; TRANSFER�NCIA DE VE�CULOS." ,"S", "S"},;   
								{ "98" ,"1507     " 	, "   " , "ATENDIMENTO E CONSULTA A CONTAS EM GERAL, POR QUALQUER MEIO OU PROCESSO, FORNECIMENTO DE SALDO, EXTRATO E DEMAIS INFORMA��ES RELATIVAS A CONTAS." ,"S", "S"},;   
								{ "98" ,"1508     " 	, "   " , "EMISS�O, REEMISS�O, ALTERA��O, CESS�O, SUBSTITUI��O, CANCELAMENTO E REGISTRO DE CONTRATO DE CR�DITO; ESTUDO, AN�LISE DE OPERA��ES DE CR�DITO." ,"S", "S"},;   
								{ "98" ,"1509     " 	, "   " , "LEASING DE QUAISQUER BENS, INCLUSIVE CESS�O DE DIREITOS E OBRIGA��ES, ALTERA��O DE CONTRATO, E DEMAIS SERVI�OS RELACIONADOS AO LEASING." ,"S", "S"},;   
								{ "98" ,"1510     " 	, "   " , "SERVI�OS RELACIONADOS A RECEBIMENTOS OU PAGAMENTOS EM GERAL, DE T�TULOS QUAISQUER." ,"S", "S"},;   
								{ "98" ,"1511     " 	, "   " , "DEVOLU��O DE T�TULOS, PROTESTO DE T�TULOS, SUSTA��O DE PROTESTO, MANUTEN��O DE T�TULOS, REAPRESENTA��O DE T�TULOS." ,"S", "S"},;   
								{ "98" ,"1512     " 	, "   " , "CUST�DIA EM GERAL, INCLUSIVE DE T�TULOS E VALORES MOBILI�RIOS." ,"S", "S"},;   
								{ "98" ,"1513     " 	, "   " , "SERVI�OS RELACIONADOS A OPERA��ES DE C�MBIO EM GERAL." ,"S", "S"},;   
								{ "98" ,"1514     " 	, "   " , "FORNECIMENTO, EMISS�O, REEMISS�O E MANUTEN��O DE CART�O MAGN�TICO, CART�O DE CR�DITO, CART�O DE D�BITO, CART�O SAL�RIO E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1515     " 	, "   " , "COMPENSA��O DE CHEQUES E T�TULOS QUAISQUER; SERVI�OS RELACIONADOS A DEP�SITO, INCLUSIVE DEP�SITO IDENTIFICADO." ,"S", "S"},;   
								{ "98" ,"1516     " 	, "   " , "EMISS�O, REEMISS�O, LIQUIDA��O, ALTERA��O, CANCELAMENTO E BAIXA DE ORDENS DE PAGAMENTO, ORDENS DE CR�DITO E SIMILARES." ,"S", "S"},;   
								{ "98" ,"1517     " 	, "   " , "EMISS�O, FORNECIMENTO, DEVOLU��O, SUSTA��O, CANCELAMENTO E OPOSI��O DE CHEQUES QUAISQUER, AVULSO OU POR TAL�O." ,"S", "S"},;   
								{ "98" ,"1518     " 	, "   " , "SERVI�OS RELACIONADOS A CR�DITO IMOBILI�RIO, AVALIA��O E VISTORIA DE IM�VEL OU OBRA." ,"S", "S"},;   
								{ "98" ,"1601     " 	, "   " , "SERVI�OS DE TRANSPORTE DE NATUREZA MUNICIPAL." ,"S", "S"},;   
								{ "98" ,"1701     " 	, "   " , "CONSULTORIA DE QUALQUER NATUREZA, N�O CONTIDA EM OUTROS ITENS DESTA LISTA; AN�LISE, PESQUISA, COMPILA��O E FORNECIMENTO DE DADOS E INFORMA��ES." ,"S", "S"},;   
								{ "98" ,"1702     " 	, "   " , "DIGITA��O, ESTENOGRAFIA, EXPEDIENTE, SECRETARIA EM GERAL, RESPOSTA AUD�VEL, REDA��O, EDI��O, INTERPRETA��O, REVIS�O, TRADU��O E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1703     " 	, "   " , "PLANEJAMENTO, COORDENA��O, PROGRAMA��O OU ORGANIZA��O T�CNICA, FINANCEIRA OU ADMINISTRATIVA." ,"S", "S"},;   
								{ "98" ,"1704     " 	, "   " , "RECRUTAMENTO, AGENCIAMENTO, SELE��O E COLOCA��O DE M�O-DE-OBRA." ,"S", "S"},;   
								{ "98" ,"1705     " 	, "   " , "FORNECIMENTO DE M�O-DE-OBRA, MESMO EM CAR�TER TEMPOR�RIO, INCLUSIVE DE EMPREGADOS, AVULSOS OU TEMPOR�RIOS, CONTRATADOS PELO PRESTADOR DE SERVI�O." ,"S", "S"},;   
								{ "98" ,"1706     " 	, "   " , "PROPAGANDA E PUBLICIDADE, PLANEJAMENTO DE CAMPANHAS OU SISTEMAS DE PUBLICIDADE, ELABORA��O DE DEMAIS MATERIAIS PUBLICIT�RIOS." ,"S", "S"},;   
								{ "98" ,"1708     " 	, "   " , "FRANQUIA (FRANCHISING)." ,"S", "S"},;   
								{ "98" ,"1709     " 	, "   " , "PER�CIAS, LAUDOS, EXAMES T�CNICOS E AN�LISES T�CNICAS." ,"S", "S"},;   
								{ "98" ,"1710     " 	, "   " , "PLANEJAMENTO, ORGANIZA��O E ADMINISTRA��O DE FEIRAS, EXPOSI��ES, CONGRESSOS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1711     " 	, "   " , "ORGANIZA��O DE FESTAS E RECEP��ES; BUF� (EXCETO O FORNECIMENTO DE ALIMENTA��O E BEBIDAS, QUE FICA SUJEITO AO ICMS)." ,"S", "S"},;   
								{ "98" ,"1712     " 	, "   " , "ADMINISTRA��O EM GERAL, INCLUSIVE DE BENS E NEG�CIOS DE TERCEIROS." ,"S", "S"},;   
								{ "98" ,"1713     " 	, "   " , "LEIL�O E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1714     " 	, "   " , "ADVOCACIA." ,"S", "S"},;   
								{ "98" ,"1715     " 	, "   " , "ARBITRAGEM DE QUALQUER ESP�CIE, INCLUSIVE JUR�DICA." ,"S", "S"},;   
								{ "98" ,"1716     " 	, "   " , "AUDITORIA." ,"S", "S"},;   
								{ "98" ,"1717     " 	, "   " , "AN�LISE DE ORGANIZA��O E M�TODOS." ,"S", "S"},;   
								{ "98" ,"1718     " 	, "   " , "ATU�RIA E C�LCULOS T�CNICOS DE QUALQUER NATUREZA." ,"S", "S"},;   
								{ "98" ,"1719     " 	, "   " , "CONTABILIDADE, INCLUSIVE SERVI�OS T�CNICOS E AUXILIARES." ,"S", "S"},;   
								{ "98" ,"1720     " 	, "   " , "CONSULTORIA E ASSESSORIA ECON�MICA OU FINANCEIRA." ,"S", "S"},;   
								{ "98" ,"1721     " 	, "   " , "ESTAT�STICA." ,"S", "S"},;   
								{ "98" ,"1722     " 	, "   " , "COBRAN�A EM GERAL." ,"S", "S"},;   
								{ "98" ,"1723     " 	, "   " , "ASSESSORIA, AN�LISE, CONSULTA, CADASTRO, GERENCIAMENTO DE INFORMA��ES, ADMINISTRA��O DE CONTAS A RECEBER OU A PAGAR, RELACIONADOS A FACTORING." ,"S", "S"},;   
								{ "98" ,"1724     " 	, "   " , "APRESENTA��O DE PALESTRAS, CONFER�NCIAS, SEMIN�RIOS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1801     " 	, "   " , "SERVI�OS DE REGULA��O DE SINISTROS VINCULADOS A CONTRATOS DE SEGUROS; AVALIA��O DE RISCOS PARA COBERTURA DE CONTRATOS DE SEGUROS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"1901     " 	, "   " , "SERVI�OS DE DISTRIBUI��O E VENDA DE BILHETES E DEMAIS PRODUTOS DE LOTERIA, BINGOS, CART�ES, PULES OU CUPONS DE APOSTAS, SORTEIOS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"2001     " 	, "   " , "SERVI�OS PORTU�RIOS, FERROPORTU�RIOS, SERVI�OS DE PRATICAGEM, CAPATAZIA, ARMAZENAGEM DE QUALQUER NATUREZA E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"2002     " 	, "   " , "SERVI�OS AEROPORTU�RIOS, UTILIZA��O DE AEROPORTO, ARMAZENAGEM DE QUALQUER NATUREZA, CAPATAZIA,  SERVI�OS ACESS�RIOS, LOG�STICA E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"2003     " 	, "   " , "SERVI�OS DE TERMINAIS RODOVI�RIOS, FERROVI�RIOS, METROVI�RIOS, LOG�STICA E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"2101     " 	, "   " , "SERVI�OS DE REGISTROS P�BLICOS, CARTOR�RIOS E NOTARIAIS." ,"S", "S"},;   
								{ "98" ,"2201     " 	, "   " , "SERVI�OS DE EXPLORA��O DE RODOVIA MEDIANTE COBRAN�A DE PRE�O OU PED�GIO DOS USU�RIOS. " ,"S", "S"},;   
								{ "98" ,"2301     " 	, "   " , "SERVI�OS DE PROGRAMA��O E COMUNICA��O VISUAL, DESENHO INDUSTRIAL E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"2401     " 	, "   " , "SERVI�OS DE CHAVEIROS, CONFEC��O DE CARIMBOS, PLACAS, SINALIZA��O VISUAL, BANNERS, ADESIVOS E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"2501     " 	, "   " , "FUNERAIS, INCLUSIVE FORNECIMENTO DE CAIX�O; ALUGUEL DE CAPELA; TRANSPORTE DO CORPO CADAV�RICO; FORNECIMENTO DE FLORES E OUTROS PARAMENTOS." ,"S", "S"},;   
								{ "98" ,"2502     " 	, "   " , "CREMA��O DE CORPOS E PARTES DE CORPOS CADAV�RICOS." ,"S", "S"},;   
								{ "98" ,"2503     " 	, "   " , "PLANOS OU CONV�NIO FUNER�RIOS." ,"S", "S"},;   
								{ "98" ,"2504     " 	, "   " , "MANUTEN��O E CONSERVA��O DE JAZIGOS E CEMIT�RIOS." ,"S", "S"},;   
								{ "98" ,"2601     " 	, "   " , "SERVI�OS DE COLETA, REMESSA OU ENTREGA DE CORRESPOND�NCIAS, DOCUMENTOS, OBJETOS, BENS OU VALORES; COURRIER E CONG�NERES." ,"S", "S"},;
								{ "98" ,"2701     " 	, "   " , "SERVI�OS DE ASSIST�NCIA SOCIAL." ,"S", "S"},;   
								{ "98" ,"2801     " 	, "   " , "SERVI�OS DE AVALIA��O DE BENS E SERVI�OS DE QUALQUER NATUREZA." ,"S", "S"},;   
								{ "98" ,"2901     " 	, "   " , "SERVI�OS DE BIBLIOTECONOMIA." ,"S", "S"},;   
								{ "98" ,"3001     " 	, "   " , "SERVI�OS DE BIOLOGIA, BIOTECNOLOGIA E QU�MICA." ,"S", "S"},;   
								{ "98" ,"3101     " 	, "   " , "SERVI�OS T�CNICOS EM EDIFICA��ES, ELETR�NICA, ELETROT�CNICA, MEC�NICA, TELECOMUNICA��ES E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"3201     " 	, "   " , "SERVI�OS DE DESENHOS T�CNICOS." ,"S", "S"},;   
								{ "98" ,"3301     " 	, "   " , "SERVI�OS DE DESEMBARA�O ADUANEIRO, COMISS�RIOS, DESPACHANTES E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"3401     " 	, "   " , "SERVI�OS DE INVESTIGA��ES PARTICULARES, DETETIVES E CONG�NERES." ,"S", "S"},;   
								{ "98" ,"3501     " 	, "   " , "SERVI�OS DE REPORTAGEM, ASSESSORIA DE IMPRENSA, JORNALISMO E RELA��ES P�BLICAS." ,"S", "S"},;   
								{ "98" ,"3601     " 	, "   " , "SERVI�OS DE METEOROLOGIA." ,"S", "S"},;   
								{ "98" ,"3701     " 	, "   " , "SERVI�OS DE ARTISTAS, ATLETAS, MODELOS E MANEQUINS." ,"S", "S"},;   
								{ "98" ,"3801     " 	, "   " , "SERVI�OS DE MUSEOLOGIA." ,"S", "S"},;   
								{ "98" ,"3901     " 	, "   " , "SERVI�OS DE OURIVESARIA E LAPIDA��O (QUANDO O MATERIAL FOR FORNECIDO PELO TOMADOR DO SERVI�O)." ,"S", "S"},;   
								{ "98" ,"4001     " 	, "   " , "OBRAS DE ARTE SOB ENCOMENDA." ,"S", "S"},;   
								{ "99" ,"01       " 	, "   " , "OPERACAO TRIBUTAVEL (BASE DE CALCULO = VALOR DA OPERACAO ALIQUOTA NORMAL (CUMULATIVO/NAO CUMULATIVO))" ,"S", "N"},;
								{ "99" ,"02       " 	, "   " , "OPERACAO TRIBUTAVEL (BASE DE CALCULO = VALOR DA OPERACAO (ALIQUOTA DIFERENCIADA))","S", "N" },;
								{ "99" ,"03       " 	, "   " , "OPERACAO TRIBUTAVEL (BASE DE CALCULO = QUANTIDADE VENDIDA X ALIQUOTA POR UNIDADE DE PRODUTO)" ,"S", "N"},;
								{ "99" ,"04       " 	, "   " , "OPERACAO TRIBUTAVEL (TRIBUTACAO MONOFASICA (ALIQUOTA ZERO))" ,"S", "N"},;
								{ "99" ,"06       " 	, "   " , "OPERACAO TRIBUTAVEL (ALIQUOTA ZERO)" ,"S", "N"},;
								{ "99" ,"07       " 	, "   " , "OPERACAO ISENTA DA CONTRIBUICAO" ,"S", "N"},;
								{ "99" ,"08       " 	, "   " , "OPERACAO SEM INCIDENCIA DA CONTRIBUICAO" ,"S", "N"},;
								{ "99" ,"09       " 	, "   " , "OPERACAO COM SUSPENSAO DA CONTRIBUICAO" ,"S", "N"},;
								{ "99" ,"49       " 	, "   " , "OUTRAS OPERACOES DE SAIDA" ,"S", "N"},;
								{ "99" ,"50       " 	, "   " , "OPERACAO COM DIREITO A CREDITO - VINCULADA EXCLUSIVAMENTE A RECEITA TRIBUTADA NO MERCADO INTERNO" ,"S", "N"},;
								{ "99" ,"51       " 	, "   " , "OPERACAO COM DIREITO A CREDITO - VINCULADA EXCLUSIVAMENTE A RECEITA NAO TRIBUTADA NO MERCADO INTERNO" ,"S", "N"},;
								{ "99" ,"52       " 	, "   " , "OPERACAO COM DIREITO A CREDITO - VINCULADA EXCLUSIVAMENTE A RECEITA DE EXPORTACAO" ,"S", "N"},;
								{ "99" ,"53       " 	, "   " , "OPERACAO COM DIREITO A CREDITO - VINCULADA A RECEITAS TRIBUTADAS E NAO-TRIBUTADAS NO MERCADO INTERNO" ,"S", "N"},;
								{ "99" ,"54       " 	, "   " , "OPERACAO COM DIREITO A CREDITO - VINCULADA A RECEITAS TRIBUTADAS NO MERCADO INTERNO E DE EXPORTACAO" ,"S", "N"},;
								{ "99" ,"55       " 	, "   " , "OPERACAO COM DIREITO A CREDITO - VINCULADA A RECEITAS NAO-TRIBUTADAS NO MERCADO INTERNO E DE EXPORTACAO" ,"S", "N"},;
								{ "99" ,"56       " 	, "   " , "OPERACAO COM DIREITO A CREDITO - VINCULADA A RECEITAS TRIBUTADAS E NAO-TRIBUTADAS NO MERCADO INTERNO,E DE EXPORTACAO" ,"S", "N"},;
								{ "99" ,"60       " 	, "   " , "CREDITO PRESUMIDO - OPERACAO DE AQUISICAO VINCULADA EXCLUSIVAMENTE A RECEITA TRIBUTADA NO MERCADO INTERNO" ,"S", "N"},;
								{ "99" ,"61       " 	, "   " , "CREDITO PRESUMIDO - OPERACAO DE AQUISICAO VINCULADA EXCLUSIVAMENTE A RECEITA N�O-TRIBUTADA NO MERCADO INTERNO" ,"S", "N"},;
								{ "99" ,"62       " 	, "   " , "CREDITO PRESUMIDO - OPERACAO DE AQUISICAO VINCULADA EXCLUSIVAMENTE A RECEITA DE EXPORTACAO" ,"S", "N"},;
								{ "99" ,"63       " 	, "   " , "CREDITO PRESUMIDO - OPERACAO DE AQUISICAO VINCULADA A RECEITAS TRIBUTADAS E NAO-TRIBUTADAS NO MERCADO INTERNO" ,"S", "N"},;
								{ "99" ,"64       " 	, "   " , "CREDITO PRESUMIDO - OPERACAO DE AQUISICAO VINCULADA A RECEITAS TRIBUTADAS NO MERCADO INTERNO E DE EXPORTACAO" ,"S", "N"},;
								{ "99" ,"65       " 	, "   " , "CREDITO PRESUMIDO - OPERACAO DE AQUISICAO VINCULADA A RECEITAS NAO-TRIBUTADAS NO MERCADO INTERNO E DE EXPORTACAO" ,"S", "N"},;
								{ "99" ,"66       " 	, "   " , "CREDITO PRESUMIDO - OPERACAO DE AQUISICAO VINCULADA A RECEITAS TRIBUTADAS E NAO-TRIBUTADAS NO MERCADO INTERNO, E DE EXPORTACAO" ,"S", "N"},;
								{ "99" ,"67       " 	, "   " , "CREDITO PRESUMIDO - OUTRAS OPERACAES" ,"S", "N"},;
								{ "99" ,"70       " 	, "   " , "OPERACAO DE AQUISICAO SEM DIREITO A CREDITO" ,"S", "N"},;
								{ "99" ,"71       " 	, "   " , "OPERACAO DE AQUISICAO COM ISENCAO" ,"S", "N"},;
								{ "99" ,"72       " 	, "   " , "OPERACAO DE AQUISICAO COM SUSPENSAO" ,"S", "N"},;
								{ "99" ,"73       " 	, "   " , "OPERACAO DE AQUISICAO A ALIQUOTA ZERO" ,"S", "N"},;
								{ "99" ,"74       " 	, "   " , "OPERACAO DE AQUISICAO SEM INCIDENCIA DA CONTRIBUICAO" ,"S", "N"},;																																																																								
								{ "99" ,"75       " 	, "   " , "OPERACAO DE AQUISICAO POR SUBSTITUICAO TRIBUTARIA" ,"S", "N"},;	
								{ "99" ,"98       " 	, "   " , "OUTRAS OPERACOES DE ENTRADA" ,"S", "N"},;	                                                                                                                   	                                                                                                                       	
								{ "99" ,"99       " 	, "   " , "OUTRAS OPERACOES" ,"S", "N"},;
								{ "DJ" , "01" , "" , "VENDA DE MERCADORIA" , "S" , "S"},;
								{ "DJ" , "02" , "" , "SIMPLES REMESSA DE MATERIAL" , "S" , "S"},;
								{ "DJ" , "03" , "" , "VENDA PARA CONSUMIDOR FINAL" , "S" , "S"},;
								{ "DJ" , "04" , "" , "BONIFICACAO" , "S" , "S"},;
								{ "DJ" , "05" , "" , "TRANSFERENCIA DE IMPOSTO - VENDA" , "S" , "S"},;
								{ "DJ" , "06" , "" , "CONSIGNACAO MERCANTIL" , "S" , "S"},;
								{ "DJ" , "07" , "" , "PRESTACAO DE SERVICO" , "S" , "S"},;
								{ "DJ" , "1" , "" , "*USO RESERVADO DISTRIBUTION*" , "S" , "S"},;
								{ "DJ" , "2" , "" , "FATURADA" , "S" , "S"},;
								{ "DJ" , "3" , "" , "NOTA ASSINADA" , "S" , "S"},;
								{ "DJ" , "4" , "" , "CORTESIA" , "S" , "S"},;
								{ "DJ" , "5" , "" , "SIMPLES REMESSA" , "S" , "S"},;
								{ "DJ" , "51" , "" , "AQUISICAO DE MERCADORIA" , "S" , "S"},;
								{ "DJ" , "52" , "" , "RETORNO DE SIMPLES REMESSA DE MATERIAL" , "S" , "S"},;
								{ "DJ" , "53" , "" , "AQUISICAO DE CONSUMIDOR FINAL" , "S" , "S"},;
								{ "DJ" , "54" , "" , "DEVOLUCAO DE BONIFICACAO" , "S" , "S"},;
								{ "DJ" , "55" , "" , "TRANSFERENCIA DE IMPOSTO - AQUISICAO" , "S" , "S"},;
								{ "DJ" , "56" , "" , "RETORNO DE CONSIGNACAO MERCANTIL" , "S" , "S"},;
								{ "DJ" , "6" , "" , "TROCA" , "S" , "S"},;
								{ "DJ" , "7" , "" , "GRATIS" , "S" , "S"},;
								{ "DJ" , "8" , "" , "EMPRESTIMOS" , "S" , "S"},;
								{ "DJ" , "9" , "" , "CONSIGNACAO" , "S" , "S"},;
								{ "DJ" , "A" , "" , "REMESSA PARA CONSERTO" , "S" , "S"},;
								{ "DJ" , "B" , "" , "COMODATO" , "S" , "S"},;
								{ "DJ" , "C" , "" , "RETORNO DE EMPRESTIMO" , "S" , "S"},;
								{ "DJ" , "D" , "" , "CONSUMO INTERNO" , "S" , "S"},;
								{ "DJ" , "F" , "" , "TRANSFERENCIA DE PRODUTOS" , "S" , "S"},;
								{ "DJ" , "G" , "" , "DEVOLUCAO DE CONSIGNACAO" , "S" , "S"},;
								{ "DJ" , "H" , "" , "ACERTOS" , "S" , "S"},;
								{ "DJ" , "L" , "" , "REM. P/VENDA FORA ESTABELECIMENTO" , "S" , "S"},;
								{ "DJ" , "M" , "" , "VENDA EFETUADA FORA DA ESTABELECIMENTO" , "S" , "S"},;
								{ "DJ" , "O" , "" , "OUTROS" , "S" , "S"},;
								{ "DJ" , "Q" , "" , "PARA IDENTIFICACAO AS QUEBRAS" , "S" , "S"},;
								{ "DJ" , "R" , "" , "REMESSA GRATIS" , "S" , "S"},;
								{ "DJ" , "T1" , "" , "ICMS Normal" , "S" , "S"},;
								{ "DJ" , "T2" , "" , "Isento" , "S" , "S"},;
								{ "DJ" , "T3" , "" , "Substituicao Tributaria" , "S" , "S"},;
								{ "DJ" , "T4" , "" , "ISS" , "S" , "S"},;
								{ "DJ" , "U" , "" , "PARA IDENIFICACAO DE VENDAS FUTURAS" , "S" , "S"},;
								{ "DJ" , "V" , "" , "PARA IDENTIFICACAO DE ENTREGAS FUTURAS" , "S" , "S"},;
								{ "DJ" , "W" , "" , "REM. PROMOCIONAL" , "S" , "S"}}
                                                                           
nARegs := Len( aRegs )

SX5->(dbSetOrder(1))

If SmExistVer( "20110311" )
	FOR nI := 1 TO nARegs
		IF !SX5->(dbSeek(xFilial("SX5")+aRegs[nI][1]+SubStr(aRegs[nI][2],1,5)+aRegs[nI][3]))
			If !SX5->(dbSeek(xFilial("SX5")+aRegs[nI][1]+aRegs[nI][2]+aRegs[nI][3]))
				RecLock('SX5',.T.)
				SX5->X5_FILIAL	:= xFilial("SX5")
				SX5->X5_TABELA	:= aRegs[nI][1]
				SX5->X5_CHAVE	:= aRegs[nI][2]
				SX5->X5_ITEM	:= aRegs[nI][3]
				SX5->X5_DESCRI	:= aRegs[nI][4]  
				If Type("SX5->X5_TIPO") <> "U"
					SX5->X5_TIPO	:= aRegs[nI][5]
					SX5->X5_LIBINC	:= aRegs[nI][6]				
				EndIf
				MsUnlock()
			EndIf
		ENDIF
	NEXT nI		
EndIf
Return .T.
            

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
��� Programa  � ChkRem     � Autor �Fernando Machima � Data �  12/03/02   ���
�������������������������������������������������������������������������͹��
��� Descricao � Altera a nomenclatura do remito de acordo com o pais      ���
�������������������������������������������������������������������������͹��
��� Sintaxe   � ChkRem(nTipo)             							      ���
�������������������������������������������������������������������������͹��
��� Retorno   � Nome do remito de acordo com pais                         ���
�������������������������������������������������������������������������͹��
��� Uso       � SMALLERP                         						  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Function ChkRem(nTipo)

Local cNome   := STR0081   //"Remito"									   

If CheckSDic()  //Verifica se o dicionario foi carregado
   If nTipo == 1  
      cNome  := FieldDIC("D2_REMITO",SD_FTITLE,"SD2")
   ElseIf nTipo == 2   //Devolucao
      cNome  := STR0082+FieldDIC("D2_REMITO",SD_FTITLE,"SD2")   //"Devolucao "									      
   ElseIf nTipo == 3   //Rel. Remito Entrada
      cNome  := STR0099+FieldDIC("D1_REMITO",SD_FTITLE,"SD1")+STR0100
   ElseIf nTipo == 4   //Rel. Remito Saida
      cNome  := STR0099+FieldDIC("D2_REMITO",SD_FTITLE,"SD2")+STR0101      
   ElseIf nTipo == 5   //Impressao de Remito
      cNome  := STR0113+FieldDIC("D2_REMITO",SD_FTITLE,"SD2")
   EndIf   
EndIf

Return cNome

Function GetAccessList()

If aAcessos == Nil
	aAcessos := {}
	//Se est� com novo patch de lib
	If FindFunction("FWPoliceLoad")
		Aadd(aAcessos,{001, STR0149, "S"})	//"Alterar Data-base"
		Aadd(aAcessos,{002, "Controle de Cr�dito", "N"})	//"Controle de Cr�dito"
		Aadd(aAcessos,{003, "Visualiza Indicadores de Gest�o", "N"})	//"Apresenta Painel"
		Aadd(aAcessos,{004, "Visualiza Transa��es Programadas", "N"})	//"Apresenta Transacoes"	
		Aadd(aAcessos,{005, "Libera��o de Or�amento", "N"})	//"Libera��o de Or�amento"
		Aadd(aAcessos,{006, "Libera��o de Pedido de Compra", "N"})
		Aadd(aAcessos,{007, "Libera��o de Contas a Pagar", "N"})
		Aadd(aAcessos,{008, "Altera Lan�amento Cont�bil", "N"})
		Aadd(aAcessos,{009, "Ordem de Servi�o Full", "N"})
		Aadd(aAcessos,{010, "Inclui Ordem de servi�o", "N"})
		Aadd(aAcessos,{011, "Altera Ordem de servi�o", "N"})
		Aadd(aAcessos,{012, "Exclui Ordem de servi�o", "N"})
		Aadd(aAcessos,{013, "Inclui Apontamentos", "N"})
		Aadd(aAcessos,{014, "Exclui Apontamentos", "N"})
		Aadd(aAcessos,{015, "Faturar Pedido de Venda", "S"})
		Aadd(aAcessos,{016, "Limite M�ximo de Desconto", "N"})//"Limite m�ximo de Desconto	
		Aadd(aAcessos,{017, "Libera��o de Pedido de Vendas", "S"})
	Else
		AcsList_()
	EndIf
EndIf
Return aAcessos

Static Function AcsList_()
	Aadd(aAcessos,{.T.,STR0149})	//"Alterar Data-base"
  	If FindFunction("FWGetProduct") .And. !(FWGetProduct() $ "RM|UO")
    	Aadd(aAcessos,{.T.,STR0168})	//"Alterar Cadastros"
    EndIf
	Aadd(aAcessos,{.F.,"Controle de Cr�dito"})	//"Controle de Cr�dito"
	Aadd(aAcessos,{.F.,"Visualiza Indicadores de Gest�o"})	//"Apresenta Painel"
	Aadd(aAcessos,{.F.,"Visualiza Transa��es Programadas"})	//"Apresenta Transacoes"	
	Aadd(aAcessos,{.T.,"Libera��o de Or�amento"})	//"Libera��o de Or�amento"
	Aadd(aAcessos,{.F.,"Libera��o de Pedido de Compra"})
	Aadd(aAcessos,{.F.,"Libera��o de Contas a Pagar"})
	Aadd(aAcessos,{.F.,"Altera Lan�amento Cont�bil"})
	Aadd(aAcessos,{.F.,"Ordem de Servi�o Full"})
	Aadd(aAcessos,{.F.,"Inclui Ordem de servi�o"})
	Aadd(aAcessos,{.F.,"Altera Ordem de servi�o"})
	Aadd(aAcessos,{.F.,"Exclui Ordem de servi�o"})
	Aadd(aAcessos,{.F.,"Inclui Apontamentos"})
	Aadd(aAcessos,{.F.,"Exclui Apontamentos"})
	Aadd(aAcessos,{.T.,"Faturar Pedido de Venda"})
	Aadd(aAcessos,{.F.,"Limite M�ximo de Desconto"})//"Limite m�ximo de Desconto	
	Aadd(aAcessos,{.T.,"Libera��o de Pedido de Vendas"})
Return aAcessos

Function VerSenha(nOper)

If nOper > 0
	Return Subs(cAcesso,nOper,1) == "S"
Else
	Return .F.
EndIf

Static aModName := {}

Function RetModName()

If Len(aModName) == 0
	//Se est� com novo patch de lib
	If FindFunction("FWPoliceLoad")
		Aadd(aModName,{1, "CAD", "Cadastro", .T., "", 0, "", 1, .T., "010"})	//"FIRST"
		Aadd(aModName,{2, "COM", STR0014, .T., "", 0, "", 1, .T., "020"})	//"Compras"
		Aadd(aModName,{3, "EST", STR0026, .T., "", 0, "", 1, .T., "030"})	//"Estoque"
		Aadd(aModName,{4, "FAT", STR0038, .T., "", 0, "", 1, .T., "040"})	//"Faturamento"
		Aadd(aModName,{5, "FIN", STR0039, .T., "", 0, "", 1, .T., "050"})	//"Financeiro"
		Aadd(aModName,{6, "TOOLS", STR0146, .T., "", 0, "", 1, .T., "060"})	//"Ferramentas"
		Aadd(aModName,{7, "MSER1", "Meu Fly01", .T., "", 0, "", 1, .T., "065"})	//"Meu Fly01"
		Aadd(aModName,{8, "MISCE", "Miscel�nea", .T., "", 0, "", 1, .T., "070"})	//"Miscel�nea"
	Else
		Aadd(aModName,{"COM",STR0014})	//"Compras"
		Aadd(aModName,{"EST",STR0026})	//"Estoque"
		Aadd(aModName,{"FAT",STR0038})	//"Faturamento"
		Aadd(aModName,{"FIN",STR0039})	//"Financeiro"
		Aadd(aModName,{"TOOLS",STR0146})	//"Ferramentas"
	EndIf
EndIf

Return aModName

Function SmRetMods(cKey, nIndex)
Local aMName := RetModName()
Local aMenus := {}
Local nI

Default nIndex := 2
cKey := Upper(Alltrim(cKey))

If nIndex == 2
	If Right(cKey, 4) == ".XNU"
		cKey := Left(cKey, Len(cKey) - 4)
	EndIf
EndIf

nI := aScan(aMName, {|aLine| aLine[nIndex] == cKey})

If nI > 0
	aAdd(aMenus, {aMName[nI, 10], aMName[nI, 2], aMName[nI, 1], aMName[nI, 3]})
EndIf

Return aMenus

Function GetMainMenu()
Return __aMainMenu

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o    � Numautp  � Autor �Wilson Jorge Tedokon   � Data � 14-01-2013 ���
���������������������������������������������������������������������������Ĵ��
���Descri��o � Verifica o valor do parametro MV_NUMAUTP - S/N               ���
���������������������������������������������������������������������������Ĵ��
��� Uso      � Generico                                                     ���
���������������������������������������������������������������������������Ĵ��
��� ATUALIZACOES SOFRIDAS DESDE A CONSTRUCAO INICIAL.                       ���
���������������������������������������������������������������������������Ĵ��
��� PROGRAMADOR  � DATA   � BOPS �  MOTIVO DA ALTERACAO                     ���
���������������������������������������������������������������������������Ĵ��
���              �        �      �                                          ���
����������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
*/
Static Function Numautp()
Local cRet       := "S"
Local aArea

If !SmExistVer( "20150523" )
	Processa({|| SmAuxChkVer({"SA1"},"20150523")},"Processando atualiza��es")
EndIf

aArea := SA1->( GetArea() )

DbSelectArea("SA1")

If RecCount() > 0
	cRet := "N" 
EndIf

RestArea( aArea )

Return cRet

//-------------------------------------------------------------------
/*/{Protheus.doc} OpenChat
Abre uma tela do browser padr�o com o chat.

@author Jefferson Dias
@since 31/01/2017
@version 2.0
/*/
//-------------------------------------------------------------------
Function OpenChat()
Local url := "http://suporte.fly01.com.br/chat/index.html?name=" + Trim(cUsuario()) + "&email=&product=Fly01 Manufatura" 
ShellExecute("open", url,"","",SW_SHOWNORMAL)
Return

/*/{Protheus.doc} SmRmtAsist
Abre uma tela para iniciar sess�o remota

@author thiago.santos
@since 03/10/2014
@version 1.0
/*/
Function SmRmtAsist(lPanel)
Local cUrl := "https://secure.logmeinrescue.com/customer/code.aspx"
Local oPanel
Default lPanel := .T.

If lPanel
	oPanel := SmPanelUtil():New("Assist�ncia remota")

oPanel:AddLayerC("BROWSER", 100)
	oPanel:AddIBrowser("IBROWSER", "BROWSER", cUrl)
OPanel:Activate()
Else
	ShellExecute("open", cUrl, "", "", SW_SHOWNORMAL)
EndIf

Return

Function SMIMPNFFAT()

Return U_IMPRNF()

Function Smtstpdv()
Local lIntPDV    := SmPDV()
Local oLoggerPDV := Logger():New()

DbSelectArea("SB2") 
dbSetOrder(1)      		
cFilter:=	"B2_FILIAL=='"+xFilial("SB2")+"'"
SB2->( dbSetFilter({|| &cFilter },cFilter)  ) 
SB2->( dbGoTop() )

Do While SB2->( !Eof() ) .And. lIntPDV
	If SB2->B2_LOCAL == Posicione("SB1",1,SB2->B2_CODPROD,"B1_LOCAL")
		cIntegra := Posicione("SB1",1,SB2->B2_CODPROD,"B1_IPDV")
	Else
		cIntegra := ""
	EndIf
	
	If !Empty(cIntegra) .Or. cIntegra == "1"
		RecLock("SB2")
		SB2->B2_MSEXP := ""
		SB2->B2_QESTINT := 0
		SB2->(MsUnLock())
	EndIf
	
	dbSkip()
EndDo

SmMsgRun(, {|| SmIntegEst(,, .F., @oLoggerPDV) }, "Por favor aguarde. O envio do estoque est� sendo executado...")

If !oLoggerPDV:IsEmpty()
	oLoggerPDV:Show()
	oLoggerPDV:Finalize()
EndIf

Return

Function SmIsSaaS()

Return (AllTrim(GetPvProfString("Environment","SCOPE","",GetADV97())) == "TOTVS_SAAS")


/*/{Protheus.doc} UrlMshpFIRST
Retorna qual URL a busca do MASHUP deve utilizar

@author mateus.barbosa
@since 10/05/2016
@version 1.0
/*/
Function UrlMshpFIRST()

Return GetMV("MV_URLMSHP")

/*/{Protheus.doc} SmObgCgc
Usado na cria��o do Par�metro MV_OBRICGC para definir se o valor inicial ser� 1(Sim) ou 2(N�o)

@author paulo.barbosa
@since 23/06/2016
@version 1.0
/*/
Function SmObgCgc()
//Para ambientes do Sa�de, o valor padr�o deve ser "2"
Return IIF(SmRamoAtiv() == "003", "2", "1")

/*/{Protheus.doc} SmDefSpec
Usado na cria��o do Par�metro MV_DEFSPEC para definir se o valor inicial ser� SPED ou RPS

@author paulo.barbosa
@since 23/06/2016
@version 1.0
/*/
Function SmDefSpec()
//Para ambientes do Sa�de, o valor padr�o deve ser "RPS"
Return IIF(SmRamoAtiv() == "003", "RPS", "SPED")

/*/{Protheus.doc} SmFormaPG
Usado na cria��o do Par�metro MV_FORMAPG para definir se o valor inicial 0 ou 2

@author paulo.barbosa
@since 23/06/2016
@version 1.0
/*/
Function SmFormaPG()
//Para ambientes do Sa�de, o valor padr�o deve ser "RPS"
Return IIF(SmRamoAtiv() == "003", 2, 0)