#INCLUDE "TOTVS.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "ISAMQry.ch"

Main function ResetaSync(nModulo)

Local aTablesFin := {"SA1", "SED", "SA6", "SE6", "Z14", "SE1", "SE2", "SE5"}
Local aTablesFat := {"SB1","SBM", "SF4", "SFZ", "SC5", "SC6", "SD2", "SF2", "SX5"}
Local aTablesCom := {"SC7", "SC8", "SF1", "SD1", "SX5"}
Local aTablesEst := {"SF5", "SB7", "SX5", "SB2"}
Local aTables    := {"SF5", "SB7", "SB2","SC7", "SC8", "SF1", "SD1","SB1","SBM", "SF4", "SFZ", "SC5", "SC6", "SD2", "SF2", "SX5","SA1", "SED", "SA6", "SE6", "Z14", "SE1", "SE2", "SE5"}
Default nModulo  := 10

If (nModulo > 4 .Or. nModulo < 0)
	Alert("Op��o Inv�lida, op��es v�lidas s�o:";
	+ CRLF + "1 - Reseta o m�dulo Financeiro";
	+ CRLF + "2 - Reseta o m�dulo Faturamento";
	+ CRLF + "3 - Reseta o m�dulo Compras";
	+ CRLF + "4 - Reseta o m�dulo Estoque";
	+ CRLF + "0 - Reseta todos os m�dulos")
Else 
	If nModulo == 1
		ResetaId(aTablesFin, nModulo)
	Elseif nModulo == 2
		ResetaId(aTablesFat, nModulo)
	Elseif nModulo == 3
		ResetaId(aTablesCom, nModulo)
	Elseif nModulo == 4
		ResetaId(aTablesEst, nModulo)
	Elseif nModulo == 0 
		ResetaId(aTables, nModulo)
	Endif	
EndIf

Return

Function ResetaId(aTables, nModulo)
Local nVoltas := 1
Local cAlias  := ""
Local nTables := Len(aTables)

While nVoltas <= nTables
	cAlias = aTables[nVoltas]
	cAlias = LEFT(cAlias, 3)
	cCampo = IIF(cAlias == "Z14", "Z14_FLYID", RIGHT(cAlias, 2) + "_FLYID")

	DbSelectArea(cAlias)
	DbGoTop(cAlias)
	
	If cValToChar(nModulo) $ "2|3|4|*" .And. cAlias == "SX5"
		cCampo := "X5_TABELA"
		While !(cAlias)->(EOF())
			If ((cAlias)->&(cCampo) == "SR" .And. cValToChar(nModulo) $ "0|2|3");
			 .Or. ((cAlias)->&(cCampo) == "IN" .And. cValtoChar(nModulo) $ "4|0");
			 .Or. ((cAlias)->&(cCampo) == "PT" .And. cValToChar(nModulo) $ "0|1|2|3")
				Reclock(cAlias, .F.)
					dbDelete()
				MsUnlock()
			EndIf
		(cAlias)->(DBSkip())
		End
	Else
		While !(cAlias)->(EOF())
			Reclock(cAlias, .F.)
				(cAlias)->&(cCampo) := ""
			MsUnlock()
			
			(cAlias)->(DBSkip())
		End
	EndIf

	DbCloseArea()
	nVoltas++
End

MsgInfo("Reset executado com �xito")

Return