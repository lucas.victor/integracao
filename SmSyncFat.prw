#INCLUDE "PROTHEUS.CH"
#INCLUDE "ISAMQry.ch"
#INCLUDE "Fileio.ch"

/*/{Protheus.doc} SmIntegraFat
Classe gen�rica com regras de integra��o para o Fly Gest�o

@author Rafael Machado
@since 03/09/2018
@version 1.0
/*/

CLASS SmIntegraFat

	METHOD New() CONSTRUCTOR
	METHOD SyncGrupoProduto()
	METHOD SyncProdServ()
	METHOD SyncGrpTrib()
	METHOD SyncSubTrib()
	METHOD SyncOrdVen()
	METHOD SyncOrdProd()
	METHOD SyncOrdServ()
	METHOD SyncStatus()
	METHOD SyncNfSaida()
	METHOD SyncItensSaida()
	METHOD SyncProdStatus()
	METHOD SyncPessoaStatus()
	METHOD SyncNFESaiStatus()

ENDCLASS

/*/{Protheus.doc} New
Construtor

@author Rafael Machado
@since 03/09/2018
@version 1.0
/*/

METHOD New() CLASS SmIntegraFat

Return


/*/{Protheus.doc} SyncGrupoProduto
M�todo para cria��o do JSON da entidade do Grupo de produto e envio � MainSync

@author Jefferson silva de sousa
@since 03/10/2018
@version 1.0
/*/
METHOD SyncGrupoProduto(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFat
	Local cJson      := ""
	Default cEntidade := "Faturamento/GrupoProduto"
	
	cJson := '{' + CRLF
	cJson += '"id":'+ SmTrimJson(cGuid) +',' + CRLF
	cJson += '"descricao":'+ SmTrimJson(AllTrim(SBM->BM_CODGRP) +" "+ AllTrim(LimpaTx(SBM->BM_DESCRI))) +',' + CRLF
	cJson += '"aliquotaIpi": 0.00,' + CRLF
	cJson += '"tipoProduto":'+ '"4",' + CRLF		// tipo de Produtos outros
	cJson += '"registroFixo":false'
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)
	
Return


/*/{Protheus.doc} SyncProdServ
Mtodo para criao do JSON da entidade de produto  e envio  MainSync

@author Jefferson silva de Sousa/Lucas da Cunha Victor
@since 03/10/2018
@version 1.0
/*/
METHOD SyncProdServ(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFat
	Local cJson      := ""
	Local cTipoProd  := ""
	Local cGrpProd   := ""
	Default cEntidade := "Faturamento/Produto"
	
	DbSelectArea("SBM")
	SBM->(DbGoTop())
		cGrpProd := oObjFly:GetGuid(1, xFilial("SBM") + IIF(Empty(SB1->B1_CODGRP), SBM->BM_CODGRP, SB1->B1_CODGRP) , "SBM", "BM_FLYID")
	DbCloseArea()

	Do Case
		Case SB1->B1_TIPO $ '1|2'
			cTipoProd := '1' //Insumo
		Case SB1->B1_TIPO $ '3'
			cTipoProd := '2'//Produto Final
		Case SB1->B1_TIPO $ '6|7' 
			cTipoProd := '3' //Servios
		Otherwise
			cTipoProd := '4' //Outros
	EndCase

	cJson := '{' + CRLF
	cJson += '"id":'+ SmTrimJson(cGuid) +',' + CRLF
	cJson += '"descricao":'+ SmTrimJson(Alltrim(SB1->B1_DESCRI) + " C�dig�o: " + Alltrim(SB1->B1_CODPROD)) +',' + CRLF

	If(cTipoProd == '3') .And. cEntidade != "Compras/Produto"
		cEntidade := "Faturamento/Servico"
		If !Empty(SB1->B1_CODISS)
			cJson +='"codigoServico":'+ SmTrimJson(SB1->B1_CODISS)+','+ CRLF
		EndIF
		If !Empty(SB1->B1_NCM)
			cJson += '"codigoNbs":'+ SmTrimJson(SB1->B1_NCM) +',' + CRLF
		EndIf
		cJson +='"codigoTributacaoMunicipal":'+ SmTrimJson(SB1->B1_TRIBMUN) +','+ CRLF
		cJson +='"valorServico":'+ SmTrimJson(SB1->B1_PRV1) +','+ CRLF
	Else
		If cGrpProd != "0"
			cJson += '"grupoProdutoId":'+ SmTrimJson(cGrpProd) +',' + CRLF // guid do grupo de produto
		EndIf
		cJson += '"abreviacaoUnidadeMedida":'+ SmTrimJson(SB1->B1_CODUM) +',' + CRLF
		cJson += '"tipoProduto":'+'"4",' + CRLF
		cJson += '"saldoProduto": "1000",' + CRLF
		cJson += '"codigoProduto":'+ SmTrimJson(SB1->B1_CODPROD) +',' + CRLF
		cJson += '"codigoBarras":'+ SmTrimJson(IIF(!EMPTY(SB1->B1_CODBAR),Alltrim(SB1->B1_CODBAR), IIF(!EMPTY(SB1->B1_CODBAR2), Alltrim(SB1->B1_CODBAR2 ), "")  )) +',' + CRLF //VERIFICAAO DE CODIGO DE BARRAS DOIS ESTA PREENCHIDO 
		cJson += '"valorVenda":'+ SmTrimJson(SB1->B1_PRV1) +',' + CRLF
		cJson += '"valorCusto":'+ SmTrimJson(SB1->B1_CUSTD) +',' + CRLF
		cJson += '"saldoMinimo":'+ SmTrimJson(SB1->B1_LM) +',' + CRLF
		cJson += '"aliquotaIpi":'+ SmTrimJson(SB1->B1_IPI) +',' + CRLF
		If !Empty(SB1->B1_NCM)
		cJson += '"codigoNcm":'+ SmTrimJson(SB1->B1_NCM) +',' + CRLF //guid ncm
		EndIf
		If !Empty(SB1->B1_CEST)
			cJson += '"codigoCest":'+ SmTrimJson(SB1->B1_CEST) +',' + CRLF    /// nao temos tabela de cest, enviando apenas o codigo  
		EndIf
		If !Empty(SB1->B1_GRPCST)
			cJson += '"codigoEnquadramentoLegalIPI":'+ SmTrimJson(SB1->B1_GRPCST) +',' + CRLF
		EndIf		
	EndIf
	cJson += '"observacao":'+ SmTrimJson(SB1->B1_OBSCOM) +',' + CRLF
	cJson += '"ativo":' + SmTrimJson(IIF(SB1->B1_ATIVO == "1", "true", "false")) + ',' + CRLF
	cJson += '"registroFixo":false'
	cJson += '}'
	
	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

Return


/*/{Protheus.doc} SyncGrpTrib
M�todo para cria��o do JSON da entidade de produto  e envio � MainSync

@author Jefferson silva de sousa
@since 06/11/2018
@version 1.0
/*/
METHOD SyncGrpTrib(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFat
	Local cJson      := ""
	Local cCalcPis   := "2"
	Local cCalcCof   := "2"
	Local ctppagiss  := SF4->F4_ISSST
	Local cSimples   := GetMv("MV_SIMPLES")
	Local cCalcst	   := ""	
	Local cTpSitIss := SF4->F4_CSTISS
	Default cEntidade  := "Faturamento/GrupoTributario"
	
	
	If SF4->F4_PISCOF $ "1|3"
		cCalcPis := "1"
	EndIf
	
	If SF4->F4_PISCOF $ "2|3"
		cCalcCof := "1" 
	EndIf
	

	If Empty(ctppagiss) .Or. ctppagiss $ '5|6'
		ctppagiss := "1"
	Else
		ctppagiss := "2"
	EndIf
	
	/*
	If (cSimples == "S") .AND. (SF4->F4_INCSOL=="S") .AND. (SF4->F4_CSOSN $ "201|202|203")
		cCalcst := "1"
	EndIf	
	*/

	If cTpSitIss $ "0|01"
        cTpSitIss := "T00"  
    ElseIf cTpSitIss $ "02|03"
        cTpSitIss := "T01"
    ElseIf cTpSitIss $ "04|06"
        cTpSitIss := "T02"
    ElseIf cTpSitIss $ "07"
        cTpSitIss := "T03"  
    ElseIf cTpSitIss $ "08"
        cTpSitIss := "T04"  
    ElseIf cTpSitIss $ "08"
        cTpSitIss := "T04"  
    ElseIf cTpSitIss $ "09"
        cTpSitIss := "T05"  
    ElseIf cTpSitIss $ "10"
        cTpSitIss := "T06"
    ElseIf cTpSitIss $ "11"
        cTpSitIss := "T07"  
    ElseIf cTpSitIss $ "12"
        cTpSitIss := "T08"
    Else
        cTpSitIss := "T09"                              
    EndIf
	
	cJson := '{' + CRLF
	cJson += '"id":'+ SmTrimJson(cGuid) +',' + CRLF
	cJson += '"descricao":'+ SmTrimJson(Alltrim(SF4->F4_COD) + " " + Alltrim(LimpaTx((SF4->F4_DESCRI)))) + ',' + CRLF
	cJson += '"codigoCfop":'+ SmTrimJson(Alltrim(SF4->F4_CF)) +',' + CRLF
	cJson += '"calculaIcms":'+ IIF(SF4->F4_ICM == '1','true','false') +',' + CRLF	
	cJson += '"tipoTributacaoICMS":"900",' + CRLF	
	cJson += '"calculaIcmsDifal":'+ IIF(SF4->F4_COMPL == '1','true','false') +',' + CRLF	
	cJson += '"aplicaIpiBaseIcms":'+ IIF(SF4->F4_INCIDE == '1','true','false') +',' + CRLF	
	cJson += '"aplicaFreteBaseIcms":'+ IIF(SF4->F4_FRETICM == '1','true','false') +',' + CRLF
	cJson += '"aplicaDespesaBaseIcms":'+ IIF(SF4->F4_DESPICM == '1','true','false') +',' + CRLF	
	cJson += '"calculaIpi":'+ IIF(SF4->F4_IPI == '1','true','false') +',' + CRLF
	cJson += '"tipoTributacaoIPI":'+ SmTrimJson(IIF(Empty(SF4->F4_CSTIPI), "08", cValToChar(Val(SF4->F4_CSTIPI)))) + ',' + CRLF
	cJson += '"aplicaFreteBaseIpi":'+ IIF(SF4->F4_IPIFRET == '1','true','false') +',' + CRLF
	cJson += '"aplicaDespesaBaseIpi":'+ IIF(SF4->F4_DESPIPI == '1','true','false') +',' + CRLF
	cJson += '"calculaPis":'+ IIF(cCalcPis == '1','true','false') +',' + CRLF
	cJson += '"tipoTributacaoPIS":'+'"'+ IIF(Empty(SF4->F4_CSTPIS), "08", AllTrim(SF4->F4_CSTPIS) ) +'",' + CRLF
	cJson += '"aplicaFreteBasePis":'+ IIF(SF4->F4_DESPPIS == "3",'true','false') +',' + CRLF
	cJson += '"aplicaDespesaBasePis":'+ IIF(SF4->F4_DESPPIS == "4","true","false") +',' + CRLF
	cJson += '"calculaCofins":'+ IIF(cCalcCof == "1","true","false" ) +',' + CRLF
	cJson += '"tipoTributacaoCOFINS":'+'"'+ IIF(Empty(SF4->F4_CSTCOF), "08", AllTrim(SF4->F4_CSTCOF) ) +'",' + CRLF 
	cJson += '"aplicaFreteBaseCofins":'+ IIF(SF4->F4_DESPCOF == "3",'true','false') +',' + CRLF
	cJson += '"aplicaDespesaBaseCofins":' + IIF(SF4->F4_DESPCOF == "4",'true','false') +',' + CRLF
	cJson += '"calculaIss":'+ IIF(SF4->F4_ISS == "1",'true','false') +',' + CRLF
	cJson += '"tipoTributacaoISS":'+ SmTrimJson(cTpSitIss) + ',' + CRLF
	cJson += '"tipoPagamentoImpostoISS":'+ SmTrimJson(ctppagiss) +',' + CRLF
	If !Empty(SF4->F4_TCFPS)
		cJson += '"tipoCFPS":'+ SmTrimJson(SF4->F4_TCFPS) +',' + CRLF
	EndIf
	cJson += '"calculaSubstituicaoTributaria":'+ IIF(SF4->F4_INCSOL == "1",'true','false' ) +',' + CRLF
	cJson += '"aplicaFreteBaseST":'+ IIF(SF4->F4_DESPICM != "2",'true','false' ) +',' + CRLF
	cJson += '"aplicaDespesaBaseST":'+ IIF(SF4->F4_DESPICM != "2",'true','false' ) +',' + CRLF
	cJson += '"aplicaIpiBaseST":'+ IIF(SF4->F4_SOMAIPI == "1","true","false") +',' + CRLF
	cJson += '"retemISS":'+IIF(SF4->F4_RETISS == "1","true","false") +',' + CRLF
	cJson += '"calculaCSLL":'+ IIF(SF4->F4_CALCCSL == "1","true","false") +',' + CRLF
	cJson += '"retemCSLL":'+ IIF(SF4->F4_RETCSL == "1","true","false") +',' + CRLF	
	cJson += '"calculaINSS":'+ IIF(SF4->F4_CALCINS == "1","true","false") +',' + CRLF
	cJson += '"retemINSS":'+ IIF(SF4->F4_RETINS == "1","true","false") +',' + CRLF
	cJson += '"calculaImpostoRenda":'+ IIF(SF4->F4_CALCIRR == "1","true","false") +',' + CRLF
	cJson += '"retemImpostoRenda":'+ IIF(SF4->F4_RETIRR == "1","true","false") +',' + CRLF
	cJson += '"retemPis":'+ IIF(SF4->F4_RETPIS == "1","true","false") +',' + CRLF
	cJson += '"retemCofins":'+ IIF(SF4->F4_RETCOF == "1","true","false") +',' + CRLF				
	cJson += '"registroFixo":false'
	cJson += '}'
	
	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

Return

/*/{Protheus.doc} SyncSubTrib
Mtodo para criao do JSON da entidade de Substituio Tributria  MainSync

@author Lucas Victor
@since 22/11/2018
@version 1.0
/*/
METHOD SyncSubTrib(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFat
    Local cJson      := ""
    Local cFCP       := POSICIONE("SB1", 1, SFZ->FZ_CODPROD, "B1_PERFCP")
    Local cCest      := POSICIONE("SB1", 1, SFZ->FZ_CODPROD, "B1_CEST")
    Local cNCM       := POSICIONE("SB1", 1, SFZ->FZ_CODPROD, "B1_NCM")
    Default cEntidade := "Faturamento/SubstituicaoTributaria"
    
	cJson := '{' + CRLF
	cJson += '"id":'+ SmTrimJson(cGuid) +',' + CRLF
	cJson += '"codigoNcm":' + SmTrimJson(cNCM) +','+ CRLF
	cJson += '"estadoOrigemCodigoIbge":' + SmTrimJson(cCodEst(SFZ->FZ_EST)) +','+ CRLF
	cJson += '"estadoDestinoCodigoIbge":' + SmTrimJson(cCodEst(SFZ->FZ_ESTDEST)) +',' + CRLF
	cJson += '"mva":' + SmTrimJson(SFZ->FZ_ALIQUF) +',' + CRLF
	cJson += '"tipoSubstituicaoTributaria":'+ SmTrimJson(IIF(SFZ->FZ_TIPO == 'S', 'Saida', 'Entrada')) + CRLF
	IIF(Empty(cCest), ,cJson += ',"codigoCest":"' + SmTrimJson(cCest) + '"' + CRLF) 
	IIF(Empty(cFCP), ,cJson += ',"fcp":"' +SmTrimJson(cFCP) + '"' + CRLF) 
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)
    
Return


/*/{Protheus.doc} SyncOrdVen
M�todo para cria��o do JSON da entidade de SyncOrdVen e envio � MainSync

@author Jefferson Silva de Sousa
@since 26/11/2018
@version 1.0
/*/
METHOD SyncOrdVen(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFat
	Local cJson      := ""
	Local cEmissao   := DTOS(SC5->C5_EMISSAO)
	Local cTpFrete   := "9"
	Local cCodCat    := " "			
	Local cNatOper   := " "		
	Local nTpNfCompl := 0
	Local nTotImp    := 0				 			
	Local nOutrosImp := 0
	Local cTpVenda   := "1"
	Local cNfOri     := POSICIONE("SC6",1,xFilial("SC5")+C5_NUM+"01","C6_NFORI" ) 
	Local cSerieOri  := POSICIONE("SC6",1,xFilial("SC5")+SC5->C5_NUM+"01","C6_SERIORI" )
	Local cChaveref  := " "
	Local cTransp    := oObjFly:GetGuid(1, xFilial("SA1") + SC5->C5_TRANSP, "SA1", "A1_FLYID")
	Default cEntidade  := "Faturamento/OrdemVenda"

	If SC5->C5_TIPO == "2" //DEVOLU��O 
		cTpVenda :="4"
	ElseIf SC5->C5_TIPO $ "3|6|7"	 // Notas de complemento 
		cTpVenda := "2"
	EndIf

	cNatOper := POSICIONE("SC6", 1, SC5->C5_NUM, "C6_CFOP")
	
	//  validar o tipod e frete(cTpFrete)

	//validar codigo de categoria financeira 
	cCodCat := Alltrim(POSICIONE("SEA",3,SC5->(C5_PESSOA+C5_NUM),"EA_CODCAT"))

	If (SC5->C5_TIPO) $ '3'
		nTpNfCompl := 1
	ElseIf (SC5->C5_TIPO) $ '6'
		nTpNfCompl := 2
	ElseIf (SC5->C5_TIPO) $ '7'
		nTpNfCompl := 4	
	EndIf
	
	//total impostos que agregam 
	nTotImp := IIF(!EMPTY(SC5->C5_VALIPI),SC5->C5_VALIPI,0)+IIF(!EMPTY(SC5->C5_ICMSRET),SC5->C5_ICMSRET,0)
	//imposto que nao agregam  ICMs, COFINS, FCP, PIS
	nOutrosImp := IIF(!EMPTY(SC5->C5_VALICM),SC5->C5_VALICM,0) 


	If (SC5->C5_TIPO $ '2') .AND. !EMPTY(cNfOri) .AND. !EMPTY(cSerieOri)
		cChaveref := POSICIONE("SF1", 1, xFilial("SC5")+ cNfOri + cSerieOri +SC5->C5_PESSOA + "1", "F1_CHVNFE")	
	EndIf


	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"numero":' + SmTrimJson(SC5->C5_NUM) +',' + CRLF
	cJson += '"tipoOrdemVenda":'+ SmTrimJson(cTpVenda) +',' + CRLF
	cJson += '"tipoVenda":'+ SmTrimJson(cTpVenda) +',' + CRLF
	cJson += '"status":"1",' + CRLF
	cJson += '"data":'+ SmTrimJson(LEFT(cEmissao, 4) + "-" + SUBSTR(cEmissao, 5, 2) + "-" +  RIGHT(cEmissao, 2)) +',' + CRLF
	cJson += '"clienteId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SA1") + SC5->C5_PESSOA, "SA1", "A1_FLYID")) +',' + CRLF
	If cTransp != "0"
		cJson += '"transportadoraId":'+ SmTrimJson(cTransp) +',' + CRLF
	EndIf
	cJson += '"tipoFrete":'+ SmTrimJson(cTpFrete) +',' + CRLF
	If !Empty(SC5->C5_PLACA)
		cJson += '"placaVeiculo":' + SmTrimJson(SC5->C5_PLACA) +',' + CRLF
		cJson += '"estadoCodigoIbge":'+ SmTrimJson(cCodEst(SC5->C5_UFPLACA)) +',' + CRLF
	EndIf
	cJson += '"valorFrete":'+ SmTrimJson(SC5->C5_FRETE) +',' + CRLF
	cJson += '"pesoBruto":'+ SmTrimJson(SC5->C5_PESBRUT) +',' + CRLF
	cJson += '"pesoLiquido":'+ SmTrimJson(SC5->C5_PESLIQ) +',' + CRLF
	cJson += '"quantidadeVolumes":'+ SmTrimJson(SC5->C5_QTDVOL) +',' + CRLF
	cJson += '"formaPagamentoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("Z14") + IIF(!Empty(SC5->C5_FORMAPG), SC5->C5_FORMAPG, "001"), "Z14", "Z14_FLYID")) +',' + CRLF
	cJson += '"condicaoParcelamentoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SE6") + IIF(!Empty(SC5->C5_COND), SC5->C5_COND, "001"), "SE6", "E6_FLYID")) +',' + CRLF
	cJson += '"categoriaId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SED") + cCodCat, "SED", "ED_FLYID")) +',' + CRLF
	cJson += '"movimentaEstoque":false,' + CRLF	
	cJson += '"geraFinanceiro":false,' + CRLF	
	cJson += '"geraNotaFiscal":false,' + CRLF
	cJson += '"observacao":' + SmTrimJson(LimpaTx(SC5->C5_OBS)) +',' + CRLF
	cJson += '"ajusteEstoqueAutomatico": false,' + CRLF
	If Alltrim(SC5->C5_ESPECIE) == "SPED"
		cJson += '"totalImpostosProdutos":'+ SmTrimJson(nTotImp) +',' + CRLF
		cJson += '"totalImpostosProdutosNaoAgrega":' + SmTrimJson(nOutrosImp) +',' + CRLF	
	EndIf
	cJson += '"naturezaOperacao":'+ SmTrimJson(POSICIONE("SFF", 1, cNatOper, "FF_DESCRI")) +',' + CRLF
	If !Empty(cChaveref)
		cJson += '"chaveNFeReferenciada":'+'"'+ Alltrim(cChaveref) +'",' + CRLF
	EndIf
	cJson += '"mensagemPadraoNota":'+ SmTrimJson(LimpaTx(SC5->C5_MENNOTA)) +',' + CRLF
	cJson += '"tipoNfeComplementar":'+ SmTrimJson(cValTochar(nTpNfCompl)) +',' + CRLF
	cJson += '"tipoEspecie":"9",'+ CRLF
	cJson += '"numeracaoVolumesTrans":'+ SmTrimJson(SC5->C5_NUMVOL) +',' + CRLF
	cJson += '"marca":'+ SmTrimJson(SC5->C5_MARCA) +',' + CRLF
	cJson += '"nFeRefComplementarIsDevolucao": false,' + CRLF
	cJson += '"registroFixo":false'
	cJson += '}'
	
	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

Return

/*/{Protheus.doc} OrdemProduto
M�todo para cria��o do JSON da entidade OrdemVendaProduto � MainSync

@author Rafael Machado
@since 27/11/2018
@version 1.0
/*/
METHOD SyncOrdProd(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFat
	Local cJson := ""
	Default cEntidade := "Faturamento/OrdemVendaProduto"
	
	cJson := '{' + CRLF
	cJson += '"id":'+ SmTrimJson(cGuid) +',' + CRLF 
	cJson += '"produtoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SB1") + SC6->C6_CODPROD, "SB1", "B1_FLYID")) +',' + CRLF
	cJson += '"icms":'+ SmTrimJson(SC6->C6_VALICM) +',' + CRLF 
	cJson += '"fcp":'+ SmTrimJson(SC6->C6_VFCP) +',' + CRLF 
	If SC6->C6_CSOSN $ "500|900"
		cJson += '"valorCreditoICMS":'+ SmTrimJson(SC6->C6_VALICM) +',' + CRLF 
		cJson += '"valorICMSSTRetido":'+ SmTrimJson(SC6->C6_ICMSRET) +',' + CRLF 
		cJson += '"valorBCSTRetido":'+ SmTrimJson(SC6->C6_BRICMS) +',' + CRLF 
		cJson += '"valorFCPSTRetidoAnterior":0.0,' + CRLF 
		cJson += '"valorBCFCPSTRetidoAnterior":0.0,' + CRLF 
	Else
		cJson += '"valorCreditoICMS":0.0,' + CRLF 
		cJson += '"valorICMSSTRetido":0.0,' + CRLF 
		cJson += '"valorBCSTRetido":0.0,' + CRLF 
		cJson += '"valorFCPSTRetidoAnterior":0.0,' + CRLF 
		cJson += '"valorBCFCPSTRetidoAnterior":0.0,' + CRLF 	
	EndIf
	cJson += '"ordemVendaId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SC5") + SC6->C6_NUM, "SC5", "C5_FLYID")) +',' + CRLF 
	cJson += '"grupoTributarioId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SF4") + SC6->C6_TES, "SF4", "F4_FLYID")) +',' + CRLF 
	cJson += '"quantidade":'+ SmTrimJson(SC6->C6_QUANT) +',' + CRLF 
	cJson += '"valor":'+ SmTrimJson(SC6->C6_PRCUNI) +',' + CRLF 
	cJson += '"desconto":'+ SmTrimJson(SC6->C6_VALDESC) +',' + CRLF 
	cJson += '"total":'+ SmTrimJson(SC6->C6_TOTAL) +',' + CRLF 
	cJson += '"observacao":'+ SmTrimJson(LimpaTx(SC6->C6_OBS)) +',' + CRLF
	cJson += '"registroFixo":false'+ CRLF 
	cJson += '}'
	
	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

Return

/*/{Protheus.doc} OrdemServico
M�todo para cria��o do JSON da entidade OrdemVendaServico � MainSync

@author Rafael Machado
@since 27/11/2018
@version 1.0
/*/
METHOD SyncOrdServ(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFat
	Local cJson := ""
	Default cEntidade := "Faturamento/OrdemVendaServico"
	
	cJson := '{' + CRLF
	cJson += '"id":'+ SmTrimJson(cGuid) +',' + CRLF 
	cJson += '"servicoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SB1") + SC6->C6_CODPROD, "SB1", "B1_FLYID")) +',' + CRLF 
	cJson += '"valorOutrasRetencoes":0.0,' + CRLF 
	cJson += '"descricaoOutrasRetencoes":"",' + CRLF 
	cJson += '"isServicoPrioritario":false,' + CRLF 
	cJson += '"ordemVendaId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SC5") + SC6->C6_NUM, "SC5", "C5_FLYID")) +',' + CRLF 
	cJson += '"grupoTributarioId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SF4") + SC6->C6_TES, "SF4", "F4_FLYID")) +',' + CRLF 
	cJson += '"quantidade":'+ SmTrimJson(SC6->C6_QUANT) +',' + CRLF 
	cJson += '"valor":'+ SmTrimJson(SC6->C6_PRCUNI) +',' + CRLF 
	cJson += '"desconto":'+ SmTrimJson(SC6->C6_VALDESC) +',' + CRLF 
	cJson += '"total":'+ SmTrimJson(SC6->C6_TOTAL) +',' + CRLF 
	cJson += '"observacao":'+ SmTrimJson(LimpaTx(SC6->C6_OBS)) +',' + CRLF 
	cJson += '"registroFixo":false'+ CRLF 
	cJson += '}'
	
	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

Return


/*/{Protheus.doc} SyncStatus 
M�todo para cria��o do JSON que atualiza os status da entidade OrdemVenda

@author Rafael Machado
@since 12/12/2018
@version 1.0
/*/
METHOD SyncStatus(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFat
	Local cJson      := ""
	Local cStatus    := "1"
	Default cEntidade := "Faturamento/OrdemVenda/" 
	
	cEntidade += Alltrim(SC5->C5_FLYID)
	
	If SC5->C5_STATUS $ '1|4|6|7|9'
		cStatus := "1"
	Else
		cStatus := "2"
	EndIf
	
	cJson := '{' + CRLF
	cJson += '"status":'+ SmTrimJson(cStatus) + CRLF 
	cJson += '}'
	
	oObjFly:MainSync("put", cEntidade, cJson, @cError, @cGuid)

Return


/*/{Protheus.doc} SyncNfSaida
Mtodo para criao do JSON da entidade de produto  e envio  MainSync

@author Jefferson silva de sousa
@since 28/11/2018
@version 1.0
/*/
METHOD SyncNfSaida(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFat
	Local cJson      := ""
	Local cEmissao   := DTOS(SF2->F2_EMISSAO)
	Local cNumOrdven := " "
	Local cTpNfe     := "1"
	Local cStatus    := " "
	Local cTpVenda   := "1"
	Local cTpFrete   := "9"
	Local cCodCat    := " "
	Local cNatOper   := " "	
	Local cTransp    := oObjFly:GetGuid(1, xFilial("SA1") + SF2->F2_TRANSP, "SA1", "A1_FLYID")
	Local cNfOri     := POSICIONE("SD2",1,xFilial("SF2")+ SF2->(F2_DOC + F2_SERIE + F2_PESSOA + F2_TIPO) + "01", "D2_NFORI" ) 
	Local cSerieOri  := POSICIONE("SD2",1,xFilial("SF2")+ SF2->(F2_DOC + F2_SERIE + F2_PESSOA + F2_TIPO) + "01", "D2_SERIORI" )
	LOcal cChaveref  := ""
	Local nTotImp    := 0				 			
	Local nOutrosImp := 0	
	Local nTotImpSrv := 0
	Local nSerNAgreg := 0
	Local nTpNfCompl := 0	
	Default cEntidade  := "Faturamento/NFe"
	
	If (SF2->F2_TIPO) $ '3'
		nTpNfCompl := 1
	ElseIf (SF2->F2_TIPO) $ '6'
		nTpNfCompl := 2
	ElseIf (SF2->F2_TIPO) $ '7'
		nTpNfCompl := 4	
	EndIf

	If SF2->F2_ESPECIE == "RPS"
		cTpNfe := "2"
		cEntidade  := "Faturamento/NFSe"
	EndIf
	
	If !Empty(POSICIONE("SC5",2,SF2->(F2_DOC+F2_SERIE), "C5_NUM"))
		cNumOrdven := POSICIONE("SC5",2,SF2->(F2_DOC+F2_SERIE),"C5_NUM")
	EndIf
	
	If SF2->F2_TIPO == "2" //DEVOLU��O 
		cTpVenda :="4"
	ElseIf SF2->F2_TIPO $ "3|6|7"	 // Notas de complemento 
		cTpVenda := "2"
	EndIf
	
	//Tipo frete CIF = 0, FOB = 1, Terceiro = 2, Remetente = 3, Destinatario = 4, SemFrete = 9
	If SF2->F2_TPFRETE == "1"
		cTpFrete := "0"
	ElseIf SF2->F2_TPFRETE == "2"	
		cTpFrete := "1"
	ElseIf SF2->F2_TPFRETE == "3"	
		cTpFrete := "2"	
	ElseIf SF2->F2_TPFRETE == "5"	
		cTpFrete := "3"		
	ElseIf SF2->F2_TPFRETE == "6"	
		cTpFrete := "4"			
	EndIf
	
	If (SF2->F2_TIPO $ '2') .AND. !EMPTY(cNfOri) .AND. !EMPTY(cSerieOri)
		cChaveref := POSICIONE("SF1", 1, xFilial("SF2")+ cNfOri + cSerieOri + SF2->F2_PESSOA + "1", "F1_CHVNFE")	
	EndIf
	
	cCodCat := Alltrim(POSICIONE("SEA",3,SF2->(F2_PESSOA+F2_DOC),"EA_CODCAT"))
	
	// STATUS POSSIVEIS NO GESTAO Transmitida = 1,Autorizada = 2,UsoDenegado = 3,NaoAutorizada = 4,Cancelada = 5,NaoTransmitida = 6,EmCancelamento = 7,
	// FalhaNoCancelamento = 8,FalhaTransmissao = 9,CanceladaForaPrazo = 10,InutilizacaoSolicitada = 11,Inutilizada = 12
	// Novas notas s� podem ser enviadas com status 1 ou 6
	Do Case
		Case SF2->F2_FIMP $ 'T'
			cStatus := "1"
		/*Case SF2->F2_FIMP $ 'S'
			cStatus := "2"
		Case SF2->F2_FIMP $ 'N'
			cStatus := "4"
		Case SF2->F2_FIMP == 'C' .And. SF2->F2_STATUS == '2'	
			cStatus := "5"
		Case SF2->F2_FIMP $ ' '
			cStatus := "6"	
		Case SF2->F2_FIMP == 'C' .And. SF2->F2_STATUS != '2'	
			cStatus := "7"*/
		Otherwise
			cStatus := "6"
	EndCase
	
	cNatOper := POSICIONE("SD2", 1, SF2->(F2_DOC+F2_SERIE+F2_PESSOA+F2_TIPO), "D2_CFOP")	

	//Total impostos que agregam 
	nTotImp := IIF(!EMPTY(SF2->F2_VALIPI), SF2->F2_VALIPI, 0) + IIF(!EMPTY(SF2->F2_ICMSRET), SF2->F2_ICMSRET, 0)
	//Imposto que nao agregam  ICMs, COFINS, FCP, PIS
	nOutrosImp := IIF(!EMPTY(SF2->F2_VALICM), SF2->F2_VALICM, 0) + IIF(!EMPTY(SF2->F2_VALPIS), SF2->F2_VALPIS, 0) + IIF(!EMPTY(SF2->F2_VALCOF), SF2->F2_VALCOF, 0)
	
	nTotImpSrv := IIF(!EMPTY(SF2->F2_ISS),SF2->F2_ISS,0)
	
	nSerNAgreg := IIF(!EMPTY(SF2->F2_VALCSLL),SF2->F2_VALCSLL,0) + IIF(!EMPTY(SF2->F2_VALPIS),SF2->F2_VALPIS,0) + ;
		IIF(!EMPTY(SF2->F2_VALCOF), SF2->F2_VALCOF, 0) + IIF(!EMPTY(SF2->F2_INSS),SF2->F2_INSS,0)
	
	cJson := '{' + CRLF
	cJson += '"id":' + SmTrimJson(cGuid) +',' + CRLF
	cJson += '"numNotaFiscal":' + SmTrimJson(SF2->F2_DOC) +',' + CRLF
	cJson += '"numeracaoVolumesTrans":'+ SmTrimJson(SF2->F2_NUMVOL) +',' + CRLF
	cJson += '"marca":'+ SmTrimJson(LimpaTx(SF2->F2_MARCA)) +',' + CRLF
	If !(Empty(cNumOrdven)) .And. cNumOrdven != "0"
		cJson += '"ordemVendaOrigemId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SC5") + cNumOrdven, "SC5", "C5_FLYID")) +',' + CRLF
	EndIf 
	cJson += '"tipoNotaFiscal":'+ SmTrimJson(cTpNfe) +',' + CRLF
	cJson += '"tipoVenda":'+ SmTrimJson(cTpVenda) +',' + CRLF
	cJson += '"status":'+ SmTrimJson(cStatus) +',' + CRLF
	cJson += '"data":'+ SmTrimJson(LEFT(cEmissao, 4) + "-" + SUBSTR(cEmissao, 5, 2) + "-" +  RIGHT(cEmissao, 2)) +',' + CRLF
	cJson += '"clienteId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SA1") + SF2->F2_PESSOA, "SA1", "A1_FLYID")) +',' + CRLF
	If cTransp != "0"
		cJson += '"transportadoraId":'+ SmTrimJson(cTransp) +',' + CRLF
	EndIf	
	cJson += '"tipoFrete":'+ SmTrimJson(cTpFrete) +',' + CRLF
	cJson += '"tipoEspecie":"0",' + CRLF 	
	If !Empty(SF2->F2_PLACA)
		cJson += '"placaVeiculo":' + SmTrimJson(SF2->F2_PLACA) +',' + CRLF
		cJson += '"estadoCodigoIbge":'+ SmTrimJson(cCodEst(SF2->F2_UFPLACA)) +',' + CRLF
	EndIf	
	cJson += '"valorFrete":'+ SmTrimJson(SF2->F2_FRETE) +',' + CRLF	
	cJson += '"pesoBruto":'+ SmTrimJson(SF2->F2_PESBRUT) +',' + CRLF
	cJson += '"pesoLiquido":'+ SmTrimJson(SF2->F2_PESLIQ) +',' + CRLF
	cJson += '"quantidadeVolumes":'+ SmTrimJson(SF2->F2_QTDVOL) +',' + CRLF
	cJson += '"formaPagamentoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("Z14") + IIF(!Empty(SF2->F2_FORMAPG), SF2->F2_FORMAPG, "001"), "Z14", "Z14_FLYID")) +',' + CRLF
	cJson += '"condicaoParcelamentoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SE6") + IIF(!Empty(SF2->F2_COND), SF2->F2_COND, "001"), "SE6", "E6_FLYID")) +',' + CRLF
	cJson += '"categoriaId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SED") + cCodCat, "SED", "ED_FLYID")) +',' + CRLF
	cJson += '"serieNotaFiscalId":'+ SmTrimJson(POSICIONE("SX5", 1, "SR" + Alltrim(SF2->F2_SERIE) , "X5_DESCRI" )) +',' + CRLF
	cJson += '"mensagem":' + SmTrimJson(SF2->F2_MENNOTA) +',' + CRLF
	cJson += '"naturezaOperacao":'+ SmTrimJson(LimpaTx(POSICIONE("SFF", 1, cNatOper, "FF_DESCRI"))) +',' + CRLF
	If !Empty(cChaveref)
		cJson += '"chaveNFeReferenciada":'+'"'+ Alltrim(cChaveref) +'",' + CRLF
	EndIf
	If !Empty(SF2->F2_CHVNFE)
		cJson += '"sefazId":' + SmTrimJson(SF2->F2_CHVNFE) +',' + CRLF	
	EndIf
	cJson += '"nFeRefComplementarIsDevolucao":false,' + CRLF
	cJson += '"geraFinanceiro": false,' + CRLF
	If (cTpNfe) == "1"
		cJson += '"totalImpostosProdutos":'+ SmTrimJson(nTotImp) +',' + CRLF
		cJson += '"totalImpostosProdutosNaoAgrega":' + SmTrimJson(nOutrosImp) +',' + CRLF		
		cJson += '"tipoNfeComplementar":'+ SmTrimJson(cValToChar(nTpNfCompl)) +',' + CRLF
	Else
		cJson += '"totalRetencoesServicos":'+ SmTrimJson(nTotImpSrv) +',' + CRLF
		cJson += '"totalImpostosServicosNaoAgrega":'+ SmTrimJson(nSerNAgreg) +',' + CRLF
	EndIf
	cJson += '"registroFixo":false'
	cJson += '}'

	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

Return


/*/{Protheus.doc} SyncItensSaida
M�todo para cria��o do JSON das entidades NFeProduto e NFSeServico � MainSync

@author Rafael Machado
@since 28/11/2018
@version 1.0
/*/
METHOD SyncItensSaida(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFat
	Local cJson      := ""
	Local cTipoProd  := POSICIONE("SB1", 1, SD2->D2_CODPROD, "B1_TIPO")
	Default cEntidade := "Faturamento/NFeProduto"

	cJson := '{' + CRLF
	cJson += '"id":'+ SmTrimJson(cGuid) +',' + CRLF  
	cJson += '"notaFiscalId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SF2") + SD2->D2_DOC + SD2->D2_SERIE + SD2->D2_PESSOA + SD2->D2_TIPO, "SF2", "F2_FLYID")) +',' + CRLF 
	cJson += '"grupoTributarioId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SF4") + SD2->D2_TES, "SF4", "F4_FLYID")) +',' + CRLF 
	cJson += '"quantidade":'+ SmTrimJson(SD2->D2_QUANT) +',' + CRLF 
	cJson += '"valor":'+ SmTrimJson(SD2->D2_PRCUNI) +',' + CRLF 
	cJson += '"desconto":'+ SmTrimJson(SD2->D2_VALDESC) +',' + CRLF 
	cJson += '"total":'+ SmTrimJson(SD2->D2_TOTAL) +',' + CRLF 
	
	If cTipoProd $ "6|7" 
		cEntidade := "Faturamento/NFSeServico"

		cJson += '"servicoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SB1") + SD2->D2_CODPROD, "SB1", "B1_FLYID")) +',' + CRLF 
		cJson += '"valorOutrasRetencoes":0.0,' + CRLF 
		cJson += '"descricaoOutrasRetencoes":"",' + CRLF 
		cJson += '"isServicoPrioritario":false,' + CRLF 

	Else
		cJson += '"produtoId":'+ SmTrimJson(oObjFly:GetGuid(1, xFilial("SB1") + SD2->D2_CODPROD, "SB1", "B1_FLYID")) +',' + CRLF 
		
		If SD2->D2_CSOSN $ "500|900"
			cJson += '"valorCreditoICMS":'+ SmTrimJson(SD2->D2_VALICM) +',' + CRLF 
			cJson += '"valorICMSSTRetido":'+ SmTrimJson(SD2->D2_ICMSRET) +',' + CRLF 
			cJson += '"valorBCSTRetido":'+ SmTrimJson(SD2->D2_BRICMS) +',' + CRLF 
			cJson += '"valorFCPSTRetidoAnterior":0.0,' + CRLF 
			cJson += '"valorBCFCPSTRetidoAnterior":0.0,' + CRLF 
		Else
			cJson += '"valorCreditoICMS":0.0,' + CRLF 
			cJson += '"valorICMSSTRetido":0.0,' + CRLF 
			cJson += '"valorBCSTRetido":0.0,' + CRLF 
			cJson += '"valorFCPSTRetidoAnterior":0.0,' + CRLF 
			cJson += '"valorBCFCPSTRetidoAnterior":0.0,' + CRLF 	
		EndIf
	
	EndIf
	
	//cJson += '"observacao":'+ SmTrimJson(SC6->C6_OBS) +',' + CRLF 
	cJson += '"registroFixo":false'+ CRLF 
	cJson += '}'
	
	oObjFly:MainSync(cMetodo, cEntidade, cJson, @cError, @cGuid)

Return

/*/{Protheus.doc} SyncProdStatus 
M�todo para cria��o do JSON que atualiza os status ativo inativo do Produto 

@author Jefferson silva de sousa 
@since 14/01/2019
@version 1.0
/*/
METHOD SyncProdStatus(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFat
	Local cJson      := ""
	Default cEntidade := "Faturamento/Produto/"
	
	cEntidade += Alltrim(SB1->B1_FLYID) 
	
	If SB1->B1_ATIVO $ "2"
		
		cJson := '{' + CRLF
		cJson += '"ativo":false' + CRLF 
		cJson += '}'
		
		oObjFly:MainSync("put", cEntidade, cJson, @cError, @cGuid)
	EndIf
Return

/*/{Protheus.doc} SyncPessoaStatus 
M�todo para cria��o do JSON que atualiza os status ativo inativo de Pessoa  

@author Jefferson silva de sousa 
@since 14/01/2019
@version 1.0
/*/
METHOD SyncPessoaStatus(cMetodo, cGuid, cError, oObjFly, cEntidade) CLASS SmIntegraFat
	Local cJson      := ""
	Default cEntidade := "Faturamento/NFe/"
	
	cEntidade += Alltrim(SA1->A1_FLYID) 
	
	
	If SA1->A1_ATIVO $ "2"

		cJson := '{' + CRLF
		cJson += '"ativo": false' + CRLF 
		cJson += '}'
		
		oObjFly:MainSync("put", cEntidade, cJson, @cError, @cGuid)
	EndIf
Return


/*/{Protheus.doc} SyncFat
Fun��o que cria a central de sincronismo para envio do hist�rico de registros no banco

@author Rafael Machado
@since 12/09/2018
@version 1.0
/*/
Function SyncFat(nTipo)
	CreateScreen(nTipo, 2)
Return



/*/{Protheus.doc} SyncAuxFat
Fun��o que faz as chamadas dos m�todos de sincronismo de dados para o Fly Gestao

@author Rafael Machado
@since 12/09/2018
@version 1.0
/*/
Function SyncAuxFat(oDlg, oObjFly)
Local cError     := ""

oObjFly:self:cToken := oObjFly:GetToken(@cError, "Faturamento")
	
If Empty(cError)
		
	SyncSerieNfe(oObjFly)
	
	SyncParTrib(oObjFly)
	
	oObjFly:SyncFly("SA1", oObjFly, oDlg, ,"Faturamento/Pessoa")
	oObjFly:SyncFly("Z14", oObjFly, oDlg, ,"Faturamento/FormaPagamento")
	oObjFly:SyncFly("SE6", oObjFly, oDlg, ,"Faturamento/CondicaoParcelamento")
	oObjFly:SyncFly("SED", oObjFly, oDlg, ,"Faturamento/Categoria")	
	oObjFly:SyncFly("SBM", oObjFly, oDlg)
	oObjFly:SyncFly("SB1", oObjFly, oDlg)	
	oObjFly:SyncFly("SF4", oObjFly, oDlg)
	oObjFly:SyncFly("SFZ", oObjFly, oDlg)
	oObjFly:SyncFly("SC5", oObjFly, oDlg)
	oObjFly:SyncFly("SC6", oObjFly, oDlg)
	oObjFly:SyncFly("SF2", oObjFly, oDlg)
	oObjFly:SyncFly("SD2", oObjFly, oDlg)
	oObjFly:SyncFly("SC5", oObjFly, oDlg, .T., ,.F.)
	oObjFly:SyncFly("SB1", oObjFly, oDlg, .T., ,.F.)
	oObjFly:SyncFly("SA1", oObjFly, oDlg, .T., "Faturamento/Pessoa", .F.)
		
EndIf

MsgInfo("Integra��o Finalizada.")

oDlg:End()

Return


/*/{Protheus.doc} SyncSerieNfe
M�todo para cria��o do JSON da entidade de s�rie de nota fiscal e envio � MainSync
@author Eduardo Grigoletto
@since 19/11/2018
@version 1.0
/*/
Function SyncSerieNfe(oObjFly, cEntidade)
Local cJson      := ""
Local cNumNf     := Alltrim(GetMv("MV_NUMNF"))
Local aNumNf     := Strtokarr( cNumNf, ";")
Local aNumAux    := {}
Local nI         := 0
Local cGuid      := ""
Local cError     := ""
Default cEntidade  := "Faturamento/SerieNotaFiscal"

For nI := 1 To Len(aNumNf)

	cGuid := FWUUIDV4(.T.)

	//As informa��es ser�o obtidas a partir da recupera��o dos valores no par�metro MV_NUMNF
    aNumAux := Strtokarr(cValToChar(aNumNf[nI]), "=")

	//Apenas s�ries com sequ�ncia num�rica ser�o enviadas a integra��o
	If Len(aNumAux) > 1
		cJson := '{' + CRLF
		cJson += '"id":'+ SmTrimJson(cGuid) +',' + CRLF
		cJson += '"serie":'+ SmTrimJson(aNumAux[1]) +',' + CRLF
		cJson += '"numNotaFiscal":'+ SmTrimJson(cValToChar(aNumAux[2])) +',' + CRLF
		cJson += '"tipoOperacaoSerieNotaFiscal":"3",' + CRLF
		cJson += '"registroFixo":false'
		cJson += '}'

		oObjFly:MainSync("post", cEntidade, cJson, @cError)
	
		If Empty(cError)
			Reclock("SX5", .T.)
				SX5->X5_FILIAL := "01"
				SX5->X5_TABELA := "SR"
				SX5->X5_CHAVE  := AllTrim(cValToChar(aNumAux[1]))
				SX5->X5_ITEM   := ""
				SX5->X5_DESCRI := cGuid
			MsUnlock()
		EndIf
	EndIf 
Next nI

Return

/*/{Protheus.doc} SyncParTrib
Fn��o para cria��o do JSON da entidade de Par�metro Tribut�rio(par�metros de configura��o do TES) e envio � MainSync
@author Eduardo Grigoletto
@since 21/11/2018
@version 1.0
/*/
Function SyncParTrib(oObjFly, cEntidade)
Local cJson      := ""
Local cSimples   := GetMv("MV_SIMPLES")
Local cError     := ""
Local cAlqICMSN  := cValtoChar(GetMV("MV_ALICMSN",,0))
Local cAlqISS    := cValtoChar(GetMV("MV_ALIQISS",,0))
Local cTxPIS     := cValtoChar(GetMV("MV_TXPIS",,0))
Local cTxCOF     := cValtoChar(GetMV("MV_TXCOF",,0))
Local cAlqCSL    := cValtoChar(GetMV("MV_ALIQCSL",,0))
Local cAlqINS    := cValtoChar(GetMV("MV_ALIQINS",,0))
Local cAlqIRR    := cValtoChar(GetMV("MV_ALIQIRR",,0))
Local lRegeSim   := GetMv("MV_REGESIM",,.F.)
Local lHorVer    := HrVerao()
Local nTpHor	 := Alltrim(GetMv("MV_UTC"))
Local nRegEsp    := GetMv("MV_REGIESP",,0) 
Local nTipo      := IIF(oObjFly:nTipo == 3, 2, oObjFly:nTipo)
Default cEntidade  := "Faturamento/ParametroTributario"


cJson := '{' + CRLF
cJson += '"simplesNacional":'+ SmTrimJson(IIF(cSimples $ 'S|E', "true", "false")) + ',' + CRLF
cJson += '"aliquotaSimplesNacional":' + SmTrimJson(cAlqICMSN) +',' + CRLF //rever com o Luan
cJson += '"aliquotaISS":'+ SmTrimJson(cAlqISS) +',' + CRLF
cJson += '"aliquotaPISPASEP":'+ SmTrimJson(cTxPIS) +',' + CRLF
cJson += '"aliquotaCOFINS":'+ SmTrimJson(cTxCOF) + ',' + CRLF
cJson += '"registroSimplificadoMT":'+ SmTrimJson(IIF(lRegeSim == .T., "true", "false")) + ',' + CRLF
cJson += '"tipoAmbiente":'+ SmTrimJson( cValtoChar(nTipo)) + ',' + CRLF
cJson += '"tipoVersaoNFe":'+'"4",' + CRLF
cJson += '"tipoModalidade": "1",' + CRLF //Default 1 - Normal
cJson += '"aliquotaFCP": "0.00",' + CRLF
cJson += '"tipoPresencaComprador": "9",' + CRLF
cJson += '"horarioVerao":'+ SmTrimJson(IIF(lHorVer == .T., "1", "2")) + ',' + CRLF

If Empty(nTpHor) .Or. nTpHor $ "4|5"
	nTpHor := "2"
EndIf

cJson += '"tipoHorario":'+ SmTrimJson(nTpHor) + ',' + CRLF
cJson += '"versaoNFSe": "4.00" ,' + CRLF
cJson += '"tipoAmbienteNFS":'+ SmTrimJson(cValtoChar(nTipo)) + ',' + CRLF
cJson += '"incentivoCultura": false ,' + CRLF
cJson += '"tipoTributacaoNFS": "6",' + CRLF
cJson += '"aliquotaCSLL":'+ SmTrimJson(cAlqCSL) + ',' + CRLF
cJson += '"aliquotaINSS":'+ SmTrimJson(cAlqINS) +',' + CRLF
cJson += '"aliquotaImpostoRenda":'+ SmTrimJson(cAlqIRR) +',' + CRLF
cJson += '"tipoRegimeEspecialTributacao":'+ nRegEsp +',' + CRLF
cJson += '"usuarioWebServer":""'+',' + CRLF
cJson += '"senhaWebServer":""'+',' + CRLF
cJson += '"chaveAutenticacao":"'+'",' + CRLF
cJson += '"autorizacao":""'+',' + CRLF
cJson += '"registroFixo": false'
cJson += '}'

oObjFly:MainSync("post", cEntidade, cJson, @cError)

Return
