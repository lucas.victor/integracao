#INCLUDE "TOTVS.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "ISAMQry.ch"
#INCLUDE "Fileio.ch"

/*/{Protheus.doc} SmIntegraFly
Classe gen�rica com regras de integra��o para o Fly Gest�o

@author Rafael Machado
@since 03/09/2018
@version 1.0
/*/

CLASS SmIntegraFly

	DATA oRest
	DATA cUrl
	DATA cToken
	DATA cPlatform
	DATA nModulo
	DATA nTipo

	METHOD New() CONSTRUCTOR
	METHOD GetToken()
	METHOD SyncFly()
	METHOD MainSync()
	METHOD GetGuid()
	METHOD SelectModule()
	METHOD ErrorLog()

ENDCLASS

/*/{Protheus.doc} New
Construtor

@author Rafael Machado
@since 03/09/2018
@version 1.0
/*/

METHOD New(nTipo, nModulo) CLASS SmIntegraFly
	
	Local cCnpj      := ""
	Local cGet       := ""
	Local cPlatform  := Alltrim(GetMV("MV_CGCPLAT"))
	Local aFilInfo   := FwFilManut(cFilAnt)
	Local oObjPlat   := {}
	
	self:nTipo := nTipo
	self:nModulo := nModulo
	self:oRest := SmRestClient():New("",, 2, 1,, .F.)
	self:oRest:nTimeOut := 30
	If self:nTipo == 1
		self:cUrl := "http://gateway.fly01.com.br/api/v2/"
	ElseIf self:nTipo == 2
		self:cUrl := "http://gateway.fly01dev.com.br/api/v2/"
	Else
		self:cUrl := "http://gateway.fly01local.com.br/api/v2/"
	EndIf
	
	If Empty(cPlatform)
		cCnpj := SmJustDig(aFilInfo[3][1][4])
		If self:nTipo == 1
			cGet := HttpGet("http://licenciamento.serie1.com.br/api/Fly01/ComplementosDocumento?documento="+cCnpj)
		Else
			cGet := HttpGet("http://licenciamentohomolog.serie1.com.br/api/Fly01/ComplementosDocumento?documento="+cCnpj)
		EndIf
		
		If SmJson2Obj(cGet, oObjPlat)
			self:cPlatform  := oObjPlat:aValues[1]
		EndIf
	Else
		If self:nTipo == 1 
			self:cPlatform := cPlatform+".fly01.com.br"
		Else
			self:cPlatform := cPlatform+".fly01dev.com.br"
		EndIf
	EndIf 
	
Return


/*/{Protheus.doc} GetToken
M�todo que retorna um token v�lido e o armazena na sysmemo

@author Rafael Machado
@since 03/09/2018
@version 1.0
/*/

METHOD GetToken(cError, cApp) CLASS SmIntegraFly
	Local cPost      := ""
	Local cError     := ""
	Local cRet       := ""
	Local cToken     := ""
	Local cSenha     := ""
	Local dDataValid
	Local oRet
	Local cPlatform  := self:cPlatform
	
	If self:nTipo == 1 //PRODU��O
	
		Do Case
			Case cApp == "Financeiro"
				dDataValid := SmSysGet("TOKENFINVALID")
				cToken     := "TOKENFIN"
				cUserName  := "9431cdb8-cb8e-4702-8276-37e91bfc2ad7"
				cSenha     := "appFinanceiro_37e91bfc2ad7"
			Case cApp == "Faturamento"
				dDataValid := SmSysGet("TOKENFATVALID")
				cToken     := "TOKENFAT"	 
				cUserName  := "dfc2cdaa-75a3-4eab-b274-fa6b4fcec9fd"
				cSenha     := "appFaturamento_fa6b4fcec9fd"
			Case cApp == "Compras"
				dDataValid := SmSysGet("TOKENFATVALID")
				cToken     := "TOKENFAT"	 
				cUserName  := "a8e5f1ce-d0b4-4b0a-8562-9b8fbf19217b"
				cSenha     := "appCompras_9b8fbf19217b"
			Case cApp == "Estoque"
				dDataValid := SmSysGet("TOKENESTVALID")
				cToken     := "TOKENEST"	 
				cUserName  := "79579272-eacf-4522-97fe-91924e9a8d20"
				cSenha     := "appEstoque_91924e9a8d20"
			Case cApp == "OS"
				dDataValid := SmSysGet("TOKENOSVALID")
				cToken     := "TOKENOS"	 
				cUserName  := "68873d7a-4dda-49ad-953a-010f1b9d2dfc"
				cSenha     := "ZaAHFhwwLxbnHBix6wHj "
		EndCase

		If dDataValid == Nil .Or. dDataValid < DTOS(Date())
			
			cPost := "PlatformUser=fly01@totvs.com.br"
			cPost += "&PlatformUrl="+cPlatform
			cPost += "&Password="+cSenha
			cPost += "&Username="+cUserName
			cPost += "&grant_type=password"
	
			oRet := self:oRest:Post("http://gateway.fly01.com.br/token",, cPost, @cError)
	
			If Empty(cError)
				SmSysSet(cToken, oRet:aValues[1])
				SmSysSet(cToken+"VALID", DTOS(Date()))
			EndIf
			
		EndIf
	
		cRet := SmSysGet(cToken)
	
	Else //HOMOLOG - FUTURAMENTE TAMB�M FAZER A SEPARA��O DOS TOKENS POR APP
	
		dDataValid := SmSysGet("DEVFLYTOKENVALID")
		
		If dDataValid == Nil .Or. dDataValid < DTOS(Date())
			
			cPost := "PlatformUser=jefferson.totvs.mpn2@gmail.com"
			cPost += "&PlatformUrl="+cPlatform
			cPost += "&Password=t0tvsFly01S@vd3"
			cPost += "&Username=acc8b796-04a0-4f4f-b6a4-924d1ca4c27b"
			cPost += "&grant_type=password"
			
			oRet := self:oRest:Post("http://gateway.fly01dev.com.br/token",, cPost, @cError)
			
			If Empty(cError)
				SmSysSet("DEVFLYTOKEN", oRet:aValues[1])
				SmSysSet("DEVFLYTOKENVALID", DTOS(Date()))
			EndIf
			
		EndIf
		
		cRet := SmSysGet("DEVFLYTOKEN")
	EndIf
	
	If !Empty(cError)	
		self:ErrorLog("GET TOKEN", cPost, cError, self:cPlatform)               
	EndIf
	
Return cRet


//-------------------------------------------------------------------
/*/{Protheus.doc} SyncFly
Realiza o envio do hist�rico da tabela recebida por par�metro para a base do Fly Gest�o.

@author Rafael Machado
@since 05/09/2018
@version 1.0
/*/
//-------------------------------------------------------------------
METHOD SyncFly(cTabela, oObjFly, oDlg, lAux, cEntidade, lGravaGuid) CLASS SmIntegraFly
	Local cFiltro    := ""
	Local cCampoGuid := ""
	Local cGuid      := ""
	Local cError     := ""
	Local lRet       := .T.
	Local lCabec     := .T.
	Local nCount     := 0
	Local nLastRec   := 0
	Local nRecAtu    := 0
	Local dAntData   := stod("  /  /    ")
	Local dAtuData   := stod("  /  /    ")
	Local bSync
	Local bSyncAux
	Local oObjFin    := SmIntegraFin():New()
	Local oObjFat    := SmIntegraFat():New()
	Local oObjCom    := SmIntegraCom():New()
	Local oObjEst    := SmIntegraEst():New()
	Default cTabela    := ""
	Default lAux       := .F.
	Default cEntidade  := ""
	Default lGravaGuid := .T.

	If !lCancela		
		DbSelectArea(cTabela)
		
		//Filtra apenas os registros que n�o possuem GUID (Nunca foram sincronizados)
		cCampoGuid := IIF(cTabela == "Z14", "Z14_FLYID", RIGHT(cTabela, 2)+"_FLYID")
		cFiltro := cTabela + "->" + cCampoGuid +" == '" + Space(TamSX3(cCampoGuid)[1]) + "'"
		
		If cTabela == "SC5" .And. lAux //Se for atualiza��o de status (lAux) filtro apenas os itens que possuem guid
			cFiltro := "!Empty(SC5->C5_FLYID)"
		EndIf

		If cTabela == "SE1"
			cFiltro += " .And. !(SE1->E1_SALDO == 0 .And. SE1->E1_STATUS == '4') .And. !(SE1->E1_TOTPARC > 1 .And. EMPTY(SE1->E1_PARCELA)) .And. Alltrim(SE1->E1_SERIE) != 'PDV'" //Exclui da listagem os t�tulos renegociados e os de cabe�alho
		EndIf
		
		If cTabela == "SE2"
			cFiltro += " .And. !(SE2->E2_SALDO == 0 .And. SE2->E2_STATUS == '4') .And. !(SE2->E2_TOTPARC > 1 .And. EMPTY(SE2->E2_PARCELA)) .And. Alltrim(SE2->E2_SERIE) != 'PDV'" //Exclui da listagem os t�tulos renegociados e os de cabe�alho
		EndIf
		
		//Retornar� somente as baixas de t�tulos
		If cTabela == "SE5" .And. lAux //lAux - Indica se o registro � referente a uma baixa.
			cFiltro += " .And. Alltrim(SE5->E5_SERIE) != 'PDV' .And. SE5->E5_NUM != '" + Space(TamSX3("E5_NUM")[1]) + "'" 
		EndIf
		
		//Retornar� as movimenta��es sem v�nculo com t�tulos (transfer�ncia banc�ria, por exemplo).
		If cTabela == "SE5" .And. !lAux //lAux - Indica se o registro � referente a uma baixa.
			cFiltro += " .And. Alltrim(SE5->E5_SERIE) != 'PDV' .And. SE5->E5_NUM == '" + Space(TamSX3("E5_NUM")[1]) + "'" 
		EndIf
		
		//If cTabela == "SB1"
		//	cFiltro += " .And. SB1->B1_ATIVO != '2' " //Retornar� somente os produtos ativos
		//EndIf

		If cTabela == "SF5"
			cFiltro += " .And. SF5->F5_TIPO != '1' " //Retornar� somente os tipo Entrada e Sa�da, desconsiderando o tipo produ��o.
		EndIf

		If cTabela == "SD3"
			cFiltro += ".And.  Empty(SD3->D3_OP) .And. Empty(SD3->D3_INVENT) "
		EndIf
	
		If cTabela == "SB7"
			cFiltro += ".And. !Empty(SB7->B7_DTPROC) "
		EndIf
		
		If cTabela == "SB1" .And. lAux //Se for atualiza��o de status (lAux) filtro apenas os itens que possuem guid
			cFiltro := "!Empty(SB1->B1_FLYID)"
		EndIf

		If cTabela == "SA1" .And. lAux //Se for atualiza��o de status (lAux) filtro apenas os itens que possuem guid
			cFiltro := "!Empty(SA1->A1_FLYID)"
		EndIf	

		If cTabela == "SF2"
			cFiltro += " .And. SF2->F2_SERIE != 'PDV' "
		EndIf

		
		If cTabela == "SD2"
			cFiltro += " .And. SD2->D2_SERIE != 'PDV' "
		EndIf
		
		If cTabela == "SF1"
		 	cFiltro += " .And. SF1->F1_SERIE != 'PDV' "
		EndIf
		
		If cTabela == "SD1"
			cFiltro += " .And. SD1->D1_SERIE != 'PDV' "
		EndIf
	
	
		(cTabela)->(DbSetOrder(0))
		If cTabela == "SB7"
			(cTabela)->(DbSetOrder(2))
		Endif
		(cTabela)->(dbSetFilter({|| &cFiltro },cFiltro))
		(cTabela)->(dbGoTop())
		
		nLastRec := (cTabela)->(LastRec())
			
		Do Case
			Case cTabela == "SA1" .And. !lAux // lAux para alterar o status de inativos
				bSync := {|| oObjFin:SyncPessoa("post", @cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, )) }
				oSay:SetText( "CLIENTE / FORNECEDOR" )
				oSay:CtrlRefresh()
			Case cTabela == "Z14"
				bSync := {|| oObjFin:SyncFormPag("post", @cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "FORMA DE PAGAMENTO" )
				oSay:CtrlRefresh()
			Case cTabela == "SE6"
				bSync := {|| oObjFin:SyncCondParc("post", @cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "COND. PARCELAMENTO" )
				oSay:CtrlRefresh()
			Case cTabela == "SED"
				bSync := {|| oObjFin:SyncCategoria("post", @cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "CATEGORIA FINANCEIRA" )
				oSay:CtrlRefresh()
			Case cTabela == "SA6"
				bSync := {|| oObjFin:SyncConta("post", @cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "CONTAS BANCRIAS" )
				oSay:CtrlRefresh()
			Case cTabela == "SE1"
				bSync := {|| oObjFin:SyncReceber("post", @cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "T�TULOS A RECEBER" )
				oSay:CtrlRefresh()
			Case cTabela == "SE2"
				bSync := {|| oObjFin:SyncPagar("post", @cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "T�TULOS A PAGAR" )
				oSay:CtrlRefresh()
			Case cTabela == "SE5" .And. lAux //lAux - Indica se o registro � referente a uma baixa.
				bSync := {|| oObjFin:SyncBaixas("post", @cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "BAIXAS" )
				oSay:CtrlRefresh()
			Case cTabela == "SE5" .And. !lAux //lAux - Indica se o registro � referente a uma baixa.
				bSync := {|| oObjFin:SyncMovBanc("post", @cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "MOVIM. BANC�RIAS" )
				oSay:CtrlRefresh()
			Case cTabela == "SBM"
				bSync := {|| oObjFat:SyncGrupoProduto("post", @cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "GRUPO DE PRODUTO" )
				oSay:CtrlRefresh()
			Case cTabela == "SB1" .And. !lAux // lAux para alterar o status de inativos 
				bSync := {|| oObjFat:SyncProdServ("post", @cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "PRODUTOS" )
				oSay:CtrlRefresh()
			Case cTabela == "SF4"
				bSync := {|| oObjFat:SyncGrpTrib("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "TES - GRUPO TRIBUTARIO" )
				oSay:CtrlRefresh()
			Case cTabela == "SFZ"
				bSync := {|| oObjFat:SyncSubTrib("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "SUBSTITUIO TRIBUTRIA" )
				oSay:CtrlRefresh()
			Case cTabela == "SC5" .And. !lAux //lAux - Indica se o sincronismo � referente a atualiza��o de status do pedido.
				bSync := {|| oObjFat:SyncOrdVen("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "ORDEM DE VENDA" )
				oSay:CtrlRefresh()
			Case cTabela == "SC6"
				bSync := {|| oObjFat:SyncOrdProd("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				bSyncAux := {|| oObjFat:SyncOrdServ("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "ORDEM DE VENDA - ITENS" )
				oSay:CtrlRefresh()
			Case cTabela == "SC5" .And. lAux //lAux - Indica se o sincronismo � referente a atualiza��o de status do pedido.
				bSync := {|| oObjFat:SyncStatus("put", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
			Case cTabela == "SF2"
				bSync := {|| oObjFat:SyncNfSaida("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "NOTAS FISCAIS" )
				oSay:CtrlRefresh()
			Case cTabela == "SD2"
				bSync := {|| oObjFat:SyncItensSaida("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "NOTAS FISCAIS - ITENS" )
				oSay:CtrlRefresh()
			Case cTabela == "SF5"
				bSync := {|| oObjEst:SyncTipMov("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "TIPOS DE MOVIMENTOS" )
				oSay:CtrlRefresh()
			Case cTabela == "SB7"
				bSyncAux := {|| oObjEst:SyncInvCab("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				bSync    := {|| oObjEst:SyncInvIte("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "INVENT�RIO" )
				oSay:CtrlRefresh()
			Case cTabela == "SB2"
				bSyncAux := {|| oObjEst:InvCabAtu("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				bSync    := {|| oObjEst:InvItemAtu("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "INVENT�RIO - SALDO ATUAL" )
				oSay:CtrlRefresh()
			Case cTabela == "SD3"
				bSync    := {|| oObjEst:SyncAjuste("put", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "ESTOQUE - AJUSTE MANUAL" )
				oSay:CtrlRefresh()
			Case cTabela == "SC7" .And. !lAux //lAux - Indica se o sincronismo � referente a atualiza��o de status do pedido.
				bSync := {|| oObjCom:SyncOrdCom("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "ORDEM DE COMPRA" )
				oSay:CtrlRefresh()
			Case cTabela == "SC8"
				bSync := {|| oObjCom:SyncOrdProd("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "ORDEM DE COMPRAS - ITENS" )
				oSay:CtrlRefresh()
			Case cTabela == "SC7" .And. lAux //lAux - Indica se o sincronismo � referente a atualiza��o de status do pedido.
				bSync := {|| oObjCom:SyncStatus("put", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
			Case cTabela == "SF1"
				bSync := {|| oObjCom:SyncNfEntrada("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "NOTAS FISCAIS" )
				oSay:CtrlRefresh()
			Case cTabela == "SD1"
				bSync := {|| oObjCom:SyncItensEntrada("post", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}
				oSay:SetText( "NOTAS FISCAIS - ITENS" )
				oSay:CtrlRefresh()
			Case cTabela == "SB1" .AND. lAux // lAux para envio de produtos inativos
				bSync := {|| oObjFat:SyncProdStatus("put", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}						
			Case cTabela == "SA1" .AND. lAux // lAux para envio de cadastro de Pessoas inativos
				bSync := {|| oObjFat:SyncPessoaStatus("put", cGuid, @cError, oObjFly, IIF(!Empty(cEntidade), cEntidade, ))}					
			OtherWise
				SmHelp(2, "", "Tabela inv�lida", "Informe uma tabela v�lida para a integra��o.")
				lRet := .F.
		EndCase	
		
		
		
		//Atualiza objeto de tela com barra de progresso completa caso n�o existam registros para integrar.
		If (cTabela)->(Eof())
			oMeter:Set(100)
		EndIf
		
		While 	lRet .And. (cTabela)->(!Eof())
			cGuid := FWUUIDV4(.T.)
			cError := ""

			//Chamada do m�todo de sincronismo
			If cTabela == "SC6"
				If	Alltrim(POSICIONE("SC5", 1, SC6->C6_NUM, "C5_ESPECIE")) == "RPS"
					Eval(bSyncAux)
				Else
					Eval(bSync)
				EndIf
			ElseIf cTabela == "SB7" .And. (cTabela)->B7_DATA != dAntData //Trocou de invent�rio.
				
				//Atualiza o status do invent�rio ap�s varrer todos os seus itens.
				DbSelectArea("SX5")
				dbSetOrder(1)
				If SX5->(DbSeek("01  IN" + DTOS(dAntData)))
					SyncStatus(SX5->X5_DESCRI, oObjFly)
				EndIf
				
				//Troca a data de referencia para o novo invent�rio
				dAntData := (cTabela)->B7_DATA
				
				Eval(bSyncAux) //Manda o cabe�alho do novo invent�rio
				cGuid := FWUUIDV4(.T.) //Cria um guid diferente para envio do primeiro item do novo invent�rio, pois o guid anterior j� foi utilizado no cabe�alho (e gravado na SX5).
				Eval(bSync)
			ElseIf cTabela == "SB2"
				//Verifica se o cabe�alho j� foi enviado e n�o envia novamente.
				If lCabec
					Eval(bSyncAux)
					lCabec := .F.
				EndIf
				cGuid := FWUUIDV4(.T.) //Cria um guid diferente para envio dos itens, pois o guid anterior j� foi utilizado no cabe�alho (e gravado na SX5).
				Eval(bSync)
			Else
				Eval(bSync)			
			EndIf
			
			If Empty(cError) .And. lGravaGuid
				Reclock(cTabela, .F.,,,.F.)
					(cTabela)->(&cCampoGuid) := cGuid
				MsUnlock()
			EndIf
			
			//Regra de 3 para atualizar barra de progress�o em tela (recno do registro atual em compara��o com o �ltimo recno)
			nRecAtu := (cTabela)->(Recno())	
			nCount := (nRecAtu * 100)/ nLastRec
			
			//Atualiza objeto de tela com barra de progresso
			oMeter:Set(nCount)
			
			//Sai do loop caso o usu�rio clique no bot�o cancelar
			If lCancela
				EXIT
			EndIf
			
			If cTabela == "SB7"
				//Atualiza o status do invent�rio ap�s varrer todos os seus itens.
				DbSelectArea("SX5")
				dbSetOrder(1)
				SX5->(DbSeek("01  IN" + DTOS(dAntData)))
			ElseIf cTabela == "SB2"
				//Atualiza o status do invent�rio ap�s varrer todos os seus itens.
				DbSelectArea("SX5")
				dbSetOrder(1)
				SX5->(DbSeek("01  INSB2"))
			EndIf
			
			(cTabela)->(dbSkip())
		End
		
		//Se certifica de que o objeto de tela est� com a barra de progresso completa 
		//(caso o registro do �ltimo recno j� tenha sido transmitido ou esteja apagado e a regra de 3 falhe)
		oMeter:Set(100)
		
		(cTabela)->(dbClearFilter())
		
		If lCancela
			oDlg:End() 
		EndIf
		
	EndIf
	
Return

/*/{Protheus.doc} MainSync
M�todo gen�rico para comunicar com o servidor do Fly Gest�o.

@author Rafael Machado
@since 03/09/2018
@version 1.0
/*/
METHOD MainSync(cMetodo, cEntidade, cJson, cError, cGuid) CLASS SmIntegraFly
	Local cError     := ""
	Local cUrl       := self:cUrl+cEntidade 
	Local cGuidRef   := ""
	Local oRet
	Default cGuid    := ""
	
	If cMetodo == "post"
		oRet := self:oRest:Post(cUrl, , cJson, @cError, , , self:cToken)
	ElseIf cMetodo == "put"
		oRet := self:oRest:Put(cUrl, , cJson, @cError, , , self:cToken)
	ElseIf cMetodo == "delete"
		oRet := self:oRest:delete(cUrl, @cError, , , self:cToken)
	EndIf
	
	//Log de erro atualmente em arquivo - Ser� implementado o m�todo para envio � tabela de log.
	If !Empty(cError)
		If !Empty(cError[2])
			//Verifica se houve erro de duplicidade de cadastro e faz os tratamentos necess�rios
			If "referenceId" $ cError[2] .and. !("SerieNotaFiscal" $ cEntidade) //N�o faz o tratamento para s�rie da NF pois a mesma n�o armazena guid.
				cGuidRef := SUBSTR(cError[2] , AT("referenceId", cError[2]) + 16, 36 )
				
				If !Empty(Val(SmJustDig(cGuidRef)))
					cError := {}
					cGuid := cGuidRef
				EndIf
			EndIf
			
			If !Empty(cError)	
				self:ErrorLog(cEntidade, cJson, cError, self:cPlatform)               
			EndIf
		Else
			cError := ""
		EndIf
	EndIf
	
Return


/*/{Protheus.doc} GetGuid
M�todo para buscar o Guid da Categoria Superior

@author Rafael Machado
@since 04/09/2018
@version 1.0
/*/
METHOD GetGuid(nOrder, cChave, cTabela, cCampo) CLASS SmIntegraFly
	Local aArea      := GetArea(cTabela)
	Local cRet       := ""
	
	dbSelectArea(cTabela) 
	(cTabela)->(DbSetOrder(nOrder))
	
	If MsSeek(cChave, .F.)
		cRet := (cTabela)->(&cCampo)
	EndIf
	
	RestArea(aArea)
	
	If Empty(cRet)
		cRet := "0" //rever busca do guid.
	EndIf
	
Return cRet

/*/{Protheus.doc} ErrorLog
M�todo para verificar o arquivo de log e incrementar os retornos de erro.

@author Lucas C. Victor
@since 10/12/2018
@version 1.0
/*/
METHOD ErrorLog(cEntidade, cJson, cError, cPlatform) CLASS SmIntegraFly
Local nHdl
Local cLogErr    := "" 
Default cError     := ""
Default cPlatform  := oObjFly:cPlatform

If Empty(cPlatform)
	Alert("N�o encontrada plataforma pra o CNPJ da empresa atual. Preencha manualmente atrav�s do par�metro MV_CGCPLAT")
Else
	If !File("C:\LOG\"+cPlatform+".txt")
		nHdl:= fCreate("C:\LOG\"+cPlatform+".txt")
	Else
		nHdl := FOpen("C:\LOG\"+cPlatform+".txt", FO_READWRITE + FO_SHARED )
	EndIf 
EndIf

If nHdl > 0
	cLogErr := "Entidade:" + CRLF + cEntidade + CRLF + CRLF + "JSON:" + CRLF + cJson + CRLF + CRLF + "Erro:" + CRLF + cError[1] + " / " + cError[2] + CRLF
	cLogErr += "-----------------------------------------------------------------------------------------" + CRLF + CRLF
	FSeek(nHdl, 0, FS_END)         
	FWrite(nHdl, CRLF + cLogErr )
EndIf 

If nHdl > 0
	FClose(nHdl) 
EndIf

Return 

/*/{Protheus.doc} SelectModule
M�todo que verifica o modulo com base no atributo do objeto inst�nciado ao iniciar a rotina.

@author Lucas C. Victor
@since 10/12/2018
@version 1.0
/*/
METHOD SelectModule(oDlg, oObjFly) CLASS SmIntegraFly
	If self:nModulo == 1
		SyncAuxFin(oDlg, oObjFly)
	ElseIf self:nModulo == 2
		SyncAuxFat(oDlg, oObjFly)
	ElseIf self:nModulo == 3
		SyncAuxCom(oDlg, oObjFly)
	ElseIf self:nModulo == 4
		SyncAuxEst(oDlg, oObjFly)
	EndIf
Return

/*/{Protheus.doc} CreateScreen
M�todo que cria a tela inicial para todos os m�dulos, chamando o identificador do m�dulo atual.

@author Lucas C. Victor
@since 10/12/2018
@version 1.0
/*///
Function CreateScreen(nTipo, nModulo)
Local oObjFly    := SmIntegraFly():New(nTipo, nModulo)
Local oFont      := TFont():New("Arial",,-15,.T.)
Default nTipo    := 0
Public lCancela := .F.

	If nTipo == 0
		Alert("Via par�metro, informe o tipo de migra��o: 1 - Produ��o, 2 - Homologa��o, 3- Local")
	Else
	    DEFINE DIALOG oDlg TITLE "Integra��o First x FLY01 - Gest�o" FROM 180,180 TO 450,900 PIXEL
			oSay := TSay():New( 040, 010, {|| "INICIANDO MIGRA��O..."},oDlg,, oFont,,,, .T., ,) 
			oMeter := TMeter():Create(oDlg,{|u|0},/*linha*/40,/*coluna*/100,100,225,15,,.T.)
			oMeter:setFastMode(.T.)	   
	   
			TButton():New( 85, 100, "INICIAR", oDlg,{|| oObjFly:SelectModule(oDlg, oObjFly) },50,015,,,.F.,.T.,.F.,,.F.,,,.F. )
			
			TButton():New( 85, 200, "CANCELAR", oDlg,{|| ShootDown(oDlg) },50,015,,,.F.,.T.,.F.,,.F.,,,.F. )
	        
	    ACTIVATE DIALOG oDlg CENTERED 
	EndIf

Return 

/*/{Protheus.doc} ShootDown
Para o envio dos dados e fecha a tela de sincronismo. 

@author Rafael Machado
@since 12/09/2018
@version 1.0
/*/
Static Function ShootDown(oDlg)

lCancela := .T.

oDlg:End()

Return
