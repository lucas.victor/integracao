#INCLUDE "TOTVS.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "ISAMQry.ch"


Main Function InstallApp(nTipo)
Local oRest      := SmRestClient():New("",, 2, 1,, .F.)
Local cUrl       := "http://licenciamento.serie1.com.br/api/Fly01Apps/InstallApp"
Local cPost      := ""
Local cError     := ""
Local cCnpj      := ""
Local cGet       := ""
Local cPlatform  := Alltrim(GetMV("MV_CGCPLAT"))
Local aFilInfo   := FwFilManut(cFilAnt)
Local oObjPlat   := {}
Default nTipo    := 2

If Empty(cPlatform)
	cCnpj := SmJustDig(aFilInfo[3][1][4])
	If nTipo == 1
		cGet := HttpGet("http://licenciamento.serie1.com.br/api/Fly01/ComplementosDocumento?documento="+cCnpj)
	Else
		cGet := HttpGet("http://licenciamentohomolog.serie1.com.br/api/Fly01/ComplementosDocumento?documento="+cCnpj)
	EndIf
	
	If SmJson2Obj(cGet, oObjPlat)
		cPlatform  := oObjPlat:aValues[1]
	EndIf
Else
	If nTipo == 1 
		cPlatform := cPlatform+".fly01.com.br"
	Else
		cPlatform := cPlatform+".fly01dev.com.br"
	EndIf
EndIf 


cPost := "clientId=9431cdb8-cb8e-4702-8276-37e91bfc2ad7"
cPost += "&secretKey=650a505a-12af-4e5a-9114-e92e6e1cc4e6"
cPost += "&PlatformUrl="+cPlatform


oRest:Post(cUrl, , cPost, @cError)


Return