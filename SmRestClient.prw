#Include 'Protheus.ch'
#Include 'ISAMQry.ch'

/*/{Protheus.doc} SmRestClient
Classe para comunica��o com servidores de WS Rest
	
@author thiago.santos
@since 26/05/2014
@version 1.0
/*/
CLASS  SmRestClient
	DATA cUrl
	DATA nTimeOut
	DATA nRetries
	DATA nRetryTime
	DATA lFuso
	DATA lCaseSensitive
	DATA nXmlJSON
	DATA lBar
	
	METHOD New(cUrl, lFuso, nXmlJSON, nRetries, nRetryTime, lBar) CONSTRUCTOR
	METHOD Post(uGet, uPost)
	METHOD Put(uGet, uPut)
	METHOD Delete(uGet, uPut)
	METHOD Get(uGet)

ENDCLASS

/*/{Protheus.doc} New
Construtor

@author thiago.santos
@since 26/05/2014
@version 1.0

@param cUrl, character, Url base do WS
@param lFuso, boolean, Se � para considerar fuso-hor�rio nas datas ou n�o (padr�o .F.)
@param nXmlJSON, numerico, Se o WS usar� 1 - XML ou 2 - JSON (padr�o 1)
@param nRetries, numerico, N�mero de tentativas caso algum m�todo retorne 503 - indispon�vel tempor�riamente (Padr�o 3)
@param nRetryTime, numerico, Delay entre cada tentativa, em milissegundos, caso algum m�todo retorne 503 (padr�o 1000)
/*/
METHOD New(cUrl, lFuso, nXmlJSON, nRetries, nRetryTime, lBar) CLASS SmRestClient
Default nRetryTime := 1000
Default nRetries   := 3
Default nXmlJSON   := 1
Default lFuso      := .F.
Default lBar       := .T.

self:cUrl     := cUrl
If Right(self:cUrl, 1) != "/" .And. lBar
	self:cUrl += "/"
EndIf
self:nRetries   := nRetries
self:nRetryTime := nRetryTime
self:nXmlJSON   := nXmlJSON
self:lCaseSensitive := .F.
self:nTimeOut := 120

Return


/*/{Protheus.doc} Post
Executa um m�todo POST do WS

@author thiago.santos
@since 26/05/2014
@version 1.0

@param cUrl, character, M�todo a executar (a requisi��o ser� URL base + cURL)
@param uGet, undefined, Par�metros Get. Pode ser um HashMap ou uma string com escape j� tratado
@param uPost, undefined, Par�metros Post. Pode ser um HashMap ou uma string com escape j� tratado
@param aError, array, Par�metro de sa�da. Caso ocorra algum erro, a primeira posi��o ser� o c�digo do erro, a segunda o retorno da requisi��o
@param cRet, character, Resposta sem tratamento do WS, isto �, sem texto, sem ser convertida em objeto
@param lAsync, boolean, Determina que a chamada ser� feita de maneira ass�ncrona, ou seja, n�o ir� travar o Sistema. Esta op��o far� com que a resposta do m�todo seja sempre nula e n�o apresente erro
@return object, um objeto de xml ou de json parseado, ou nulo caso ocorra algum erro
/*/
METHOD Post(cMethod, uGet, uPost, aError, cRet, lAsync, cToken) CLASS SmRestClient
Local cErro      := ""
Local cAviso     := ""
Local oRet       := nil
Default cToken   := ""

cRet := SmRestPost(self:cUrl+cMethod, uGet, uPost, @aError, self:lFuso, self:nXmlJSON, self:nRetries, self:nRetryTime,,, lAsync, self:nTimeOut, cToken)

If !Empty(cRet)
	If  (self:nXmlJSON == 1)
		oRet := XMLParser(cRet,"_", @cErro, @cAviso)
		If !Empty(cErro)
			aError := { "XML", cErro }
		EndIf
	Else
		If !SmJSON2Obj(cRet, @oRet, @cErro,, , self:lCaseSensitive)
			aError := { "JSON", cErro }
		EndIf
	Endif 
EndIf

Return oRet

/*/{Protheus.doc} SmRestPost
Executa um m�todo POST do WS

@author thiago.santos
@since 26/05/2014
@version 1.0

@param cUrl, character, M�todo a executar (a requisi��o ser� URL base + cURL)
@param uGet, undefined, Par�metros Get. Pode ser um HashMap ou uma string com escape j� tratado
@param uPost, undefined, Par�metros Post. Pode ser um HashMap ou uma string com escape j� tratado
@param aError, array, Par�metro de sa�da. Caso ocorra algum erro, a primeira posi��o ser� o c�digo do erro, a segunda o retorno da requisi��o
@param lFuso, boolean, Se � para considerar fuso-hor�rio nas datas ou n�o (padr�o .F.)
@param nXmlJSON, num�rico, Se o WS usar� 1 - XML ou 2 - JSON (padr�o 1)
@param nRetries, num�rico, N�mero de tentativas caso algum m�todo retorne 503 - indispon�vel tempor�riamente (Padr�o 3)
@param nRetryTime, num�rico, Delay entre cada tentativa, em milissegundos, caso algum m�todo retorne 503 (padr�o 1000)
@param cContentType, character, tipo do conte�do a enviar (assume-se application/json se nXmlJSOn = 2
@param cNoConnection, character, Erro padr�o a ser retornado caso n�o haja conex�o com a Internet
@param lAsync, boolean, determina se o retorno a execu��o ser� ass�ncrona. Se sim, o retorno da requisi��o n�o pode ser obtido.

@return character, retorno do m�todo ou vazio caso ocorra um erro
/*/
Function SmRestPost(cUrl, uGet, uPost, aError, lFuso, nXmlJSON, nRetries, nRetryTime, cContentType, cNoConnection, lAsync, nTimeOut, cToken)
Local nTries     := 0
Local cGet
Local cPost
Local cRet

Default nRetries := 3
Default nRetryTime := 1000
Default lAsync := .F.
Default cToken := ""

If nXmlJSON == 2
	Default cContentType := "application/json"
EndIf

If ValType(uGet) == "C"
	cGet := uGet
ElseIf ValType(uGet) == "O"
	cGet := SmHM2Http(uGet, lFuso, nXmlJSON)
EndIf

If ValType(uPost) == "C"
	cPost := uPost
ElseIf ValType(uPost) == "O"
	If cContentType == "application/json"
		cPost := SmObj2JSON(uPost, lFuso, nXmlJSON)
	Else
		cPost := SmHM2Http(uPost, lFuso, nXmlJSON)
	EndIf
EndIf

If lAsync
	StartJob("SmPostAux", GetEnvServer(), .F., cUrl, cGet, cPost, aError, cContentType, cNoConnection, nTries, nRetries, nRetryTime, lAsync, , cToken)
Else
	cRet := SmPostAux(@cUrl, @cGet, @cPost, @aError, @cContentType, @cNoConnection, @nTries, @nRetries, @nRetryTime, lAsync, , cToken)
EndIf

Return cRet

Function SmPostAux(cUrl, cGet, cPost, aError, cContentType, cNoConnection, nTries, nRetries, nRetryTime, lAsync, nTimeOut, cToken)
Local cRet
Default cToken := ""
                                
While Empty(cRet := SmHttpPost(cUrl, cGet, cPost, @aError, cContentType, cNoConnection, lAsync, nTimeOut, cToken)) ;
	.And. (Empty(aError) .Or. (aError[1] == "503")) ; //Servi�o temporariamente indispon�vel. Pode tentar de novo
	.And. nTries < nRetries //N�o atingiu o n�mero m�ximo de tentativas
	//
	++nTries
	If nTries < nRetries
		Sleep(nRetryTime) //Espera o tempo indicado pra tentar de novo
	EndIf
End

Return cRet

/*/{Protheus.doc} SmHttpPost
Executa o HttpGet com tratamentos para que, caso a resposta seja inv�lido,
o retorno seja apenas em branco
	
@author thiago.santos
@since 08/05/2014
@version 1.0
		
@param cUrl, character, url a ser chamada
@param cGet, character, Par�metros a serem informados para a url via get
@param cPost, character, Par�metros a serem informados para a url via post
@param aError, array, Caso ocorra algum erro, este par�metro ser� preenchido com um array onde a primeira posi��o � o c�digo do erro e a segunda o texto de retorno da requisi��o

@return character, retorno da resquisi��o ou vazio caso ocorra algum erro
/*/
Function SmHttpPost(cUrl, cGet, cPost, aError, cContentType, cNoConnection, lLog, nTimeOut, cToken)
Local aHeader    := {}
Local cHeader
Local nPos
Local cRet := ""
Default cPost := ""
Default cContentType := 'application/x-www-form-urlencoded'
Default cNoConnection := "Verifique sua conex�o com a Internet"
Default cToken := ""
Default nTimeOut := 120

aadd(aHeader,'User-Agent: Mozilla/4.0 (compatible; Protheus '+GetBuild()+')')
aadd(aHeader,'Content-Type: '+cContentType)
If !Empty(cToken)
	aadd(aHeader,'Authorization: bearer '+ cToken)
EndIf

cRet := HttpPost(cURL, cGet, cPost, nTimeOut, aHeader, @cHeader)

If lLog
	MemoWrite("HttpPost.log",	"URL: " + cUrl + CRLF ;
							+	"Params: " + SmIsNull(cGet, "") + CRLF ;
							+	"Post form: " + CRLF + SmIsNull(cPost, "") + CRLF ;
							+	"Post Header: " + CRLF + SmIsNull(aHeader[1], "") + CRLF + SmIsNull(aHeader[2], "") + CRLF ;
							+	"Return Header: " + CRLF ;
							+	SmIsNull(cHeader, "") + CRLF ;
							+	"Return: " + CRLF ;
							+	SmIsNull(cRet, ""))
EndIf

If  Empty(cHeader)
	aError := {"503", cNoConnection}
    cRet := ""
Else    
    //quebrando o cabe�alho da requisi��o
    nPos := at(chr(13)+chr(10), cHeader)
    aHeader := StrToKarr(Left(cHeader, nPos-1), " ")

    //O tratamento deve ser feito de acordo com o cHeader, que ter� informa��es
    //mais relevantes quanto ao sucesso ou n�o da opera��o.
    //� necess�rio analisar casos de retorno para decidir como isso deve ser feito
    If aHeader[2] >= "400" //Erro de cliente (4xx) ou servidor (5xx)
	    aError := {aHeader[2], cRet}
	    cRet := ""
	ElseIf Empty(cRet)
		aError := {"500", "Retorno com sucesso mas em branco"}
    EndIf
EndIf

Return cRet


/*/{Protheus.doc} Put
Executa um m�todo Put do WS

@author Rafael Machado
@since 20/09/2018
@version 1.0
/*/

METHOD Put(cUrl, uGet, uPut, aError, cRet, lAsync, cToken) CLASS SmRestClient
Local cErro      := ""
Local cAviso     := ""
Local oRet       := nil
Default cToken   := ""

cRet := SmRestPut(cUrl, uGet, uPut, @aError, self:lFuso, self:nXmlJSON, self:nRetries, self:nRetryTime,,, lAsync, self:nTimeOut, cToken)

If !Empty(cRet)
	If  (self:nXmlJSON == 1)
		oRet := XMLParser(cRet,"_", @cErro, @cAviso)
		If !Empty(cErro)
			aError := { "XML", cErro }
		EndIf
	Else
		If !SmJSON2Obj(cRet, @oRet, @cErro,, , self:lCaseSensitive)
			aError := { "JSON", cErro }
		EndIf
	Endif 
EndIf

Return oRet


/*/{Protheus.doc} SmRestPut
Executa um m�todo Put do WS

@author Rafael Machado
@since 20/09/2018
@version 1.0/*/
Function SmRestPut(cUrl, uGet, uPut, aError, lFuso, nXmlJSON, nRetries, nRetryTime, cContentType, cNoConnection, lAsync, nTimeOut, cToken)
Local nTries     := 0
Local cGet
Local cPut
Local cRet

Default nRetries := 3
Default nRetryTime := 1000
Default lAsync := .F.
Default cToken := ""

If nXmlJSON == 2
	Default cContentType := "application/json"
EndIf

If ValType(uGet) == "C"
	cGet := uGet
ElseIf ValType(uGet) == "O"
	cGet := SmHM2Http(uGet, lFuso, nXmlJSON)
EndIf

If ValType(uPut) == "C"
	cPut := uPut
ElseIf ValType(uPut) == "O"
	If cContentType == "application/json"
		cPut := SmObj2JSON(uPut, lFuso, nXmlJSON)
	Else
		cPut := SmHM2Http(uPut, lFuso, nXmlJSON)
	EndIf
EndIf

If lAsync
	StartJob("SmPutAux", GetEnvServer(), .F., cUrl, cGet, cPut, aError, cContentType, cNoConnection, nTries, nRetries, nRetryTime, lAsync, , cToken)
Else
	cRet := SmPutAux(@cUrl, @cGet, @cPut, @aError, @cContentType, @cNoConnection, @nTries, @nRetries, @nRetryTime, lAsync, , cToken)
EndIf

Return cRet

/*/{Protheus.doc} SmPutAux
Executa um m�todo Put do WS

@author Rafael Machado
@since 20/09/2018
@version 1.0/*/
Function SmPutAux(cUrl, cGet, cPut, aError, cContentType, cNoConnection, nTries, nRetries, nRetryTime, lAsync, nTimeOut, cToken)
Local cRet
Default cToken := ""
                                
While Empty(cRet := SmHttpPut(cUrl, cGet, cPut, @aError, cContentType, cNoConnection, lAsync, nTimeOut, cToken)) ;
	.And. (Empty(aError) .Or. (aError[1] == "503")) ; //Servi�o temporariamente indispon�vel. Pode tentar de novo
	.And. nTries < nRetries //N�o atingiu o n�mero m�ximo de tentativas
	//
	++nTries
	If nTries < nRetries
		Sleep(nRetryTime) //Espera o tempo indicado pra tentar de novo
	EndIf
End

Return cRet

/*/{Protheus.doc} SmHttpPut
Executa o SmHttpPut com tratamentos para que, caso a resposta seja inv�lido,
o retorno seja apenas em branco
	
@author Rafael Machado
@since 20/09/2018
@version 1.0
/*/
Function SmHttpPut(cUrl, cGet, cPut, aError, cContentType, cNoConnection, lLog, nTimeOut, cToken)
Local aHeader    := {}
Local cHeader
Local nPos
Local cRet := ""
Default cPut := ""
Default cContentType := 'application/x-www-form-urlencoded'
Default cNoConnection := "Verifique sua conex�o com a Internet"
Default cToken := ""
Default nTimeOut := 120

aadd(aHeader,'User-Agent: Mozilla/4.0 (compatible; Protheus '+GetBuild()+')')
aadd(aHeader,'Content-Type: '+cContentType)
If !Empty(cToken)
	aadd(aHeader,'Authorization: bearer '+ cToken)
EndIf

cRet := HttpQuote(cURL, "put", cGet, cPut, nTimeOut, aHeader, @cHeader)

If lLog
	MemoWrite("HttpPut.log",	"URL: " + cUrl + CRLF ;
							+	"Params: " + SmIsNull(cGet, "") + CRLF ;
							+	"Put form: " + CRLF + SmIsNull(cPut, "") + CRLF ;
							+	"Put Header: " + CRLF + SmIsNull(aHeader[1], "") + CRLF + SmIsNull(aHeader[2], "") + CRLF ;
							+	"Return Header: " + CRLF ;
							+	SmIsNull(cHeader, "") + CRLF ;
							+	"Return: " + CRLF ;
							+	SmIsNull(cRet, ""))
EndIf

If Empty(cHeader)
	aError := {"503", cNoConnection}
    cRet := ""
Else    
    //quebrando o cabe�alho da requisi��o
    nPos := at(chr(13)+chr(10), cHeader)
    aHeader := StrToKarr(Left(cHeader, nPos-1), " ")

    //O tratamento deve ser feito de acordo com o cHeader, que ter� informa��es
    //mais relevantes quanto ao sucesso ou n�o da opera��o.
    //� necess�rio analisar casos de retorno para decidir como isso deve ser feito
    If aHeader[2] >= "400" //Erro de cliente (4xx) ou servidor (5xx)
	    aError := {aHeader[2], cRet}
	    cRet := ""
	ElseIf Empty(cRet)
		aError := {"", ""}
    EndIf
EndIf

Return cRet


/*/{Protheus.doc} Delete
Executa um m�todo Delete do WS

@author Rafael Machado
@since 28/09/2018
@version 1.0
/*/

METHOD Delete(cUrl, aError, cRet, lAsync, cToken) CLASS SmRestClient
Local cErro      := ""
Local cAviso     := ""
Local oRet       := nil
Default cToken   := ""

cRet := SmRestDel(cUrl, @aError, self:lFuso, self:nXmlJSON, self:nRetries, self:nRetryTime,,, lAsync, self:nTimeOut, cToken)

If !Empty(cRet)
	If  (self:nXmlJSON == 1)
		oRet := XMLParser(cRet,"_", @cErro, @cAviso)
		If !Empty(cErro)
			aError := { "XML", cErro }
		EndIf
	Else
		If !SmJSON2Obj(cRet, @oRet, @cErro,, , self:lCaseSensitive)
			aError := { "JSON", cErro }
		EndIf
	Endif 
EndIf

Return oRet

/*/{Protheus.doc} SmRestDel
Executa um m�todo Delete do WS

@author Rafael Machado
@since 28/09/2018
@version 1.0/*/
Function SmRestDel(cUrl, aError, lFuso, nXmlJSON, nRetries, nRetryTime, cContentType, cNoConnection, lAsync, nTimeOut, cToken)
Local nTries     := 0
Local cGet
Local cPut
Local cRet

Default nRetries := 3
Default nRetryTime := 1000
Default lAsync := .F.
Default cToken := ""

If lAsync
	StartJob("SmDelAux", GetEnvServer(), .F., cUrl, aError, cContentType, cNoConnection, nTries, nRetries, nRetryTime, lAsync, , cToken)
Else
	cRet := SmDelAux(@cUrl, @aError, @cContentType, @cNoConnection, @nTries, @nRetries, @nRetryTime, lAsync, , cToken)
EndIf

Return cRet

/*/{Protheus.doc} SmDelAux
Executa um m�todo Delete do WS

@author Rafael Machado
@since 28/09/2018
@version 1.0/*/
Function SmDelAux(cUrl, aError, cContentType, cNoConnection, nTries, nRetries, nRetryTime, lAsync, nTimeOut, cToken)
Local cRet
Default cToken := ""
                                
While Empty(cRet := SmHttpDel(cUrl, @aError, cContentType, cNoConnection, lAsync, nTimeOut, cToken)) ;
	.And. (Empty(aError) .Or. (aError[1] == "503")) ; //Servi�o temporariamente indispon�vel. Pode tentar de novo
	.And. nTries < nRetries //N�o atingiu o n�mero m�ximo de tentativas
	//
	++nTries
	If nTries < nRetries
		Sleep(nRetryTime) //Espera o tempo indicado pra tentar de novo
	EndIf
End

Return cRet

/*/{Protheus.doc} SmHttpDel
Executa o delete com tratamentos para que, caso a resposta seja inv�lido,
o retorno seja apenas em branco
	
@author Rafael Machado
@since 28/09/2018
@version 1.0
/*/
Function SmHttpDel(cUrl, aError, cContentType, cNoConnection, lLog, nTimeOut, cToken)
Local aHeader    := {}
Local cHeader
Local nPos
Local cRet := ""
Default cContentType := 'application/x-www-form-urlencoded'
Default cNoConnection := "Verifique sua conex�o com a Internet"
Default cToken := ""
Default nTimeOut := 120

aadd(aHeader,'User-Agent: Mozilla/4.0 (compatible; Protheus '+GetBuild()+')')
aadd(aHeader,'Content-Type: '+cContentType)
If !Empty(cToken)
	aadd(aHeader,'Authorization: bearer '+ cToken)
EndIf

cRet := HttpQuote(cURL, "delete", "", "", nTimeOut, aHeader, @cHeader)

If lLog
	MemoWrite("HttpDel.log",	"URL: " + cUrl + CRLF ;
							+	"Del Header: " + CRLF + SmIsNull(aHeader[1], "") + CRLF + SmIsNull(aHeader[2], "") + CRLF ;
							+	"Return Header: " + CRLF ;
							+	SmIsNull(cHeader, "") + CRLF ;
							+	"Return: " + CRLF ;
							+	SmIsNull(cRet, ""))
EndIf

If  Empty(cHeader)
	aError := {"503", cNoConnection}
    cRet := ""
Else    
    //quebrando o cabe�alho da requisi��o
    nPos := at(chr(13)+chr(10), cHeader)
    aHeader := StrToKarr(Left(cHeader, nPos-1), " ")

    //O tratamento deve ser feito de acordo com o cHeader, que ter� informa��es
    //mais relevantes quanto ao sucesso ou n�o da opera��o.
    //� necess�rio analisar casos de retorno para decidir como isso deve ser feito
    If aHeader[2] >= "400" //Erro de cliente (4xx) ou servidor (5xx)
	    aError := {aHeader[2], cRet}
	    cRet := ""
	ElseIf Empty(cRet)
		aError := {"", ""}
    EndIf
EndIf

Return cRet


/*/{Protheus.doc} Get
Executa um m�todo GET do WS

@author thiago.santos
@since 26/05/2014
@version 1.0

@param cUrl, character, M�todo a executar (a requisi��o ser� URL base + cURL)
@param uGet, undefined, Par�metros Get. Pode ser um HashMap ou uma string com escape j� tratado
@param aError, array, Par�metro de sa�da. Caso ocorra algum erro, a primeira posi��o ser� o c�digo do erro, a segunda o retorno da requisi��o

@return object, um objeto de xml ou de json parseado, ou nulo caso ocorra algum erro
/*/
METHOD Get(cMethod, uGet, aError, cRet, lAsync) CLASS SmRestClient
Local cErro      := ""
Local cAviso     := ""
Local oRet       := nil

cRet := SmRestGet(self:cUrl+cMethod, uGet, @aError, self:lFuso, self:nXmlJSON, self:nRetries, self:nRetryTime, lAsync, self:nTimeOut)

If !Empty(cRet)
	If  Empty(self:nXmlJSON) .Or. (self:nXmlJSON == 1)
		oRet := XMLParser(cRet,"_", @cErro, @cAviso)
		If !Empty(cErro) .or. !Empty(cAviso)
			aError := { "XML", cErro, cAviso }
		EndIf
	Else
		If !SmJSON2Obj(cRet, @oRet, @cErro)
			aError := { "JSON", cErro }
		EndIf
	Endif 
EndIf
Return oRet

/*/{Protheus.doc} RestTest
M�todo de teste para o objeto SmRestClient. Veja que a vari�vel cUrl deve ser
alterada para um m�todo de WS que esteja funcionando.
Atualmente a fun��o usa m�todos de um ambiente uMov.me. Se o token deste ambiente
for recriado, o retorno ser� um erro

@author thiago.santos
@since 26/05/2014
@version 1.0

/*/
Main Function RestTest()
	Local cUrl    := "10459ece4d259427d3ce2ec3518372a9019c6/item.xml"
	Local aParams := Array(#) //Par�metros Post
	Local aXml    := nil
	Local oRestClient := SmRestClient():New("http://api.umov.me/CenterWeb/api")
	Local aError

	aParams[#"data"] := Array(#)
	aXml := aParams[#"data"]
	aXml[#"item"] := Array(#) //Tag item
	PutSubGrupo(aXml[#"item"]) //Tag subGroup
	aXml[#"item", "description"] := "Refri 2"
	aXml[#"item", "active"] := .t.
	aXml[#"item", "alternativeIdentifier"] := "9999"
	PutitCateg(aXml[#"item"]) //Tag subGroup
	cRet := SmObj2Xml(aXml)
	MsgInfo("XML: "+cRet)
	cRet := SmHM2Http(aParams)
	MsgInfo("Par�metros: "+cRet)
	
	//cParam := "<activityHistory><executionExportStatus>true</executionExportStatus></activityHistory>"
	//cParam := "data="+EscapeGet(cParam)

	//If !Empty(oRet := oRestClient:Get(cUrl, cParam, @aError))

	If !Empty(oRet := oRestClient:Post(cUrl,, aParams, @aError))
		MsgInfo("Resposta: "+cRet)
	Else
		MsgInfo("Erro: "+cError)
	EndIf
Return

/*/{Protheus.doc} putSubGrupo
Fun��o que faz parte do teste de SmRestClient. Ela adiciona um n� no hashmap

@author thiago.santos
@since 26/05/2014
@version 1.0

@param aNode, object, hashmap onde o n� ser� adicionado
/*/
Static Function putSubGrupo(aNode)

aNode[#"subGroup"] := Array(#)
aNode[#"subGroup", "id"] := "118591"

Return

/*/{Protheus.doc} PutitCateg
Fun��o que faz parte do teste de SmRestClient. Ela adiciona um n� no hashmap

@author thiago.santos
@since 26/05/2014
@version 1.0

@param aNode, object, hashmap onde o n� ser� adicionado
/*/
Static Function PutitCateg(aNode)

aNode[#"itemCategory"] := Array(#)
aNode[#"itemCategory", "id"] := "40960"

Return


/*/{Protheus.doc} SmRestGet
Executa um m�todo GET do WS

@author thiago.santos
@since 26/05/2014
@version 1.0

@param cUrl, character, M�todo a executar (a requisi��o ser� URL base + cURL)
@param uGet, undefined, Par�metros Get. Pode ser um HashMap ou uma string com escape j� tratado
@param aError, array, Par�metro de sa�da. Caso ocorra algum erro, a primeira posi��o ser� o c�digo do erro, a segunda o retorno da requisi��o
@param lFuso, boolean, Se � para considerar fuso-hor�rio nas datas ou n�o (padr�o .F.)
@param nXmlJSON, num�rico, Se o WS usar� 1 - XML ou 2 - JSON (padr�o 1)
@param nRetries, num�rico, N�mero de tentativas caso algum m�todo retorne 503 - indispon�vel tempor�riamente (Padr�o 3)
@param nRetryTime, num�rico, Delay entre cada tentativa, em milissegundos, caso algum m�todo retorne 503 (padr�o 1000)

@return character, retorno do m�todo ou vazio caso ocorra um erro
/*/
Function SmRestGet(cUrl, uGet, aError, lFuso, nXmlJSON, nRetries, nRetryTime, lAsync, nTimeOut)
Local nTries     := 0
Local cGet
Local cRet
Default nRetries := 3
Default nRetryTime := 1000
Default lAsync := .F.

If ValType(uGet) == "C"
	cGet := uGet
ElseIf ValType(uGet) == "O"
	cGet := SmHM2Http(uGet, lFuso, nXmlJSON)
EndIf

If lAsync
	StartJob("SmGetAux", GetEnvServer(), .F., cUrl, cGet, aError, nTries, nRetries, nRetryTime, lAsync, nTimeOut)
Else
	cRet := SmGetAux(@cUrl, @cGet, @aError, @nTries, @nRetries, @nRetryTime, lAsync, nTimeOut)
EndIf

Return cRet

Function SmGetAux(cUrl, cGet, aError, nTries, nRetries, nRetryTime, lAsync, nTimeOut)
Local cRet
While Empty(cRet := SmHttpGet(cUrl, cGet, @aError, lAsync, nTimeOut)) ;
	.And. (Empty(aError) .Or. (aError[1] == "503")) ; //Servi�o temporariamente indispon�vel. Pode tentar de novo
	.And. nTries < nRetries //N�o atingiu o n�mero m�ximo de tentativas
	//
	++nTries
	If nTries < nRetries
		Sleep(nRetryTime) //Espera o tempo indicado pra tentar de novo
	EndIf
End

Return cRet

/*/{Protheus.doc} SmHttpGet
Executa o HttpGet com tratamentos para que, caso a resposta seja inv�lido,
o retorno seja apenas em branco
	
@author thiago.santos
@since 08/05/2014
@version 1.0
		
@param cUrl, character, url a ser chamada
@param cParams, character, Par�metros a serem informados para a url
@param aError, array, Caso ocorra algum erro, este par�metro ser� preenchido com um array onde a primeira posi��o � o c�digo do erro e a segunda o texto de retorno da requisi��o

@return character, retorno da resquisi��o ou vazio caso ocorra algum erro
/*/
Function SmHttpGet(cUrl, cParams, aError, lLog, nTimeOut)
Local cHeader
Local aHeader
Local nPos
Local cRet
Default nTimeOut := 120

cRet := HttpGet(cURL, cParams, nTimeOut, {}, @cHeader)
If lLog
	MemoWrite("HttpGet.log",	"URL: " + SmIsNull(cUrl, "") + CRLF ;
							+	"Params: " + SmIsNull(cParams, "") + CRLF ;
							+	"Header: " + CRLF + SmIsNull(cParams, "") + CRLF ;
							+	SmIsNull(cHeader, "") + CRLF ;
							+	"Return: " + CRLF ;
							+	SmIsNull(cRet, ""))
EndIf

If  Empty(cHeader)
	aError := {"503", "Verifique sua conex�o com a Internet"}
    cRet := ""
Else    
    //quebrando o cabe�alho da requisi��o
    nPos := at(chr(13)+chr(10), cHeader)
    aHeader := StrToKarr(Left(cHeader, nPos-1), " ")

    //O tratamento deve ser feito de acordo com o cHeader, que ter� informa��es
    //mais relevantes quanto ao sucesso ou n�o da opera��o.
    //� necess�rio analisar casos de retorno para decidir como isso deve ser feito
    If aHeader[2] >= "400" //Erro de cliente (4xx) ou servidor (5xx)
	    aError := {aHeader[2], cRet}
	    cRet := ""
    EndIf
EndIf

Return cRet

Static Function SmIsNull(uValue, uDefault)

Return IIF(ValType(uValue) == "U", uDefault, uValue)