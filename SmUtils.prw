#Include 'Protheus.ch'
#Include 'SmUtils.ch'
#Include 'ISamqry.ch'
#INCLUDE "FWMVCDEF.CH"

Function TryGet(bCode, uDefault)
Local lError     := .F.
Local bError     := ErrorBlock( { |oError| lError := .T. } )
Local uRet

BEGIN SEQUENCE
	uRet := Eval(bCode)
END
ErrorBlock(bError)

If lError
	uRet := uDefault
Endif
Return uRet

Function IsNull(uValue)

Return ValType(uValue) == "U"

/*/{Protheus.doc} Between
Retorna se nValor est� entre nInicial e nFinal  

@param uValor, undefined, Valor a verificar
@param uInicial, undefined, Valor inicial do intervalo
@param uFinal, undefined, Valor final do intervalo
@param nAberto, boolean,  Determina se o intervalo � fechado (0), aberto na esquerda (1), aberto na direita (2) ou aberto (3)

@return  cRet  Retorna .T. se nValor est� dentro do intervalo, falso sen�o
@author Thiago Oliveira Santos
@since 13/11/2012
@version 1.0
/*/
Function Between(uValor, uInicial, uFinal, nAberto)
Local lRet := .F.

if (nAberto == NIL .or. nAberto == 0)
	lRet := (uInicial <= uValor .And. uValor <= uFinal)
elseif nAberto == 1
	lRet := (uInicial < uValor .And. uValor <= uFinal)
elseif nAberto == 2
	lRet := (uInicial <= uValor .And. uValor < uFinal)
Else
	lRet := (uInicial < uValor .And. uValor < uFinal)
endif

Return lRet

/*/{Protheus.doc} xSeconds
Retorna os segundos decorridos a partir de uma data  

@param dDate, date, data base para o c�lculo

@return  integer, segundos

@author Thiago Oliveira Santos
@since 29/11/2013
@version 1.0
/*/
Function xSeconds(dDate)

Return (Date() - dDate)*86400 + Seconds()

/*/{Protheus.doc} ChkTimeOut
Calcula o tempo decorrido para controle de timeout  

@param aTimeout, array, informa��es para c�lculo do TimeOut

@return  integer, Segundos decorridos

@author Thiago Oliveira Santos
@since 29/11/2013
@version 1.0
/*/
Function ChkTimeOut(aTimeout)

Default aTimeout := { date(), xSeconds(date()) }

Return xSeconds(aTimeout[1]) - aTimeout[2]


/*/{Protheus.doc} SmIsHtml
Determina se o Smartclient � html  

@return  boolean, verdadeiro se est� rodando em smartclienthtml, falso sen�o

@author Thiago Oliveira Santos
@since 29/11/2013
@version 1.0
/*/
Function SmIsHtml()

Return GetRemoteType() == 5


/*/{Protheus.doc} SmIsJob
Determina se o processo foi chamado a partir de Job  

@return  boolean, verdadeiro se est� rodando em job, falso sen�o

@author Thiago Oliveira Santos
@since 22/08/2014
@version 1.0
/*/
Function SmIsJob()

Return GetRemoteType() == -1

	
/*/{Protheus.doc} SmProtExec
	Executa uma fun��o de maneira protegida. Utilize para chamadas de novas fun��es de bin�rio
@author thiago.santos
@since 23/01/2014
@version 1.0
		
@param cFunction, character, Fun��o a ser chamada
@param aParameters, array, par�metros que dever�o ser passados para a fun��o

@return undefined, retorno da fun��o

/*/
Function SmProtExec(cFunction, aParameters)
Local cCode      := ""
Local uRet
Local nI
Default aParameters := {}

If FindFunction(cFunction)
	For nI := 1 To Len(aParameters)
		If nI > 1
			cCode += ","
		EndIf
		cCode += Any2Str(aParameters[nI])
	Next
	cCode := cFunction+"("+cCode+")"
	uRet := &cCode.
EndIf

Return uRet

/*/{Protheus.doc} SmNextSeq
	Calcula o pr�ximo valor em uma sequ�ncia e retorna
@author thiago.santos
@since 23/01/2014
@version 1.0
		
@param uSeq, undefined, Nome da sequ�ncia ou bloco de c�digo que retorna seu valor
@param cTable, character, nome da tabela onde a sequ�ncia � procurada
@param nIndex, numeric, n�mero do ind�ce a procurar
@param nMax, numeric, n�mero m�ximo de tentativas de obter uma chave n�o utilizada
@param bNext, codeblock, bloco de c�digo para retornar o pr�ximo n�mero da sequ�ncia
@param uPartKey, undefined, se caracter, in�cio da chave a buscar, onde a sequ�ncia restante � concatenada. Se bloco de c�digo, define a chave de busca
@param cError, character, retorna algum erro ocorrido
@param bCond, codeblock, condi��o que deve ser atendida para o registro ser aceito, al�m do pr�prio seek em si

@return undefined, retorno da fun��o

/*/
Function SmNextSeq(uSeq, cTable, nIndex, nMax, bNext, uPartKey, cError, bCond)
Local cChave     := Alltrim(IIF(ValType(uSeq) == "B", Eval(uSeq), GetMv(uSeq)))
Local aArea      := GetAreas({ cTable })
Local nCont      := 0
Local bPartKey
Local cIndex
Local cNewChave

If !Empty(cChave)
	Default uPartKey := ""
	If ValType(uPartKey) == "C"
		bPartKey := {|cChave, cTable| xFilial(cTable)+uPartKey+cChave }
	Else
		bPartKey := uPartKey
	EndIf
	Default cIndex := Eval(bPartKey, cChave, cTable)
	Default nIndex := 1
	Default nMax := 0
	Default bNext := {|| Soma1(cChave, Len(cChave)) }

	If SmCondSeek(cIndex, bCond, nIndex, cTable)
		While !Eof() .And. (nMax == 0 .Or. nCont < nMax) ;
			.And. Left(&(IndexKey()), Len(cIndex)) == cIndex ;
			//
			//Se houver condi��o de filtor, ignora registros que n�o a atendam para a valida��o
			If Empty(bCond) .Or. Eval(bCond)
				nCont++
				//O While abaixo � para evitar registros que j� est�o duplicados na tabela
				While !Eof() .And. Left(&(IndexKey()), Len(cIndex)) == cIndex
					dbSkip()
				End
				cNewChave := Eval(bNext)
				If cChave > cNewChave //Significa que estourou o n�mero m�ximo de combina��es e voltou para a inicial
					cError := STR0016
					cChave := Space(Len(cChave))
					Exit
				EndIf
				cChave := cNewChave
				cIndex := Eval(bPartKey, cChave, cTable)
			Else
				dbSkip()
			EndIf
		End
	EndIf
	
	RestAreas(aArea)
EndIf

Return cChave

/*/{Protheus.doc} SmGetSX8
	Calcula o pr�ximo valor em uma sequ�ncia e retorna
@author thiago.santos
@since 23/01/2014
@version 1.0
		
@param uSeq, undefined, Nome da sequ�ncia ou bloco de c�digo que retorna seu valor
@param cTable, character, nome da tabela onde a sequ�ncia � procurada
@param nIndex, numeric, n�mero do ind�ce a procurar
@param nMax, numeric, n�mero m�ximo de tentativas de obter uma chave n�o utilizada
@param bNext, codeblock, bloco de c�digo para retornar o pr�ximo n�mero da sequ�ncia
@param uPartKey, character, in�cio da chave a buscar, onde a sequ�ncia restante � concatenada
@param cError, character, retorna algum erro ocorrido
@param bCond, codeblock, condi��o que deve ser atendida para o registro ser aceito, al�m do pr�prio seek em si

@return undefined, retorno da fun��o

/*/
Function SmGetSX8(cTable, cField, nIndex, nMax, uPartKey, cError, bCond)
Default uPartKey := ""

Return SmNextSeq({|| GetSX8Num(cTable, cField)}, cTable, nIndex, nMax;
				, {|| ConfirmSx8(), GetSX8Num(cTable, cField)}, uPartKey, @cError, bCond)

/*/{Protheus.doc} ValidCodMun
Valida��o de codmun para cadastro de empresa/filial  

@author Bruno Santos
@since 09/09/2015
@version 1.0
/*/
Function ValidCodMun(cCod)
lRet := .T.
If Empty(cCod)
	HELP(2,"",STR0015 + CHR(13)+ CHR(10)+STR0011, STR0012 ) //"O campo C�DIGO DO MUNICIPIO n�o foi informado","Preencha o campo C�DIGO DO MUNICIPIO")  
	lRet := .F.
EndIf

If lRet .And. !Empty(cCod) .And. Len(Alltrim(cCod)) != 7
	HELP(2,"",STR0015 + CHR(13)+ CHR(10)+STR0013, STR0014 ) //"O campo C�DIGO DO MUNICIPIO deve conter 7 d�gitos.","Preencha o campo C�DIGO DO MUNICIPIO com os 7 d�gitos, em caso de d�vidas, selecione o campo, aperte F3 e escolha o munic�pio")  
	lRet := .F.
EndIf
Return lRet

/*/{Protheus.doc} ValidCep
Valida��o de cep para cadastro de empresa/filial  

@author Bruno Santos
@since 09/09/2015
@version 1.0
/*/

Function ValidCep(cCep)
lRet := .F.
cCep := StrTran(cCep,"-","")
If Empty(cCep)
	HELP(2,"",STR0015 + CHR(13)+ CHR(10)+STR0009, STR0010 ) //"O campo CEP n�o foi informado","Preencha o campo CEP")  
	lRet := .F.
Else
	lRet := .T.
EndIf
Return lRet

/*/{Protheus.doc} ValidCnpj
Valida��o de cnpj para cadastro de empresa/filial  

@author Bruno Santos
@since 09/09/2015
@version 1.0
/*/
Function ValidCnpj(cCnpj)
Local lRet := .T.

If Empty(SmJustDig(cCnpj))
	HELP(2,"",STR0015 + CHR(13)+ CHR(10)+STR0001, STR0002 ) //"O campo CNPJ n�o foi informado","Preencha o campo CNPJ")
	lRet := .F.
EndIf
Return lRet

/*/{Protheus.doc} CadFilTdOK
P�s valida��o do cadastro de filial.  

@author Rafael Santos Machado
@since 30/01/2014
@version 1.0
/*/
Function CadFilTdOK(oModel0)
Local lRet       := .T.
Local nOper      := oModel0:GetOperation()
Local cId        := oModel0:GetId()
Local oModel
Local oXX1
Local oXX2
Local nQtd
Local nI

If cId == "FWCADFIL"
	oModel := oModel0	
Else
	oModel := oModel0:GetModel()
EndIf

oXX1 := oModel:GetModel("FWCADFIL_XX1")
oXX2 := oModel:GetModel("FWCADFIL_XX2")
nQtd       := oXX2:GetQtdLine()

//TODO: Implementar valida��o de nome de pa�s no futuro
//Por enquanto, sempre for�ar Brasil
If nOper == MODEL_OPERATION_INSERT .Or. nOper == MODEL_OPERATION_UPDATE
	oXX1:SetValue("XX1_PAIS", "BRASIL")
	For nI := 1 To nQtd
		oXX2:GoLine(nI)
		oXX2:SetValue("XX2_PAIS", "BRASIL")
	Next
EndIf

Return lRet

/*/{Protheus.doc} cCodEst
Fun��o que retorna o c�digo do estado para o correto preenchimento do c�digo do munic�pio no cadastro de filiais.

@author Rafael Santos Machado
@since 03/02/2014
@version 1.0
/*/
Function cCodEst(cEstado)
Local uRet       := ""
Local nPesq
Local nPosRet
Local nRet

If !Type("aUF") == "A"
	Static aUF := {}
	aadd(aUF,{"RO","11"})
	aadd(aUF,{"AC","12"})
	aadd(aUF,{"AM","13"})
	aadd(aUF,{"RR","14"})
	aadd(aUF,{"PA","15"})
	aadd(aUF,{"AP","16"})
	aadd(aUF,{"TO","17"})
	aadd(aUF,{"MA","21"})
	aadd(aUF,{"PI","22"})
	aadd(aUF,{"CE","23"})
	aadd(aUF,{"RN","24"})
	aadd(aUF,{"PB","25"})
	aadd(aUF,{"PE","26"})
	aadd(aUF,{"AL","27"})
	aadd(aUF,{"MG","31"})
	aadd(aUF,{"ES","32"})
	aadd(aUF,{"RJ","33"})
	aadd(aUF,{"SP","35"})
	aadd(aUF,{"PR","41"})
	aadd(aUF,{"SC","42"})
	aadd(aUF,{"RS","43"})
	aadd(aUF,{"MS","50"})
	aadd(aUF,{"MT","51"})
	aadd(aUF,{"GO","52"})
	aadd(aUF,{"DF","53"})
	aadd(aUF,{"SE","28"})
	aadd(aUF,{"BA","29"})
	aadd(aUF,{"EX","99"})
EndIf

If ValType(cEstado) == "C" .And. !Empty(cEstado)
	nPesq := IIF(IsDigit(Left(cEstado, 1)), 1, 2)
	nPosRet := IIF(nPesq == 2, 1, 2)
	nRet := aScan(aUF, {|aLine| aLine[nPosRet] == cEstado })
	If nRet > 0
		uRet := aUF[nRet, nPesq]
	EndIf
//Else
	//uRet := aUF
EndIf
	
Return uRet

/*/{Protheus.doc} SmAbateDev
Fun��o para verificar de h� notas de devolu��o
@param cDoc, String, Numero do documento
@param cSerie, String, Serie do documento  
@param cPessoa, String, Pessoa do documento
@param cProduto, String, C�digo de Produto
@param cItem, String, Item do documento
@param lImposto, Logico, verifica o parametro mv_par12
@return array, Posi��o 1: Quantidade; Posi��o 2: Valor
@author Janaina Jesus
@since 19/02/2014
/*/
Function SmAbateDev(cDoc, cSerie, cPessoa, cProduto, cItem, lImposto, lDesc)
Local aArea      := GetAreas({ "SD1" })
Local aRet       := {}

Default lImposto := .T.
Default lDesc    := .F.

dbSelectArea("SD1")
dbSetOrder(5) //D1_FILIAL+D1_TIPO+D1_CODPROD+D1_PESSOA

If SMSeek(xFilial("SD1")+'2'+cProduto+cPessoa)
	While !Eof() .And. SD1->D1_FILIAL+SD1->D1_TIPO+SD1->D1_CODPROD+SD1->D1_PESSOA == xFilial("SD1")+'2'+cProduto+cPessoa 
	
		If SD1->D1_NFORI == cDoc .And. SD1->D1_SERIORI == cSerie .And. SD1->D1_ITEMORI == cItem
			dbSelectArea("SF1")  
			dbSetOrder(1)//F1_FILIAL+F1_DOC+F1_SERIE+F1_PESSOA+F1_TIPO 
			
			SF1->(SMSeek(xFilial("SF1")+SD1->D1_DOC+SD1->D1_SERIE+SD1->D1_PESSOA))
			If SF1->F1_STATUS <> "2"
				aAdd (aRet, {IIF(lImposto,SD1->D1_VALBRUT,IIF(lDesc, SD1->D1_TOTAL - SD1->D1_VALDESC, SD1->D1_TOTAL)),SD1->D1_QUANT,SD1->D1_VALIPI})
			EndIf
			Exit
		EndIf
		
		DbSkip()
	EndDo
EndIf

RestAreas( aArea )
Return aRet

/*/{Protheus.doc} SmGetDev
	Fun��o para verificar se h� devolu��o 
	
@param cPessoa, String, Pessoa do documento
@param cDoc, String, Numero do documento
@param cSerie, String, Serie do documento  
@param cItem, String, Item do documento
@param cTipo, String, tipo de documento
@param cProduto, String, C�digo de Produto

@return array, Posi��o 1: Valor total bruto; Posi��o 2: Valor total; Posi��o 3: Total quantidade 

@author Bruno Akyo Kubagawa
@since 19/02/2014
/*/
Function SmGetDev(cPessoa, cDoc, cSerie, cItem, cTipo, cProduto)
Local aArea      := GetArea()
Local aRet       := {}
Local aTotalDev  := {}
Local aParams    := {}
Local cWhere     := ""

//D1_FILIAL+D1_PESSOA+D1_NFORI+D1_SERIORI+D1_ITEMORI+D1_TIPO
cWhere := "D1_FILIAL == ?xFilial('SB1')? .And. "
cWhere += "D1_PESSOA == ?cPessoa? .And. "
cWhere += "D1_NFORI == ?cDoc? .And. "
cWhere += "D1_SERIORI == ?cSerie?"
aAdd(aParams,{"cPessoa", cPessoa})
aAdd(aParams,{"cDoc", cDoc})
aAdd(aParams,{"cSerie", cSerie})

If !Empty(cItem)
	cWhere += " .And. D1_ITEMORI == ?cItem?"
	aAdd(aParams,{"cItem", cItem})
EndIf

If !Empty(cTipo)
	cWhere += " .And. D1_TIPO == ?cTipo?"
	aAdd(aParams,{"cTipo", cTipo})
EndIf

If !Empty(cProduto)
	cWhere += " .And. D1_CODPROD == ?cProduto?"
	aAdd(aParams,{"cProduto", cProduto})
EndIf

aRet := XSELECT #SUM(D1_VALBRUT), #SUM(D1_TOTAL), #SUM(D1_QUANT) ;
	FROM (TAB SD1 ;
		WHERE &cWhere ;
		ORDER 13 ;
	) ;
PARAMS aParams 


RestArea( aArea )
Return aRet

/*/{Protheus.doc} SmModCent
	Verificar se a empresa � centralizada de acordo com o modulo passado.

@param cEmpresa, character , C�digo da Empresa/Filial
@param cModulo, character, Estoque - Financeiro - Estoque - Faturamento

@return boolean, se � centralizado .T., sen�o .F. 

@author Bruno Akyo Kubagawa
@since 19/02/2014
/*/
Function SmModCent(cEmpFil, cModulo)
Local lRet       := .T.
Local aAreaX1    := XX1->(GetArea())
Local cModulos   := ""
Local cCentral   := ""

/* XX1_MODULO
1 - Financeiro
2 - Compras
3 - Estoque 
4 - Faturamento
*/

DbSelectArea("XX1")
XX1->(DbSetOrder(1))
If XX1->(SMSeek(SubStr(cEmpFil,1,2)))
	cModulos := XX1->XX1_MODULO
EndIf

Do Case 
	Case AllTrim(Upper(cModulo)) == "FINANCEIRO"
		cCentral := SubStr(cModulos,1,1)
	Case AllTrim(Upper(cModulo)) == "COMPRAS"
		cCentral := SubStr(cModulos,2,1)
	Case AllTrim(Upper(cModulo)) == "ESTOQUE"
		cCentral := SubStr(cModulos,3,1)
	Case AllTrim(Upper(cModulo)) == "FATURAMENTO"
		cCentral := SubStr(cModulos,4,1)
End Case

lRet := cCentral == "S"
RestArea(aAreaX1)

Return lRet

/*/{Protheus.doc} SmGatTes
Faz o gatilho do campo de TES de acordo com o par�metro MV_GETUPRC

@return cTes, character, C�digo da TES 
@author Rafael Santos Machado
@since 14/05/2014
/*/

Function SmGatTes(cPessoa, cCodProd)
Local aArea	  := GetArea()
Local cTes      := ""

dbSelectArea("SA5")
dbSetOrder(1) // A5_FILIAL+A5_FORNECE+A5_PRODUTO
DbGoTop()
If SMSeek(xFilial("SA5")+cPessoa+cCodProd) .And. GetMv("MV_GETUPRC") == "1" .And. !Empty(SA5->A5_TES)	
	cTes := SA5->A5_TES
	MaFisRef("IT_TES","MT100",cTes)
Else
	cTes := FRefTES(cCodProd,"E")
EndIf	

RestArea(aArea)

Return cTes


/*/{Protheus.doc} SmGatPrcUn
Faz o gatilho do campo de Pre�o Unit�rio de acordo com o par�metro MV_GETUPRC

@return cTes, character, C�digo da TES 
@author Rafael Santos Machado
@since 14/05/2014
/*/

Function SmGatPrcUn(cPessoa, cCodProd, lForce)
Local aArea	  := GetArea()
Local nPrcUn    := 0
Default lForce  := .F.

dbSelectArea("SA5")
dbSetOrder(1) // A5_FILIAL+A5_FORNECE+A5_PRODUTO
DbGoTop()
If (lForce .Or. GetMv("MV_GETUPRC") == "1") .And. SMSeek(xFilial("SA5")+cPessoa+cCodProd)	
	nPrcUn := SA5->A5_UPRC
	MaFisRef("IT_PRCUNI","MT100",nPrcUn)
EndIf

RestArea(aArea)

Return nPrcUn

/*/{Protheus.doc} SmGetSet
Cria um bloco de c�digo para utiliza��o em objetos msget
	
@author thiago.santos
@since 26/05/2014
@version 1.0
		
@param uInfo, undefined, vari�vel que receber� o valor

@return undefined, Valor retornado ou atribu�do
/*/
Function SmGetSet(uInfo)

Return {|uNewValue| IIF(pCount() == 1, uInfo := uNewValue, uInfo) }

/*/{Protheus.doc} SmArrGetSet
Cria um bloco de c�digo para utiliza��o em objetos msget
	
@author thiago.santos
@since 26/05/2014
@version 1.0
		
@param aArray, array, Array onde a posi��o ser� manipulada
@param nPos, integer, Posi��o a ser manipulada

@return undefined, Valor retornado ou atribu�do
/*/
Function SmArrGetSet(aArray, nPos)

Return {|uNewValue| IIF(pCount() == 1, aArray[nPos] := uNewValue, aArray[nPos]) }

/*/{Protheus.doc} SmHMGetSet
Cria um bloco de c�digo para utiliza��o em objetos msget
	
@author thiago.santos
@since 26/05/2014
@version 1.0
		
@param oHM, object, hashmap que receber� o valor
@param uKey, undefined, chave ao qual o valor ser� associado

@return undefined, Valor retornado ou atribu�do
/*/
Function SmHMGetSet(oHM, uKey)

Return {|uNewValue| IIF(pCount() == 1, oHM:Put(uKey, uNewValue), oHM:Get(uKey)) }

/*/{Protheus.doc} SmWBrwGetS
Retorna um get/set para ser usado em um TWBrowse
	
@author thiago.santos
@since 23/06/2014
@version 1.0
		
@param oWBrose, object, objeto TWbrowse
@param nCOl, numeric, posi��o da coluna

@return undefined, Valor retornado ou atribu�do
/*/
Function SmWBrwGetS(oWBrowse, nCol)

Return {|uNewValue| IIF(pCount() == 1,	oWBrowse:aCols[oBrowse:nAt, nCol] ;
									,	oWBrowse:aCols[oBrowse:nAt, nCol] := uNewValue) }

/*/{Protheus.doc} SmSafeExec
Executa uma fun��o de maneira protegida, para impedir problemas com diferentes vers�es de bin�rio.
(Por exemplo, fun��es de bin�rio criadas a partir da vers�o 0420A devem ser chamadas usando SmSafeExec,
porque o bin�rio usado na compila��o do rob� � mais antigo e daria erro por n�o reconhecer esta fun��o.
	
@author thiago.santos
@since 28/05/2014
@version 1.0
		
@param cFunction, character, Nome da fun��o a ser chamada
@param aParams, array, Par�metros a serem fornecidos para a fun��o
@param uRet, undefined, Retorno da fun��o ou nil, caso n�o seja executada

@return boolean, Se a fun��o foi executada
/*/
Function SmSafeExec(cFunction, aParams, uRet)
Local lRet := FindFunction(cFunction)
Local nLen

If lRet
	Default aParams := {}

	If (nLen := Len(aParams)) == 0
		uRet := &cFunction.()
	ElseIf nLen == 1
		uRet := &cFunction.(aParams[1],,,)
	ElseIf nLen == 2
		uRet := &cFunction.(aParams[1], aParams[2],,)
	ElseIf nLen == 3
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3],)
	ElseIf nLen == 4
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4])
	ElseIf nLen == 5
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5])
	ElseIf nLen == 6
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6])
	ElseIf nLen == 7
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7])
	ElseIf nLen == 8
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8])
	ElseIf nLen == 9
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9])
	ElseIf nLen == 10
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10])
	ElseIf nLen == 11
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11])
	ElseIf nLen == 12
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11], aParams[12])
	ElseIf nLen == 13
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11], aParams[12], aParams[13])
	ElseIf nLen == 14
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11], aParams[12], aParams[13], aParams[14])
	ElseIf nLen == 15
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11], aParams[12], aParams[13], aParams[14], aParams[15])
	ElseIf nLen == 16
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11], aParams[12], aParams[13], aParams[14], aParams[15], aParams[16])
	ElseIf nLen == 17
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11], aParams[12], aParams[13], aParams[14], aParams[15], aParams[16], aParams[17])
	ElseIf nLen == 18
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11], aParams[12], aParams[13], aParams[14], aParams[15], aParams[16], aParams[17], aParams[18])
	ElseIf nLen == 19
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11], aParams[12], aParams[13], aParams[14], aParams[15], aParams[16], aParams[17], aParams[18], aParams[19])
	ElseIf nLen == 20
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11], aParams[12], aParams[13], aParams[14], aParams[15], aParams[16], aParams[17], aParams[18], aParams[19], aParams[20])
	ElseIf nLen == 21
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11], aParams[12], aParams[13], aParams[14], aParams[15], aParams[16], aParams[17], aParams[18], aParams[19], aParams[20], aParams[21])
	ElseIf nLen == 22
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11], aParams[12], aParams[13], aParams[14], aParams[15], aParams[16], aParams[17], aParams[18], aParams[19], aParams[20], aParams[21], aParams[22])
	ElseIf nLen == 23
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11], aParams[12], aParams[13], aParams[14], aParams[15], aParams[16], aParams[17], aParams[18], aParams[19], aParams[20], aParams[21], aParams[22], aParams[23])
	ElseIf nLen == 24
		uRet := &cFunction.(aParams[1], aParams[2], aParams[3], aParams[4], aParams[5], aParams[6], aParams[7], aParams[8], aParams[9], aParams[10], aParams[11], aParams[12], aParams[13], aParams[14], aParams[15], aParams[16], aParams[17], aParams[18], aParams[19], aParams[20], aParams[21], aParams[22], aParams[23], aParams[24])
	Endif
EndIf

Return lRet

Function SmSemaforo(cSemaforo, cMensagem, bExec, uOutput, nTimeOut)
Local cPathSem  := "/SEMAFORO/"+cSemaforo
Local lContinue := .T.
Local nCount    := 30
Local lRet      := .F.
Local lStop     := .F.
Local nHandle

While lContinue .And. nCount > 0
	If File(cPathSem) .And. FErase(cPathSem) < 0
		SmMSGRun(, {|| lStop := DoSemaforo(cPathSem, nTimeOut) }, cMensagem)
		If !lStop //Se deu o timeout na espera do sem�foro, ent�o o Sistema vai abortar o login
			Conout( 'System is updating' )
			RpcClearEnv()
			Final("O Sistema est� passando por uma atualiza��o longa a partir de outro terminal. Por favor, tente novamente mais tarde.")
			Return .F.
		EndIf
	EndIf
	
	nHandle := FCreate(cPathSem)
	lContinue := (nHandle <= 0) //Se n�o criar o arquivo dever� checar se n�o houve tentativa simult�nea de cria��o
	nCount-- //Decline o n�mero de tentativas efetuadas de lock
End

If nHandle > 0
	lRet := .T.
	uOutput := Eval(bExec)
	FClose(nHandle)
	FErase(cPathSem)
EndIf

Return lRet

Function SmTypeDef(cType, nTam, nDec)
Local uRet
If ValType(nTam) == "A"
	nDec := nTam[2]
	nTam := nTam[1]
EndIf

If cType == "C"
	uRet := Space(nTam)
ElseIf cType == "N"
	uRet := Replicate("0", nTam-nDec)
	If nDec > 0
		uRet += "." + Replicate("0", nDec)
	EndIf
	uRet := Val(uRet)
ElseIf cType == "D"
	uRet := stod("")
ElseIf cType == "L"
	uRet := .F.
EndIf

Return uRet

Static Function DoSemaforo(cPathSem, nTimeOut)
Local nCount     := 0
Default nTimeOut   := 500

While File(cPathSem) .And. FErase(cPathSem) < 0 .And. nCount < nTimeOut
	Sleep(500)
	nCount++
End

Return !(File(cPathSem) .And. FErase(cPathSem) < 0)

/*/{Protheus.doc} SmHelp
Mesma intuito da fun��o help, mas ele ajusta o texto a ser exibido para n�o haver "quebras" incorretas nas palavras
	
@author thiago.santos
@since 22/08/2014
@version 1.0
		
@param nCompat, numeric, Primeiro par�metro para Help()
@param cTitulo, character, Segundo par�metro para help
@param cErro, character, Descri��o do erro ocorrido
@param cSolucao, character, Descri��o da solu��o sugerida

@return undefined, retorno da fun��o Help
/*/
Function SmHelp(nCompat, cTitulo, cErro, cSolucao, cProperty, aParams)
Local aRet

If cProperty != nil .And. FindFunction("FWHlpCpo")
	FWHlpCpo(RESTReadVar(cProperty))
EndIf

If SmIsFly01() .And. !Empty(aRet := SmFly01Help(cTitulo, cErro, cSolucao, aParams))
	cErro := aRet[1]
	cSolucao := aRet[2]
EndIf

If GetRemoteType() != -1
	cErro := PrepMsgSpc(cErro, 40)
	cSolucao := PrepMsgSpc(cSolucao, 40)
EndIf

Return Help(nCompat, cTitulo, cErro, cSolucao)

/*/{Protheus.doc} SmWaitLock
Fun��o utilizada para aguardar que determinado lock seja liberado
	
@author thiago.santos
@since 22/08/2014
@version 1.0
		
@param cLock, character, nome do lock a aguardar
@param cMensagem, character, mensagem a exibir enquanto o lock n�o � liberado
@param nMaximo, numeric, n�mero m�ximo de tentativas de bloqueio do lock
@param nDelay, numeric, Delay entre uma tentativa e outra (milisegundos)

@return undefined, retorno da fun��o Help
/*/
Function SmWaitLock(cLock, cMensagem, nMaximo, nDelay)
Local lRet       := .F.

If SmIsJob()
	lRet := WaitLock(cLock, nMaximo, nDelay)
Else
	SmMsgRun(, {|| lRet := WaitLock(cLock, nMaximo, nDelay) }, cMensagem)
EndIf

Return lRet

/*/{Protheus.doc} WaitLock
Fun��o auxiliar de SmWaitLock
	
@author thiago.santos
@since 22/08/2014
@version 1.0
		
@param cLock, character, nome do lock a aguardar
@param nMaximo, numeric, n�mero m�ximo de tentativas de bloqueio do lock
@param nDelay, numeric, Delay entre uma tentativa e outra (milisegundos)

@return undefined, retorno da fun��o Help
/*/
Static Function WaitLock(cLock, nMaximo, nDelay)
Local nTentativas := 0
Default nMaximo := 50
Default nDelay := 1000

While nTentativas <= nMaximo .And. !LockByName(cLock, .T.)
	nTentativas++
	Sleep(nDelay)
EndDo

Return (nTentativas <= 50)

/*/{Protheus.doc} SmCondSeek
Procura pelo primeiro registro que atenda a chave de �ndice e a condi��o informada
	
@author thiago.santos
@since 23/09/2014
@version 1.0

@param cSeek, character, chave de �ndice a buscar
@param bCond, boolean, condi��o a verificar
@param nIndex, numeric, �ndice a utilizar
@param cTable, character, Table da busca

@return boolean, verdadeiro se encontrou, falso caso contr�rio
/*/
Function SmCondSeek(cSeek, bCond, nIndex, cTable)
Local lRet       := .F.
Local nLen       := Len(cSeek)
Local lCond
If !Empty(cTable)
	DbSelectArea(cTable)
EndIf

If !Empty(nIndex)
	DbSetOrder(nIndex)
EndIf

If SMSeek(cSeek)
	If !(lRet := Empty(bCond))
		While !Eof() .And. Left(&(IndexKey()), nLen) == cSeek .And. !(lRet := Eval(bCond))
			dbSkip()
		End
	EndIf
EndIf

Return lRet

/*/{Protheus.doc} SmEstSB2
Retorna o saldo atual do registro posicionado na SB2
	
@author thiago.santos
@since 19/09/2014
@version 1.0
		
@return numeric, o saldo do registro posicionado na SB2
/*/
Function SmEstSB2(cTipoSaldo)
Local nRet := 0
Default cTipoSaldo := GETMV("MV_PVSALDO", .F., "1")

If cTipoSaldo == "1" .Or. IsInCallStack("SM4M090")
	nRet := SB2->B2_QATU
ElseIf cTipoSaldo == "2" .Or. cTipoSaldo == "3" 
	nRet := SB2->(B2_QATU - B2_SALOPS - B2_SALPV)
EndIf
	
Return nRet

Function SmDispSB2(cCodprod, cTipoSaldo)
Local nRet := 0
Default cTipoSaldo := GETMV("MV_PVSALDO", .F., "1")

nRet := XSELECT #SUM(SmEstSB2(cTipoSaldo)) ;
			FROM (TAB SB2 ;
			WHERE B2_FILIAL == ?xFilial("SB2")?;
				.And. B2_CODPROD == ?cCodProd? ;
				.And. B2_LOCAL != 'CQ' ;
				ORDER 1 ;
			) PARAMS {{"cCodProd", Padr(cCodProd, Len(SB2->B2_CODPROD))}}

Return nRet

Function Nvl(uValue1, uValue2)
Local uRet

If ValType(uValue1) != "U"
	uRet := uValue1
Else
	uRet := uValue2
EndIf

Return uRet

Function SmTrtEmpty(uValue1, uValue2)

If !Empty(uValue1)
	uRet := uValue1
Else
	uRet := uValue2
EndIf

Return

Function SmTPFis(cTipo)
Local cRet  := "N"

Do Case
	Case cTipo == "1"
		cRet := "N"
	Case cTipo == "2"
		cRet := "D"
	Case cTipo == "3"
		cRet := "C"
	Case cTipo == "4"
		cRet := "B"			
	Case cTipo == "6"
		cRet := "I"
	Case cTipo == "7"
		cRet := "P"
EndCase


Return cRet

Function SmMsgRun(oParent, bAction, cTitle)
Local uRet

If GetRemoteType() == -1
	Eval(bAction)
Else
	uRet := FWMsgRun(oParent, bAction, cTitle)
EndIf

Return

Static __Timer
Function SmFinal(cStr,cStr1,lNormal, lLogoff, nTimer)
Local uRet

If GetRemoteType() != -1
	uRet := Final(cStr, cStr1, lNormal, lLogoff, nTimer)
ElseIf !Empty(nTimer)
	__Timer :=  TTimer():New(nTimer*1000, {|| MS_QUIT()})
Else
	MS_QUIT()
EndIf

Return uRet

/*/{Protheus.doc} IfExistChav
Retorna cValor se ele for encontrado como chave em ctabela. Do contr�rio, retorna cDefault
	
@author thiago.santos
@since 19/09/2014
@version 1.0

@param cTabela, character, Tabela a pesquisar
@param cValor, character, Valor a validar
@param cDefault, character, Valor a retornar caso cValor n�o passe na valida��o (default "")
@param cPrefix, character, Prefixo a utilizar no existchav, caso necess�rio (default "")
@param nIndex, integer, �ndice a buscar (default 1)

@return character, cValor ou cDefault
/*/
Function IfExistChav(cTabela, cValor, cDefault, cPrefix, nIndex)
Local cRet
Local aAreas := GetAreas({ctabela})
Default cPrefix := ""
Default nIndex := 1
Default cDefault := ""
cRet := cDefault

dbSelectArea(cTabela)
dbSetorder(nIndex)

If SMSeek(xFilial(cTabela) + cPrefix + cValor)
	cRet := cValor
EndIf

RestAreas(aAreas)
Return cRet

/*/{Protheus.doc} SmCB
Compila um bloco de c�digo a partir de uma String
	
@author thiago.santos
@since 06/06/2017
@version 1.0

@param cBlkCode, character, C�digo do bloco de c�digo
@param cParams, character, String com par�metros que o bloco de c�digo gerado receber� (opcional)

@return codeblock, Bloco de c�digo gerado
/*/
Function SmCB(cBlkCode, cParams)
Local cAux   := Upper(cBlkCode)
Local cBlock
Local bRet

If cAux == ".T."
	bRet := {|| .T. }
ElseIf cAux == ".F."
	bRet := {|| .F. }
ElseIf cAux == "0"
	bRet := {|| 0 }
ElseIf cAux == "{}"
	bRet := {|| {} }
ElseIf cAux == "NIL"
	bRet := {|| nil }
ElseIf Empty(cParams)
	bRet := &("{||" + cBlkCode + "}")
Else
	bRet := &("{|" + cParams + "|" + cBlkCode + "}")
EndIf

Return bRet

/*/{Protheus.doc} SmCBExec
Compila e executa um bloco de c�digo a partir de uma String
	
@author thiago.santos
@since 06/06/2017
@version 1.0

@param cBlkCode, character, C�digo do bloco de c�digo
@param cParams, character, String com par�metros que o bloco de c�digo gerado receber� (opcional)
@param p1, undefined, Par�metro 1 a ser passado para o bloco de c�digo
@param p2, undefined, Par�metro 2 a ser passado para o bloco de c�digo
@param p3, undefined, Par�metro 3 a ser passado para o bloco de c�digo
@param p4, undefined, Par�metro 4 a ser passado para o bloco de c�digo
@param p5, undefined, Par�metro 5 a ser passado para o bloco de c�digo
@param p6, undefined, Par�metro 6 a ser passado para o bloco de c�digo
@param p7, undefined, Par�metro 7 a ser passado para o bloco de c�digo
@param p8, undefined, Par�metro 8 a ser passado para o bloco de c�digo
@param p9, undefined, Par�metro 9 a ser passado para o bloco de c�digo
@param p10, undefined, Par�metro 10 a ser passado para o bloco de c�digo

@return undefined, resultado do bloco de c�digo
/*/
Function SmCBExec(cBlkCode, cParams, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10)
Local cAux := Upper(cBlkCode)
Local uValue

If cAux == ".T."
	uValue := .T.
ElseIf cAux == ".F."
	uValue := .F.
ElseIf cAux == "0"
	uValue := 0
ElseIf cAux == "{}"
	uValue := {}
ElseIf cAux == "NIL"
	uValue := nil
ElseIf Empty(cParams)
	uValue := Eval(&("{||" + cBlkCode + "}"))
Else
	uValue := Eval(&("{|" + cParams + "|" + cBlkCode + "}"), p1, p2, p3, p4, p5, p6, p7, p8, p9, p10)
EndIf

Return uValue

/*/{Protheus.doc} SmIsFly01
Retorna se a instala��o atual � Fly01
	
@author thiago.santos
@since 03/07/2017
@version 1.0

@return boolean, se a instala��o atual � Fly01
/*/
Function SmIsFly01()
Local oLic := SmGetLicense()

Return (oLic:nProdId == 18)


/*/{Protheus.doc} A2CGC
Verifica formato do documento

@param cTipPes, character, Tipo do documento: "F" - CPF, "J" - CNPJ
@param cCNPJ, character, N�mero do CPF/CNPJ (padr�o &(readvar()))
@return boolean, se o CPF/CNPJ � v�lido

@author Jose C. Frasson
@since 24/06/2010

/*/
Function A2CGC(cTipPes, cCNPJ)
Local aArea       := GetArea()
Local lRetorno    := .T.
Local cMv_ValCNPJ := SuperGetMV("MV_VALCNPJ")
Local cMv_ValCPF  := SuperGetMV("MV_VALCPF")
Local cNoMask     := JustChars(cCNPJ, "0123456789")
Local oUser      := SmUserUtil():New()

DEFAULT cCNPJ     := &(ReadVar())

If !Empty(cNoMask)
	//��������������������������������������������������������������Ŀ
	//� Valida o tipo de pessoa                                      �
	//����������������������������������������������������������������
	If cTipPes == "F" .AND. !(Len(cNoMask)==11)
		If SmIsFly01()
			SmHelp(2,"Document","CPF inv�lido","CPF deve ter 11 d�gitos")
		Else
			HELP(2,"Aten��o","Tipo de Documento informado inv�lido.","")
		EndIf
		lRetorno := .F.
	ElseIf cTipPes == "J" .AND. !(Len(cNoMask)==14)
		If SmIsFly01()
			SmHelp(2,"Document","CNPJ inv�lido","CNPJ deve ter 14 d�gitos")
		Else
			HELP(2,"Aten��o","Tipo de Documento informado inv�lido.","")
		EndIf
		lRetorno := .F.
	ElseIf Empty(cTipPes)
		If SmIsFly01()
			SmHelp(2,"DocumentType","Tipo de pessoa inv�lido","O tipo de pessoa deve ser F�sica ou Jur�dica")
		Else
			HELP(2,"Aten��o","Tipo de Documento informado inv�lido.","")
		EndIf
		lRetorno := .F.
	EndIf
EndIf

Return lRetorno

/*/{Protheus.doc} LimpaTx
Retira caracteres especiais nos moldes da fun��o cClearChars por�m adaptado tratar os caractres n�o aceitos pela sefaz segundo o 
manual da NF-e 4.00 na p�gina 85

"Todos os textos de um documento XML passam por uma an�lise do �parser� espec�fico da
linguagem. Alguns caracteres afetam o funcionamento deste �parser�, n�o podendo
aparecer no texto de uma forma n�o controlada.

Os caracteres que afetam o �parser� s�o:
� > (sinal de maior),
� < (sinal de menor),
� & (e-comercial),
� � (aspas),
� � (sinal de ap�strofo)."

@param cTexto, texto a ser tratado pela fun��o
@return cTexto, texto tratado pela fun��o

@author Jose C. Frasson
@since 24/06/2010

/*/
Function LimpaTx(cTexto)

Default  cTexto:= ""

cTexto := StrTran(cTexto, '"', "")
cTexto := StrTran(cTexto, "'", "")
cTexto := StrTran(cTexto, "<", "")
cTexto := StrTran(cTexto, ">", "")
cTexto := StrTran(cTexto, "&", "")

return cTexto